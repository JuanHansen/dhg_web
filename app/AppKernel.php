<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
	        new Craue\FormFlowBundle\CraueFormFlowBundle(),
            new DHG\coreBundle\DHGcoreBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Brown298\DataTablesBundle\Brown298DataTablesBundle(),
            new DHG\PersonalBundle\DHGPersonalBundle(),
            new DHG\ContactosBundle\DHGContactosBundle(),
            new DHG\InventarioBundle\DHGInventarioBundle(),
            new DHG\MaquinariaBundle\DHGMaquinariaBundle(),
            new DHG\UserBundle\DHGUserBundle(),
            new Ivory\GoogleMapBundle\IvoryGoogleMapBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new DHG\EntityHistoryBundle\DHGEntityHistoryBundle(),
            

            //new DHG\RESTBundle\DHGRESTBundle(),            
            //new DHG\CampanasBundle\DHGCampanasBundle(),
            //new DHG\BovinosBundle\DHGBovinosBundle(),
            //new DHG\LluviasBundle\DHGLluviasBundle(),
            //new DHG\LaboresBundle\DHGLaboresBundle(),
            //new DHG\EstablecimientosBundle\DHGEstablecimientosBundle(),
        );
        if (in_array($this->getEnvironment(), array('dev', 'test'))) {

            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
