<?php
namespace DHG\coreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use DHG\coreBundle\Event\FormBuildEvents;
use DHG\coreBundle\Event\BuildFormEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

abstract class FormWithDependencyInjectionType extends AbstractType 
{
    private $ed;

	/**
     * 
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder,$options);

       $event = new BuildFormEvent($this->getName(), $builder,$options);
       $this->ed->dispatch(FormBuildEvents::BUILD_FORM, $event);

    }

    /**
     * @return ed
     */
    public function getEventDispatcher($ed){
        return $this->ed;
    }

    /**
     * @param EventDispatcher ed
     */
    public function setEventDispatcher($ed){
        $this->ed = $ed;
    }

    /**
     * @param EventDispatcher eventDispatcher
     */
    public function __construct($ed)
    {
        $this->ed = $ed;
    }

}