<?php
namespace DHG\coreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use DHG\coreBundle\Event\FormBuildEvents;
use DHG\coreBundle\Event\BuildFormEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Forms;

class FormEditableTableType extends AbstractType 
{
    private $name;
    private $ed;
    private $formType;
    private $objList;
    private $formParams;
    private $header;
    private $headerForm;

	/**
     * 
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
       parent::buildForm($builder,$options);
       $reflection = new \ReflectionClass($this->formType); 

       //Obtengo y quito el primer elemento del array para setear el header de la tabla
       $firstKey = current(array_keys($this->objList));
       $builder->add($firstKey, $reflection->newInstanceArgs($this->formParams), array(
                    'data_class' => get_class($this->objList[$firstKey]),
                    'label' => false,
                ));

       $firstForm = $builder->get($firstKey);
       $this->header = array();
       foreach ($firstForm->all() as $key => $value) {
          $this->header[] = $value->getOption('label');
       }

       //Continuo con el resto de los objetos cargando las filas de la tabla
       foreach ($this->objList as $key => $obj){
            $builder
                ->add($key, $reflection->newInstanceArgs($this->formParams), array(
                    'data_class' => get_class($obj),
                    'label' => false,
                ));
       }

       //Creo el formheader
       $this->headerForm = $builder->create('headerForm', $reflection->newInstanceArgs($this->formParams), array(
            'mapped' => false,
            'label' => false,
            'required' => false,
        ));

       $event = new BuildFormEvent($this->getName(), $builder,$options);
       $this->ed->dispatch(FormBuildEvents::BUILD_FORM, $event);

    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = $valid = !$form->isSubmitted() || $form->isValid();
        $view->vars['header'] = $this->header;
        $view->vars['headerForm'] = $this->headerForm->getForm()->createView();
    }


    /**
     * @return ed
     */
    public function getEventDispatcher($ed){
        return $this->ed;
    }

    /**
     * @param EventDispatcher ed
     */
    public function setEventDispatcher($ed){
        $this->ed = $ed;
    }

    /**
     * @param string name
     * @param EventDispatcher ed
     * @param String formType 
     * @param array formParams
     * @param array objList
     */
    public function __construct( $name, $ed, $formType, $formParams, $objList )
    {
        $this->name = $name;
        $this->ed = $ed;
        $this->formType = $formType;
        $this->formParams = $formParams;
        $this->objList = $objList;
    }



    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'header' => array(),
        ));
    }


    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }


}