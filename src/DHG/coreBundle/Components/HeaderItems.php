<?php
namespace DHG\coreBundle\Components;


class HeaderItems
{

    /**
     * @var array
     */
    protected $items;

    /**
     * Constructor.
     *
     * @param array $items
     */
    public function __construct()
    {
        $this->items = array();
    }

    /**
     * Get header items.
     *
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Carga un nuevo item para ubicarlo en el header
     *
     * @param name Nombre unico del elemento
     * @param weight peso de referencia para ubicarlo. Mayor peso lo alinea mas a la derecha o a la izq, dependiendo de lo escogido
     * @param twig el twig que se utilizara para el renderizado
     * @param right si es verdadero se ubica el elemento a la derecha
     *	
     */
    public function addElement($name, $weight, $twig, $right = true){
    	if (empty($this->items[$weight])){
    		$this->items[$weight] = array();
    	}
    	$this->items[$weight] = array_merge( $this->items[$weight], 
    									array(  
    										'name' => $name,
    										'twig' => $twig,
    										'right' => $right
    										)
    							);
    }


}
