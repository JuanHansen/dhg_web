<?php
namespace DHG\coreBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetaData,
    Doctrine\ORM\Query\Filter\SQLFilter;

class RemovedFilter extends SQLFilter
{
    public function addFilterConstraintOld(ClassMetadata $targetEntity, $targetTableAlias)
    {
    	if (!in_array('eliminado',$targetEntity->columnNames)){
    		return "";
    	}
    	try{
    		return $targetTableAlias.'.eliminado = ' . $this->getParameter('eliminado') ; 
    	}catch( \InvalidArgumentException $e){
    		return $targetTableAlias.'.eliminado = 0'; 
    	}
        return $targetTableAlias.'.eliminado = 0'; 
    }

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!in_array('eliminado',$targetEntity->columnNames)){
            return "";
        }
        try{
            $param = $this->getParameter('eliminado'); 
            if($param === "'0'"){
                return $targetTableAlias.'.eliminado = 0';}
            if($param === "'1'"){
                return $targetTableAlias.'.eliminado = 1';}
            if($param === "'2'"){
                return $targetTableAlias.'.eliminado != 3';}
        }catch( \InvalidArgumentException $e){
            return $targetTableAlias.'.eliminado = 0'; 
        }
        return $targetTableAlias.'.eliminado = 0'; 
    }
}