<?php

namespace DHG\coreBundle\Menu;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DefMenuEventsListener{

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem($this->mensaje, array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
    }
}