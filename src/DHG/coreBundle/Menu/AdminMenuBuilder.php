<?php
namespace DHG\coreBundle\Menu;

use DHG\coreBundle\Event\MenuEvents;
use DHG\coreBundle\Event\ConfigureMenuEvent;
use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;

/**
*
*/
class AdminMenuBuilder{
   
    private $factory;
    private $ed;
    private $securityContext;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, $event_dispatcher, SecurityContextInterface $securityContext)
    {
        $this->factory = $factory;
        $this->ed = $event_dispatcher;
        $this->securityContext = $securityContext;
    }

    /**
     * Devuelve toda la estructura del menu de navegacion
     *
     */
    public function createAdminMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav');
        $this->ed->dispatch(MenuEvents::CONFIGURE_ADMINMENU, new ConfigureMenuEvent($this->factory, $menu));

        $this->reorderMenuItems($menu);
        $this->filterPermissionMenuItems($menu);

        return $menu;
    }
        

    /**
     * Reordena los elementos del menu, en base al atributo orderNumber
     *
     * @param $menu Objeto menu a reordenar
     */
    public function reorderMenuItems($menu)
    {
        $menuOrderArray = array();
        $addLast = array();
        $alreadyTaken = array();
        foreach ($menu->getChildren() as $key => $menuItem) {

            if ($menuItem->hasChildren()) {
                $this->reorderMenuItems($menuItem);
            }

            $orderNumber = $menuItem->getExtra('orderNumber');

            if ($orderNumber != null) {
                if (!isset($menuOrderArray[$orderNumber])) {
                    $menuOrderArray[$orderNumber] = $menuItem->getName();
                } else {
                    $alreadyTaken[$orderNumber] = $menuItem->getName();
                    // $alreadyTaken[] = array('orderNumber' => $orderNumber, 'name' => $menuItem->getName());
                }
            } else {
                $addLast[] = $menuItem->getName();
            }
        }

        // sort them after first pass
        ksort($menuOrderArray);

        // handle position duplicates
        if (count($alreadyTaken)) {
            foreach ($alreadyTaken as $key => $value) {
                // the ever shifting target
                $keysArray = array_keys($menuOrderArray);

                $position = array_search($key, $keysArray);

                if ($position === false) {
                    continue;
                }

                $menuOrderArray = array_merge(array_slice($menuOrderArray, 0, $position), array($value), array_slice($menuOrderArray, $position));
            }
        }

        // sort them after second pass
        ksort($menuOrderArray);

        // add items without ordernumber to the end
        if (count($addLast)) {
            foreach ($addLast as $key => $value) {
                $menuOrderArray[] = $value;
            }
        }

        if (count($menuOrderArray)) {
            $menu->reorderChildren($menuOrderArray);
        }
    }



    /**
     * Filtra los permisos y setea la url por defecto para los items de menu con hijos
     *
    * @param $menu Objeto menu a filtrar
     */
    public function filterPermissionMenuItems($menu){
        foreach ($menu->getChildren() as $key => $menuItem) {
            if ($menuItem->hasChildren()) {
                $this->filterPermissionMenuItems($menuItem);
                if (!$menuItem->hasChildren()){
                    $menu->removeChild($key);
                }else{
                    $firstMenuItem = NULL;
                    foreach ($menuItem->getChildren() as $key => $menuItem2) {
                        if (!$menuItem2->hasChildren()){
                            $firstMenuItem = $menuItem2;
                            break;
                        }
                    }
                    if ($firstMenuItem != NULL){
                        $menuItem->setUri($menuItem2->getUri());
                    }
                }
            }else{
                $permission = $menuItem->getExtra('permission');
                if ( $permission != NULL ){
                    if (false === $this->securityContext->isGranted($permission['permission'], $permission['entity'])){
                        $menu->removeChild($key);
                    }
                }
            }
        }
    }




}