var controladorFiltros;

/**
* Inicializa el controlador de filtros de la tabla
*/
function inicializarFiltros(){
    controladorFiltros = new FilterControler($('#'+metadatatableid).DataTable());
    controladorFiltros.setAutorefresh(true);
    controladorFiltros.setItemActivated($("#filter-activated-icon"),"bg-danger");
    inicializarSelector($(".select2-tokenizer"));
    collection_number = 1;
}

/**
* Inicializa el conjunto de selectores pasado por parametro
* @param select Selectores (select2)
*/
function inicializarSelector(select){
    select.each(function(e,i){
        $(this).select2({
            tags: [],
            tokenSeparators: [',','.'],
            formatNoMatches: function(){return "Sin Opciones"},
            formatSearching: function(){return "Buscando.."}
        });
        setSelectorTags(e);
    });
}

/*
* Establece las selecciones correspondientes en funcion al numero de columna al select del filtro establecido
* @param filterNum Numero de filtro
* @param col Numero de columna
*/
function setSelectorTags(col){
    var tags = controladorFiltros.getColOptions(col);
    $("#input_select_"+col).select2({tags:tags});
}

/**
* Funcionalidad para actualizar el filtro establecido
* @param filterNum Numero de filtro a actualizar
*/
function filterByCol(filterNum,dataType){
    actualizarFiltroNum(filterNum,dataType);
}

/**
* Actualiza el filtro establecido
* @param filterNum Numero de filtro
*/
function actualizarFiltroNum(filterNum,dataType){
    actualizarFiltro(filterNum,filterNum,dataType);
}

function restartFilters(){
    $("#collectionFilter_Pane_form_group").find(".select2-tokenizer").each(function(){$(this).select2('data', null)});
    $("#collectionFilter_Pane_form_group").find(".input-daterange input").each(function(){$(this).val("");});
    controladorFiltros.reset();
}

/**
* Actualiza el filtro de tipo, columna y numero establecido
* @param filterNum Numero de filtro
* @param columna Columna a la que hace referencia
* @param dataType Tipo de filtro (true si es de fecha, false si es comun)
*/
function actualizarFiltro(filterNum,columna,dataType){
    if(dataType){
        var dateRan = $("#input_daterange_"+filterNum);
        var fechaM= dateRan.children()[0].value;
        var fechaX= dateRan.children()[2].value;
        var data = [fechaM,fechaX];
        controladorFiltros.setFilter(filterNum,columna,true,data);
    }else{
        var data = $("#input_select_"+filterNum).get(0).value.split(",");
        controladorFiltros.setFilter(filterNum,columna,false,data);
    }
    calcularSumas();
}

/**
* Calcula las sumas de las columnas correspondientes
*/
function calcularSumas(){
    $(".sumable").each(function(i,e){
        calcularSuma(e,$(e).data("col"));
    });
}
/**
* Calcula la suma de la columna especificada
* @param columna Columna (DOM) donde mostrar el resultado
* @param num Numero de columna a calcular
*/
function calcularSuma(columna, num){
    var table = $('#'+metadatatableid).DataTable();
    var suma = 0;
    table.rows({filter: 'applied'}).data().each(function(value,i){
        var mat = value[num].match(/[+-]?\d+(?:\.\d+)?/g, '');
        if (mat == null){

        }else{
            var numberb = parseFloat(mat[0]);
            suma+=numberb;
        }
    });
    $(columna).find(".sumResult").text("Suma: "+suma);
}

/**
* Muestra/Oculta la columna establecida. Guarda en DB la opcion
* @param $number Numero de columna
* @param item Item que llamo al evento
* @columnName MetaName de la columna
*/
function toggleSelectedColumn($number,item,columName){
    var table = $('#'+metadatatableid).DataTable();
    var activo = !$(item).hasClass('active');
    table.column($number).visible(activo);
    var tableName = metadatatableid;
    var ruta = colroute;
    var saveData = {};
    saveData.tabla = tableName;
    saveData.numero = columName;
    saveData.activo = activo;
    $.ajax({
        type        : "post",
        url         : ruta,
        data        : saveData,
    });
}


function exportar(header,extencion){
    var table = $('#'+metadatatableid).DataTable();
    var data = table.buttons.exportData( {
        columns: ':visible',
        trim: true,
        orthogonal:"display",
        decodeEntities:true,
        stripHtml:true,
        format: {
        body: function ( data, columnIdx ) {
            return getExportData(data,true);
        }
    }
    });
    if(header){
        data['body'].unshift(data['header']);
    }
    exportToCsv("DHG."+extencion,data['body']);
}

function getExportData(inputText) {
    var tempDiv = document.createElement('div');
    tempDiv.innerHTML = inputText;
    if($(tempDiv).find("[data-export]").length>0){
        return $(tempDiv).find("[data-export]").data('export');
    }
    var res = get_visible_text(tempDiv);
    res = res.toString().replace("/\n/g","");
    res = res.toString().replace("/\s/g","");
    res = res.toString().trim();
    return res.toString();
}
function get_visible_text(content) {
    var saved = $(content).clone();
    $(content).find('.data-hidden').remove();
    var visible_text = $(content).text();
    $(content).replaceWith(saved);
    return visible_text;
}
function merge_options(obj1,obj2){
    var obj3 = {};
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }
    return obj3;
}
function exportToCsv(filename, rows) {
    var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\r\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}