var mySelf;

var FilterControler = function(table){
    mySelf = this;
    this.table = table;
    this.filterList = [];
    this.autorefresh = false;
    this.activatedItem = null;
    this.activatedClass = null;
    //Extencion para filtro por fechas
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            if(mySelf.filterList == null) return true;
            for (var index in mySelf.filterList){
                var simpFilt = mySelf.filterList[index];
                if(simpFilt.getDataType()){
                    var columna = simpFilt.getCol();
                    var fechaMinima = simpFilt.getData()[0].toString().split('/');
                    var fechaMaxima = simpFilt.getData()[1].toString().split('/');
                    var valor = data[columna].toString().split('/');
                    var fechaMin = "Invalid Date";
                    var fechaMax = "Invalid Date";
                    var fechaVal = "Invalid Date";
                    if(fechaMinima.length==3)
                        fechaMin = new Date(fechaMinima[2],fechaMinima[1],fechaMinima[0]);
                    if(fechaMaxima.length==3)
                        fechaMax = new Date(fechaMaxima[2],fechaMaxima[1],fechaMaxima[0]);
                    if(valor.length==3)
                        fechaVal = new Date(valor[2],valor[1],valor[0]);
                    if(fechaVal!="Invalid Date"){
                        if(fechaMax!="Invalid Date" && fechaVal>fechaMax){
                            return false;
                        }
                        if(fechaMin!="Invalid Date" && fechaVal<fechaMin){
                            return false;
                        }
                    }else{
                        if(fechaMin!="Invalid Date"||fechaMax!="Invalid Date")
                            return false;
                    }
                }else{
                    //return true;
                }
            }
            return true;
        } 
    );
}

/**
* Añade un filtro a la lista de filtros.
* @param filtro Filtro a agregar
*/
FilterControler.prototype.addFilterItem = function(filtro){
    this.filterList[filtro.getId()]=filtro;
    this.tryRefresh();
}

/**
* Setea (o agrega) un filtro a la lista de filtros
* @param filtro Filtro a setear/agregar
*/
FilterControler.prototype.setFilterItem = function(filtro){
    this.filterList[filtro.getId()]=filtro;
    this.tryRefresh();
}

/**
* Crea un Filtro en funcion de los parametros y lo agrega al listao de filtros
* @param id ID del filtro
* @param col Columna a filtrar
* @param dataType Tipo de dato. True para Datarange, false para texto
* @para data Data para filtrar (arreglo de string)
*/
FilterControler.prototype.addFilter = function(id,col,dataType,data){
    var filtro = new SimpleFilter(id,col,dataType,data);
    this.filterList[filtro.getId()]=filtro;
    this.tryRefresh();
}

/**
* Setea un Filtro en funcion de los parametros y lo agrega al listao de filtros
* @param id ID del filtro
* @param col Columna a filtrar
* @param dataType Tipo de dato. True para Datarange, false para texto
* @para data Data para filtrar (arreglo de string)
*/
FilterControler.prototype.setFilter = function(id,col,dataType,data){
    var filtro = new SimpleFilter(id,col,dataType,data);
    this.filterList[filtro.getId()]=filtro;
    this.tryRefresh();
}

/**
* Remueve el filtro con el ID dado
* @param id ID del filtro a remover
*/
FilterControler.prototype.removeFilterById = function(id){
    delete this.filterList[id];
    this.tryRefresh();
}

/**
* Remueve los filtros asociados a la columna dada
* @param col Numero de columna a remover
*/
FilterControler.prototype.removeFilterByCol = function(col){
    for (var index in this.filterList){
        var simpFilt = this.filterList[index];
        if(simpFilt.getCol()==col){
            this.removeFilterById(simpFilt.getId());
        }
    }
    this.tryRefresh();
}

/**
* Especifica si la tabla se actualiza automaticamente al modificar los filtros
* @param auto True para que se modifique automaticamente
*/
FilterControler.prototype.setAutorefresh = function(auto){
    this.autorefresh = auto;
}

/**
* Resetea los filtros aplicados a la tabla
*/
FilterControler.prototype.reset = function(){
    this.filterList = [];
    this.tryRefresh();
}

/**
* Redibuja la tabla
*/
FilterControler.prototype.refresh = function(){
    this.table.draw();
    if(this.activatedItem!=null){
        this.activatedItem.toggleClass(this.activatedClass,this.isActive());
    }
}

/**
* Si autorefresh está activado refresca la tabla, sino no hace nada
*/
FilterControler.prototype.tryRefresh = function(){
    if(this.autorefresh){
        this.applyFilters();
        this.refresh();
    };
}

/**
* Aplica los filtros de la lista de filtros a la tabla
*/
FilterControler.prototype.applyFilters = function(){
this.table.search('').columns().search('');
    var realSearch = this.computeColFilters();
    for (var index in realSearch){
        var simpFilt = realSearch[index];
        this.table.column(index).search("("+simpFilt+")",true,false);  
    }      
}

/**
* Setea la tabla a la que se le aplican los filtros
* @param table Tabla
*/
FilterControler.prototype.setTable = function(table){
    this.table = table;
}

/**
* Cambia el mapeo de filtros por ID a filtros por Columna, respetando los correspondientes AND y OR
*/
FilterControler.prototype.computeColFilters = function(){
    var columnFilters = [];
    for (var index in this.filterList){
        var simpFilt = this.filterList[index];
        if(!simpFilt.getDataType()&&simpFilt.getData().length>0&&simpFilt.getCol()!=null){
            var eachResult = escapeRegExp(simpFilt.getData().join("|"));
            if(simpFilt.getCol() in columnFilters){
                columnFilters[simpFilt.getCol()]+="(?=.*("+eachResult+"))";
            }else{
                columnFilters[simpFilt.getCol()]="(?=.*("+eachResult+"))";
            }
        }
    }
    return columnFilters;
}

/**
* Escapa los caracteres especiales del filtro
*/
function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$]/g, "\\$&");
}

/**
* Retorna un arreglo con todas las opciones de filtro disponibles para la columna especificada
* @param col Numero de la columna que se desea consultar
* @return Array de String
*/
FilterControler.prototype.getColOptions = function(col){
    var toR =[];
    var todos = this.table.column(col).data().unique();
    for(var i=0;i<todos.length;i++){
        var ele = todos[i];
        var tempDiv = document.createElement('div');
        tempDiv.innerHTML = ele;
        if($(tempDiv).find("[data-filter]").length>0){
            var filters = $(tempDiv).find("[data-filter]").data('filter');
            $.each( filters, function( index, value ){
                toR.push(value.trim());
            });
        }else{
            var elepartido = $(tempDiv).text().trim().split("(/)");
            for(var j=0;j<elepartido.length;j++){
                if(elepartido[j]!=null&&elepartido[j]!=""&&elepartido[j].toString().length<50){
                    if($.inArray(elepartido[j].trim(), toR)==-1)
                        toR.push(elepartido[j].trim());
                }
            }
        }
    }
    return toR.sort().filter(function(el,i,a){if(i==a.indexOf(el))return 1;return 0})
}

/**
* Asocia un item al controlador para mostrar cuando existe algun filtro activo
* Si hay un filtro activo, agrega la clase al item, sino la remueve
* @param activatedItem Item jQuery
* @param activatedClass Clase que se activa cuando hay algun filtro activo
*/
FilterControler.prototype.setItemActivated = function(activatedItem,activatedClass){
    this.activatedItem = activatedItem;
    this.activatedClass = activatedClass;
}


/**
* Evalua si hay algun filtro activo
* @return true si hay algun filtro activo, false en otro caso
*/
FilterControler.prototype.isActive = function(){
    var toR = false;
    for (var index in this.filterList){
        var simpFilt = this.filterList[index];
        for(var datIndex in simpFilt.getData()){
            if(simpFilt.getData()[datIndex]!=""){
                toR = true;
                break;
            }
        }
        if(toR)break;
    }
    return toR;
}



/**
* Constructor de SimpleFilter
* @param id ID del filtro
* @param col Columna a filtrar
* @param dataType Tipo de dato
* @param data Datos del filtro
*/
var SimpleFilter = function(id,col,dataType,data){
    this.id = id;
    this.col = col;
    this.dataType = dataType;
    this.data = data;
}
/**
* Getters de SimpleFilter
*/
SimpleFilter.prototype.getId = function(){return this.id;}
SimpleFilter.prototype.getCol = function(){return this.col;}
SimpleFilter.prototype.getDataType = function(){return this.dataType;}
SimpleFilter.prototype.getData = function(){return this.data;}

/**
* Funcionalidad para quitar los filtros de la tabla (internamente)
* No deberia ser accedido desde fuera del modulo
*/
function fnResetAllFilters(table) {
    var oSettings = table.fnSettings();
    for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
        oSettings.aoPreSearchCols[iCol].sSearch = '';
    }
    oTable.fnDraw();
}