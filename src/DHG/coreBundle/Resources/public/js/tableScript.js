if (typeof dTables === 'undefined') { 
    var dTables = { "tables": {}, "keys": [] };
}

function openModalRemove($id){
    var href = $('#modal-'+tableId+' a').attr('href');
    $('#modal-'+tableId+' a').get(0).setAttribute('href',href.replace(/(.*)\/(.*)$/,  "$1/"+$id) );
    $('#modal-'+tableId+'').modal('show');
   
}

function openModalView($url,$viewId){
    $viewId = $viewId || ("view"+tableId+"");
    document.getElementById($viewId).innerHTML = "No se puede visualizar la informacion";
  
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById($viewId).innerHTML = xmlhttp.responseText;
            eval($("#modal-js-block").html());
        }
    }
    xmlhttp.open("GET", $url, false);
    xmlhttp.send();    

    $('#modal-'+$viewId).modal('show');
}

function openModalRequest(item){
    $(item.parentNode.nextSibling.nextSibling).modal('show');
}

function openModalEdit($url){
    document.getElementById("edit"+tableId+"FormLocation").innerHTML = "No se puede visualizar la informacion";
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200||xmlhttp.status==201)
        {
            document.getElementById("edit"+tableId+"FormLocation").innerHTML = xmlhttp.responseText;
            $('#modal-edit'+tableId+' form').get(0).setAttribute('action',$url);
            $('#modal-edit'+tableId).modal('show');

            inicializarComponentesFormulario("#modal-edit"+tableId+"");
            var $modalForms = $('#edit'+tableId+'FormLocation form');
            $modalForms.on('submit',function(e){
                isFormAjaxEdition(e,$url); 
            });
        }
        if (xmlhttp.readyState==4 && xmlhttp.status==403)
        {
            document.getElementById("edit"+tableId+"FormLocation").innerHTML = "No tiene permisos para acceder a este formulario";  
            $('#modal-edit'+tableId).modal('show');
        }
    }
    xmlhttp.open("GET", $url, false);
    xmlhttp.send();       
}



function isFormAjaxEdition(e,$url){
    waitingDialog.show("Enviando datos");
    e.preventDefault();
    var $modal = $('#modal-edit'+tableId);
    var $formModal = $('#edit'+tableId+'FormLocation');
    var $modalForms = $('#modal-edit'+tableId+' form');
    var Data = new FormData($modalForms[0]);
    $form = $modalForms;
    $.ajax({
        type        : $form.attr( 'method' ),
        url         : $form.attr( 'action' ),
        data        : Data,
        processData: false,
        contentType: false,
        cache:false,
        statusCode  :{
            200     : //Se creó la entidad
            function(datar) {
                var table = $('#'+tableId).DataTable();
                table.ajax.reload(null,false);
                showFlashMessages();
                waitingDialog.hide();
                $modal.modal('hide');
            },
            201     : //Hubo un error en validacion del formulario
            function(datar){
                waitingDialog.hide();
                showFlashMessages();
                $formModal.html(datar);
                $modalForms = $('#modal-edit'+tableId+' form');
                $('#modal-edit'+tableId+' form').get(0).setAttribute('action',$url);
                $modalForms.on('submit',function(e){
                    isFormAjaxEdition(e,$url); 
                });
            },
            202     ://Hubo un error percatado en el servidor
            function(datar){
                waitingDialog.hide();
                $formModal.html(datar);
            },
            500     : //Hubo un error no percatado en el servidor
            function(datar){
                waitingDialog.hide();
                $formModal.html("Ocurrio un error al procesar el formulario");
            },
            403     : //El usuario no tiene acceso
            function(datar){
                waitingDialog.hide();
                $formModal.html("No tiene permisos para realizar esta accion");
            }

        }
    });
}


function inicializarComponentesTabla(){
    $('[data-toggle="tooltip"]').tooltip({
        html:true
    });
    $("[data-toggle='popover']").popover(); 
}
function tablaInicializada(){
    dTables.keys.push(tableId);
    inicializarFiltros();

    $('.modal').on('show.bs.modal', function (event) {
        var idx = $('.modal:visible').length;
        $(this).css('z-index', 1040 + (10 * idx));
        setTimeout(function(e){
            var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
            $('.modal-backdrop').not('.stacked').css('z-index', 1039 + (10 * idx));
            $('.modal-backdrop').not('.stacked').addClass('stacked');
        },200);
    });

    $('#'+tableId+'').on('xhr.dt', function ( e, settings, json, xhr ) {
        setTimeout(function(e){inicializarComponentesTabla()},200);
    });
    aplicarDataMask($(document));
}



