<?php

namespace DHG\coreBundle\EntityManager;

use Doctrine\ORM\EntityManager;
use DHG\coreBundle\Event\DefEvent;

class DefEntityManager{
	protected $em;
	protected $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la entidad
     *
	 * @param $entity Entidad con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createEntity($entity, &$error = [] ,$event = null){
    	try{
            $this->em->persist($entity);
            $this->em->flush();
            if ($event != null)
                $this->ed->dispatch($event, new DefEvent($entity));
        } catch (\Exception $e){
            $error[] = $e->getMessage();
        	return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la entidad
     *
	 * @param $entity Entidad con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editEntity($entity, &$error = [] ,$event = null){
    	try{

            if ($event != null)
                $this->ed->dispatch($event, new DefEvent($entity));
            $this->em->persist($entity);
            $this->em->flush();
        } catch (\Exception $e){
            $error[] = $e->getMessage();
        	return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la entidad
     *
	 * @param $entity Entidad con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeEntity($entity, &$error = array() ,$event = null,$event1 = null){
    	if ($this->removeVerificationEntity($entity, $error,$event1)){
			try{
	            $this->em->remove($entity);
	            $this->em->flush();
            if ($event != null)
                $this->ed->dispatch($event, new DefEvent($entity));
            } catch (\Exception $e){
                $error[] = $e->getMessage();
	        	return false;
	        }
    	}else{
        	$error = $event->getMessages();
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $entity Entidad con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationEntity($entity, &$error = array() ,$event1 = null){
        if ($event1 == null) return true; 
        $event = new DefEvent($entity);
        $this->ed->dispatch($event1,$event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    }  
}
