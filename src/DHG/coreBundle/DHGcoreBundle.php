<?php
namespace DHG\coreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use DHG\coreBundle\DependencyInjection\ConnectionCompilerPass;

class DHGcoreBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
	    parent::build($container);
	    $container->addCompilerPass(new ConnectionCompilerPass());
	}
}
