<?php
namespace DHG\coreBundle\Entity;

use DHG\coreBundle\UUID;
use DHG\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;


/** @ORM\MappedSuperclass */
class MappedSuperclassBase implements UserAware 
{

    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creado;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    protected $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastupdate;


    /**
     * @ORM\Column(type="boolean")
     */
    protected $eliminado;


    public function __construct(){
        $this->id = UUID::v4();
        $this->creado = new \DateTime('now');
        $this->lastupdate = new \DateTime('now');
        $this->localized = true;
        $this->eliminado = false;
    }

  public function delete($event)
  {
    // this tells the system to NOT delete the record, and just go directly to postDelete()
    $event->skipOperation();
  }

  public function postDelete($event)
  {
     $this->setEliminado(true);
  }

    /**
     * {inheritdoc}
     */
    public function prepareToJson()
    {
        return array(
            'id' => $this->id,
            'creado' => JsonParserHelper::dateToString($this->creado),
            'lastupdate' => JsonParserHelper::dateToString($this->lastupdate),
        );
    } 

    public function __toString(){
        if($this->eliminado)
            return "(ELIMINADO) - ".$this->toString();
        else
            return $this->toString();

    }
    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PersonalClasification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Reset id
     *
     * @return this
     */
    public function resetId()
    {
        $this->id = UUID::v4();

        return $this;
    }

    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;
    }

    /**
     * Get lastupdate
     *
     * @return \DateTime 
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return \DateTime
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime 
     */
    public function getCreado()
    {
        return $this->creado;
    }

    public function setUsuario(User $usuario = null)
    {
        $this->usuario = $usuario;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;
    }

    /**
     * Get eliminado
     *
     * @return boolean 
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }
}
