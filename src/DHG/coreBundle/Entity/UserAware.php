<?php
namespace DHG\coreBundle\Entity;

use DHG\UserBundle\Entity\User;
 
interface UserAware
{
    /**
     * Sets the user
     *
     * @param UserInterface|null $user A user instance or null
     */
    public function setUsuario(User $user = null);
}