<?php
namespace DHG\coreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DHG\coreBundle\UUID;

/**
 * @ORM\Entity
 * @ORM\Table(name="Accounts")
 */
class Account 
{
    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creado;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $db;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $username;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $version;
    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $fin;


    public function __construct(){
        $this->id = UUID::v4();
        $this->creado = new \DateTime('now');
        $this->localized = true;
        $this->version = 1;
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->name; 
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PersonalClasification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return \DateTime
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime 
     */
    public function getCreado()
    {
        return $this->creado;
    }
    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return \DateTime
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Unit
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $version
     * @return version
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * Set db
     *
     * @param string $db
     * @return accounts
     */
    public function setDb($db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Get db
     *
     * @return string 
     */
    public function getDb()
    {
        return $this->db;
    }
}
