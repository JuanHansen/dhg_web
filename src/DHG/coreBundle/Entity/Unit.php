<?php
namespace DHG\coreBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Units")
 */
class Unit extends MappedSuperclassBase 
{

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $abbr;
        /**
     * @ORM\ManyToOne(targetEntity="UnitClasif")
     * @ORM\JoinColumn(name="fk_clasification_id", referencedColumnName="id", nullable=false)
     */
    protected $clasificacion;

    
    public function __construct(){
        parent::__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->name.' ('.$this->abbr.')'; 
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set abbr
     *
     * @param string $abbr
     * @return Unit
     */
    public function setAbbr($abbr)
    {
        $this->abbr = $abbr;

        return $this;
    }

    /**
     * Get abbr
     *
     * @return string 
     */
    public function getAbbr()
    {
        return $this->abbr;
    }

    /**
     * Set clasificacion
     *
     * @param \DHG\coreBundle\Entity\UnitClasif $clasificacion
     * @return Unit
     */
    public function setClasificacion(\DHG\coreBundle\Entity\UnitClasif $clasificacion)
    {
        $this->clasificacion = $clasificacion;

        return $this;
    }

    /**
     * Get clasificacion
     *
     * @return \DHG\coreBundle\Entity\UnitClasif 
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }


}
