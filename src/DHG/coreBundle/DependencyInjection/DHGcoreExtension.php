<?php

namespace DHG\coreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class DHGcoreExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        $xmlloader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $xmlloader->load('twig.xml');
        
        /**
         * Form
         */
        if (isset($config['form'])) {
            $xmlloader->load('form.xml');
            foreach ($config['form'] as $key => $value) {
                if (is_array($value)) {
                    $this->remapParameters($container, 'dhg_core.form.'.$key, $config['form'][$key]);
                } else {
                    $container->setParameter(
                        'dhg_core.form.'.$key,
                        $value
                    );
                }
            }
        }
        
        /**
         * Flash
         */
        if (isset($config['flash'])) {
            $mapping = array();

            foreach ($config['flash']['mapping'] as $alertType => $flashTypes) {
                foreach ($flashTypes as $type) {
                    $mapping[$type] = $alertType;
                }
            }

            $container->getDefinition('dhg_core.twig.extension.bootstrap_flash')
                ->replaceArgument(0, $mapping);
        }
        
        /**
         * Icons
         */
        if (isset($config['icons'])) {
            $this->remapParameters($container, 'dhg_core.icons', $config['icons']);
        }
    }
    
    /**
     * Remap parameters.
     *
     * @param ContainerBuilder $container
     * @param string           $prefix
     * @param array            $config
     */
    private function remapParameters(ContainerBuilder $container, $prefix, array $config)
    {
        foreach ($config as $key => $value) {
            $container->setParameter(sprintf('%s.%s', $prefix, $key), $value);
        }
    }
}
