<?php
namespace DHG\coreBundle\EventsListener;

use DHG\coreBundle\Entity\Unit;
use DHG\coreBundle\Entity\UnitClasif;

class BundleEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Metodo de instalacion
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    static public function onInstall($event)
    {
        $em = $event->getEntityManager();
        $logger = $event->getLogger();
        $logger->info('Install Core');

        //Cargo clasificacion de unidades
        $xml = simplexml_load_file(__DIR__.'/../Resources/static/UnitiClasif.xml');
        foreach($xml->categoria as $clas)
        {
            $clasif = $em->getRepository('DHGcoreBundle:UnitClasif')->findOneByNombre($clas->nombre);
            if ( empty($clasif) ) {
                $clasif = new UnitClasif();
                $clasif->setNombre($clas->nombre);
                $clasif->setDetalle($clas->detalle);
                try {
                    $em->persist($clasif);
                } catch(Exception $e) {
                    $logger->error($e);
                }
                $em->flush();
            }
            //Cargo las unidades correspondientes a la ultima clasificacion cargada
            foreach($clas->unidades->unidad as $child) {
                $unidad = $em->getRepository('DHGcoreBundle:Unit')->findOneByName($child->nombre);
                if ( empty($unidad) ){
                    $obj = new Unit();
                    $obj->setName($child->nombre);
                    $obj->setAbbr($child->abbr);
                    $obj->setClasificacion($clasif);
                    try {
                        $em->persist($obj);
                        $em->flush();
                    } catch(Exception $e) {
                        $logger->error($e);
                    }
                }
            }
        }
    }

}
