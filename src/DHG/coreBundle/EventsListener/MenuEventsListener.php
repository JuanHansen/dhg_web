<?php
namespace DHG\coreBundle\EventsListener;

use DHG\coreBundle\Event\MenuEvents;
use DHG\coreBundle\Event\ConfigureMenuEvent;

class MenuEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }



    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMainMenu($event) {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAnidado = $factory->createItem('Ajustes', array())
                               ->setExtra('orderNumber', 9)
                                ->setAttribute('dropdown', true)
                               ->setAttribute('icon', 'cogs');
        $MenuActions = $factory->createItem('action_menu', array())
                               ->setExtra('orderNumber', 100)
                               ->setAttribute('dropdown', true)
                               ->setDisplay(false);
        $SubMenuActions = $factory->createItem('sub_action_menu', array('childrenAttributes'=>["class"=>"nav nav-pills nav-stacked no-radius"]))
                               ->setExtra('orderNumber', 1000)
                               ->setAttribute('dropdown', true)
                               ->setDisplay(false);
       $parentMenu->addChild($MenuAnidado); 
       $parentMenu->addChild($MenuActions); 
       $parentMenu->addChild($SubMenuActions); 
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureAdminMenu($event) {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuActions = $factory->createItem('action_menu', array())
                               ->setExtra('orderNumber', 100)
                                ->setAttribute('dropdown', true)
                               ->setDisplay(false);
        $parentMenu->addChild($MenuActions); 
    }
}
