<?php

namespace DHG\coreBundle\EventsListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DHG\coreBundle\Entity\UserAware;

class MapperSuperclassBaseListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        $logger = $this->container->get('logger');

        if (  $entity instanceof UserAware ) {
            $usuario = $this->container->get('security.context')->getToken()->getUser();
            $entity->setUsuario($usuario);
        }
        if (method_exists($entity, 'setLastupdate'))
            $entity->setLastupdate(new \DateTime('now'));
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityInsertions() AS $entity) {
            if (method_exists($entity, 'setLastupdate'))
                $entity->setLastupdate(new \DateTime('now'));
        }
        foreach ($uow->getScheduledEntityUpdates() AS $entity) {
            if (method_exists($entity, 'setLastupdate'))
                $entity->setLastupdate(new \DateTime('now'));
        }
        foreach ($uow->getScheduledEntityDeletions() AS $entity) {
            if (method_exists($entity, 'setEliminado')){
                $entity->setEliminado(true);
                $em->persist($entity);
                $uow->propertyChanged($entity, 'eliminado', false, true);
                $uow->scheduleExtraUpdate($entity, array(
                   'eliminado' => array(false, true),
                ));
            }
            if (method_exists($entity, 'setLastupdate'))
                $entity->setLastupdate(new \DateTime('now'));
        }
    }

    public function preQuery(OnFlushEventArgs $args)
    {

    }

}
