<?php

namespace DHG\coreBundle\Twig;

use Symfony\Component\HttpFoundation\Response;
use DHG\coreBundle\Event\HeaderRenderEvents;
use DHG\coreBundle\Event\ConfigureHeaderRenderEvent;
use DHG\coreBundle\Components\HeaderItems;

class HeaderItemsExtension extends \Twig_Extension
{

    private $ed;

    /**
     * @param DHG\coreBundle\Components\HeaderItems $headerItems
     */
    public function __construct($ed){
        $this->ed = $ed;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('render_headerItems', array($this, 'getElements'), array('is_safe' => array('html'))),
        );
    }

    /**
     * Get header items.
     *
     * @return array
     */
    public function getElements()
    {
        $items = new HeaderItems();
        $this->ed->dispatch(HeaderRenderEvents::CONFIGURE_HEADER_RENDER, new ConfigureHeaderRenderEvent($items));
        return $items->getItems();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dhg_core.twig.extension.headerItems';
    }


}
