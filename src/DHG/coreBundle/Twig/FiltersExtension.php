<?php

namespace DHG\coreBundle\Twig;

use Symfony\Component\HttpFoundation\Response;
use DHG\coreBundle\Event\HeaderRenderEvents;
use DHG\coreBundle\Event\ConfigureHeaderRenderEvent;
use DHG\coreBundle\Components\HeaderItems;

class FiltersExtension extends \Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dhg_core.twig.extension.filters';
    }


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('is_array', array($this, 'is_array')),
            new \Twig_SimpleFilter('array_sort', array($this, 'array_sort')),
            new \Twig_SimpleFilter('toArray', array($this, 'toArray')),
            new \Twig_SimpleFilter('tostring_sort', array($this, 'tostring_sort')),
        );
    }

    public function is_array($posiblearray)
    {
        return is_array($posiblearray);
    }

    public function array_sort($arreglo) {
        ksort($arreglo);
        return $arreglo;
    }
    public function toArray($arreglo) {
        if(is_array($arreglo))
            return $arreglo;

        $toR = [];
        foreach ($arreglo as $key => $value) {
            $toR[$key]=$value;
        }
        
        return $toR;
    }

    public function tostring_sort($arreglo){
        usort($arreglo,function($a, $b){
            if ($a->__toString() == $b->__toString()) {
                return 0;
            }
            return ($a->__toString() < $b->__toString()) ? -1 : 1;
        });
        return $arreglo;
    }
}
