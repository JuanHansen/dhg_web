<?php

namespace DHG\coreBundle\Twig;

use Symfony\Component\HttpFoundation\Response;

class FlashExtension extends \Twig_Extension
{
    /**
     * @var array
     */
    protected $mapping = array();

    /**
     * Constructor.
     *
     * @param array $mapping
     */
    public function __construct(array $mapping)
    {
        $this->mapping = $mapping;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('dhg_core_flash_mapping', array($this, 'getMapping'), array('is_safe' => array('html'))),
        );
    }

    /**
     * Get flash mapping.
     *
     * @return array
     */
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dhg_core_flash';
    }
}
