<?php


namespace DHG\coreBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;


class InitializrTwigExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        $meta = $this->container->getParameter('dhg_core.initializr.meta');
        $dnsPrefetch = $this->container->getParameter('dhg_core.initializr.dns_prefetch');
        $google = $this->container->getParameter('dhg_core.initializr.google');
        $diagnosticMode = $this->container->getParameter('dhg_core.initializr.diagnostic_mode');

        return array(
            'dns_prefetch'      => $dnsPrefetch,
            'meta'              => $meta,
            'google'            => $google,
            'diagnostic_mode'   => $diagnosticMode
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'form_help' => new \Twig_Function_Node('Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode', array('is_safe' => array('html'))),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'initializr';
    }
}
