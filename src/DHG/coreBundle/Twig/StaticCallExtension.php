<?php

namespace DHG\coreBundle\Twig;

use Symfony\Component\HttpFoundation\Response;
use DHG\coreBundle\Event\HeaderRenderEvents;
use DHG\coreBundle\Event\ConfigureHeaderRenderEvent;
use DHG\coreBundle\Components\HeaderItems;

class StaticCallExtension extends \Twig_Extension
{

    private $ed;

    /**
     * @param DHG\coreBundle\Components\HeaderItems $headerItems
     */
    public function __construct($ed){
        $this->ed = $ed;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('staticCall', array($this, 'staticCall')),
        );
    }

    /**
     * LLama a la funcion
     *
     */
    public function staticCall($class, $function, $args = array())
    {

        if (class_exists($class) && method_exists($class, $function))
           
            return call_user_func_array(array($class, $function), $args);
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dhg_core.twig.extension.staticCall';
    }

}
