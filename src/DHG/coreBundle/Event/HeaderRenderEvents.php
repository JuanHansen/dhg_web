<?php
namespace DHG\coreBundle\Event;

final class HeaderRenderEvents {

    /**
     * @var string
     */
    const CONFIGURE_HEADER_RENDER = 'dhg_core.headerRenderConfigure';

}