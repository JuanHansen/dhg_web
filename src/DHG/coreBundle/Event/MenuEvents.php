<?php
namespace DHG\coreBundle\Event;

final class MenuEvents {

    /**
     * @var string
     */
    const CONFIGURE_NAVBAR = 'dhg_core.menuConfigureMainMenu';

    /**
     * @var string
     */
    const CONFIGURE_ADMINMENU = 'dhg_core.menuConfigureAdminMenu';
}
