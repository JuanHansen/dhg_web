<?php

namespace DHG\coreBundle\Event;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\Event;

class BuildFormEvent extends Event{
    private $formName;
    private $options;
    private $builder;

    /**
     * @param FormBuilderInterface $builder 
     * @param array $options 
     *    
     */
    public function __construct($formName, FormBuilderInterface $builder, array $options){
        $this->builder = $builder;
        $this->options = $options;
        $this->formName = $formName;
    }

    /**
     * @return builder
     */
    public function getBuilder(){
        return $this->builder;
    }

    /**
     * @param FormBuilderInterface builder
     */
    public function setBuilder(FormBuilderInterface $builder){
        $this->builder = $builder;
    }

    /**
     * @return options
     */
    public function getOptions(){
        return $this->options;
    }

    /**
     * @return formName
     */
    public function getFormName(){
        return $this->formName;
    }
}