<?php

namespace DHG\coreBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class DefEvent extends Event{
    private $entity;
    private $stoped; 
    private $messages;

    /**
     * @param  $entity 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct($entity){
        $this->entity = $entity;
        $this->stoped   = false;
        $this->messages = array();
    }

    /**
     * @return entity
     */
    public function getEntity(){
        return $this->entity;
    }

    
    /**
     * Indicar que existe un problema que evita que el evento se produzca para el elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return entity
     */
    public function stop($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
