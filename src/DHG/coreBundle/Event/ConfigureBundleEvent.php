<?php

namespace DHG\coreBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Doctrine\ORM\EntityManager;

class ConfigureBundleEvent extends Event
{
    private $em;
    private $logger;

    /**
     */
    public function __construct(EntityManager $em, $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     */
    public function getLogger()
    {
        return $this->logger;
    }

}