<?php
namespace DHG\coreBundle\Event;

final class BundleEvents {

    /**
     * @var string
     */
    const CONFIGURE_BUNDLEINSTALL = 'dhg_core.configureBundleInstall';

    /**
     * @var string
     */
    const CONFIGURE_BUNDLEUPDATE = 'dhg_core.configureBundleUpdate';
}
