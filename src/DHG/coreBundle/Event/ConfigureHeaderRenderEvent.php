<?php

namespace DHG\coreBundle\Event;

use DHG\coreBundle\Components\HeaderItems;
use Symfony\Component\EventDispatcher\Event;

class ConfigureHeaderRenderEvent extends Event{
    private $headerItems;

    /**
     * @param DHG\coreBundle\Components\HeaderItems $headerItems
     */
    public function __construct(HeaderItems $headerItems){
        $this->headerItems = $headerItems;
    }

    /**
     * @return DHG\coreBundle\Components\HeaderItems
     */
    public function getHeaderItems(){
        return $this->headerItems;
    }
}
