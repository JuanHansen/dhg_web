<?php
namespace DHG\coreBundle\Event;

final class FormBuildEvents {

    /**
     * @var string
     */
    const BUILD_FORM = 'dhg_core.BuildForm';
}
