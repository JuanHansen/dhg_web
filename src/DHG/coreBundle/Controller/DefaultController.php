<?php

namespace DHG\coreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use DHG\coreBundle\EntityManager\DefEntityManager;
use Symfony\Component\Translation\Translator;

class DefaultController extends Controller {

    protected $namespace;
    protected $namespace_;
	protected $name;
    protected $nombre;
	protected $nameLC;
	protected $className;
	protected $formTypeName;
	protected $managerName;
	protected $urlList;
	protected $tableName;
	protected $menuName;
	protected $route;
    protected $bundle_name;
    protected $constGestion;
    protected $genero;
    protected $dateFilterCols;
    protected $noFilteredCols;
    protected $sumColumn;


	public function showFlashAction(){
        return $this->render('DHGcoreBundle:Default:justFlash.html.twig');
    }


	public function autoGenerateVars(){
        $translator = $this->get('translator');
		$this->genero       = "m";
        $this->nombre       = $translator->trans($this->name);
        $this->nameLC 		= strtolower($this->name);
		$this->namespace_ 	= str_replace("\\","",$this->namespace);
        $this->bundle_name  = str_replace("Bundle","",str_replace("DHG","",$this->namespace_));
		$this->className	= $this->namespace."\Entity\\".$this->name;
		$this->formTypeName = $this->namespace."\Form\\"."Form".$this->name;
        if ($this->managerName == null){
            $this->managerName  = "DHG\coreBundle\EntityManager\DefEntityManager";
        }

		$this->urlList 		= $this->route.$this->nameLC."List";
		$this->urlRem 		= $this->route.$this->nameLC."Remove";
		$this->urlEdit		= $this->route.$this->nameLC."Edit";
		$this->urlCreate	= $this->route.$this->nameLC."Create";
		$this->tableName 	= $this->name."Table";
		$this->menuName 	= $this->namespace."\EventsListener\MenuEventsListener";
        $this->eventRoute   = $this->namespace."\Events\\".$this->bundle_name."Events::".strtoupper($this->name);

        $this->constGestion = constant($this->namespace."\Security\\".$this->bundle_name."Permissions::"."GESTIONAR".strtoupper($this->name));

        $this->dateFilterCols = array();
        $this->noFilteredCols = array();
        $this->sumColumn      = array();
	}

    public function defListAction(Request $request){
    	$entidad = new $this->className;
        if (false === $this->get('security.context')->isGranted(constant($this->namespace."\Security\\".$this->bundle_name."Permissions::VER".strtoupper($this->name)), $entidad)) {
            throw new AccessDeniedException('No tiene acceso para acceder a esta página!');
        }

        $em = $this->getDoctrine()->getManager();
        
        $form = $this->get('form.factory')->create( $formType = new $this->formTypeName($em), $entidad);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $um = new $this->managerName($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = [];
                if ($um->createEntity($entidad, $error,constant($this->eventRoute."_CREATED"))){
                    $this->get('session')->getFlashBag()->add('success',array('creado.exitoso.'.$this->genero,"%entidad%"=>$this->nombre));
                }else{
                    $this->get('session')->getFlashBag()->add('danger',array('creado.erroneo.'.$this->genero,"%entidad%"=>$this->nombre));
                    foreach ($error as $key => $value) {
                        $this->get('session')->getFlashBag()->add('danger', $value);
                    }
                }
                return $this->redirect($this->generateUrl($this->urlList));
            }else{
                $showFormAtInit = true;
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable($this->tableName);
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        

        $result = array('dataTable' => $dataTable,
                        'dataTable_twigVars' => array(
                          'dateFilterCols' => $this->dateFilterCols,
                          'sumColumn'      => $this->sumColumn,
                          'noFilteredCols' => $this->noFilteredCols));
        if ($this->get('security.context')->isGranted($this->constGestion, $entidad)) {
            //Agrego menu
            $listener = new $this->menuName($this->container->get('event_dispatcher'),$this->name);
            $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateItems'), -99);

            $result = array_merge($result, array(
                      'dataTable' => $dataTable,
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl($this->urlList),
                      'form_submit_value' => 'Crear',
                      'form_class' => 'form-horizontal',
                      'form_show_at_init' => $showFormAtInit,
                      'dataTable_twigVars' => array(
                          'dateFilterCols' => $this->dateFilterCols,
                          'sumColumn'      => $this->sumColumn,
                          'noFilteredCols' => $this->noFilteredCols,
                          'modalConfirmID' => $metaData['table']->id,
                          'modalConfirmTitle' => 'Atencion!',
                          'modalConfirmText' => 'Se procedera a eliminar '.($this->genero=='f'?'la ':'el ').$this->nombre.' seleccionado. Esta operacion no puede deshacerse.',
                          'modalConfirmSubmitValue' => 'Eliminar',
                          'modalConfirmUrlAction' => $this->generateUrl($this->urlRem,  array('id' => '#')),
                          'modalEditID' => 'edit'.$metaData['table']->id,
                       ),
                  )
            );     
        }
        return $result;   
    }

/**
* Edita un elemento
* 
* @param Id ID del elemento
*
*/
public function defEditAction($id){
    $entidad = new $this->className;
    if (false === $this->get('security.context')->isGranted($this->constGestion, $entidad)) {
        throw new AccessDeniedException('No tiene acceso para acceder a esta página!');
    }
    $request = $this->getRequest();
    $em = $this->getDoctrine()->getManager();
    $entidad = $em->getRepository($this->namespace_.":".$this->name)->find($id);
    if ($entidad){
        $form = $this->get('form.factory')->create( $formType = new $this->formTypeName($em), $entidad);
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $um = new $this->managerName($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = [];
                if ($um->editEntity($entidad, $error,constant($this->eventRoute."_EDITED"))){
                    $this->get('session')->getFlashBag()->add('success',array('editado.exitoso.'.$this->genero,"%entidad%"=>$this->nombre));
                	return new Response("",Response::HTTP_OK);
                }else{
                    $this->get('session')->getFlashBag()->add('danger',array('editado.erroneo.'.$this->genero,"%entidad%"=>$this->nombre));
                    return new Response("",Response::HTTP_ACCEPTED);
                }
            }
        }

        return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => $this->generateUrl($this->urlEdit),
                'form_title' => 'Editar '.$entidad->__toString(),
                'form_submit_value' => 'Aplicar cambios',
                'form_class' => 'form-horizontal',
                )
            ),Response::HTTP_CREATED);
    }else{
        $this->get('session')->getFlashBag()->add('success',array('eliminado.no_existe.'.$this->genero,"%entidad%"=>$this->nombre));
        return new Response("",Response::HTTP_INTERNAL_SERVER_ERROR);
    };
}

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function defRemoveAction($id){
        $entity = new $this->className;;
        if (false === $this->get('security.context')->isGranted($this->constGestion, $entity)) {
              throw new AccessDeniedException('No tiene acceso para eliminar!');
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->namespace_.":".$this->name)->find($id);
        if (!$entity) {
            $this->get('session')->getFlashBag()->add('success',array('eliminado.no_existe.'.$this->genero,"%entidad%"=>$this->nombre));        
        }else{
            $um = new $this->managerName($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeEntity($entity, $error,constant($this->eventRoute."_REMOVED"),constant($this->eventRoute."_REMOVED_VERIFICATION"))){
              $this->get('session')->getFlashBag()->add('success',array('eliminado.exitoso.'.$this->genero,"%entidad%"=>$this->nombre));
            }else{
              foreach ($error as $key => $value) {
                    $this->get('session')->getFlashBag()->add('success',array('eliminado.erroneo.'.$this->genero,"%entidad%"=>$this->nombre));
                    $this->get('session')->getFlashBag()->add('danger', $value);
              }
            }
        }
        return $this->redirect($this->generateUrl($this->urlList));
    }

    /**
    * Crea una entidad. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las entidades
    * @return Formulario de creacion (200) si no hay parametros (POST) o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la entidad con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function defCreateAjaxAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $entity = new $this->className;;
        if (false === $this->get('security.context')->isGranted($this->constGestion, $entity)) {
            throw new AccessDeniedException('No tiene acceso para crear!');
        }
        $form = $this->get('form.factory')->create( $formType = new $this->formTypeName("Nuevo"), $entity);
        $toR = null;
        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()){
                $um = new $this->managerName($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = null;
                if ($um->createEntity($entity, $error)){
                    $toR = $this->getList();
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturn.html.twig',
                        array(
                            'listado' => $toR,
                            'selected'=>$entity
                            )
                        ),Response::HTTP_CREATED);
                    return $response;
                }else{
                    $toR=$error; 
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturnError.html.twig',
                        array('error' => $toR,)
                        ),Response::HTTP_ACCEPTED);
                    return $response;               
                }
            }else{
            }
        }
        return $this->render('DHGcoreBundle:Form:modularForms.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => $this->generateUrl($this->urlCreate),
                'form_title' => 'Crear '.$this->nombre,
                'form_submit_value' => 'Crear',
                'form_class' => 'form-horizontal',
            )
        ); 
    }
    /**
    * Retorna una lista
    * @return array
    */
    private function getList(){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository($this->namespace_.":".$this->name);
        $list = array();
        //$query = $repo->createQueryBuilder('u')->orderBy('u.nombre', 'ASC')->getQuery();
        $query = $repo->createQueryBuilder('u')->getQuery();

        foreach($query->getResult() as $plu){
            $list[$plu->getId()]=$plu;
        }
        return $list;
    }

}
