<?php
namespace DHG\coreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DHG\coreBundle\Event\BundleEvents;
use DHG\coreBundle\Event\ConfigureBundleEvent;

class DevController extends Controller{


	protected $em;
	protected $db_connection;
	/**
 	 * Inicializa el proceso de instalacion de todos los bundles
	 */
    public function installBundlesAction()
    {
    	$this->container->get('event_dispatcher')->dispatch(BundleEvents::CONFIGURE_BUNDLEINSTALL, new ConfigureBundleEvent($this->getDoctrine()->getManager(), $this->get('logger')));
    	return $this->redirect($this->generateUrl('dhg_core_homepage'));
    }
	/**
 	 * Inicializa el proceso de actualizacion de base de datos
	 */
    public function updateBundlesAction()
    {
    	$this->db_connection = $this->get('doctrine.dbal.default_connection');
    	$cuentas = $this->getAccounts();
    	foreach ($cuentas as $cuenta) {
    		$this->db_connection->forceSwitchTOAccountDB($cuenta->getDB(),$cuenta->getUsername());
    		$version = $cuenta->getVersion();
    		$this->executePreComands($version);
    		$update = $this->excecuteUpdateComands($version);
    		$this->executePostComands($version);

			$this->db_connection->forceSwitchToDefault();

            if($update != $version){
                $this->get('session')->getFlashBag()->add('success',sprintf('Cuenta '.$cuenta->getName()." actualizada a version ".$update));
        		$cuenta->setVersion($update);
        		$this->em->persist($cuenta);
                $this->em->flush();
            }
    	}
    	return $this->redirect($this->generateUrl('dhg_core_homepage'));
    }

    private function getAccounts(){
    	$this->db_connection->forceSwitchToDefault();
    	$this->em = $this->getDoctrine()->getManager();
        $repo = $this->em->getRepository('DHGcoreBundle:Account');
        $q = $repo->findAll();
        return $q;
    }


    private function executePreComands($version){

    }
    private function executePostComands($version){

    }
    private function excecuteUpdateComands($version){
    	$toR = $version;
        $kernel = $this->get('kernel');
        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $application->setAutoExit(false);
        //Create de Schema 
        $options = array('command' => 'doctrine:schema:update',"--force" => true);
        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
        echo "Ok";
        exit;
    	return $toR;
    }
}
