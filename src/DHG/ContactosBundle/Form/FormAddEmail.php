<?php
namespace DHG\ContactosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormAddEmail extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
 
        $builder
            ->add('direccion', null, array(
                'label' => 'Direccion',
                'horizontal'=>true
            ))
        ;
    }


   public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\ContactosBundle\Entity\Email',
        ));
    }

    public function getName()
    {
        return 'addEmail';
    }

}