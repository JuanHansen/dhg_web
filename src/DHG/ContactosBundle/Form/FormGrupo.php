<?php
namespace DHG\ContactosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormGrupo extends AbstractType{
    private $name = 'Nuevo_Grupo';
    private $em;
    /**
     * Builds the FormGrupo form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('nombre', 'text', array(
                'label' => "Nombre",
                'attr' => array(
                    'placeholder' => "Nombre",
                ),
                'constraints' =>  new NotBlank(array('message'=>'El nombre es requerido')),
            ))

            ->add('detalle', 'textarea', array(
                'label' => "Detalles",
                'required' => false
            ))
        ;
        $builder->setAttribute('show_legend', true); 

    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\ContactosBundle\Entity\Grupo'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

        /**
     * @param name Nombre del formulario
     */
    public function __construct($em, $name='Editar_Grupo'){
        $this->name = $name;
        $this->em = $em;
    }
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


