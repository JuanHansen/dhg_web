<?php
namespace DHG\ContactosBundle\Form;

use DHG\coreBundle\Form\Type\FormWithDependencyInjectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use DHG\coreBundle\Form\Extension\CollectionTypeExtension;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DHG\ContactosBundle\Entity\Telefono;
use DHG\ContactosBundle\Entity\Direccion;
use DHG\ContactosBundle\Entity\Email;
use DHG\PersonalBundle\Form\FormPersonal;
use Symfony\Component\Validator\Constraints\NotBlank;
use \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractTypeExtension;
use DHG\InventarioBundle\Entity\CliPro;

class FormContacto extends FormWithDependencyInjectionType{
    private $name = 'Nuevo_contacto';
    private $ed;

    private $espersonal=false;
    private $esmecanico=false;
    private $escliente = false;
    private $esproveedor=false;
    private $precios = [null,null,null,null,null];
    private $rutas_ajax;
    private $readOnly = false;
    /**
     * Builds the FormContacto form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        
        $builder

            ->add('nombre', 'text', array(
                'label' => "Nombre/Razon Social",
                'help_block' => 'Nombre y apellido del contacto o razon social de la empresa',
                'attr' => array(
                    'placeholder' => "Nombre",
                ),
                'constraints' =>  new NotBlank(array('message'=>'El contacto debe tener un nombre')),
            ))
            ->add('identificacion', 'text', array(
                'label' => "Identificacion (cuil/cuit/DNI)",
                'required' => false,
                'attr' => array('endSection'=>''),
            ))    
           ->add('telefonos', 'collection', array(
                'type'           => new FormAddTelefono(),
                'label'          => 'Telefonos',
                'by_reference'   => false,
                'prototype_data' => new Telefono(),
                'allow_delete'   => true,
                'allow_add'      => true,
                'attr' => array('endSection'=>''),
                'required' => false,
                'widget_add_btn' => array('label' => "Telefonos", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),  
                'options' => array(
                    'horizontal' => true,
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger'))),
            ))
            ->add('direcciones', 'collection', array(
                'type'           => new FormAddDireccion(),
                'label'          => 'Direcciones',
                'by_reference'   => false,
                'prototype_data' => new Direccion(),
                'allow_delete'   => true,
                'allow_add'      => true,
                'required' => false,
                'attr' => array('endSection'=>''),
                'widget_add_btn' => array('label' => "Direcciones", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),  
                'options' => array(
                    'horizontal' => true,
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger'))),
            ))
            ->add('emails', 'collection', array(
                'type'           => new FormAddEmail(),
                'label'          => 'Emails',
                'by_reference'   => false,
                'prototype_data' => new Email(),
                'allow_delete'   => true,
                'allow_add'      => true,
                'required' => false,
                'attr' => array('endSection'=>''),
                'widget_add_btn' => array('label' => "Emails", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),  
                'options' => array(
                    'horizontal' => true,
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger'))),
            ))
            ->add('grupos','collection', array(
                'type' => 'entity',
                'allow_add' => true,
                'allow_delete' => true, 
                'prototype' => true,
                'required' => false,
                'options' => array( 
                    'class' => 'DHG\ContactosBundle\Entity\Grupo',
                    'empty_value' => 'Seleccione un grupo',
                    'empty_data'  => null,
                    'constraints' =>  new NotBlank(),

                    'horizontal' => true,
                    'label_render' => false,
                    'horizontal_input_wrapper_class' => "col-lg-4",
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger')),
                    'widget_addon_append' => array(
                        'form_ajax'=>$this->rutas_ajax['grupo_create'],
                    ),
                ),
                'widget_add_btn' => array('label' => "Grupos", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),   
                'attr' => array('endSection'=>''),
                'by_reference' => false,
            ))

            ->add('escliente', 'checkbox', array(
                'label'        => "Es cliente",
                'widget_checkbox_label' => "label",
                'data'=>$this->escliente,
                'mapped'      => false,
                'required' => false,
                'help_label_popover'=>array('title'=>"Cliente",'content'=>"Si un contacto es cliente, podrá seleccionarlo como destino en las operaciones de venta"),
                'read_only'=> $this->escliente&&$this->readOnly
            ))
            ->add('esproveedor', 'checkbox', array(
                'label'        => "Es proveedor",
                'widget_checkbox_label' => "label",
                'data'=>$this->esproveedor,
                'mapped'      => false,
                'required' => false,
                'help_label_popover'=>array('title'=>"Proveedor",'content'=>"Si un contacto es proveedor, podrá seleccionarlo como encargado en las operaciones de compra"),
                'read_only'=> $this->esproveedor&&$this->readOnly
            ))
            ->add('esmecanico', 'checkbox', array(
                'label'        => "Es mecanico",
                'widget_checkbox_label' => "label",
                'data'=>$this->esmecanico,
                'mapped'      => false,
                'required' => false,
                'help_label_popover'=>array('title'=>"Mecanico",'content'=>"Si un contacto es mecanico, podrá seleccionarlo como mecanico en las operaciones de maquinarias"),
                'read_only'=> $this->esmecanico&&$this->readOnly
            ))
            ->add('espersonal', 'checkbox', array(
                'label'        => "Es personal",
                'widget_checkbox_label' => "label",
                'data'=>$this->espersonal,
                'mapped'      => false,
                'required' => false,
                'help_label_popover'=>array('title'=>"Personal",'content'=>"Si un contacto es personal, podrá seleccionarlo como encargado en las operaciones"),
                'attr' => array(
                    'data_source' => "checkpersonal",
                ),
                'read_only'=> $this->espersonal&&$this->readOnly
            ))
            ->add('clasification', 'entity', array(
                'label' => 'Clasificación',
                'class' => 'DHG\PersonalBundle\Entity\PersonalClasification',
                'property' => 'name',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC');
                },
                'empty_value' => 'Seleccione una clasificación',
                'empty_data'  => null,
                'required' => false,
                'mapped'      => false,
                'attr' => array(
                    'data_destination' => "personalclasification",
                ),
                'data'=>$this->precios[4],
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['personalClasification_create'],
                ),
            ))

            ->add('price_hour', 'number', array(
                'label' => 'Precio por hora',
                'attr' => array('placeholder' => '0.00'),
                'widget_addon_prepend' => array('text' => '$'),
                'required' => false,
                'mapped'      => false,
                'attr' => array(
                    'data_destination' => "personalprice",
                ),
                'data'=>$this->precios[0]
            ))

            ->add('price_hour_extra', 'number', array(
                'label' => 'Precio por hora extra',
                'attr' => array('placeholder' => '0.00'),
                'widget_addon_prepend' => array('text' => '$'),
                'required' => false,
                'mapped'      => false,
                'attr' => array(
                    'data_destination' => "personalprice",
                ),
                'data'=>$this->precios[1]
            ))

            ->add('price_kg', 'number', array(
                'label' => 'Precio por Kg',
                'attr' => array('placeholder' => '0.00'),
                'widget_addon_prepend' => array('text' => '$'),
                'required' => false,
                'mapped'      => false,
                'attr' => array(
                    'data_destination' => "personalprice",
                ),
                'data'=>$this->precios[2]
            ))

            ->add('price_ha', 'number', array(
                'label' => 'Precio por Ha',
                'attr' => array('placeholder' => '0.00'),
                'widget_addon_prepend' => array('text' => '$'),
                'required' => false,
                'mapped'      => false,
                'attr' => array(
                    'data_destination' => "personalprice",
                    'endSection'=>''
                ),
                'data'=>$this->precios[3]
            ))
            ->add('details', 'textarea', array(
                'label' => "Detalles",
                'required' => false
            ));

        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postsubmitEvent'));
        $builder->setAttribute('show_legend', true); 
        //parent::buildForm($builder,$options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\ContactosBundle\Entity\Contacto',
        ));
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($ed, $name='Nuevo_contacto',$em=null,$contacto=null,$rutas_ajax=array()){
        $this->name = $name;
        $this->ed = $ed;
        $this->rutas_ajax = $rutas_ajax;

        if($em!=null&&$contacto!=null){
            $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
            if($personal){
                $this->espersonal=true;
                $this->precios[0]=$personal->getPriceHour();
                $this->precios[1]=$personal->getPriceHourExtra();
                $this->precios[2]=$personal->getPriceKg();
                $this->precios[3]=$personal->getPriceHa();
                $this->precios[4]=$personal->getClasification();

            }
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
            if($mecanico){
                $this->esmecanico=true;
            }
            $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));
            if($clipro!=null){
                if($clipro->getCliente())
                    $this->escliente=true;
                if($clipro->getProveedor())
                    $this->esproveedor=true;
            }
        }


    }
    public function getName()
    {
        return $this->name;
    }

    /**
    * Establece que el formulario inicie con las propiedades checkeadas
    * @param $esPersonal true para que el contacto sea personal
    * @param $esMecanico true para que el contacto sea mecanico
    * @param $esCliente true para que el contacto sea cliente
    * @param $esProveedor true para que el contacto sea proveedor
    * @param $readOnly si es true, las propiedades checkeadas no se pueden descheckear
    */
    public function setChecked($esPersonal=false, $esMecanico=false, $esCliente=false,  $esProveedor=false, $readOnly=false){
        if($esPersonal){
            $this->espersonal=true;
        }
        if($esMecanico){
            $this->esmecanico=true;
        }
        if($esCliente)
            $this->escliente=true;
        if($esProveedor)
            $this->esproveedor=true;
        $this->readOnly = $readOnly;
    }

    /**
     * Rearmo el objeto Contacto
     *
     */
     public function postsubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        if (is_array($data)) {
            $espersonal = $data['espersonal'];
            $clasificacion = $data['clasification'];
        }else{
            $espersonal = $form->get('espersonal')->getData();
            $clasificacion =  $form->get('clasification')->getData();
        }
        if($espersonal && $clasificacion==null){
            $form->get('clasification')->addError(new \Symfony\Component\Form\FormError('Si el contacto es personal debe elegir una clasificacion'));
        }
    }
}