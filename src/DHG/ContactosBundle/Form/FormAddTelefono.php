<?php
namespace DHG\ContactosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormAddTelefono extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
 
        $builder
            ->add('numero', null, array(
                'label' => "Numero",
                'horizontal'=>true
            ))
        ;
    }


   public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\ContactosBundle\Entity\Telefono',
        ));
    }

    public function getName()
    {
        return 'addTelefono';
    }

}