<?php
namespace DHG\ContactosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormAddDireccion extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
 
        $builder
            ->add('calle', null, array(
                'label' => 'Calle',
                'horizontal'=>true
            ))
        ;
    }


   public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\ContactosBundle\Entity\Direccion',
        ));
    }

    public function getName()
    {
        return 'addDireccion';
    }

}