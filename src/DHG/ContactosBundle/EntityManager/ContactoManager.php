<?php

namespace DHG\ContactosBundle\EntityManager;

use DHG\ContactosBundle\Entity\Contacto;
use Doctrine\ORM\EntityManager;

use DHG\ContactosBundle\Events\ContactoEvents;
use DHG\ContactosBundle\Events\ContactoCreatedEvent;
use DHG\ContactosBundle\Events\ContactoEditedEvent;
use DHG\ContactosBundle\Events\ContactoRemovedEvent;
use DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent;
use DHG\ContactosBundle\Events\ContactoCreatedVerificationEvent;
use DHG\InventarioBundle\Entity\CliPro;
use DHG\MaquinariaBundle\Entity\Mecanico;
use DHG\PersonalBundle\Entity\Personal;
use DHG\InventarioBundle\EntityManager\CliProManager;
use DHG\MaquinariaBundle\EntityManager\MecanicoManager;
use DHG\PersonalBundle\EntityManager\PersonalManager;
use Symfony\Component\Form\Form;

use DHG\ContactosBundle\Security\ContactosPermissions;

class ContactoManager{
	private $em;
	private $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em, $ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la contacto
     *
	 * @param $contacto Contacto con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createContacto(Contacto $contacto, &$error = [] ){
    	try{
            $this->em->persist($contacto);
            $this->em->flush();
            $this->ed->dispatch(ContactoEvents::CONTACTO_CREATED, new ContactoCreatedEvent($contacto));
        } catch (\Exception $e){
        	//$error[] = $e->getMessage(); 
        	$error[] = "Ocurrio un error al intentar crear el Contacto";
            return false;
        }
        return true;
    }      
    /**
     * Crea en base de datos la contacto
     *
     * @param $contacto Contacto con la informacion a almacenar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si todo fue executado correctamente
     *
     */
    public function createMultiContacto(Contacto $contacto, Form $form, &$error = [] ,$verificador=null){
        $espersonal = $form->get('espersonal')->getData();
        $esproveedor = $form->get('esproveedor')->getData();
        $escliente = $form->get('escliente')->getData();
        $esmecanico = $form->get('esmecanico')->getData();
        $toR = true;
        try{
            $this->em->persist($contacto);
            $this->em->flush();
            $this->ed->dispatch(ContactoEvents::CONTACTO_CREATED, new ContactoCreatedEvent($contacto));
        } catch (\Exception $e){
            //$error[] = $e->getMessage(); 
            $error[] = "Ocurrio un error al intentar crear el Contacto";
            $toR = false;
        }


        if($esmecanico){
            $mecanico = new Mecanico();
            if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARMECANICOS, $mecanico)){
                $mecanico->setContacto($contacto);
                $mm = new MecanicoManager($this->em,$this->ed);
                $mm->createMecanico($mecanico,$error);
            }else{
                $error[]="No tiene acceso para gestionar mecanicos";
                $toR = false;
            }
        }
        if($esproveedor||$escliente){
            $clipro = new Clipro();
            if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARCLIPRO, $clipro)){
                $clipro->setContacto($contacto);
                $clipro->setCliente($escliente);
                $clipro->setProveedor($esproveedor);
                $cpm = new CliProManager($this->em,$this->ed);
                $cpm->createCliPro($clipro,$error);
            }else{
                $error[]= "No tiene acceso para crear clientes o proveedores";
                $toR = false;
            }
        }
        if($espersonal){
            $clasification = $form->get('clasification')->getData();
            $price_hour = $form->get('price_hour')->getData();
            $price_hour_extra = $form->get('price_hour_extra')->getData();
            $price_kg = $form->get('price_kg')->getData();
            $price_ha = $form->get('price_ha')->getData();
            $personal = new Personal();
            if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARPERSONAL, $personal)){
                $personal->setClasification($clasification);
                $personal->setPriceHour($price_hour);
                $personal->setPriceHourExtra($price_hour_extra);
                $personal->setPriceKg($price_kg);
                $personal->setPriceHa($price_ha);
                $personal->setContacto($contacto);
                $pm = new PersonalManager($this->em,$this->ed);
                if(!$pm->createPersonal($personal,$error)){
                    $toR = false;
                }
            }else{
                $error[]="No tiene acceso para crear personal";
                $toR = false;
            }
        }
        return $toR;
    }     

    /**
     * Edita en base de datos la contacto
     *
	 * @param $contacto Contacto con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editContacto(Contacto $contacto,Form $form, &$error = [],$verificador=null ){
        $espersonal = $form->get('espersonal')->getData();
        $esproveedor = $form->get('esproveedor')->getData();
        $escliente = $form->get('escliente')->getData();
        $esmecanico = $form->get('esmecanico')->getData();

        $filtro = $this->em->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);

        $personal = $this->em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
        $mecanico = $this->em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
        $clipro = $this->em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));


        try{
            $this->em->persist($contacto);
            $this->em->flush();
            $this->ed->dispatch(ContactoEvents::CONTACTO_EDITED, new ContactoEditedEvent($contacto));
        } catch (\Exception $e){
            //$error[] = $e->getMessage(); 
            $error[] = "Ocurrio un error al intentar editar el contacto";
        	return false;
        }

        if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARMECANICOS, new Mecanico())){
            if($esmecanico){
                if($mecanico==null){
                    $mecanico = new Mecanico();
                    $mecanico->setContacto($contacto);
                    $mm = new MecanicoManager($this->em,$this->ed);
                    $mm->createMecanico($mecanico,$error);
                }else{
                    if($mecanico->getEliminado()){
                        $mecanico->setEliminado(false);
                        $this->em->persist($mecanico);
                        $this->em->flush();
                    }
                }
            }else{
                if($mecanico!=null&&!$mecanico->getEliminado()){
                    $mm = new MecanicoManager($this->em,$this->ed);
                    $mm->removeMecanico($mecanico,$error);
                }
            }
        }else{
            $error[]="No tiene acceso para gestionar mecanicos";
        }

        if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARCLIPRO, new CliPro())){
            if($esproveedor||$escliente){
                if($clipro==null){
                    $clipro = new Clipro();
                    $clipro->setContacto($contacto);
                    $clipro->setCliente($escliente);
                    $clipro->setProveedor($esproveedor);
                    $cpm = new CliProManager($this->em,$this->ed);
                    $cpm->createCliPro($clipro,$error);
                }else{
                    $clipro->setEliminado(false);
                    $clipro->setCliente($escliente);
                    $clipro->setProveedor($esproveedor);
                    $cpm = new CliProManager($this->em,$this->ed);
                    $cpm->editCliPro($clipro,$error);
                }
            }else{
                if($clipro!=null&&!$clipro->getEliminado()){
                    $cpm = new CliProManager($this->em,$this->ed);
                    $cpm->removeCliPro($clipro,$error);
                }
            }
        }else{
            $error[]="No tiene acceso para gestionar clientes o provedores";
        }
        if($verificador!=null&&$verificador->isGranted(ContactosPermissions::GESTIONARPERSONAL, new Personal())){
            if($espersonal){
                $clasification = $form->get('clasification')->getData();
                $clasification = $this->em->getRepository('DHGPersonalBundle:PersonalClasification')->findOneBy(array("id"=>$clasification));
                $price_hour = $form->get('price_hour')->getData();
                $price_hour_extra = $form->get('price_hour_extra')->getData();
                $price_kg = $form->get('price_kg')->getData();
                $price_ha = $form->get('price_ha')->getData();
                if($personal==null){
                    if($clasification==null||$clasification==""){
                        $error[]="Debe seleccionar una clasificacion para el personal";
                    }else{
                        $personal = new Personal();
                        $personal->setClasification($clasification);
                        $personal->setPriceHour($price_hour);
                        $personal->setPriceHourExtra($price_hour_extra);
                        $personal->setPriceKg($price_kg);
                        $personal->setPriceHa($price_ha);
                        $personal->setContacto($contacto);
                        $pm = new PersonalManager($this->em,$this->ed);
                        $pm->createPersonal($personal,$error);
                    }
                }else{
                    $personal->setEliminado(false);
                    $personal->setClasification($clasification);
                    $personal->setPriceHour($price_hour);
                    $personal->setPriceHourExtra($price_hour_extra);
                    $personal->setPriceKg($price_kg);
                    $personal->setPriceHa($price_ha);
                    $pm = new PersonalManager($this->em,$this->ed);
                    $pm->editPersonal($personal,$error);
                }
            }else{
                if($personal!=null&&!$personal->getEliminado()){
                    $pm = new PersonalManager($this->em,$this->ed);
                    $pm->removePersonal($personal,$error);
                }
            }
        }else{
            $error[]="No tiene acceso para gestionar personal";
        }
        $filtro = $this->em->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',0);
        return true;
    }      


    /**
     * Elimina en base de datos la contacto
     *
	 * @param $contacto Contacto con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
  public function removeContacto(Contacto $contacto, &$error = array(),$verificador = null ){
        $personal = $this->em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
        $mecanico = $this->em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
        $clipro = $this->em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));

        if($personal!=null&&$verificador!=null&&!$verificador->isGranted(ContactosPermissions::GESTIONARPERSONAL, $personal)){
            $error[] = "No tiene permisos para eliminar un personal";
            return false;
        }
        if($mecanico!=null&&$verificador!=null&&!$verificador->isGranted(ContactosPermissions::GESTIONARMECANICOS, $mecanico)){
            $error[] = "No tiene permisos para eliminar un mecanico";
            return false;
        }
        if($clipro!=null&&$verificador!=null&&!$verificador->isGranted(ContactosPermissions::GESTIONARCLIPRO, $clipro)){
            $error[] = "No tiene permisos para eliminar un cliente o proveedor";
            return false;
        }

        if ($this->removeVerificationContacto($contacto, $error)){
            $this->ed->dispatch(ContactoEvents::CONTACTO_REMOVED, new ContactoRemovedEvent($contacto));
			try{
	            $this->em->remove($contacto);
	            $this->em->flush();

            } catch (\Exception $e){
	        	//$error[] = $e->getMessage(); 
                $error[] = "Ocurrio un error al intentar eliminar el Contacto";
	        	return false;
	        }
    	}else{
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la creacion
     *
     * @param $contacto Contacto con la informacion a crear
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al creado
     *
     */
    public function createVerificationContacto(Contacto $contacto, &$error = array() ){
        $event = new ContactoCreatedVerificationEvent($contacto);
        $this->ed->dispatch(ContactoEvents::CONTACTO_CREATED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error =array_merge($error,$event->getMessages());
            return false;
        }
        return true;
    } 
    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $contacto Contacto con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationContacto(Contacto $contacto, &$error = array() ){
        $event = new ContactoRemovedVerificationEvent($contacto);
        $this->ed->dispatch(ContactoEvents::CONTACTO_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error =array_merge($error,$event->getMessages());
        	return false;
        }
        return true;
    }  
}
