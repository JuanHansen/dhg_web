<?php

namespace DHG\ContactosBundle\EntityManager;

use DHG\coreBundle\EntityManager\DefEntityManager;




use DHG\ContactosBundle\Entity\Grupo;
use Doctrine\ORM\EntityManager;

use DHG\ContactosBundle\Events\ContactoEvents;
use DHG\ContactosBundle\Events\GrupoCreatedEvent;
use DHG\ContactosBundle\Events\GrupoEditedEvent;
use DHG\ContactosBundle\Events\GrupoRemovedEvent;
use DHG\ContactosBundle\Events\GrupoRemovedVerificationEvent;

class GrupoManager extends DefEntityManager{
	private $em;
	private $ed;

    /**
     * Crea en base de datos la grupo
     *
	 * @param $grupo Grupo con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createGrupo(Grupo $grupo, &$error = [] ){
    	try{
            $this->em->persist($grupo);
            $this->em->flush();
            $this->ed->dispatch(ContactoEvents::GRUPO_CREATED, new GrupoCreatedEvent($grupo));
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar crear el Grupo de Contactos";
        	return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la grupo
     *
	 * @param $grupo Grupo con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editGrupo(Grupo $grupo, &$error = [] ){
    	try{

            $this->em->persist($grupo);
            $this->em->flush();
            $this->ed->dispatch(ContactoEvents::GRUPO_EDITED, new GrupoEditedEvent($grupo));
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar editar el Grupo de Contacos";
        	return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la grupo
     *
	 * @param $grupo Grupo con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeGrupo(Grupo $grupo, &$error = array() ){
    	if ($this->removeVerificationGrupo($grupo, $error)){
			try{
	            $this->em->remove($grupo);
	            $this->em->flush();
	            $this->ed->dispatch(ContactoEvents::GRUPO_REMOVED, new GrupoRemovedEvent($grupo));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar el Grupo de Contactos";
	        	return false;
	        }
    	}else{
        	$error = $event->getMessages();
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $grupo Grupo con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationGrupo(Grupo $grupo, &$error = array() ){
        $event = new GrupoRemovedVerificationEvent($grupo);
        $this->ed->dispatch(ContactoEvents::GRUPO_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    }  
}
