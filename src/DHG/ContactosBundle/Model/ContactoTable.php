<?php
namespace DHG\ContactosBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class ContactoTable
 *
 * @DataTable\Table(id="ContactosTable"  )
 */
class ContactoTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="Contacto.nombre", name="Nombre")
     * @DataTable\DefaultSort()
     */
    public $nombre;
    /**
     * @var string
     * @DataTable\Column(source="Contacto.identificacion", name="Identificacion")
     */
    public $identificacion;
    /**
     * @var boolean
     * @DataTable\Column(source="Contacto.grupos", name="Grupos" )
     * @DataTable\Format(dataFields={"grupos":"Contacto.grupos"}, template="DHGcoreBundle:Datatable:cellGrupos.html.twig")
     */
    public $grupos;
    /**
     * @var string
     * @DataTable\Column(source="Contacto.usuariorelacionado.username", name="Usuario Relacionado")
     * @DataTable\Format(dataFields={"test":"Contacto.usuariorelacionado","valor":"Contacto.usuariorelacionado.username"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    public $usuariorelacionado;
    /**
     * @var boolean
     * @DataTable\Column(source="Contacto.esCliente", name="Cliente" )
     * @DataTable\Format(dataFields={"value":"Contacto.esCliente"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    public $esCliente;
    /**
     * @var boolean
     * @DataTable\Column(source="Contacto.esProveedor", name="Proveedor" )
     * @DataTable\Format(dataFields={"value":"Contacto.esProveedor"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    public $esProveedor;
    /**
     * @var boolean
     * @DataTable\Column(source="Contacto.esMecanico", name="Mecanico" )
     * @DataTable\Format(dataFields={"value":"Contacto.esMecanico"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    public $esMecanico;
    /**
     * @var boolean
     * @DataTable\Column(source="Contacto.esPersonal", name="Personal" )
     * @DataTable\Format(dataFields={"value":"Contacto.esPersonal"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    public $esPersonal;


    /**
     * @var string
     * @DataTable\Column(source="Contacto.details", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "Detalle", "text":"Contacto.details"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $details;
    /**
     * @var int
     * @DataTable\Column(source="Contacto.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Contacto"}, template="DHGContactosBundle:Table:ContactosCellAction.html.twig")
     */
    public $action;

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\ContactosBundle\Entity\Contacto');
        $qb = $userRepository->createQueryBuilder('Contacto');
        return $qb;
    }

}