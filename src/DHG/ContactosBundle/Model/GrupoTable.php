<?php
namespace DHG\ContactosBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class GrupoTable
 *
 * @DataTable\Table(id="GrupoTable"  )
 */
class GrupoTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="Grupo.nombre", name="Nombre")
     * @DataTable\DefaultSort()
     */
    public $nombre;

    /**
     * @var string
     * @DataTable\Column(source="Grupo.detalle", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "nombre", "text":"Grupo.detalle"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $detalle;

    /**
     * @var int
     * @DataTable\Column(source="Grupo.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Grupo", "sistema":"Grupo.sistema"}, template="DHGContactosBundle:Table:GruposCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\ContactosBundle\Entity\Grupo');
        $qb = $userRepository->createQueryBuilder('Grupo');
        return $qb;
    }

}