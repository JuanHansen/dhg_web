<?php

namespace DHG\ContactosBundle\EventsListener;

use DHG\ContactosBundle\Events\ContactoEvents;


class ContactoEventsListener
{
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\ContactoBundle\Events\ContactoEvents $event
     */
    public function onContactoRemovedVerification($event){
        
    }
    /**
     *
     * @param DHG\ContactoBundle\Events\ContactoEvents $event
     */
    public function onContactoCreatedVerification($event){
        $em = $this->entityManager;
        $contacto= $event->getContacto();
        $user = $contacto->getUsuariorelacionado();
        if($user!=null){

            $contacto1 = $em->getRepository('DHGContactosBundle:Contacto')->findOneBy(array('usuariorelacionado' => $user->getId()));
            if($contacto1 != null&& $contacto1->getId()!=$contacto->getId()){
                $event->stopRemove(sprintf('No es posible asignar el usuario "%s" al contacto, ya que esta asociado a otro contacto.', $user->getUsername() ), 'Contacto');
            }
        }
    }

}
