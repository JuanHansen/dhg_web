<?php

namespace DHG\ContactosBundle\EventsListener;

use DHG\ContactosBundle\Events\ContactoEvents;


class GrupoEventsListener
{
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\GrupoBundle\Events\ContactoEvents $event
     */
    public function onGrupoVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getGrupo()->getId();

        $grupo = $this->entityManager
                        ->createQueryBuilder()
                        ->addSelect('d')
                        ->from('DHGContactosBundle:Contacto', 'd')
                        ->leftJoin('d.grupos', 'm')
                        ->groupBy('m.id')
                        ->where('m.id LIKE :id')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getOneOrNullResult();

        if($grupo != null){
            $event->stopRemove(sprintf('No es posible eliminar el grupo "%s", ya que existen contactos que lo utilzian.', $event->getGrupo()->getNombre() ), 'Grupo');
        }

    }

}
