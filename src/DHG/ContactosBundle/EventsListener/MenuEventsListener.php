<?php

namespace DHG\ContactosBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use DHG\ContactosBundle\Entity\Contacto;
use DHG\ContactosBundle\Entity\Grupo;
use DHG\ContactosBundle\Security\ContactosPermissions;
use DHG\PersonalBundle\Security\PersonalPermissions;
use DHG\PersonalBundle\Entity\PersonalClasification;


class MenuEventsListener
{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher,$mensaje = "Agregar"){
        $this->eventDispatcher = $eventDispatcher;
        $this->mensaje = $mensaje;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBar($event){
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
 
        $MenuAnidado = $factory ->createItem('Contactos', array())
                                ->setExtra('orderNumber', 6)
                                ->setAttribute('icon', 'phone');
        
        $contacto = new Contacto();
        $MenuAnidado->addChild(
                    $factory->createItem('Contactos', array('route' => 'dhg_contactos_contactoList'))
                            ->setExtra('orderNumber',2)
                            ->setExtra('permission',array('permission'=>ContactosPermissions::VERCONTACTOS,'entity'=>$contacto))
                    );

        $MainMenuAjustes = $parentMenu->getChild('Ajustes');
        $MenuAjustes = $factory ->createItem('Contactos', array())
                                ->setAttribute('dropright', true);

        $grupo = new Grupo();
        $clasification = new PersonalClasification();
        $MenuAjustes->addChild(
                    $factory->createItem('Grupos de contacto', array('route' => 'dhg_contactos_grupoList'))
                            ->setExtra('orderNumber',2)
                            ->setExtra('permission',array('permission'=>ContactosPermissions::VERGRUPOSCONTACTOS,'entity'=>$grupo))
                    );              
        $MenuAjustes->addChild(
                    $factory->createItem('Clasificacion de personal', array('route' => 'dhg_personal_claslistado'))
                            ->setExtra('orderNumber',2)
                            ->setExtra('permission',array('permission'=>ContactosPermissions::VERPERSONALCLASIFICACION,'entity'=>$clasification))
                    );
        $MainMenuAjustes->addChild($MenuAjustes); 
        $parentMenu->addChild($MenuAnidado); 
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem($this->mensaje, array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
    }
    
}