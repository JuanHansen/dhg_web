<?php

namespace DHG\ContactosBundle\EventsListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DHG\ContactosBundle\Entity\Contacto;

class ContactoListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
     public function postLoad(LifecycleEventArgs $args)
    {   
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Contacto){
            $per = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array('contacto'=>$entity->getId()));
            if($per!=null){
                $entity->setEsPersonal(true);
            }
            $per = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array('contacto'=>$entity->getId()));
            if($per!=null){
                $entity->setEsMecanico(true);
            }
            $per = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array('contacto'=>$entity->getId()));
            if($per!=null){
                if($per->getCliente())
                    $entity->setEsCliente(true);
                if($per->getProveedor())
                    $entity->setEsProveedor(true);
            }
               
        }
    }

}