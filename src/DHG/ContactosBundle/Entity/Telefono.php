<?php
namespace DHG\ContactosBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Telefono")
 */
class Telefono extends MappedSuperclassBase
{
	/**
     * @ORM\Column(type="string", length=100)
     */
    protected $numero;


    public function __construct(){
        parent::__construct();

    }
    public function __toString(){
        return $this->numero;
    }
    /**
     * Set numero
     *
     * @param string $numero
     * @return Telefono
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }






}