<?php
namespace DHG\ContactosBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\Collection;
use \Doctrine\Common\Collections\ArrayCollection;
use DHG\ContactosBundle\Entity\Telefono;
use DHG\ContactosBundle\Entity\Direccion;
use DHG\ContactosBundle\Entity\Email;
use Symfony\Component\Validator\Constraints as Assert;
use DHG\EntityHistoryBundle\Entity\Versionable;
/**
 * @ORM\Entity
 * @ORM\Table(name="Contacto")
 */
class Contacto extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $nombre;
    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\ContactosBundle\Entity\Telefono",cascade={"persist","remove"})
     * @ORM\JoinTable(name="Contacto_Telefono",
     *   joinColumns={@ORM\JoinColumn(name="contacto_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="telefono_id", referencedColumnName="id")}
     * ) 
     */
    protected $telefonos;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\ContactosBundle\Entity\Direccion",cascade={"persist","remove"})
     * @ORM\JoinTable(name="Contacto_Direccion",
     *   joinColumns={@ORM\JoinColumn(name="contacto_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="direccion_id", referencedColumnName="id")}
     * ) 
     */
    protected $direcciones;
    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\ContactosBundle\Entity\Email",cascade={"persist","remove"})
     * @ORM\JoinTable(name="Contacto_Email",
     *   joinColumns={@ORM\JoinColumn(name="email_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="telefono_id", referencedColumnName="id")}
     * ) 
     */
    protected $emails;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $identificacion;
    /**
     * @ORM\OneToOne(targetEntity="DHG\UserBundle\Entity\User", mappedBy="contacto")
     * @ORM\JoinColumn(name="fk_userrelacionado_id", referencedColumnName="id", nullable=true)
     */
    protected $usuariorelacionado;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\ContactosBundle\Entity\Grupo")
     * @ORM\JoinTable(name="Contacto_Grupo",
     *   joinColumns={@ORM\JoinColumn(name="contacto_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="grupo_id", referencedColumnName="id")}
     * ) 
     */
    protected $grupos;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $details;

    private $esPersonal=false;
    private $esMecanico=false;
    private $esCliente=false;
    private $esProveedor=false;
     static public function getResourceEntityName(){
        return 'Contacto';
    }

    static public function getResourceIcon(){
        return 'phone';
    }

    static public function getSeccionIcon(){
        return 'phone';
    } 
    static public function getResourceColor(){
        return '#1E75DA';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre/Razon Social',
                'telefonos' => 'Telefonos',
                'direcciones' => 'Direcciones',
                'emails' => 'Emails',
                'identificacion' => 'Identificacion',
                'usuariorelacionado' => 'Usuario Realcionado',
                'grupos' => 'Grupos',
                'details' => 'Detalles',
            );
    }
    public function __construct(){
        parent::__construct();
        $this->telefonos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->direcciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        if($this->nombre!=null)
            return $this->nombre;
        else
            return "";
    }
    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Contacto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Set identificacion
     *
     * @param string $identificacion
     * @return Contacto
     */
    public function setIdentificacion($identificacion)
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    /**
     * Get identificacion
     *
     * @return string 
     */
    public function getIdentificacion()
    {
        return $this->identificacion;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Contacto
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string userrelacionado
     */
    public function getDetails()
    {
        return $this->details;
    }



    /**
     * Add grupos
     *
     * @param \DHG\ContactosBundle\Entity\Grupo $grupo
     * @return Contacto
     */
    public function addGrupo(\DHG\ContactosBundle\Entity\Grupo $grupo)
    {
        if (!$this->grupos->contains($grupo))
            $this->grupos->add($grupo);

        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \DHG\ContactosBundle\Entity\Grupo $grupo
     */
    public function removeGrupo(\DHG\ContactosBundle\Entity\Grupo $grupo)
    {
        $this->grupos->removeElement($grupo);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos(){
        return $this->grupos;
    }



    /**
     * Remove telefonos
     *
     * @param \DHG\ContactosBundle\Entity\Telefono $telefonos
     */
    public function removeTelefonos(\DHG\ContactosBundle\Entity\Telefono $telefonos)
    {
        $this->telefonos->removeElement($telefonos);
    }
    /**
     * Add telefono
     *
     * @param \Doctrine\Common\Collections\Collection $telefonos
     * @return Contacto
     */
    public function setTelefonos(\Doctrine\Common\Collections\Collection $telefonos)
    {
        $this->telefonos = $telefonos;
    }
    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelefonos()
    {
        return $this->telefonos;
    }




    /**
     * Add direccion
     *
     * @param \Doctrine\Common\Collections\Collection $direcciones
     * @return Contacto
     */
    public function setDirecciones(\Doctrine\Common\Collections\Collection $direcciones)
    {
        $this->direcciones = $direcciones;

    }
    /**
     * Get direcciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDirecciones()
    {
        return $this->direcciones;
    }

    /**
     * Remove direccion
     *
     * @param \DHG\ContactosBundle\Entity\Direccion $direccion
     */
    public function removeDireccion(\DHG\ContactosBundle\Entity\Direccion $direccion)
    {
        $this->direcciones->removeElement($direccion);
    }

    /**
     * Add email
     *
     * @param \Doctrine\Common\Collections\Collection $emails
     * @return Contacto
     */
    public function setEmails(\Doctrine\Common\Collections\Collection $emails)
    {
        $this->emails = $emails;
    }
    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmails()
    {
        return $this->emails;
    }
    /**
     * Remove email
     *
     * @param \DHG\ContactosBundle\Entity\Email $email
     */
    public function removeEmail(\DHG\ContactosBundle\Entity\Email $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Set usuariorelacionado
     *
     * @param \DHG\UserBundle\Entity\User $usuariorelacionado
     * @return Contacto
     */
    public function setUsuariorelacionado(\DHG\UserBundle\Entity\User $usuariorelacionado = null)
    {
        $this->usuariorelacionado = $usuariorelacionado;

        return $this;
    }

    /**
     * Get usuariorelacionado
     *
     * @return \DHG\UserBundle\Entity\User 
     */
    public function getUsuariorelacionado()
    {
        return $this->usuariorelacionado;
    }

    /**
    *
    *@return boolean
    */
    public function getEsPersonal(){
        return $this->esPersonal;
    }

    public function setEsPersonal($b){
        $this->esPersonal=$b;
    }
        /**
    *
    *@return boolean
    */
    public function getEsMecanico(){
        return $this->esMecanico;
    }

    public function setEsMecanico($b){
        $this->esMecanico=$b;
    }
        /**
    *
    *@return boolean
    */
    public function getEsCliente(){
        return $this->esCliente;
    }

    public function setEsCliente($b){
        $this->esCliente=$b;
    }
        /**
    *
    *@return boolean
    */
    public function getEsProveedor(){
        return $this->esProveedor;
    }

    public function setEsProveedor($b){
        $this->esProveedor=$b;
    }

}