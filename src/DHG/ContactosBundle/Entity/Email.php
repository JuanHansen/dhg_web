<?php
namespace DHG\ContactosBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Email")
 */
class Email extends MappedSuperclassBase
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $direccion;


    public function __construct(){
        parent::__construct();

    }

    public function __toString(){
        return $this->direccion;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Email
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }







}