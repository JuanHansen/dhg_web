<?php
namespace DHG\ContactosBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Direccion")
 */
class Direccion extends MappedSuperclassBase
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $calle;


    public function __construct(){
        parent::__construct();

    }

    public function __toString(){
        return $this->calle;
    }
    /**
     * Set calle
     *
     * @param string $calle
     * @return Direccion
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->calle;
    }





}