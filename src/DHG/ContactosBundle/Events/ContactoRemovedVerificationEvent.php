<?php

namespace DHG\ContactosBundle\Events;

use DHG\ContactosBundle\Entity\Contacto;
use Symfony\Component\EventDispatcher\Event;

class ContactoRemovedVerificationEvent extends Event{
    private $contacto;
    private $stoped; 
    private $messages;

    /**
     * @param Contacto $contacto 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Contacto $contacto){
        $this->contacto = $contacto;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return contacto
     */
    public function getContacto(){
        return $this->contacto;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return contacto
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}