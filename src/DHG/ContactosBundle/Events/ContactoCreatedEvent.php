<?php

namespace DHG\ContactosBundle\Events;

use DHG\ContactosBundle\Entity\Contacto;
use Symfony\Component\EventDispatcher\Event;

class ContactoCreatedEvent extends Event{
    private $contacto;

    /**
     * @param Contacto $contacto 
     *      contacto created
     */
    public function __construct(Contacto $contacto){
        $this->contacto = $contacto;
    }

    /**
     * @return Contacto
     */
    public function getContacto(){
        return $this->contacto;
    }
}