<?php
namespace DHG\ContactosBundle\Events;

final class ContactoEvents {

    /**
     * @var string
     */
    const CONTACTO_CREATED = 'dhg_contactos.contactoCreated';

    /**
     * @var string
     */
    const CONTACTO_EDITED = 'dhg_contactos.contactoEdited';

    /**
     * @var string
     */
    const CONTACTO_REMOVED = 'dhg_contactos.contactoRemoved';

    /**
     * @var string
     */
    const CONTACTO_REMOVED_VERIFICATION = 'dhg_contactos.contactoRemovedVerification';
    
    /**
     * @var string
     */
    const CONTACTO_CREATED_VERIFICATION = 'dhg_contactos.contactoCreatedVerification';


    /**
     * @var string
     */
    const GRUPO_CREATED = 'dhg_contactos.grupoCreated';

    /**
     * @var string
     */
    const GRUPO_EDITED = 'dhg_contactos.grupoEdited';

    /**
     * @var string
     */
    const GRUPO_REMOVED = 'dhg_contactos.grupoRemoved';

    /**
     * @var string
     */
    const GRUPO_REMOVED_VERIFICATION = 'dhg_contactos.grupoRemovedVerification';


}
