<?php

namespace DHG\ContactosBundle\Events;

use DHG\ContactosBundle\Entity\Grupo;
use Symfony\Component\EventDispatcher\Event;

class GrupoCreatedEvent extends Event{
    private $grupo;

    /**
     * @param Grupo $grupo 
     *      grupo created
     */
    public function __construct(Grupo $grupo){
        $this->grupo = $grupo;
    }

    /**
     * @return Grupo
     */
    public function getGrupo(){
        return $this->grupo;
    }
}