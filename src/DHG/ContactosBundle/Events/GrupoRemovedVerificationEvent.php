<?php

namespace DHG\ContactosBundle\Events;

use DHG\ContactosBundle\Entity\Grupo;
use Symfony\Component\EventDispatcher\Event;

class GrupoRemovedVerificationEvent extends Event{
    private $grupo;
    private $stoped; 
    private $messages;

    /**
     * @param Grupo $grupo 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Grupo $grupo){
        $this->grupo = $grupo;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return grupo
     */
    public function getGrupo(){
        return $this->grupo;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return grupo
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}