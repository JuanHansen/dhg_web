<?php
namespace DHG\ContactosBundle\Security;

use DHG\UserBundle\Components\DHGPermissionVoter;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Components\DHGPermissionInterface;

class ContactosPermissions extends DHGPermissionVoter
{
    const PREFIX = 'contacto';
    const VERCONTACTOS = 'contacto_ver_contacto';
    const GESTIONARCONTACTOS = 'contacto_gestionar_contacto';

    const VERGRUPOSCONTACTOS = 'contacto_ver_grupoContacto';
    const GESTIONARGRUPOSCONTACTOS = 'contacto_gestionar_grupoContacto';

    const GESTIONARPERSONAL = 'contacto_gestionar_personal';
    
    const VERPERSONALCLASIFICACION = 'contacto_ver_personalClasif';
    const GESTIONPERSONALCLASIFICACION = 'contacto_gestionar_personalClasif';

    const GESTIONARCLIPRO = 'contacto_gestionar_clipro';

    const GESTIONARMECANICOS = 'maquinaria_gestionar_mecanicos';
    /**
     * {@inheritdoc}
     */
    public function supportsAttribute($attribute)
    {
        return 0 === strpos($attribute, PREFIX);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedClasses(){
        $supportedClasses = array();
        $supportedClasses[] = 'DHG\ContactosBundle\Entity\Contacto';
        $supportedClasses[] = 'DHG\ContactosBundle\Entity\Grupo';

        $supportedClasses[] = 'DHG\PersonalBundle\Entity\Personal';
        $supportedClasses[] = 'DHG\PersonalBundle\Entity\PersonalClasification';

        $supportedClasses[] = 'DHG\InventarioBundle\Entity\CliPro';

        $supportedClasses[] = 'DHG\MaquinariaBundle\Entity\Mecanico';

        return $supportedClasses;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissions(ConfigurePermissions $event){
        $perm = $event->getPermissions();

        $contactoClasif = new PermissionClasification();
        $contactoClasif->setMachinename('contacto');
        $contactoClasif->setName('Gestion de contacto');
        $contactoClasif->setDetails('Gestion de la seccion de contacto');
        
        $permissions = array();
        $permissions[ContactosPermissions::VERCONTACTOS] = new Permission();
        $permissions[ContactosPermissions::VERCONTACTOS]->setMachinename(ContactosPermissions::VERCONTACTOS);
        $permissions[ContactosPermissions::VERCONTACTOS]->setName('Ver contacto');
        $permissions[ContactosPermissions::VERCONTACTOS]->setDetails('Permite a los usuarios ver informacion del contacto');
        $permissions[ContactosPermissions::VERCONTACTOS]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::VERCONTACTOS]->setClasification($contactoClasif);

        $permissions[ContactosPermissions::GESTIONARCONTACTOS] = new Permission();
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setMachinename(ContactosPermissions::GESTIONARCONTACTOS);
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setName('Gestionar contacto');
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setDetails('Permite a los usuarios crear, modificar y eliminar contacto');
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setDepends($permissions[ContactosPermissions::VERCONTACTOS]);
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONARCONTACTOS]->setClasification($contactoClasif);

        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS] = new Permission();
        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS]->setMachinename(ContactosPermissions::VERGRUPOSCONTACTOS);
        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS]->setName('Ver grupos de contacto');
        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS]->setDetails('Permite a los usuarios ver informacion de los grupos de contactos');
        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::VERGRUPOSCONTACTOS]->setClasification($contactoClasif);

        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS] = new Permission();
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setMachinename(ContactosPermissions::GESTIONARGRUPOSCONTACTOS);
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setName('Gestionar grupos de contacto');
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setDetails('Permite a los usuarios crear, modificar y eliminar los grupos de contactos');
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setDepends($permissions[ContactosPermissions::VERGRUPOSCONTACTOS]);
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONARGRUPOSCONTACTOS]->setClasification($contactoClasif);





        $permissions[ContactosPermissions::GESTIONARPERSONAL] = new Permission();
        $permissions[ContactosPermissions::GESTIONARPERSONAL]->setMachinename(ContactosPermissions::GESTIONARPERSONAL);
        $permissions[ContactosPermissions::GESTIONARPERSONAL]->setName('Gestionar personal');
        $permissions[ContactosPermissions::GESTIONARPERSONAL]->setDetails('Permite a los usuarios crear, modificar y eliminar personal');
        $permissions[ContactosPermissions::GESTIONARPERSONAL]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONARPERSONAL]->setClasification($contactoClasif);

        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION] = new Permission();
        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION]->setMachinename(ContactosPermissions::VERPERSONALCLASIFICACION);
        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION]->setName('Ver clasificacion de personal');
        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION]->setDetails('Permite a los usuarios ver informacion de la clasificacion de personal');
        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::VERPERSONALCLASIFICACION]->setClasification($contactoClasif);

        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION] = new Permission();
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setMachinename(ContactosPermissions::GESTIONPERSONALCLASIFICACION);
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setName('Gestionar clasificacion de personal');
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setDetails('Permite a los usuarios crear, modificar y eliminar la clasificacion de personal');
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setDepends($permissions[ContactosPermissions::VERPERSONALCLASIFICACION]);
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONPERSONALCLASIFICACION]->setClasification($contactoClasif);



        $permissions[ContactosPermissions::GESTIONARCLIPRO] = new Permission();
        $permissions[ContactosPermissions::GESTIONARCLIPRO]->setMachinename(ContactosPermissions::GESTIONARCLIPRO);
        $permissions[ContactosPermissions::GESTIONARCLIPRO]->setName('Gestionar clientes/proveedores');
        $permissions[ContactosPermissions::GESTIONARCLIPRO]->setDetails('Permite a los usuarios crear, editar y eliminar contactos clientes o proveedores');
        $permissions[ContactosPermissions::GESTIONARCLIPRO]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONARCLIPRO]->setClasification($contactoClasif);




        $permissions[ContactosPermissions::GESTIONARMECANICOS] = new Permission();
        $permissions[ContactosPermissions::GESTIONARMECANICOS]->setMachinename(ContactosPermissions::GESTIONARMECANICOS);
        $permissions[ContactosPermissions::GESTIONARMECANICOS]->setName('Gestionar mecanicos');
        $permissions[ContactosPermissions::GESTIONARMECANICOS]->setDetails('Permite a los usuarios crear, editar y eliminar mecanicos');
        $permissions[ContactosPermissions::GESTIONARMECANICOS]->setModule(ContactosPermissions::PREFIX);
        $permissions[ContactosPermissions::GESTIONARMECANICOS]->setClasification($contactoClasif);






        $perm['permissions'] = array_merge($perm['permissions'], $permissions);

        $perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO], array(ContactosPermissions::VERCONTACTOS,ContactosPermissions::VERGRUPOSCONTACTOS  ));
        $perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN], array(ContactosPermissions::VERCONTACTOS,ContactosPermissions::VERGRUPOSCONTACTOS,ContactosPermissions::GESTIONARCONTACTOS,ContactosPermissions::GESTIONARGRUPOSCONTACTOS));


        $perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO], array(ContactosPermissions::VERPERSONALCLASIFICACION  ));
        $perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN], array(ContactosPermissions::VERPERSONALCLASIFICACION,ContactosPermissions::GESTIONARPERSONAL,ContactosPermissions::GESTIONPERSONALCLASIFICACION,ContactosPermissions::GESTIONARMECANICOS,ContactosPermissions::GESTIONARCLIPRO));



        $event->setPermissions($perm);
    }


}