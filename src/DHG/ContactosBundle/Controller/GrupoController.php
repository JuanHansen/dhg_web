<?php

namespace DHG\ContactosBundle\Controller;

use DHG\coreBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use DHG\ContactosBundle\Entity\Grupo;
use DHG\ContactosBundle\Form\FormGrupo;
use DHG\ContactosBundle\Model\GrupoTable;
use DHG\ContactosBundle\EntityManager\GrupoManager;
use DHG\ContactosBundle\EventsListener\MenuEventsListener;
use DHG\ContactosBundle\Security\ContactosPermissions;


class GrupoController extends DefaultController {
    /**
     * @Template("DHGcoreBundle:Default:tablelist.html.twig")
     */
    public function listAction(Request $request){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        return $this->defListAction($request);
 
    }

    /**
    * Edita un elemento
    * 
    * @param Id ID del elemento
    *
    */
    public function editAction($id){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        return $this->defEditAction($id);
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        return $this->defRemoveAction($id);
    }

    /**
    * Crea un grupo. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las grupos
    * @return Formulario de creacion (200) si no hay parametros (POST) o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la grupo con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAjaxAction(){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        return $this->defCreateAjaxAction();
    }

    /**
    * Retorna una lista de grupo
    * @return array de grupo
    */
    private function getList(){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        return $this->defGetList();
    }

    public function iniciarVariables(){
        $this->namespace    = "DHG\ContactosBundle";
        $this->name         = "Grupo";
        $this->route         = "dhg_contactos_";
    }
}
