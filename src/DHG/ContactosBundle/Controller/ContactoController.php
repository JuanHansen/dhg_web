<?php

namespace DHG\ContactosBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DHG\ContactosBundle\Entity\Contacto;
use DHG\ContactosBundle\Form\FormContacto;
use DHG\ContactosBundle\Model\ContactoTable;
use DHG\ContactosBundle\EntityManager\ContactoManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\ContactosBundle\EventsListener\MenuEventsListener;
use DHG\ContactosBundle\Security\ContactosPermissions;
use DHG\MaquinariaBundle\Security\MaquinariaPermissions;
use DHG\InventarioBundle\Security\InventarioPermissions;
use DHG\PersonalBundle\Security\PersonalPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

class ContactoController extends Controller {

    /**
     * Presenta el listado de clasificacion de contactos
     *
     * @Template("DHGContactosBundle:Default:contactoList.html.twig")
     */
    public function listAction(Request $request){
        $contacto = new Contacto();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::VERCONTACTOS, $contacto)) {
            throw new AccessDeniedException('No tiene acceso para acceder al listado de contactos!');
        }
        $rutas_ajax = array('grupo_create'=>$this->generateUrl('dhg_contactos_grupoCreate'),
                            'personalClasification_create'=>$this->generateUrl('dhg_personal_personalClasificationCreate'));
        $formType = new FormContacto($this->container->get('event_dispatcher'),"Nuevo_contacto",null,null,$rutas_ajax);
        $form = $this->get('form.factory')->create($formType , $contacto);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $um = new ContactoManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = [];
                if ($um->createVerificationContacto($contacto, $error)){
                  if ($um->createMultiContacto($contacto,$form, $error,$this->get('security.context'))){
                      $this->get('session')->getFlashBag()->add('success',sprintf('Contacto "%s" creado exitosamente!', $contacto->getNombre() ));
                      foreach($error as $key => $val){
                        $this->get('session')->getFlashBag()->add('warning', sprintf('%s', $val) );
                      }
                  }else{
                        foreach($error as $key => $val){
                            $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                        }
                  }
                  }else{
                  }
                return $this->redirect($this->generateUrl('dhg_contactos_contactoList'));
            }else{
                $showFormAtInit = true;
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('ContactosTable');
        $metaData = $dataTable->getMetaData(); 
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        $result = array('dataTable' => $dataTable);
        if ($this->get('security.context')->isGranted(ContactosPermissions::GESTIONARCONTACTOS, $contacto)) {
        //Agrego menu
        $listener = new MenuEventsListener($this->container->get('event_dispatcher'),"Contacto");
        $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateItems'), -99);
        $formActivo = array('personal'=>false,'cliente'=>true,'proveedor'=>false,'mecanico'=>false);

            $result = array_merge($result, array(
                      'dataTable' => $dataTable,
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl('dhg_contactos_contactoList'),
                      'form_title' => 'Nuevo contacto',
                      'form_submit_value' => 'Crear',
                      'form_class' => 'form-horizontal addContacto',
                      'form_show_at_init' => $showFormAtInit,
                      'form_edditing'=>"_create",
                      'formActivo' => $formActivo,
                      'dataTable_twigVars' => array(
                          'modalConfirmID' => $metaData['table']->id,
                          'modalConfirmTitle' => 'Atencion!',
                          'modalConfirmText' => 'Se procedera a eliminar el contacto seleccionado. Esta operacion no puede deshacerse.',
                          'modalConfirmSubmitValue' => 'Eliminar',
                          'modalConfirmUrlAction' => $this->generateUrl('dhg_contactos_contactoRemove',  array('id' => '#')),
                          'modalEditID' => 'edit'.$metaData['table']->id,
                          'modalViewID' => 'view'.$metaData['table']->id,
                       ),
                  )
               );  
            }else{
              $result = array_merge($result, array('dataTable_twigVars' => array('modalViewID' => 'view'.$metaData['table']->id)));
            }      
          return $result;
    }


    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $contacto = new Contacto();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONARCONTACTOS, $contacto)) {
              throw new AccessDeniedException('No tiene acceso para editar contactos!');
        }
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGContactosBundle:Contacto')->find($id);
        if ($contacto){
        $rutas_ajax = array('grupo_create'=>$this->generateUrl('dhg_contactos_grupoCreate'),
                            'personalClasification_create'=>$this->generateUrl('dhg_personal_personalClasificationCreate'));
          $form = $this->get('form.factory')->create( $formType = new FormContacto($this->container->get('event_dispatcher'),'Editar_contacto',$em,$contacto,$rutas_ajax), $contacto);
          if ($request->getMethod() == 'POST'){
              $form->handleRequest($request);
              if ($form->isValid()){
                  $um = new ContactoManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                  $error = [];
                  if ($um->createVerificationContacto($contacto, $error)){
                    if ($um->editContacto($contacto,$form, $error,$this->get('security.context'))){
                        $this->get('session')->getFlashBag()->add('success',sprintf('Contacto "%s" editado exitosamente!', $contacto->getNombre() ));
                        foreach($error as $key => $val){
                            $this->get('session')->getFlashBag()->add('warning', sprintf('%s', $val));
                        }
                    }else{
                        return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                    }
                  }else{
                    foreach($error as $key => $val){
                      $this->get('session')->getFlashBag()->add('danger', sprintf('%s', implode($val)));
                    }
                  }
                  return new Response("",Response::HTTP_OK);
              }
          }
          $formActivo = array('personal'=>false,'cliente'=>true,'proveedor'=>false,'mecanico'=>false);
          return new Response($this->renderView('DHGContactosBundle:Form:ContactoForm.html.twig',
              array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_contactos_contactoEdit'),
                    'form_title' => 'Editar contacto '.$contacto->getNombre(),
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editContacto',
                    'form_edditing'=>"_edit",
                    'formActivo' => $formActivo
                )
              ),Response::HTTP_CREATED);     
        }else{
            return new Response("No existe el Contacto",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $contacto = new Contacto();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONARCONTACTOS, $contacto)){
            throw new AccessDeniedException('No tiene acceso para eliminar el contacto!');
        }
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGContactosBundle:Contacto')->find($id);
        if (!$contacto) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('El contacto que desea eliminar no existe') );
        }else{
            $name = $contacto->getNombre();
            $um = new ContactoManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeContacto($contacto, $error,$this->get('security.context'))){
                $this->get('session')->getFlashBag()->add('success',sprintf('Contacto "%s" eliminado exitosamente!', $name));
                foreach($error as $key => $val){
                    $this->get('session')->getFlashBag()->add('warning', sprintf('%s', implode($val)) );
                }
            }else{
                foreach($error as $key => $val){
                    $this->get('session')->getFlashBag()->add('danger', sprintf('%s', implode($val)) );
                }
            }          
        }
        return $this->redirect($this->generateUrl('dhg_contactos_contactoList'));
    }





    /**
     * Muestra un elemento
     * 
     * @param Id ID del elemento
     * @return array
     */
    public function viewAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGContactosBundle:Contacto')->find($id);
        $filtro->setParameter('eliminado',0);
        $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
        $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
        $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));
        if ($contacto){         
          $request->attributes->add(array('id'=>$id)); 
          return $this->render('DHGContactosBundle:Modal:contacto.html.twig',
              array(
                    'modalViewTitle' => 'Contactos',
                    'content' => $contacto,
                    'personal' =>$personal,
                    'mecanico' =>$mecanico,
                    'clipro' =>$clipro,
                )
          );  
        }else{
           return $this->render('DHGcoreBundle:Modal:modalError.html.twig',array('msg'=> "No existe el contacto"));
        };
    }

    /**
     * Muestra un elemento
     * 
     * @param Id ID del elemento
     * @return array
     */
    public function viewPersonalAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGPersonalBundle:Personal')->find($id);
        return $this->viewAction($contacto->getContacto()->getId());
    }
    /**
     * Muestra un elemento
     * 
     * @param Id ID del elemento
     * @return array
     */
    public function viewMecanicoAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGMaquinariaBundle:Mecanico')->find($id);
        return $this->viewAction($contacto->getContacto()->getId());
    }
    /**
     * Muestra un elemento
     * 
     * @param Id ID del elemento
     * @return array
     */
    public function viewCliProAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = $em->getRepository('DHGInventarioBundle:CliPro')->find($id);
        return $this->viewAction($contacto->getContacto()->getId());
    }


    /**
     * Retorna la informacion del personal
     * 
     * @param Id ID del elemento
     */
    public function getInfoPersonalAjaxAction($id){
        $personal =  $this->getDoctrine()->getManager()->getRepository('DHGPersonalBundle:Personal')->find($id);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if($personal==null){
          $response->setContent(json_encode(
            ["horas"=>0,"horasextra"=>0,"horaskg"=>0,"horasha"=>0]
          ));
          return $response;
        }

        $response->setContent(json_encode(
          ["horas"=>$personal->getPriceHour(),"horasextra"=>$personal->getPriceHourExtra(),"horaskg"=>$personal->getPriceKg(),"horasha"=>$personal->getPriceHa(),]
        ));

        return $response;
    }

    /**
    * Crea un contacto. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las contactos
    * @param $esPersonal true para que el contacto sea personal
    * @param $esMecanico true para que el contacto sea mecanico
    * @param $esCliente true para que el contacto sea cliente
    * @param $esProveedor true para que el contacto sea proveedor
    * @return Formulario de creacion (200) concordando con los parametros especificados si no hay parametros o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la contacto con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAxajAction($esPersonal=false, $esMecanico=false, $esCliente=false,  $esProveedor=false){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $contacto = new Contacto();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONARCONTACTOS, $contacto)) {
            throw new AccessDeniedException('No tiene acceso para crear contactos!');
        }

        $rutas_ajax = array('grupo_create'=>$this->generateUrl('dhg_contactos_grupoCreate'),
                            'personalClasification_create'=>$this->generateUrl('dhg_personal_personalClasificationCreate'));
        $formType = new FormContacto($this->container->get('event_dispatcher'),"Nuevo_contacto",null,null,$rutas_ajax);
        $formType->setChecked($esPersonal,$esMecanico,$esCliente,$esProveedor,true);
        $form = $this->get('form.factory')->create($formType , $contacto);
        $toR = null;
        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()){
                $um = new ContactoManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = null;
                if ($um->createVerificationContacto($contacto, $error)&&$um->createMultiContacto($contacto,$form, $error,$this->get('security.context'))){
                    $toR = $this->getList($esPersonal,$esMecanico,$esCliente,$esProveedor);
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturn.html.twig',
                        array(
                            'listado' => $toR,
                            'selected'=>$contacto
                            )
                        ),Response::HTTP_CREATED);
                    return $response;
                }else{
                    $toR=$error; 
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturnError.html.twig',
                        array(
                            'error' => $toR,
                            )
                        ),Response::HTTP_ACCEPTED);
                    return $response;               
                }
            }else{
            }
        }
        $returRoute = $this->generateUrl('dhg_contacto_contactoCreate');
        if($esProveedor)
            $returRoute = $this->generateUrl('dhg_contacto_contactoProveedorCreate');
        if($esCliente)
            $returRoute = $this->generateUrl('dhg_contacto_contactoClienteCreate');
        if($esMecanico)
            $returRoute = $this->generateUrl('dhg_contacto_contactoMecanicoCreate');
        if($esPersonal)
            $returRoute = $this->generateUrl('dhg_contacto_contactoPersonalCreate');
        if($esProveedor&&$esCliente)
            $returRoute = $this->generateUrl('dhg_contacto_contactoCliProCreate');

        $formActivo = array(
            'personal'=>$esPersonal,
            'mecanico'=>$esMecanico,
            'cliente'=>$esCliente,
            'proveedor'=>$esProveedor
        );
        return $this->render('DHGContactosBundle:Form:ContactoForm.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => $returRoute,
                'form_title' => 'Crear Contacto',
                'form_submit_value' => 'Crear',
                'form_class' => 'form-horizontal',
                'form_edditing'=>"_create",
                'formActivo' => $formActivo
                )
            ); 
    }

            /**
    * Crea un contacto. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las contactos
    * @return Formulario de creacion (200) concordando con los parametros especificados si no hay parametros o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la contacto con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createProveedorAxajAction(){
        return $this->createAxajAction(false,false,false,true);
    }
    public function createClienteAxajAction(){
        return $this->createAxajAction(false,false,true,false);
    }
    public function createMecanicoAxajAction(){
        return $this->createAxajAction(false,true,false,false);
    }
    public function createPersonalAxajAction(){
        return $this->createAxajAction(true,false,false,false);
    }
    public function createCliProAxajAction(){
        return $this->createAxajAction(false,false,true,true);
    }
    /**
    * Retorna una lista de contacto
    * @param $esPersonal true para que el retorne los contactos personal
    * @param $esMecanico true para que el retorne los contactos mecanico
    * @param $esCliente true para que el retorne los contactos cliente
    * @param $esProveedor true para que el retorne los contactos proveedor
    * @return array de contacto
    */
    private function getList($esPersonal=false, $esMecanico=false, $esCliente=false,  $esProveedor=false){
        $em = $this->getDoctrine()->getManager();
        $list = array();
        if($esPersonal){
            $repo = $em->getRepository('DHGPersonalBundle:Personal');
            $query = $repo->createQueryBuilder('u')->getQuery();
            foreach($query->getResult() as $plu){
                $list[$plu->getId()]=$plu;
            }
        }
        if($esMecanico){
            $repo = $em->getRepository('DHGMaquinariaBundle:Mecanico');
            $query = $repo->createQueryBuilder('u')->getQuery();
            foreach($query->getResult() as $plu){
                $list[$plu->getId()]=$plu;
            }
        }
        if($esCliente&&!$esProveedor){
            $repo = $em->getRepository('DHGInventarioBundle:CliPro');
            $query = $repo->createQueryBuilder('u')->getQuery();
            foreach($query->getResult() as $plu){
                if($plu->getCliente())
                    $list[$plu->getId()]=$plu;
            }
        }
        if(!$esCliente&&$esProveedor){
            $repo = $em->getRepository('DHGInventarioBundle:CliPro');
            $query = $repo->createQueryBuilder('u')->getQuery();
            foreach($query->getResult() as $plu){
                if($plu->getProveedor())
                    $list[$plu->getId()]=$plu;
            }
        }
        if($esCliente&&$esProveedor){
            $repo = $em->getRepository('DHGInventarioBundle:CliPro');
            $query = $repo->createQueryBuilder('u')->getQuery();
            $list['Clientes']=array();
            $list['Proveedores']=array();
            foreach($query->getResult() as $plu){
                if($plu->getCliente())
                    $list['Clientes'][$plu->getId()]=$plu;
                if($plu->getProveedor())
                    $list['Proveedores'][$plu->getId()]=$plu;
            }
        }

        return $list;
    }
}
