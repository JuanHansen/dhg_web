<?php

namespace DHG\MaquinariaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DHG\MaquinariaBundle\Entity\Maquinaria;
use DHG\MaquinariaBundle\Form\FormMaquinaria;
use DHG\MaquinariaBundle\Model\MaquinariaTable;
use DHG\MaquinariaBundle\EntityManager\MaquinariaManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\MaquinariaBundle\EventsListener\MenuEventsListener;
use DHG\MaquinariaBundle\Security\MaquinariaPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;


class MaquinariaController extends Controller {

    /**
     * Presenta el listado de clasificacion de maquinarias
     *
     * @Template("DHGMaquinariaBundle:Default:maquinariaList.html.twig")
     */
    public function listAction(Request $request){
        $maquinaria = new Maquinaria();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::VERMAQUINAS, $maquinaria)) {
            throw new AccessDeniedException('No tiene acceso para ver el listado de Maquinarias!');
        }
        $em = $this->getDoctrine()->getManager();
        $rutas_ajax = array('maquinariaClasif_create'=>$this->generateUrl('dhg_maquinaria_maquinariaClasifCreate'),
                            'producto_create'=>$this->generateUrl('dhg_inventario_productoCreate'));
        $form = $this->get('form.factory')->create( $formType = new FormMaquinaria("Nueva_Maquinaria",$rutas_ajax), $maquinaria);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $cuenta = $this->get('security.context')->getToken()->getAccount();
                $um = new MaquinariaManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'),$cuenta);
                $error = [];
                if ($um->createMaquinaria($maquinaria, $error)){
                    $this->get('session')->getFlashBag()->add('success',sprintf('Maquinaria "%s" creado exitosamente!', $maquinaria->getNombre() ));
                }else{
                    foreach ($error as $key => $value) {
                        $this->get('session')->getFlashBag()->add('danger', $value);
                    }
                }
                return $this->redirect($this->generateUrl('dhg_maquinaria_maquinariaList'));
            }else{
                $showFormAtInit = true;
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('MaquinariaTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        $result = array('dataTable' => $dataTable);
         
        if ($this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinaria)) {
            //Agrego menu
            $listener = new MenuEventsListener($this->container->get('event_dispatcher'),"Maquinaria");
            $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateMaquinariaItems'), -99);
            $result = array_merge($result, array(
                      'dataTable' => $dataTable,
                      'modularForm' => $form->createView(),
                      'formType' => $formType,            
                      'form_action' => $this->generateUrl('dhg_maquinaria_maquinariaList'),
                      'form_submit_value' => 'Crear',
                      'form_class' => 'form-horizontal addMaquinaria',
                      'form_show_at_init' => $showFormAtInit,
                      'dataTable_twigVars' => array(
                          'modalConfirmID' => $metaData['table']->id,
                          'modalConfirmTitle' => 'Atencion!',
                          'noFilteredCols' => [0],
                          'modalConfirmText' => 'Se procedera a eliminar el maquinaria seleccionada. Esta operacion no puede deshacerse.',
                          'modalConfirmSubmitValue' => 'Eliminar',
                          'modalConfirmUrlAction' => $this->generateUrl('dhg_maquinaria_maquinariaRemove',  array('id' => '#')),
                          'modalEditID' => 'edit'.$metaData['table']->id,
                          'modalViewID' => 'view'.$metaData['table']->id,
                       ),
                  )
               );    
        }else{
            $result = array_merge($result, array('dataTable_twigVars' => array(
              'modalViewID' => 'view'.$metaData['table']->id),
              'noFilteredCols' => [0],
          ));
        } 

      return $result;    
    }


    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $maquinaria = new Maquinaria();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinaria)) {
              throw new AccessDeniedException('No tiene acceso para editar una maquinaria!');
        }
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $maquinaria = $em->getRepository('DHGMaquinariaBundle:Maquinaria')->find($id);
        if ($maquinaria){
            $rutas_ajax = array('maquinariaClasif_create'=>$this->generateUrl('dhg_maquinaria_maquinariaClasifCreate'),
                                'producto_create'=>$this->generateUrl('dhg_inventario_productoCreate'));
            $form = $this->get('form.factory')->create( $formType = new FormMaquinaria('Editar_maquinaria',$rutas_ajax), $maquinaria);
            if ($request->getMethod() == 'POST'){
                $form->handleRequest($request);
                if ($form->isValid()){
                    $cuenta = $this->get('security.context')->getToken()->getAccount();
                    $um = new MaquinariaManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'),$cuenta);
                    $error = [];
                    if ($um->editMaquinaria($maquinaria, $error)){
                        $this->get('session')->getFlashBag()->add('success',sprintf('Maquinaria "%s" editado exitosamente!', $maquinaria->getNombre() ));
                    }else{
                        return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                    }
                    return new Response("",Response::HTTP_OK);
                }
            }
          
          return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
              array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_maquinaria_maquinariaEdit'),
                    'form_title' => 'Editar maquinaria '.$maquinaria->getNombre(),
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editMaquinaria',
                )
          ),Response::HTTP_CREATED);     
        }else{
            return new Response("No existe Maquinaria",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $maquinaria = new Maquinaria();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinaria)) {
              throw new AccessDeniedException('No tiene acceso para eliminar una maquinaria!');
        }
        $em = $this->getDoctrine()->getManager();
        $maquinaria = $em->getRepository('DHGMaquinariaBundle:Maquinaria')->find($id);
        if (!$maquinaria) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al eliminar el maquinaria "%s". No existe. ', $id) );
        }else{
            $name = $maquinaria->getNombre() ;
            $um = new MaquinariaManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeVerificationMaquinaria($maquinaria, $error)){
              if ($um->removeMaquinaria($maquinaria, $error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('Maquinaria "%s" eliminado exitosamente!', $name));
              }else{
                  foreach ($error as $key => $value) {
                      $this->get('session')->getFlashBag()->add('danger', $value);
                  }
              }
            }else{
                foreach($error as $key => $val){
                   $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                }
            }
        }
        return $this->redirect($this->generateUrl('dhg_maquinaria_maquinariaList'));
    }


    /**
     * Muestra un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function viewAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $maquinaria = $em->getRepository('DHGMaquinariaBundle:Maquinaria')->find($id);
        if ($maquinaria){          
          return $this->render('DHGMaquinariaBundle:Modal:maquinaria.html.twig',
              array(
                    'modalViewTitle' => 'Maquinaria',
                    'content' => $maquinaria,
                )
          );     
        }else{
           return $this->render('DHGcoreBundle:Modal:modalError.html.twig',array('msg'=> "No existe "));
        };
    }

    /**
    * Crea una maquinaria. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las maquinarias
    * @return Formulario de creacion (200) si no hay parametros o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la maquinaria con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAxajAction($conMotor=false){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $maquinaria = new Maquinaria();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinaria)) {
            throw new AccessDeniedException('No tiene acceso para editar maquinarias!');
        }
        $maquinaria->setConMotor($conMotor);
        $rutas_ajax = array('maquinariaClasif_create'=>$this->generateUrl('dhg_maquinaria_maquinariaClasifCreate'),
                            'producto_create'=>$this->generateUrl('dhg_inventario_productoCreate'));
        $form = $this->get('form.factory')->create( $formType = new FormMaquinaria("Nueva_Maquinaria",$rutas_ajax), $maquinaria);
        $toR = null;
        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()){
                $um = new MaquinariaManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = null;
                if ($um->createMaquinaria($maquinaria, $error)){
                    $toR = $this->getList($conMotor);
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturn.html.twig',
                        array(
                            'listado' => $toR,
                            'selected'=>$maquinaria
                            )
                        ),Response::HTTP_CREATED);
                    return $response;
                }else{
                    $toR=$error; 
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturnError.html.twig',
                        array(
                            'error' => $toR,
                            )
                        ),Response::HTTP_ACCEPTED);
                    return $response;               
                }
            }else{
            }
        }
        return $this->render('DHGcoreBundle:Form:modularForms.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => ($conMotor?$this->generateUrl('dhg_maquinaria_maquinariaMotorCreate'):$this->generateUrl('dhg_maquinaria_maquinariaCreate')),
                'form_title' => 'Crear Maquinaria',
                'form_submit_value' => 'Crear',
                'form_class' => 'form-horizontal',
                )
            ); 
    }
    /**
    * Crea una maquinaria con motor. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las maquinarias
    * @return Formulario de creacion (200) si no hay parametros o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la maquinaria con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createMotorAxajAction(){
        return $this->createAxajAction(true);
    }
    /**
    * Retorna una lista de maquinarias agrupadas por clasificacion
    * @return array de maquinarias agrupadas por clasificacion
    */
    private function getList($conMotor=false){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('DHGMaquinariaBundle:Maquinaria');
        $list = array();
        if($conMotor){
            $query = $repo->createQueryBuilder('u')
                    ->where('u.con_motor = :m')
                    ->setParameter('m',true)
                    ->orderBy('u.nombre', 'ASC')->getQuery();
        }
        else{
            $query = $repo->createQueryBuilder('u')->orderBy('u.nombre', 'ASC')->getQuery();
        }
        foreach($query->getResult() as $plu){
            if ($plu->getClasificacion() ){
                if (empty( $list[$plu->getClasificacion()->__toString()])){
                     $list[$plu->getClasificacion()->getNombre()] = array();
                }
                $list[$plu->getClasificacion()->getNombre()][$plu->getId()] =$plu->getNombre();
            }else{
                if (empty( $list['Sin Clasificacion?'])){
                     $list['Sin Clasificacion?'] = array();
                }
                $list['Sin Clasificacion?'][$plu->getId()] = $plu->getNombre();
            }
        }
        return $list;
    }
}
