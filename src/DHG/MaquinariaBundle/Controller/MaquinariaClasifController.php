<?php

namespace DHG\MaquinariaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use DHG\MaquinariaBundle\Form\FormMaquinariaClasif;
use DHG\MaquinariaBundle\Model\MaquinariaClasifTable;
use DHG\MaquinariaBundle\EntityManager\MaquinariaClasifManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\MaquinariaBundle\EventsListener\MenuEventsListener;
use DHG\MaquinariaBundle\Security\MaquinariaPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

class MaquinariaClasifController extends Controller {

    /**
     * Presenta el listado de clasificacion de maquinariaClasifs
     *
     * @Template("DHGMaquinariaBundle:Default:maquinariaClasifList.html.twig")
     */
    public function listAction(Request $request){
        $maquinariaClasif = new MaquinariaClasif();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::VERMAQUINAS, $maquinariaClasif)) {
            throw new AccessDeniedException('No tiene acceso para ver el listado de Clasificacion de Maquinarias!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormMaquinariaClasif(), $maquinariaClasif);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $um = new MaquinariaClasifManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = [];
                if ($um->createMaquinariaClasif($maquinariaClasif, $error)){
                     $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion de maquinaria "%s" creado exitosamente!', $maquinariaClasif->getNombre() ));
                }else{
                    foreach ($error as $key => $value) {
                        $this->get('session')->getFlashBag()->add('danger', $value);
                    }
                }
                return $this->redirect($this->generateUrl('dhg_maquinaria_maquinariaClasiflList'));
            }else{
                $showFormAtInit = true;
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('MaquinariaClasifTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        $result = array('dataTable' => $dataTable);
         
        if ($this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinariaClasif)) {
            //Agrego menu
            $listener = new MenuEventsListener($this->container->get('event_dispatcher'),"Tipo de Maquinaria");
            $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateMaquinariaItems'), -99);

            $result = array_merge($result, array(
                      'dataTable' => $dataTable,
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl('dhg_maquinaria_maquinariaClasiflList'),
                      'form_submit_value' => 'Crear',
                      'form_class' => 'form-horizontal addMaquinariaClasif',
                      'form_show_at_init' => $showFormAtInit,
                      'dataTable_twigVars' => array(
                          'modalConfirmID' => $metaData['table']->id,
                          'modalConfirmTitle' => 'Atencion!',
                          'modalConfirmText' => 'Se procedera a eliminar la clasificacion seleccionada. Esta operacion no puede deshacerse.',
                          'modalConfirmSubmitValue' => 'Eliminar',
                          'modalConfirmUrlAction' => $this->generateUrl('dhg_maquinaria_maquinariaClasifRemove',  array('id' => '#')),
                          'modalEditID' => 'edit'.$metaData['table']->id,
                       ),
                  )
               );    
            }

            return $result;

    }


    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $maquinariaClasif = new MaquinariaClasif();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinariaClasif)) {
              throw new AccessDeniedException('No tiene acceso para editar una clasificacion de maquinaria!');
        }
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $maquinariaClasif = $em->getRepository('DHGMaquinariaBundle:MaquinariaClasif')->find($id);
        if ($maquinariaClasif){
          $form = $this->get('form.factory')->create( $formType = new FormMaquinariaClasif('Editar_maquinariaClasif'), $maquinariaClasif);
          if ($request->getMethod() == 'POST'){
              $form->handleRequest($request);
              if ($form->isValid()){
                  $um = new MaquinariaClasifManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                  $error = [];
                  if ($um->editMaquinariaClasif($maquinariaClasif, $error)){
                      $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion de maquinaria "%s" editada exitosamente!', $maquinariaClasif->getNombre() ));
                  }else{
                      return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                  }
                  return new Response("",Response::HTTP_OK);
              }
          }
          
          return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
              array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_maquinaria_maquinariaClasifEdit'),
                    'form_title' => 'Editar la clasificacion de maquinaria '.$maquinariaClasif->getNombre(),
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editMaquinariaClasif',
                )
          ),Response::HTTP_CREATED);   
        }else{
           return new Response("No existe la Clasificacion de Maquinarias",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $maquinariaClasif = new MaquinariaClasif();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinariaClasif)) {
              throw new AccessDeniedException('No tiene acceso para eliminar una clasificacion de maquinaria!');
        }
        $em = $this->getDoctrine()->getManager();
        $maquinariaClasif = $em->getRepository('DHGMaquinariaBundle:MaquinariaClasif')->find($id);
        if (!$maquinariaClasif) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al eliminar la clasificacion de memoria "%s". No existe. ', $id) );
        }else{
            $name = $maquinariaClasif->getNombre() ;
            $um = new MaquinariaClasifManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeVerificationMaquinariaClasif($maquinariaClasif, $error)){
              if ($um->removeMaquinariaClasif($maquinariaClasif, $error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion de maquinaria "%s" eliminada exitosamente!', $name));
              }else{
                  foreach ($error as $key => $value) {
                      $this->get('session')->getFlashBag()->add('danger', $value);
                  }
              }
            }else{
                foreach($error as $key => $val){
                   $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                }
            }
        }
        return $this->redirect($this->generateUrl('dhg_maquinaria_maquinariaClasiflList'));
    }




    /**
    * Crea una maquinariaClasif. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las maquinariaClasifs
    * @return Formulario de creacion (200) si no hay parametros o los parametros no validan
    *     Forbidden (403) si no se tiene acceso
    *      Options (201) Si se creo la maquinariaClasifs con exito (retorna el listado de entidades listo para poner en el select)
    *      Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *      Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAxajAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $maquinariaClasif = new MaquinariaClasif();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONARMAQUINAS, $maquinariaClasif)) {
            throw new AccessDeniedException('No tiene acceso para editar maquinariaClasifs!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormMaquinariaClasif("Nueva_Clasificacion_de_Maquinaria"), $maquinariaClasif);
        $toR = null;
        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()){
                $um = new MaquinariaClasifManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                $error = null;
                if ($um->createMaquinariaClasif($maquinariaClasif, $error)){
                    $toR = $this->getList();
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturn.html.twig',
                        array(
                            'listado' => $toR,
                            'selected'=>$maquinariaClasif
                            )
                        ),Response::HTTP_CREATED);
                    return $response;
                }else{
                    $toR=$error; 
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturnError.html.twig',
                        array(
                            'error' => $toR,
                            )
                        ),Response::HTTP_ACCEPTED);
                    return $response;               
                }
            }else{
            }
        }
        return $this->render('DHGcoreBundle:Form:modularForms.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => $this->generateUrl('dhg_maquinaria_maquinariaClasifCreate'),
                'form_title' => 'Crear Clasificacion de Maquinaria',
                'form_submit_value' => 'Crear',
                'form_class' => 'form-horizontal',
                )
            ); 
    }
    
    
    /**
    * Retorna una lista de maquinariaClasifs
    * @return array de maquinariaClasifs
    */
    private function getList(){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('DHGMaquinariaBundle:MaquinariaClasif');
        $list = array();
        $query = $repo->createQueryBuilder('u')->orderBy('u.nombre', 'ASC')->getQuery();

        foreach($query->getResult() as $plu){
            $list[$plu->getId()]=$plu;
        }
        return $list;
    }
}
