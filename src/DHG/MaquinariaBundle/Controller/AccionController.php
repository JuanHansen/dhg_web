<?php

namespace DHG\MaquinariaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DHG\MaquinariaBundle\Entity\Accion;
use DHG\MaquinariaBundle\Entity\Mantenimiento;
use DHG\MaquinariaBundle\Entity\Repostaje;
use DHG\MaquinariaBundle\Entity\OtroGasto;
use DHG\MaquinariaBundle\Form\FormMantenimiento;
use DHG\MaquinariaBundle\Form\FormRepostaje;
use DHG\MaquinariaBundle\Model\AccionsTable;
use DHG\MaquinariaBundle\EntityManager\AccionManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\MaquinariaBundle\EventsListener\MenuEventsListener;
use Symfony\Component\HttpFoundation\Response;
use DHG\MaquinariaBundle\Security\MaquinariaPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class AccionController extends Controller {

    /**
     * Presenta el listado de clasificacion de accions
     *
     * @Template("DHGMaquinariaBundle:Default:accionList.html.twig")
     */
    public function listAction(Request $request){
        $mantenimiento = new Mantenimiento();
        if (false === $this->get('security.context')->isGranted(MaquinariaPermissions::VEROPERACIONES, $mantenimiento)) {
            throw new AccessDeniedException('No tiene acceso para ver el listado de Operaciones!');
        }

        $em = $this->getDoctrine()->getManager();
        $rutas_ajax = array('maquinaria_create'=>$this->generateUrl('dhg_maquinaria_maquinariaCreate'),
                            'maquinaria_motor_create'=>$this->generateUrl('dhg_maquinaria_maquinariaMotorCreate'),
                            'combustible_create'=>$this->generateUrl('dhg_inventario_productoCombustibleCreate'),
                            'producto_create'=>$this->generateUrl('dhg_inventario_productoCreate'),
                            'proveedor_create'=>$this->generateUrl('dhg_contacto_contactoProveedorCreate'),
                            'mecanico_create'=>$this->generateUrl('dhg_contacto_contactoMecanicoCreate'),
                            'almacen_create'=>$this->generateUrl('dhg_Almacenes_almacenCreate'),);
        //Accion mantenimiento
        $formMantenimiento = $this->get('form.factory')->create( $formMantenimientoType = new FormMantenimiento($em,"Nuevo_mantenimiento",$rutas_ajax), $mantenimiento);
        $formMantenimiento->render_required_asterisk = true;

        //Accion repostaje
        $repostaje = new Repostaje();
        $formRepostaje = $this->get('form.factory')->create( $formRepostakeType = new FormRepostaje($em,"Nuevo_repostaje",$rutas_ajax), $repostaje);
        $formRepostaje->render_required_asterisk = true;

        $showFormRepostajeAtInit = false;
        $showFormMantenimientoAtInit = false;
        if ($request->getMethod() == 'POST'){
            $data = $request->request->all();
            if (isset($data['Nuevo_mantenimiento'])) {
                $formMantenimiento->bind($request);
                if ($formMantenimiento->isValid()){
                    $um = new AccionManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = [];
                    if ($um->createAccion($mantenimiento, $error,$formMantenimiento->get('Accion')->get('modificar')->getData())){
                         $this->get('session')->getFlashBag()->add('success', 'Mantenimiento creado exitosamente!');
                    }else{
                        foreach ($error as $key => $value) {
                            $this->get('session')->getFlashBag()->add('danger', $value);
                        }
                    }
                    return $this->redirect($this->generateUrl('dhg_maquinaria_accionlList'));
                }else{
                    $showFormMantenimientoAtInit = true;
                }
            }elseif (isset($data['Nuevo_repostaje'])) {
                $formRepostaje->bind($request);
                if ($formRepostaje->isValid()){
                    $um = new AccionManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = null;
                    if ($um->createAccion($repostaje, $error,$formRepostaje->get('Accion')->get('modificar')->getData())){
                         $this->get('session')->getFlashBag()->add('success', 'Repostaje creado exitosamente!');
                    }else{
                        foreach ($error as $key => $value) {
                            $this->get('session')->getFlashBag()->add('danger', $value);
                        }
                    }
                    return $this->redirect($this->generateUrl('dhg_maquinaria_accionlList'));
                  
                }else{
                    $showFormRepostajeAtInit = true;
                }
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('AccionTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        $result = array('dataTable' => $dataTable); 
        
        if ($this->get('security.context')->isGranted(MaquinariaPermissions::GESTIONAROPERACIONES, $mantenimiento)) {
              //Agrego menu
              $listener = new MenuEventsListener($this->container->get('event_dispatcher'));
              $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateOperationsItems'), -99);

              $result = array_merge($result, array(
                        'modularFormMantenimiento' => $formMantenimiento->createView(),
                        'formMantenimientoType' => $formMantenimientoType,
                        'formMantenimiento_action' => $this->generateUrl('dhg_maquinaria_accionlList'),
                        'formMantenimiento_submit_value' => 'Crear',
                        'form_title' => "Nuevo Mantenimiento",
                        'formMantenimiento_class' => 'form-horizontal addMantenimiento',
                        'formMantenimiento_show_at_init' => $showFormMantenimientoAtInit,

                        'modularFormRepostaje' => $formRepostaje->createView(),
                        'formRepostajeType' => $formRepostakeType,
                        'formRepostaje_action' => $this->generateUrl('dhg_maquinaria_accionlList'),
                        'formRepostaje_submit_value' => 'Crear',
                        'formRepostaje_class' => 'form-horizontal addRepostaje',
                        'formRepostaje_show_at_init' => $showFormRepostajeAtInit,

                        'dataTable_twigVars' => array(
                            'defSortColumn' => [0,"desc"],
                            'modalConfirmID' => $metaData['table']->id,
                            'modalConfirmTitle' => 'Atencion!',
                            'dateFilterCols' => [0],
                            'sumColumn' => [3,4],
                            'modalConfirmText' => 'Se procedera a eliminar el accion seleccionada. Esta operacion no puede deshacerse.',
                            'modalConfirmSubmitValue' => 'Eliminar',
                            'modalConfirmUrlAction' => $this->generateUrl('dhg_maquinaria_accionRemove',  array('id' => '#')),
                            'modalEditID' => 'edit'.$metaData['table']->id,
                            'modalViewID' => 'view'.$metaData['table']->id,
                         ),
                    )
                 );    
        }else{
              $result = array_merge($result, array('dataTable_twigVars' => array(
                  'modalViewID' => 'view'.$metaData['table']->id,
                  'defSortColumn' => [0,"desc"],
                  'dateFilterCols' => [0],
                  'sumColumn' => [3],
              )));
          }
        return $result;    
    }


    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $accion = $em->getRepository('DHGMaquinariaBundle:Accion')->find($id);
        if ($accion){
            $rutas_ajax = array('maquinaria_create'=>$this->generateUrl('dhg_maquinaria_maquinariaCreate'),
                            'maquinaria_motor_create'=>$this->generateUrl('dhg_maquinaria_maquinariaMotorCreate'),
                            'combustible_create'=>$this->generateUrl('dhg_inventario_productoCombustibleCreate'),
                            'producto_create'=>$this->generateUrl('dhg_inventario_productoCreate'),
                            'proveedor_create'=>$this->generateUrl('dhg_contacto_contactoProveedorCreate'),
                            'mecanico_create'=>$this->generateUrl('dhg_contacto_contactoMecanicoCreate'),
                            'almacen_create'=>$this->generateUrl('dhg_Almacenes_almacenCreate'));

            if ($accion instanceof Repostaje) {
                $repostaje = $em->getRepository('DHGMaquinariaBundle:Repostaje')->find($id);
                $form = $this->get('form.factory')->create( $formType = new FormRepostaje($em,'Editar_Repostaje',$rutas_ajax), $repostaje);
            }else{
                $mantenimiento = $em->getRepository('DHGMaquinariaBundle:Mantenimiento')->find($id);
                $form = $this->get('form.factory')->create( $formType = new FormMantenimiento($em,'Editar_Mantenimiento',$rutas_ajax), $mantenimiento);
            }
            if ($request->getMethod() == 'POST'){
                $form->handleRequest($request);
                if ($form->isValid()){
                    $um = new AccionManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = [];
                    if ($um->editAccion($accion, $error,$form->get('Accion')->get('modificar')->getData())){
                        $this->get('session')->getFlashBag()->add('success','Accion editada exitosamente!');
                    }else{
                        return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                    }
                    return new Response("",Response::HTTP_OK);
                }
            }
            if ($accion instanceof Repostaje) {
                return new Response($this->renderView('DHGMaquinariaBundle:Form:repostajeForm.html.twig',
                array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_maquinaria_accionEdit'),
                    'form_title' => 'Editar accion ',
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editAccion',
                    )
                ),Response::HTTP_CREATED); 
            }else{
                return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
                array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_maquinaria_accionEdit'),
                    'form_title' => 'Editar accion ',
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editAccion',
                    )
                ),Response::HTTP_CREATED);
            }    
        }else{
           return new Response("No existe la Accion sobre Maquinaria",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
          $em = $this->getDoctrine()->getManager();
        $accion = $em->getRepository('DHGMaquinariaBundle:Accion')->find($id);
        if (!$accion) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al eliminar el accion "%s". No existe. ', $id) );
        }else{
            $name = $accion->__toString() ;
            $um = new AccionManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeVerificationAccion($accion, $error)){
              if ($um->removeAccion($accion, $error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('Accion eliminada exitosamente!'));
              }else{
                  foreach ($error as $key => $value) {
                      $this->get('session')->getFlashBag()->add('danger', $value);
                  }
              }
            }else{
                foreach($error as $key => $val){
                   $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                }
            }
        }
        return $this->redirect($this->generateUrl('dhg_maquinaria_accionlList'));
    }



     /**
     * Muestra un elemento
     * @param Id ID del elemento
     *
     */
    public function viewAction($id){
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $accion = $em->getRepository('DHGMaquinariaBundle:Accion')->find($id);
        if ($accion){          
          $request->attributes->add(array('id'=>$id));
          

          return $this->render('DHGMaquinariaBundle:Modal:accion.html.twig',
              array(
                    'modalViewTitle' => 'Accion',
                    'content' => $accion,
              )
          );     
        }else{
           return $this->render('DHGcoreBundle:Modal:modalError.html.twig',array('msg'=> "No existe "));
        };
    }


    /**
     * Obtiene la disponibilidad de un producto para un determinado Almacen
     * 
     * @param Id ID del elemento
     *
     */
    public function getOdometrosAction($id){
      $em = $this->getDoctrine()->getManager();
      $maq = $em->getRepository('DHGMaquinariaBundle:Maquinaria')->findOneBy(array('id'=>$id));
      if($maq){
        $response = array("message" => $maq->getOdometro(),"km"=>$maq->getOdometroKm());
      }else{
        $response = array("message" => "0","km"=>"0");
      }
      return new Response(json_encode($response));
    }
}
