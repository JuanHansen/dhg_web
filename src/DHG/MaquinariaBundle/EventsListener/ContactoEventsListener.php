<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Events\InventarioEvents;
use DHG\MaquinariaBundle\EntityManager\MecanicoManager;

class ContactoEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }


    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent $event
     */
    public function onContactoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array('contacto' => $idcontacto));
            if($mecanico!=null){
                $um = new MecanicoManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeVerificationMecanico($mecanico,$error)){
                    return true;
                }else{
                    $event->stopRemove($error,'Mecanico');
                    return false;
                }
            }
        }
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedEvent $event
     */
    public function onContactoRemovedEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array('contacto' => $idcontacto));
            if($mecanico!=null){
                $um = new MecanicoManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeMecanico($mecanico,$error)){
                    return true;
                }else{
                    //$error[]='No se pudo eliminar el mecanico';
                    $event->stopRemove($error,'Mecanico');
                    return false;
                }
            }
        }
    }

}
