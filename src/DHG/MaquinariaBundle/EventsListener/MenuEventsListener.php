<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use DHG\MaquinariaBundle\Entity\Maquinaria;
use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use DHG\MaquinariaBundle\Entity\Mecanico;
use DHG\MaquinariaBundle\Entity\Mantenimiento;
use DHG\MaquinariaBundle\Entity\OtroGasto;
use DHG\MaquinariaBundle\Entity\Repostaje;
use DHG\MaquinariaBundle\Security\MaquinariaPermissions;

class MenuEventsListener{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher,$mensaje = "Agregar")
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->mensaje = $mensaje;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBar($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
 
        $MenuAnidado = $factory ->createItem('Maquinaria', array())
                        	    ->setExtra('orderNumber', 4)
                                ->setAttribute('dropdown', true)
                        	    ->setAttribute('icon', 'truck');
        
        $mantenimiento = new Mantenimiento();
        $MenuAnidado->addChild(
                    $factory->createItem('Operaciones', array('route' => 'dhg_maquinaria_accionlList'))
                            ->setExtra('orderNumber',2)
                            ->setAttribute('icon', 'history')
                            ->setExtra('permission',array('permission'=>MaquinariaPermissions::VEROPERACIONES,'entity'=>$mantenimiento))
                    );
        $maquinaria = new Maquinaria();
        $MenuAnidado->addChild(
                    $factory->createItem('Maquinarias', array('route' => 'dhg_maquinaria_maquinariaList'))
                            ->setExtra('orderNumber',3)
                            ->setAttribute('icon', 'truck')
                            ->setExtra('permission',array('permission'=>MaquinariaPermissions::VERMAQUINAS,'entity'=>$maquinaria))
                    );
        $MainMenuAjustes = $parentMenu->getChild('Ajustes');
        $MenuAjustes = $factory ->createItem('Maquinaria', array())
                                ->setAttribute('dropright', true);

        $clasiMaquin = new MaquinariaClasif();
        $MenuAjustes->addChild(
                    $factory->createItem('Clasificacion de maquinarias', array('route' => 'dhg_maquinaria_maquinariaClasiflList'))
                            ->setExtra('orderNumber',2)
                            ->setExtra('permission',array('permission'=>MaquinariaPermissions::VERMAQUINAS,'entity'=>$clasiMaquin))
                    );
        //$MainMenuAjustes->addChild($MenuAjustes); 

        $parentMenu->addChild($MenuAnidado);
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateMaquinariaItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem($this->mensaje, array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateItems($event)
    {
        /*
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $menuInventario = $parentMenu->getChild('Maquinaria');

        $MenuAcciones = $factory->createItem('Acciones', array())
        ->setExtra('orderNumber', 1)
        ->setAttribute('dropdownmenu', true)
        ->setAttribute('icon', 'gears');
        $MenuAcciones->addChild($factory->createItem('Agregar', array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
        $menuInventario->addChild($MenuAcciones);
        */ 
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateOperationsItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem('Repostaje', array('uri' => '#modal-create-repostaje'))->setAttribute('data-toggle', 'modal'));
        $MenuAcciones->addChild($factory->createItem('Mantenimiento', array('uri' => '#modal-create-mantenimiento'))->setAttribute('data-toggle', 'modal'));
        //$MenuAcciones->addChild($factory->createItem('Agregar otro gasto', array('uri' => '#modal-create-otrogasto'))->setAttribute('data-toggle', 'modal'));
    }

}