<?php
namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Entity\MaquinariaClasif;

class BundleEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Metodo de instalacion
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    static public function onInstall($event)
    {
        $em = $event->getEntityManager();
        $logger = $event->getLogger();
        $logger->info('Install Maquinaria');
     
        //Cargo clasificacion de maquinarias
        $xml = simplexml_load_file(__DIR__.'/../Resources/static/MaquinariaClasificaciones.xml');
        foreach($xml->children() as $child) {
            $maquClasif = $em->getRepository('DHGMaquinariaBundle:MaquinariaClasif')->findOneByNombre($child->nombre);
            if (empty($maquClasif)) {
                $obj = new MaquinariaClasif();
                $obj->setNombre((string)$child->nombre);
                $obj->setDetalle((string)$child->detalle);
                $obj->setSistema(((string)$child->sistem)=="true");
                $em->persist($obj);
            }
        }
        try {
            $em->flush();
        } catch(Exception $e) {
            $logger->error($e);
        }
    }

}
