<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Events\InventarioEvents;
use DHG\MaquinariaBundle\EntityManager\MecanicoManager;

class MecanicoEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\MaquinariaBundle\Events\MaquinariaEvents $event
     */
    public function onMecanicoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $mecanico = $event->getMecanico();
        $movimiento = $em->getRepository('DHGMaquinariaBundle:Mantenimiento')->findOneBy(array('mecanico' => $mecanico->getId()));
        if($movimiento != null){
            $event->stopRemove(sprintf('No es posible eliminar el mecanico "%s", ya que esta asociado a un mantenimiento.', $event->getMecanico()->getNombre() ), 'Mecanico');
            return false;
        }
        $otrogasto = $em->getRepository('DHGMaquinariaBundle:OtroGasto')->findOneBy(array('Mecanico' => $mecanico->getId()));
        if($otrogasto != null){
            $event->stopRemove(sprintf('No es posible eliminar el mecanico "%s", ya que esta asociado a un mantenimiento.', $event->getMecanico()->getNombre() ), 'Mecanico');
            return false;
        }
    }





    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent $event
     */
    public function onContactoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array('contacto' => $idcontacto));
            if($mecanico!=null){
                $um = new MecanicoManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeVerificationMecanico($mecanico,$error)){
                    return true;
                }else{
                    $event->stopRemove($error,'Mecanico');
                    return false;
                }
            }
        }
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedEvent $event
     */
    public function onContactoRemovedEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array('contacto' => $idcontacto));
            if($mecanico!=null){
                $um = new MecanicoManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeMecanico($mecanico,$error)){
                    return true;
                }else{
                    //$error[]='No se pudo eliminar el mecanico';
                    $event->stopRemove($error,'Mecanico');
                    return false;
                }
            }
        }
    }

}
