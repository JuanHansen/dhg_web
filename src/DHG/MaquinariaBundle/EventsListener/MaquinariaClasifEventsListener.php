<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Events\MaquinariaEvents;


class MaquinariaClasifEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * Determina si existe almenos una maquinaria dentro de la clasificacion. Si es asi, evita la eliminacion de esta
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onMaquinariaClasifRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getMaquinariaClasif()->getId();
        $maquinaria = $em->getRepository('DHGMaquinariaBundle:Maquinaria')->findOneBy(array('clasificacion' => $id));
        if($maquinaria != null){
            $event->stopRemove('Existe almenos una maquinaria con esta clasificacion. Elimine primero la maquinaria con esta clasificacion o reasignelo antes de proseguir.', 'Maquinaria');
        }
    }


}
