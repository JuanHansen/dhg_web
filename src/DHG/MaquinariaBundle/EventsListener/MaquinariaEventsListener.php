<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Events\MaquinariaEvents;


class MaquinariaEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onMaquinariaRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getMaquinaria()->getId();
        $maquinaria = $em->getRepository('DHGMaquinariaBundle:Accion')->findOneBy(array('maquinaria' => $id));
        if($maquinaria != null){
            $event->stopRemove('Existen acciones realizadas sobre esta maquinaria, por lo cual no puede ser eliminada.', 'Maquinaria');
        }
    }


}
