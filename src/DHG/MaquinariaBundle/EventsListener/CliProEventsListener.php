<?php

namespace DHG\MaquinariaBundle\EventsListener;

use DHG\MaquinariaBundle\Events\InventarioEvents;
use DHG\MaquinariaBundle\EntityManager\CliProManager;

class CliProEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\MaquinariaBundle\Events\InventarioEvents $event
     */
    public function onCliProRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getCliPro()->getId();
        $movimiento = $em->getRepository('DHGMaquinariaBundle:Repostaje')->findOneBy(array('proveedor' => $id));
        if($movimiento != null){
            $event->stopRemove(sprintf('No es posible eliminar el Cliente/Proveedor "%s", ya que esta asociado a un repostaje.', $event->getCliPro()->getNombre() ), 'Inventario');
            return false;
        }
    }
}
