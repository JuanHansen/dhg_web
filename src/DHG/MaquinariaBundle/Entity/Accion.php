<?php
namespace DHG\MaquinariaBundle\Entity;

use DHG\coreBundle\UUID;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\coreBundle\Entity\UserAware;
use DHG\UserBundle\Entity\User;

/**
 *  Clase abstracta para definir las acciones
 * @ORM\Entity
 * @ORM\Table(name="Acciones")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap( {"mantenimiento" = "Mantenimiento", "repostaje" = "Repostaje", "otrogasto" = "OtroGasto"} )
 */
class Accion implements UserAware
{

    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $eliminado;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $creado;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $odometro;

    /**
     * @ORM\ManyToOne(targetEntity="Maquinaria")
     * @ORM\JoinColumn(name="fk_maquinaria_id", referencedColumnName="id", nullable=false)
     */
    protected $maquinaria;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $costo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $detalle;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    protected $usuario;
    

    public function __construct(){
        $this->id = UUID::v4();
        $this->creado = new \DateTime('now');
        $this->lastupdate = new \DateTime('now');
        $this->localized = true;
        $this->eliminado = false;
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->id;
    }


    /**
     * Get id
     *
     * @return string 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PersonalClasification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getDetalle(){
       return $this->detalle;
    }

    public function setDetalle($detalle){
       $this->detalle = $detalle;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return \DateTime
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime 
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Accion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set odometro
     *
     * @param string $odometro
     * @return Accion
     */
    public function setOdometro($odometro)
    {
        $this->odometro = $odometro;

        return $this;
    }

    /**
     * Get odometro
     *
     * @return string 
     */
    public function getOdometro()
    {
        return $this->odometro;
    }

    /**
     * Set costo
     *
     * @param string $costo
     * @return Accion
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set maquinaria
     *
     * @param \DHG\MaquinariaBundle\Entity\Maquinaria $maquinaria
     * @return Accion
     */
    public function setMaquinaria(\DHG\MaquinariaBundle\Entity\Maquinaria $maquinaria)
    {
        $this->maquinaria = $maquinaria;

        return $this;
    }

    /**
     * Get maquinaria
     *
     * @return \DHG\MaquinariaBundle\Entity\Maquinaria 
     */
    public function getMaquinaria()
    {
        return $this->maquinaria;
    }

    public function setUsuario(User $usuario = null)
    {
        $this->usuario = $usuario;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;
    }

    /**
     * Get eliminado
     *
     * @return boolean 
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }
}
