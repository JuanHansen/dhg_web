<?php
namespace DHG\MaquinariaBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use DHG\MaquinariaBundle\Entity\Archivo;
use DHG\EntityHistoryBundle\Entity\Versionable;


/**
 * @ORM\Entity
 * @ORM\Table(name="Maquinarias")
 */
class Maquinaria extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha_compra;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $anio;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $precio_compra;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $fabricante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $modelo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $con_motor;

    /**
     * @ORM\ManyToOne(targetEntity="MaquinariaClasif")
     * @ORM\JoinColumn(name="fk_clasificacion_id", referencedColumnName="id", nullable=false)
     */
    protected $clasificacion;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $odometro;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $odometro_km;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $dominio;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\InventarioBundle\Entity\Producto", cascade={"persist"})
     * @ORM\JoinTable(name="Maquinaria_respuestos",
     *   joinColumns={@ORM\JoinColumn(name="maquinaria_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="respuesto_id", referencedColumnName="id")}
     * ) 
     */
    protected $respuestos;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="Archivo", cascade={"persist"})
     * @ORM\JoinTable(name="Maquinaria_archivos",
     *   joinColumns={@ORM\JoinColumn(name="maquinaria_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="archivo_id", referencedColumnName="id")}
     * ) 
     */
    protected $archivos;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $detalle;
    
     static public function getResourceEntityName(){
        return 'Maquinaria';
    }

    static public function getResourceIcon(){
        return 'truck';
    }
 
    static public function getSeccionIcon(){
        return 'truck';
    }

    static public function getResourceColor(){
        return '#E8321F';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'fecha_compra' => 'Fecha de Compra',
                'anio' => 'Año',
                'precio_compra' => 'Precio de Compra',
                'fabricante' => 'Fabricante',
                'modelo' => 'Modelo',
                'con_motor' => 'Con Motor',
                'clasificacion' => 'Clasificacion',
                'odometro' => 'Odometro',
                'odometro_km' => 'Km',
                'dominio' => 'Dominio',
                'repuestos' => 'Repuestos',
                'archivos' => 'Archivos',
                'detalle' => 'Detalle',
            );
    }
    public function __construct(){
        parent::__construct();
        $this->odometro = 0;
        $this->respuestos = new \Doctrine\Common\Collections\ArrayCollection();
    }



    public function getImagen(){
        foreach ($this->archivos as $key => $value) {
            if($value->getImgDefault()){
                return $value;
            }
        }
        return null;
    }
    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->nombre;
    }

    public function getEgreso(){
       return $this->egreso;
    }

    public function setEgreso($egreso){
       $this->egreso = $egreso;
    }

    public function getDetalle(){
       return $this->detalle;
    }

    public function setDetalle($detalle){
       $this->detalle = $detalle;
    }
  

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Maquinaria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fecha_compra
     *
     * @param \DateTime $fechaCompra
     * @return Maquinaria
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fecha_compra = $fechaCompra;

        return $this;
    }

    /**
     * Get fecha_compra
     *
     * @return \DateTime 
     */
    public function getFechaCompra()
    {
        return $this->fecha_compra;
    }

    /**
     * Set anio
     *
     * @param \DateTime $anio
     * @return Maquinaria
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return \DateTime 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set precio_compra
     *
     * @param string $precioCompra
     * @return Maquinaria
     */
    public function setPrecioCompra($precioCompra)
    {
        $this->precio_compra = $precioCompra;

        return $this;
    }

    /**
     * Get precio_compra
     *
     * @return string 
     */
    public function getPrecioCompra()
    {
        return $this->precio_compra;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     * @return Maquinaria
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string 
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Maquinaria
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set con_motor
     *
     * @param boolean $conMotor
     * @return Maquinaria
     */
    public function setConMotor($conMotor)
    {
        $this->con_motor = $conMotor;

        return $this;
    }

    /**
     * Get con_motor
     *
     * @return boolean 
     */
    public function getConMotor()
    {
        return $this->con_motor;
    }

    /**
     * Set odometro
     *
     * @param string $odometro
     * @return Maquinaria
     */
    public function setOdometro($odometro)
    {
        $this->odometro = $odometro;

        return $this;
    }

    /**
     * Get odometro
     *
     * @return string 
     */
    public function getOdometro()
    {
        return $this->odometro;
    }

    /**
     * Set dominio
     *
     * @param string $dominio
     * @return Maquinaria
     */
    public function setDominio($dominio)
    {
        $this->dominio = $dominio;

        return $this;
    }

    /**
     * Get dominio
     *
     * @return string 
     */
    public function getDominio()
    {
        return $this->dominio;
    }

    /**
     * Set odometro_km
     *
     * @param string $odometro_km
     */
    public function setOdometroKm($odometro_km)
    {
        $this->odometro_km = $odometro_km;

        return $this;
    }

    /**
     * Get odometro_km
     *
     * @return string 
     */
    public function getOdometroKm()
    {
        return $this->odometro_km;
    }


    /**
     * Set clasificacion
     *
     * @param \DHG\MaquinariaBundle\Entity\MaquinariaClasif $clasificacion
     * @return Maquinaria
     */
    public function setClasificacion(\DHG\MaquinariaBundle\Entity\MaquinariaClasif $clasificacion)
    {
        $this->clasificacion = $clasificacion;

        return $this;
    }

    /**
     * Get clasificacion
     *
     * @return \DHG\MaquinariaBundle\Entity\MaquinariaClasif 
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

    /**
     * Add respuestos
     *
     * @param \DHG\InventarioBundle\Entity\Producto $respuestos
     * @return Respuesto
     */
    public function addRespuesto(\DHG\InventarioBundle\Entity\Producto $respuesto)
    {
        if (!$this->respuestos->contains($respuesto)) {
            $this->respuestos->add($respuesto);
        }
        return $this;
    }

    /**
     * Remove respuestos
     *
     * @param \DHG\InventarioBundle\Entity\Producto $respuestos
     */
    public function removeRespuesto(\DHG\InventarioBundle\Entity\Producto $respuesto)
    {
        $this->respuestos->removeElement($respuesto);
    }

    /**
     * Get respuestos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRespuestos()
    {
        return $this->respuestos;
    }

        /**
     * Add archivos
     *
     * @param \DHG\MaquinariaBundle\Entity\Archivo $archivo
     * @return Respuesto
     */
    public function addArchivo($archivo)
    {
        $this->archivos[] = $archivo;

        return $this;
    }

    /**
     * Remove archivos
     *
     * @param \DHG\MaquinariaBundle\Entity\Archivo $archivo
     */
    public function removeArchivo($archivo)
    {
        $this->archivos->removeElement($archivo);
    }

    /**
     * Get archivos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArchivos()
    {
        return $this->archivos;
    }
}
