<?php
namespace DHG\MaquinariaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DHG\EntityHistoryBundle\Entity\Versionable;


/**
 * Entidad heredada de accion
 * @ORM\Entity
 */
class Mantenimiento extends Accion implements Versionable{

    /**
     * @ORM\ManyToOne(targetEntity="Mecanico")
     * @ORM\JoinColumn(name="fk_mecanico_id", referencedColumnName="id")
     */
    protected $mecanico;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\InventarioBundle\Entity\Movimiento", cascade={"persist","remove","detach"}, orphanRemoval=true)
     * @ORM\JoinTable(name="Acciones_mantenimiento_movimientos",
     *   joinColumns={@ORM\JoinColumn(name="movimiento_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="accion_id", referencedColumnName="id")}
     * ) 
     */
    protected $movimientos;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $costo_manoobra;


     static public function getResourceEntityName(){
        return 'Mantenimiento';
    }

    static public function getResourceIcon(){
        return 'cogs';
    } 
    static public function getSeccionIcon(){
        return 'truck';
    }

    static public function getResourceColor(){
        return '#E8321F';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'maquinaria' => 'Maquinaria',
                'odometro' => 'Odometro',
                'costo' => 'Costo Total',
                'mecanico' => 'Mecanico',
                'costo_manoobra' => 'Costo de Mano de Obra',
                'detalle' => 'Detalle',
            );
    }


    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return "Mantenimiento a ".$this->getMaquinaria()->__toString();   
    }
    /**
     * Set Mecanico
     *
     * @param \DHG\MaquinariaBundle\Entity\Mecanico $mecanico
     * @return Mantenimiento
     */
    public function setMecanico(\DHG\MaquinariaBundle\Entity\Mecanico $mecanico)
    {
        $this->mecanico = $mecanico;

        return $this;
    }

    /**
     * Get Mecanico
     *
     * @return \DHG\MaquinariaBundle\Entity\Mecanico 
     */
    public function getMecanico()
    {
        return $this->mecanico;
    }

    /**
     * Add movimientos
     *
     * @param \DHG\InventarioBundle\Entity\Movimiento $movimientos
     * @return Mantenimiento
     */
    public function addMovimiento(\DHG\InventarioBundle\Entity\Movimiento $movimientos)
    {
        $this->movimientos[] = $movimientos;

        return $this;
    }

    /**
     * Remove movimientos
     *
     * @param \DHG\InventarioBundle\Entity\Movimiento $movimientos
     */
    public function removeMovimiento(\DHG\InventarioBundle\Entity\Movimiento $movimientos)
    {
        $this->movimientos->removeElement($movimientos);
    }

    /**
     * Get movimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientos()
    {
        return $this->movimientos;
    }
 
    /**
     * setCostoManoobra
     *
     * @param string $costo_manoobra
     * @return Accion
     */
    public function setCostoManoobra($costo_manoobra)
    {
        $this->costo_manoobra = $costo_manoobra;

        return $this;
    }

    /**
     * Get costo_manoobra
     *
     * @return string 
     */
    public function getCostoManoobra()
    {
        return $this->costo_manoobra;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto()
    {
        $costo = $this->costo_manoobra;
        if($this->movimientos==null)
            return $costo;
        foreach ($this->movimientos as $key => $value) {
            $costo+= ($value->getCantidad()*$value->getPrecioUnidad());
        }
        return $costo;
    }
}
