<?php
namespace DHG\MaquinariaBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use DHG\ContactosBundle\Entity\Contacto;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="Mecanicos")
 */
class Mecanico extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\OneToOne(targetEntity="DHG\ContactosBundle\Entity\Contacto",cascade={"persist"})
     * @ORM\JoinColumn(name="fk_Contacto_id", referencedColumnName="id")
     */
    protected $contacto;
    

    public function __construct(){
        parent::__construct();
        $this->contacto = new Contacto();
        $this->contacto->__construct();
    }

     static public function getResourceEntityName(){
        return 'Mecanico';
    }

    static public function getResourceIcon(){
        return 'people';
    }
 
    static public function getSeccionIcon(){
        return 'truck';
    }

    static public function getResourceColor(){
        return '#D9DA03';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'contacto' => 'Contacto',
            );
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString(){
      if($this->contacto!=null){
        return $this->contacto->getNombre();}
    }


    public function getNombre(){
      if($this->contacto!=null){
       return $this->contacto->getNombre();}
    }

    public function setNombre($nombre){
      if($this->contacto!=null){
       $this->contacto->setNombre($nombre);}
    }

    public function setContacto($contacto){
      if($this->contacto!=null){
      $this->contacto = $contacto;}
    }
    public function getContacto(){
      if($this->contacto!=null){
      return $this->contacto;}
    }


}
