<?php
namespace DHG\MaquinariaBundle\Entity;

use DHG\coreBundle\UUID;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\EntityHistoryBundle\Entity\Versionable;


/**
 * Entidad heredada de accion
 * @ORM\Entity
 */
class Repostaje extends Accion implements Versionable{

    /**
     * @ORM\OneToOne(targetEntity="DHG\InventarioBundle\Entity\Movimiento", cascade={"persist","remove"},orphanRemoval=true)
     * @ORM\JoinColumn(name="fk_movimiento_id", referencedColumnName="id")
     */
    protected $movimiento;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $precio;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\Producto")
     * @ORM\JoinColumn(name="fk_combustible_id", referencedColumnName="id")
     */
    protected $combustible;


    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\CliPro")
     * @ORM\JoinColumn(name="fk_contacto_id", referencedColumnName="id")
     */
    protected $proveedor;

    /**
     * Si este valor es verdadero indicara que el repostaje se hizo desde una Almacen
     * @ORM\Column(type="boolean")
     */
    protected $interno;

     static public function getResourceEntityName(){
        return 'Repostaje';
    }

    static public function getResourceIcon(){
        return 'battery-1';
    } 
    static public function getSeccionIcon(){
        return 'truck';
    }

    static public function getResourceColor(){
        return '#E8321F';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'maquinaria' => 'Maquinaria',
                'odometro' => 'Odometro',
                'costo' => 'Costo Total',
                'cantidad' => 'Cantidad',
                'precio' => 'Precio',
                'combustible' => 'Combustible',
                'proveedor' => 'Proveedor',
                'interno' => 'Movimiento Interno',
                'detalle' => 'Detalle',
            );
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return "Repostaje a ".$this->getMaquinaria()->__toString();   
    }
    /**
     * Set proveedor
     *
     * @param \DHG\InventarioBundle\Entity\Contacto $proveedor
     * @return Repostaje
     */
    public function setProveedor(\DHG\InventarioBundle\Entity\CliPro $proveedor = null)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \DHG\InventarioBundle\Entity\CliPro 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     * @return Repostaje
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return Repostaje
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Get movimiento
     * 
     */
    public function getMovimiento()
    {
        return $this->movimiento;
    }

    /**
     * Set movimiento
     * 
     */
    public function setMovimiento($movimiento)
    {
        $this->movimiento = $movimiento;
    }

    public function getInterno(){
       return $this->interno;
    }

    public function setInterno($interno){
       $this->interno = $interno;
    }


    public function getCombustible(){
        return $this->combustible;
    }

    public function setCombustible($combustible)
    {
        $this->combustible = $combustible;
    }
    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto()
    {
        if($this->interno&&$this->movimiento!=null){
            return $this->movimiento->getCantidad()*$this->getMovimiento()->getPrecioUnidad();
        }
        return $this->precio*$this->cantidad;
    }
}
