<?php 
namespace DHG\MaquinariaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Entidad heredada de accion
 * @ORM\Entity
 */
class OtroGasto extends Accion{

    /**
     * @ORM\ManyToOne(targetEntity="Mecanico")
     * @ORM\JoinColumn(name="fk_mecanico_id", referencedColumnName="id", nullable=false)
     */
    protected $Mecanico;

    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\InventarioBundle\Entity\Movimiento", cascade={"persist","remove"})
     * @ORM\JoinTable(name="Acciones_repostajes_otrosgastos",
     *   joinColumns={@ORM\JoinColumn(name="movimiento_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="accion_id", referencedColumnName="id")}
     * ) 
     */
    protected $movimientos;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $costo_manoobra;


       /**
     * Set Mecanico
     *
     * @param \DHG\MaquinariaBundle\Entity\Mecanico $mecanico
     * @return Mantenimiento
     */
    public function setMecanico(\DHG\MaquinariaBundle\Entity\Mecanico $mecanico)
    {
        $this->Mecanico = $mecanico;

        return $this;
    }

    /**
     * Get Mecanico
     *
     * @return \DHG\MaquinariaBundle\Entity\Mecanico 
     */
    public function getMecanico()
    {
        return $this->Mecanico;
    }

    /**
     * Add movimientos
     *
     * @param \DHG\InventarioBundle\Entity\Movimiento $movimientos
     * @return Mantenimiento
     */
    public function addMovimiento(\DHG\InventarioBundle\Entity\Movimiento $movimientos)
    {
        $this->movimientos[] = $movimientos;

        return $this;
    }

    /**
     * Remove movimientos
     *
     * @param \DHG\InventarioBundle\Entity\Movimiento $movimientos
     */
    public function removeMovimiento(\DHG\InventarioBundle\Entity\Movimiento $movimientos)
    {
        $this->movimientos->removeElement($movimientos);
    }

    /**
     * Get movimientos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientos()
    {
        return $this->movimientos;
    }
 

    /**
     * Set costo_manoobra
     *
     * @param string $costoManoobra
     * @return OtroGasto
     */
    public function setCostoManoobra($costoManoobra)
    {
        $this->costo_manoobra = $costoManoobra;

        return $this;
    }

    /**
     * Get costo_manoobra
     *
     * @return string 
     */
    public function getCostoManoobra()
    {
        return $this->costo_manoobra;
    }

}
