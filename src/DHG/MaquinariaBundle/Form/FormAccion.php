<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class FormAccion extends AbstractType
{
    private $em;
    private $rutas_ajax;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('costo', 'hidden', array('data' => 0))
            ->add('fecha','date',array(
                    'label' => 'Fecha',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array('class' => 'input-sm input-l datepicker-input',
                        'data-date-format'=>'dd-mm-yyyy',
                        'price_date'=>'producto_mantenimiento',
                        'onChange'=>'changePrice(this,"producto_mantenimiento")',
                        'endSection'=>''),
                    'constraints' =>  new NotBlank(array('message'=>'Es necesario ingresar una fecha')),
            ))
            ->add('maquinaria', 'entity', array(
                'label' => 'Maquinaria',
                'class' => 'DHG\MaquinariaBundle\Entity\Maquinaria',
                'group_by' => 'clasificacion',
                'query_builder' => function(EntityRepository $er) {
                                return $er->createQueryBuilder('m')
                                    ->orderBy('m.nombre', 'ASC');
                },
                'empty_value' => 'Seleccione una maquinaria',
                'empty_data'  => null,
                'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar una maquinaria')),
                'required' => true,
                'attr' => array( 'data_choice_change_source' => 'maq_odometro'),
                'widget_addon_append' => array(
                    'text' => 'Crear Maquinaria',
                    'form_ajax'=>$this->rutas_ajax['maquinaria_create'],
                ),
            ))
            ->add('odometro', 'number', array(
                'label' => "Odometro",
                'attr' => array(
                    'placeholder' => "0",
                    'data_choice_change_destination' => 'maq_odometro'
                ),
                'widget_addon_append' => array(
                    'text' => 'Km',
                ),
                'widget_suffix'  => 'Ultimo Registro: -',
                'required' => false
            ))
            ->add('modificar', 'checkbox', array(
                'label' => 'Modificar Odometro',
                'mapped'=>false,
                'widget_checkbox_label' => "label",
                'help_block' => 'Seleccione esta opcion si desea que se actualice el odometro de la maquinaria',
                'required' => false,
                'render_optional_text' => false,
                'attr' => array('endSection'=>'')
            ))
            ->add('detalle', 'textarea', array(
                'label' => "Detalles",
                'required' => false,
                'attr' => array('endSection'=>'')
            ));
            $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postsubmitAccionEvent'));
    }

     public function postsubmitAccionEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        $modificar = $form->get('modificar')->getData();
        $maquinaria = $form->get('maquinaria')->getData();
        $odometro = $form->get('odometro')->getData();
         if ($maquinaria != null && $modificar==true){
            if ( ($maquinaria!=null) && (floatval($odometro) < $maquinaria->getOdometro())){
                $form->get('odometro')->addError(new FormError( sprintf('El odometro ingresado es menor al odometro de la maquinaria %s: "%s"',$maquinaria->getNombre(), $maquinaria->getOdometro() ) ));
            }
        }
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em,$rutas_ajax = array()){
            $this->em = $em;
            $this->rutas_ajax = $rutas_ajax;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'inherit_data' => true
        ));
    }

    public function getName()
    {
        return 'accion';
    }

   
}