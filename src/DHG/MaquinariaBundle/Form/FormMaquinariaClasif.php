<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;


class FormMaquinariaClasif extends AbstractType{
    private $name = 'Nueva_clasificacion_de_maquinaria';
    /**
     * Builds the FormMaquinariaClasif form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('nombre', 'text', array(
                'label' => "Nombre",
                'help_block' => 'Nombre de la clasificacion.',
                'attr' => array(
                    'placeholder' => "Nombre",
                ),
                'constraints' =>  new NotBlank(array('message'=>'Es necesario ingresar un nombre')),
            ))

            ->add('detalle', 'textarea', array(
                'label' => "Detalles",
                'required' => false
            ))
        ;
        $builder->setAttribute('show_legend', true); 

    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\MaquinariaClasif'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct($name='Nueva_clasificacion_de_maquinaria'){
        $this->name = $name;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


