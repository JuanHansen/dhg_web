<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;

class FormMantenimiento extends AbstractType{
    private $name = 'Nuevo_mantenimiento';
    private $em;
    private $rutas_ajax;

    /**
     * Builds the FormMaquinaria form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
           ->add('Accion', new FormAccion($this->em,$this->rutas_ajax), array(
                'data_class' => 'DHG\MaquinariaBundle\Entity\Accion',
                'label' => false,
                'label_render'=>false
            ))

            ->add('mecanico', 'entity', array(
                'label' => 'Mecanico',
                'class' => 'DHG\MaquinariaBundle\Entity\Mecanico',
                'query_builder' => function(EntityRepository $er) {
                                return $er->createQueryBuilder('m')
                                    ->orderBy('m.contacto', 'ASC');
                },
                'empty_value' => 'Seleccione un mecanico',
                'empty_data'  => null,
                'required' => false,
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['mecanico_create'],
                ),
            )) 

            ->add('costo_manoobra', 'text', array(
                'label' => "Precio de mano de obra",
                'attr' => array(
                    'placeholder' => "0.00",
                    'endSection'=>''
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'required' => false
            ))

            ->add('movimientos','collection', array(
                'label' => 'Productos utilizados',
                'type' => new FormAddProductMov($this->em,$this->rutas_ajax),
                'allow_add' => true,
                'allow_delete' => true, 
                'prototype' => true,
                'attr' => array(
                    'endSection'=>''
                ),
                'options' => array( 
                    'clasificacion' => 'Mantenimiento',
                    'horizontal' => true,
                    'label_render' => false,
                    'horizontal_input_wrapper_class' => "col-lg-8",
                    'widget_remove_btn' => array('label' => "", "icon" => "trash",'wrapper_div'=>false, 'attr' => array('class' => 'btn btn-danger')),
                ),
                'widget_add_btn' => array('label' => "Productos", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),   
                'by_reference' => false,
            ));
            
        $builder->setAttribute('show_legend', true); 
        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postsubmitEvent'));

    }

    public function postsubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        if (is_array($data)) {
            $costo_manoobra = $data['costo_manoobra'];
            $movimientos = $data['movimientos'];
        }elseif (is_object($data)) {
            $costo_manoobra = $data->getCostoManoobra();
            $movimientos = $data->getMovimientos();
        }else {
            $costo_manoobra = null;
        }

        $costo = 0;
        if($costo_manoobra != null){
            $costo = floatval($costo_manoobra);
        }

        if($movimientos != null){
            foreach ($movimientos as $key => $mov) {
                $costo += $mov->getPrecioUnidad() * $mov->getCantidad();
            }
        }
        $data->setCosto($costo);
    }
    
    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\Mantenimiento',
            'cascade_validation' => true,
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em, $name='Nuevo_mantenimiento',$rutas_ajax){
            $this->name = $name;
            $this->em = $em;
            $this->rutas_ajax = $rutas_ajax;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }

}


