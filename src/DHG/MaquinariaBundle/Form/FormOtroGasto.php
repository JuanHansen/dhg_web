<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;

class FormOtroGasto extends AbstractType{
    private $name = 'Nuevo_otro_gasto';
    private $em;

    /**
     * Builds the FormMaquinaria form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
           ->add('Accion', new FormAccion($this->em), array(
                'data_class' => 'DHG\MaquinariaBundle\Entity\Accion',
                'label' => false,
                'label_render'=>false
            ))
            ->add('mecanico', 'entity', array(
                'label' => 'Mecanico',
                'class' => 'DHG\MaquinariaBundle\Entity\Mecanico',
                'query_builder' => function(EntityRepository $er) {
                                return $er->createQueryBuilder('m')
                                    ->orderBy('m.contacto', 'ASC');
                },
                'empty_value' => 'Seleccione un mecanico',
                'empty_data'  => null,
                'required' => false
            ))  

            ->add('costo_manoobra', 'text', array(
                'label' => "Precio de mano de obra",
                'attr' => array(
                    'placeholder' => "0.00",
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'required' => false
            ))

            ->add('movimientos','collection', array(
                'label' => 'Productos utilizados',
                'type' => new FormAddProductMov($this->em),
                'allow_add' => true,
                'allow_delete' => true, 
                'prototype' => true,
                'options' => array( 
                    'clasificacion' => 'OtroGasto',
                    'horizontal' => true,
                    'label_render' => false,
                    'horizontal_input_wrapper_class' => "col-lg-4",
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger')),
                ),
                'widget_add_btn' => array('label' => "Agregar producto", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),   
                'by_reference' => false,
            ))

  

        ;
        $builder->setAttribute('show_legend', true); 
        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postsubmitEvent'));

    }

    public function postsubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        if (is_array($data)) {
            $maquinaria = $data['maquinaria'];
            $odometro = $data['odometro'];
        }elseif (is_object($data)) {
            $maquinaria = $data->getMaquinaria();
            $odometro = $data->getOdometro();
        }else {
            $maquinaria = null;
            $odometro = null;
        }

        if ($maquinaria != null){
            $maq = $this->em->getRepository('DHGMaquinariaBundle:Maquinaria')->findOneBy(array('id'=>$maquinaria)); 
            if ( ($maq!=null) && (floatval($odometro) < $maq->getOdometro()))
                $form->addError(new FormError( sprintf('El odometro ingresado es menor al odometro de la maquinaria %s: "%s"',$maq->getNombre(), $maq->getOdometro() ) ));//TODO agregar la unidad del odometro
        }
    }
    
    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\OtroGasto',
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em, $name='Nuevo_otro_gasto'){
            $this->name = $name;
            $this->em = $em;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }

}


