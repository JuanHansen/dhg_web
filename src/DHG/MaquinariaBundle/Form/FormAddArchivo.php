<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormAddArchivo extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', array(
                'label' => false,
                'horizontal_input_wrapper_class' => "col-lg-12",
                "file_path" => "webPath",
                "file_name" => "path",
            ))
            ->add('description', 'text', array(
                'label' => false,
                'horizontal_input_wrapper_class' => "col-lg-12",
                'attr'=>array("placeholder"=>"Descripcion")
            ))
            ->add('imgDefault', 'checkbox', array(
                'label' => 'Default',
                'widget_checkbox_label' => "label",
                'required' => false,
                'render_optional_text' => false,
                'attr' => array('endSection'=>'')
            ))
            ;
    }

    public function getName()
    {
        return 'AddArchivo';
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\Archivo',
        ));
    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\Archivo'
        );
    }

}