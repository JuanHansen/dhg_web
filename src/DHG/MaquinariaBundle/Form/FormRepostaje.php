<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\NotBlank;
use DHG\MaquinariaBundle\Entity\Maquinaria;
use DHG\InventarioBundle\EntityManager\MovimientoManager;
use DHG\InventarioBundle\Entity\Movimiento;
use DHG\InventarioBundle\Entity\MovClasif;
use Doctrine\ORM\EntityRepository;

class FormRepostaje extends AbstractType{
    private $name = 'Nuevo_repostaje';
    private $em;
    private $rutas_ajax;

    /**
     * Builds the FormMaquinaria form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
           ->add('Accion', new FormAccion($this->em,$this->rutas_ajax), array(
                'data_class' => 'DHG\MaquinariaBundle\Entity\Accion',
                'label' => false
            ))

            ->add('interno', 'checkbox', array(
                'label' => 'Consumo Interno',
                'help_block' => 'Seleccione esta opcion si se toma el combustible de un Almacen especifico. De lo contrario debera indicar la compra al proveedor.',
                'required' => false,
                'render_optional_text' => false,
                'widget_checkbox_label' => "label",
                'attr'=>array('onChange'=>'internoChanged(this)','class'=>'input_interno','endSection'=>''),
            ))

            ->add('combustible', 'entity', array(
                'label' => 'Combustible',
                'class' => 'DHG\InventarioBundle\Entity\Producto',
                'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('p')
                                      ->join("p.clasificacion", "c", "WITH", "c.nombre LIKE 'Combustibles'");
                },
                'empty_value' => 'Seleccione un combustible',
                'empty_data'  => null,
                'required' => false,
                'render_optional_text' => false,
                'attr' => array('onChange'=>'changeUnit(this,"combustible_externo");'),
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['combustible_create'],
                ),
                'attr' => array('endSection'=>'')
            ))

            ->add('precio', 'text', array(
                'label' => "Precio de compra",
                'attr' => array(
                    'placeholder' => "0.00",
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'required' => false,
                'render_optional_text' => false,
            ))

            ->add('cantidad', 'number', array(
                'label' => "Cantidad",
                'attr' => array(
                    'placeholder' => "0",
                ),
                'widget_addon_append' => array(
                    'text' => 'L',
                ),
                'required' => false,
                'render_optional_text' => false,
                'help_block'  => 'Cantidad de litros adquirido',
                'attr' => array('stock_destination' => 'combustible_externo','endSection'=>'')
            ))

            ->add('proveedor', null, array(
                'label' => "Proveedor *",
                'choices' => $this->getProveedoresList(),
                'empty_value' => 'Seleccione un proveedor',
                'empty_data'  => null,
                'render_optional_text' => false,
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['proveedor_create'],
                ),
            ))

            ->add('movimiento', new FormAddCombusMov($this->em,$this->rutas_ajax), array(
                'label' => false,
                'data_class' => 'DHG\InventarioBundle\Entity\Movimiento'
            ))

        ;
        $builder->setAttribute('show_legend', true); 
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'presetEvent'));
        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postsubmitEvent'));

    }


    public function presetEvent(FormEvent $event)   {
        $form = $event->getForm()->get('Accion');

        $form->add('maquinaria', null, array(
                'label' => 'Maquinaria',
                'choices' => $this->getMaquinariaList(),
                'group_by' => 'clasificacion',
                'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar una maquinaria')),
                'empty_value' => 'Seleccione una maquinaria',
                'empty_data'  => null,
                'attr' => array( 'data_choice_change_source' => 'maq_odometro'),
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['maquinaria_motor_create'],
                ),
        ));        
    }


    /**
     * Rearmo el objeto Repostaje
     *
     */
     public function postsubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        $interno = $data->getInterno();
        $proveedor = $data->getProveedor();
        $movimiento = $data->getMovimiento();

        //Si es externo
        if (!$interno) {
            if($data->getCantidad()<=0){
                $form->get('cantidad')->addError(new FormError("Debe ingresar una cantidad mayor que 0"));
            }
            if($data->getCombustible()==null){
                $form->get('combustible')->addError(new FormError("Debe seleccionar un combustible"));
            }
            if($data->getProveedor()==null){
                $form->get('proveedor')->addError(new FormError("Debe seleccionar un proveedor"));
            }
            $data->setCosto($data->getCantidad()*$data->getPrecio());
            $data->setMovimiento(null);
        }else{
            $error = false;
            if($movimiento==null){
                $form->addError(new FormError( sprintf('Debe elegir algun tipo de consumo')));
            }
            if($movimiento->getAlmacen()==null){
                $form->get('movimiento')->get('almacen')->addError(new FormError("Debe seleccionar un almacen"));
            }
            if($movimiento->getCantidad()<=0){
                $form->get('movimiento')->get('cantidad')->addError(new FormError("Debe ingresar una cantidad mayor que 0"));
            }
            if($movimiento->getProducto()==null){
                $form->get('movimiento')->get('producto')->addError(new FormError("Debe seleccionar un producto"));
            }
            $data->setProveedor(null);
            $data->setCombustible($movimiento->getProducto());
            $data->setCantidad($movimiento->getCantidad());
            $movimiento->setDetalle("Movimiento generado por el sistema"); 
            $movimiento->setSistema(true);
            //Seteo la clasificacion
            $repo = $this->em->getRepository('DHGInventarioBundle:MovClasif');
            $clasif = $repo->findOneByNombre('Repostaje');
            
            if ($clasif != null){
                $movimiento->setClasificacion($clasif);
            }else{
                $clasif = new MovClasif();
                $clasif->setEgreso(true);
                $clasif->setNombre('Repostaje');
                $clasif->setDetalle("Clasificacion generada por el sistema");
                $clasif->setSistema(true);
                try{
                    $this->em->persist($clasif);
                    $this->em->flush();
                    $movimiento->setClasificacion($clasif);
                } catch (\Exception $e){
                    $error = true; 
                    $form->addError(new FormError("Ocurrio un error al dar de alta la clasificacion de repostaje"));
                }
            }          
        }
    }

    /**
     * Obtiene una lista con lproductos, agrupandolo por su clasificacion
     *
     */
    private function getCombustibleList(){
        $repo = $this->em->getRepository('DHGInventarioBundle:Producto');
        $query = $repo->createQueryBuilder('p')
            ->join("p.clasificacion", "c", "WITH", "c.nombre LIKE 'Combustibles'")
            ->getQuery();
        $list = array();
        foreach( $query->getResult() as $comb){
            $list[$comb->getId()] = $comb->getNombre();
        }
        return $list;
    }

    /**
     * Obtiene una lista con las maquinarias con motor
     *
     */
    private function getMaquinariaList(){
        $repo = $this->em->getRepository('DHGMaquinariaBundle:Maquinaria');
        $query = $repo->createQueryBuilder('m')
            ->where('m.con_motor = 1')
            ->getQuery();
        return $query->getResult();

    } 

    /**
     * Obtiene una lista con los proveedores
     *
     */
    private function getProveedoresList(){
        $repo = $this->em->getRepository('DHGInventarioBundle:CliPro');
        $query = $repo->createQueryBuilder('c')
            ->where('c.proveedor = 1')
            ->getQuery();
        return $query->getResult();
    } 

    
    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\Repostaje',
            'cascade_validation' => true,
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em, $name='Nuevo_repostaje',$rutas_ajax=array()){
            $this->name = $name;
            $this->em = $em;
            $this->rutas_ajax = $rutas_ajax;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }

}


