<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\FormCallbackValidator;
use DHG\MaquinariaBundle\Entity\Maquinaria;
use DHG\InventarioBundle\EntityManager\MovimientoManager;
use DHG\InventarioBundle\Entity\Movimiento;
use DHG\InventarioBundle\Entity\MovClasif;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FormAddProductMov extends AbstractType{
    private $em;
    private $movClasif;
    private $rutas_ajax;
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder

           // ->add('id', 'hidden', array())
            ->add('detalle', 'hidden', array('data' => 'Movimiento generado por el sistema'))
            ->add('sistema', 'hidden', array('data' => true))   
            ->add('clasificacion', 'hidden', array('data_class' => 'DHG\InventarioBundle\Entity\MovClasif'))

            ->add('producto', 'entity', array( 
                    'label' => 'Producto',
                    'class' => 'DHG\InventarioBundle\Entity\Producto',
                    'group_by' => 'clasificacion',
                    'empty_value' => 'Seleccione un producto',
                    'empty_data'  => null,
                    'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un producto')),
                    'horizontal' => true,
                    'attr' => array('stock_producto' => 'producto_mantenimiento','onChange'=>'changeUnitAndStock(this,"producto_mantenimiento");'),
                    'widget_addon_append' => array(
                        'form_ajax'=>$this->rutas_ajax['producto_create'],
                    ),
            ))

            ->add('Almacen', 'entity', array(
                'label' => 'Almacen',
                'class' => 'DHG\InventarioBundle\Entity\Almacen',
                'query_builder' => function(EntityRepository $er) {
                                return $er->createQueryBuilder('m')
                                    ->orderBy('m.nombre', 'ASC');
                },
                'group_by' => 'father',
                'horizontal' => true,
                'empty_value' => 'Seleccione una Almacen',
                'empty_data'  => null,
                'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un almacen')),
                'attr' => array('stock_almacen' => 'producto_mantenimiento','onChange'=>'changeStock(this,"producto_mantenimiento")'),
                'required' => true,
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['almacen_create'],
                ),
            ))

            ->add('cantidad', 'number', array(
                'label' => "Cantidad",
                'widget_addon_append' => array(
                    'text' => 'T', //Que sea dinamico dependiendo del producto
                ),
                'horizontal' => true,
                'attr' => array('stock_destination' => 'producto_mantenimiento'),
                'widget_suffix' => 'Disponibles:-',
                'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
                'required' => true
            ))
            ->add('precio_unidad', 'text', array(
                'label' => "$/unidad",
                'horizontal' => true,
                'attr' => array(
                    'placeholder' => "0.00",
                    'price_destination' => 'producto_mantenimiento',
                    'endSection'=>'',
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'widget_suffix' => 'Ultimo Precio: $0',
                'required' => true,
                'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"El precio debe ser positivo")),
            ))
            ;  

        $this->movClasif = $options['clasificacion'];
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'postsubmitEvent') );
    }


    /**
     * Rearmo el objeto 
     *
     */
     public function postsubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        if (is_array($data)) {
           // $id = $data['id'];
            $producto = $data['producto'];
            $cantidad = $data['cantidad'];
            $Almacen = $data['Almacen'];
        }elseif (is_object($data)) {
            $producto = $data->getProducto();
            $cantidad = $data->getCantidad();
            $Almacen = $data->getAlmacen();
        }else {
            $producto = null;
            $cantidad = null;
            $Almacen = null;
        }

        //Verifico que producto, cantidad y Almacen esten seteados
        $error = false;
        $movimiento = null;
         if (!$error){
            //Seteo la clasificacion
            $repo = $this->em->getRepository('DHGInventarioBundle:MovClasif');
            $clasif = $repo->findOneByNombre($this->movClasif);
            
            if ($clasif != null){
                $data['clasificacion'] = $clasif;
            }else{
                $clasif = new MovClasif();
                $clasif->setEgreso(true);
                $clasif->setNombre($this->movClasif);
                $clasif->setDetalle("Clasificacion generada por el sistema");
                $clasif->setSistema(true);
                try{
                    $this->em->persist($clasif);
                    $this->em->flush();
                    $data['clasificacion'] = $clasif;

                } catch (\Exception $e){
                    $error = true; 
                    $form->addError(new FormError( sprintf('Error al crear el movimiento relacionado al producto "%s" en el Almacen "%s"',$prod->getNombre(), $ubi->getName() ) ));
                }
            }
        }
        if (!$error){
            $event->setData($data );
        }     
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'clasificacion' => 'Mantenimiento',
            'data_class' => 'DHG\InventarioBundle\Entity\Movimiento',
        ));
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em,$rutas_ajax=array()){
            $this->em = $em;
            $this->rutas_ajax=$rutas_ajax;
    }

    public function getName()
    {
        return 'AddMovProd';
    }






}