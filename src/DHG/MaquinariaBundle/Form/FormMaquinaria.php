<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\NotBlank;
use DHG\coreBundle\Form\Transformers\YearOnlyDateTransformer;
use Doctrine\ORM\EntityRepository;

class FormMaquinaria extends AbstractType{
    private $name = 'Nueva_maquinaria';
    private $ed;
    private $rutas_ajax;
    private $filtroClasificacion=null;
    /**
     * Builds the FormMaquinaria form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder

            ->add('nombre', 'text', array(
                'label' => "Nombre",
            ));

        if($this->filtroClasificacion==null){
            $builder
                ->add('clasificacion', 'entity', array(
                    'label' => 'Clasificacion',
                    'class' => 'DHG\MaquinariaBundle\Entity\MaquinariaClasif',
                    'query_builder' => function(EntityRepository $er) {
                                    return $er->createQueryBuilder('m')
                                        ->orderBy('m.nombre', 'ASC');
                    },
                    'empty_value' => 'Seleccione una clasificacion',
                    'empty_data'  => null,
                    'required' => true,
                    'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar una clasificacion')),
                ));
        }else{
            $builder
                ->add('clasificacion', 'entity', array(
                    'label' => 'Clasificacion',
                    'class' => 'DHG\MaquinariaBundle\Entity\MaquinariaClasif',
                    'choices' => $this->filtroClasificacion,
                    'empty_value' => 'Seleccione una clasificacion',
                    'empty_data'  => null,
                    'required' => true,
                    'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar una clasificacion')),
                ));
        }
        $builder
            ->add('con_motor', 'checkbox', array(
                'label'        => 'Con motor',
                'widget_checkbox_label' => "label",
                'help_block'  => 'Seleccione esta opcion si esta maquinaria posee motor.',
                'required' => false,
                'attr' => array('endSection'=>'')
            ))


            ->add('fecha_compra','date',array(
                        'label' => 'Fecha de Compra',
                        'widget' => 'single_text',
                        'format' => 'dd-MM-yyyy',
                        'attr' => array('class' => 'input-sm input-l datepicker-input',
                            'data-date-format'=>'dd-mm-yyyy',),
                        'required' => false,
            ))
            ->add('precio_compra', 'text', array(
                'label' => "Precio de compra",
                'attr' => array(
                    'placeholder' => "0.00",
                    'endSection'=>''
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'required' => false,
            ))


            ->add('modelo', 'text', array(
                'label' => "Modelo",
                'required' => false
            ))
            ->add('anio','choice',array(
                'required'=>false,
                'label' => 'Año fabricacion',
                'choices'=>array_combine(range(2020,1920,1),range(2020,1920,1)),
            ))
            ->add('fabricante', 'text', array(
                'label' => "Fabricante",
                'required' => false,
                'attr' => array('endSection'=>'')
            ))

            ->add('dominio', 'text', array(
                'label' => "Dominio",
                'required' => false,
                'attr' => array('endSection'=>'')
            ))



            ->add('odometro', 'number', array(
                'label' => "Odometro",
                'attr' => array(
                    'placeholder' => "0",
                ),
                'required' => false,
            ))
            ->add('odometro_km', 'choice', array(
                'label' => 'Unidad',
                'choices'   => array( '1' => 'Km', '0' => 'Hr'),
                'constraints' =>  new NotBlank(),
                'required' => true,
                'attr' => array('endSection'=>'')
            ))


            ->add('respuestos','collection', array(
                'type' => 'entity',
                'label'=>'Productos asociados',
                'allow_add' => true,
                'allow_delete' => true, 
                'prototype' => true,
                'options' => array( 
                    'class' => 'DHG\InventarioBundle\Entity\Producto',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('p');
                     },
                    'group_by' => 'clasificacion',
                    'empty_value' => 'Seleccione un producto',
                    'empty_data'  => null,
                    'constraints' =>  new NotBlank(),
                    
                    'horizontal' => true,
                    'label_render' => false,
                    //'horizontal_input_wrapper_class' => "col-lg-4",
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger')),
                    'widget_addon_append' => array(
                        'form_ajax'=>$this->rutas_ajax['producto_create'],
                    ),
                ),
                'widget_add_btn' => array('label' => "Productos", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')), 
                'horizontal_input_wrapper_class' => "col-lg-9",
                'by_reference' => false,
                'attr' => array('endSection'=>'')
            ))
            


            ->add('archivos','collection', array(
                'label' => 'Archivos',
                'type' => new FormAddArchivo($this->em),
                'allow_add' => true,
                'allow_delete' => true, 
                'prototype' => true,
                'options' => array( 
                    'horizontal' => true,
                    'label_render' => false,
                    'horizontal_input_wrapper_class' => "col-lg-12",
                    'widget_remove_btn' => array('label' => "", "icon" => "trash", 'attr' => array('class' => 'btn btn-danger')),
                ),
                'widget_add_btn' => array('label' => "Archivos", "icon" => "plus-sign",'attr' => array('class' => 'btn btn-success')),
                'by_reference' => false, 
            ))

            ->add('detalle', 'textarea', array(
                'label' => "Detalles",
                'required' => false
            ))
            ->addViewTransformer(new YearOnlyDateTransformer())
        ;
        $builder->setAttribute('show_legend', true); 
      
    }




    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\MaquinariaBundle\Entity\Maquinaria'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct($name='Nueva_maquinaria',$rutas_ajax=array()){
            $this->name = $name;
            $this->rutas_ajax=$rutas_ajax;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }

    public function setClasificaciones($clasificaciones){
        $this->filtroClasificacion=$clasificaciones;
    }

}


