<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class FormAddCombusCompra extends AbstractType{
    private $em;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder

            ->add('precio', 'text', array(
                'label' => "Precio total de compra",
                'attr' => array(
                    'placeholder' => "0.00",
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'horizontal_input_wrapper_class' => 'col-lg-4'
            ))

            ->add('cantidad', 'number', array(
                'label' => "Cantidad",
                'attr' => array(
                    'placeholder' => "0",
                ),
                'widget_addon_append' => array(
                    'text' => 'L',
                ),
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'help_block'  => 'Cantidad de litros adquirido',
                'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
            ))

            ->add('proveedor', 'choice', array(
                'label' => "Proveedor",
                'choices' => $this->getProveedoresList(),
                'empty_value' => 'Seleccione un proveedor',
                'empty_data'  => null,
                'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un proveedor')),
            ))
        ;       
  
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct( $em){
            $this->em = $em;
    }

    public function getName()
    {
        return 'AddCombCompra';
    }

    /**
     * Obtiene una lista con los proveedores
     *
     */
    private function getProveedoresList(){
        $repo = $this->em->getRepository('DHGInventarioBundle:Contacto');
        $query = $repo->createQueryBuilder('c')
            ->where('c.proveedor = 1')
            ->orderBy("c.nombre")
            ->getQuery();
        return $query->getResult();
    } 



}