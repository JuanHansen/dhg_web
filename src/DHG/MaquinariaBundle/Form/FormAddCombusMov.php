<?php
namespace DHG\MaquinariaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;


class FormAddCombusMov extends AbstractType{
    private $em;
    private $rutas_ajax;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('producto', 'entity', array(
                'label' => 'Combustible',
                'choices' => $this->getProductosList(),
                'empty_value' => 'Seleccione un combustible',
                'empty_data'  => null,
                'required' => false,
                'class' => 'DHG\InventarioBundle\Entity\Producto',
                'attr' => array('stock_producto' => 'combustible_interno','onChange'=>'changeUnitAndStock(this,"combustible_interno");'),
                'render_optional_text' => false,
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['combustible_create'],
                ),
            ))
            ->add('almacen', 'entity', array(
                'label' => "Almacen",
                'choices' => $this->getAlmacenList(),
                'empty_value' => 'Seleccione una Almacen',
                'empty_data'  => null,
                'required' => false,
                'class' => 'DHG\InventarioBundle\Entity\Almacen',
                'attr' => array('stock_almacen' => 'combustible_interno','onChange'=>'changeStock(this,"combustible_interno")'),
                'render_optional_text' => false,
                'widget_addon_append' => array(
                    'form_ajax'=>$this->rutas_ajax['almacen_create'],
                ),
            ))
            ->add('cantidad', 'number', array(
                'label' => "Cantidad",
                'attr' => array(
                    'placeholder' => "0",
                ),
                'widget_addon_append' => array(
                    'text' => 'L',
                ),
                'required' => false,
                'attr' => array('stock_destination' => 'combustible_interno'),
                'widget_suffix' => 'Disponibles:-',
                'render_optional_text' => false
            ))
            ->add('precio_unidad', 'text', array(
                'label' => "$/unidad",
                'horizontal' => true,
                'attr' => array(
                    'placeholder' => "0.00",
                    'price_destination' => 'combustible_interno',
                    'endSection'=>'',
                ),
                 'widget_addon_prepend' => array(
                    'text' => '$'
                ),
                'widget_suffix' => 'Ultimo Precio: $0',
                'required' => false,
                'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"El precio debe ser positivo")),
            ))
        ;       
       
  
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct($em,$rutas_ajax=array()){
            $this->em = $em;
            $this->rutas_ajax=$rutas_ajax;
    }

    public function getName()
    {
        return 'AddCombMov';
    }

    /**
     * Obtiene una lista con lproductos, agrupandolo por su clasificacion
     *
     */
    private function getProductosList(){
        $repo = $this->em->getRepository('DHGInventarioBundle:Producto');
        $query = $repo->createQueryBuilder('p')
            ->join("p.clasificacion", "c", "WITH", "c.nombre LIKE 'Combustibles'")
            ->getQuery();
        $list = array();
        foreach( $query->getResult() as $comb){
            $list[$comb->getId()] = $comb;
        }
        return $list;
    } 

    /**
     * Obtiene una lista con las Almacenes, agrupandolo por ubicacino padre
     *
     */
    private function getAlmacenList(){
        $repo = $this->em->getRepository('DHGInventarioBundle:Almacen');
        $list = array();
        foreach($repo->findAll() as $ubi){
            if ($ubi->getFather() ){
                if (empty( $list[$ubi->getFather()->getName()])){
                     $list[$ubi->getFather()->getName()] = array();
                }
                $list[$ubi->getFather()->getName()][$ubi->getId()] = $ubi;
            }else{
                if (empty( $list['Sin padre'])){
                     $list['Sin padre'] = array();
                }
                $list['Sin padre'][$ubi->getId()] = $ubi;
            }
        }
        return $list;
    } 
    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\InventarioBundle\Entity\Movimiento'
        );
    }

}