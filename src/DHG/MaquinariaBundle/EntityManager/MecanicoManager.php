<?php

namespace DHG\MaquinariaBundle\EntityManager;

use DHG\MaquinariaBundle\Entity\Mecanico;
use Doctrine\ORM\EntityManager;

use DHG\MaquinariaBundle\Events\MaquinariaEvents;
use DHG\MaquinariaBundle\Events\MecanicoCreatedEvent;
use DHG\MaquinariaBundle\Events\MecanicoEditedEvent;
use DHG\MaquinariaBundle\Events\MecanicoRemovedEvent;
use DHG\MaquinariaBundle\Events\MecanicoRemovedVerificationEvent;
use DHG\ContactosBundle\EventsListener\MecanicoEventsListener;

class MecanicoManager{
	private $em;
	private $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la mecanico
     *
	 * @param $mecanico Mecanico con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createMecanico(Mecanico $mecanico, &$error = null ){
        $event = new MecanicoCreatedEvent($mecanico);
        if($this->ed->dispatch(MaquinariaEvents::MECANICO_CREATED, $event)){
    	   try{
                $this->em->persist($mecanico);
                $this->em->flush();
            } catch (\Exception $e){
        	   $error[] = "Ocurrio un error al intentar crear el Mecanico";
        	   return false;
            }
        }else{
            $error =array_merge($error,$event->getMessages());
            return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la mecanico
     *
	 * @param $mecanico Mecanico con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editMecanico(Mecanico $mecanico, &$error = null ){
        $event = new MecanicoEditedEvent($mecanico);
    	if($this->ed->dispatch(MaquinariaEvents::MECANICO_EDITED, $event)){
        try{
            $this->em->persist($mecanico);
            $this->em->flush();
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar editar el Mecanico";
        	return false;
        }
    }else{
        $error =array_merge($error,$event->getMessages());
        return false;
    }
        return true;
    }      


    /**
     * Elimina en base de datos la mecanico
     *
	 * @param $mecanico Mecanico con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeMecanico(Mecanico $mecanico, &$error = array() ){
    	if ($this->removeVerificationMecanico($mecanico, $error)){
            $event = new MecanicoRemovedEvent($mecanico);
            if($this->ed->dispatch(MaquinariaEvents::MECANICO_REMOVED, $event)){
                try{
                    $this->em->remove($mecanico);
                    $this->em->flush();
                } catch (\Exception $e){
                    $error[] = "Ocurrio un error al intentar eliminar el Mecanico";
                    return false;
                } 
                }else{
                    $error =array_merge($error,$event->getMessages());
                    return false;
                }
            }else{
                return false;
            }   
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $mecanico Mecanico con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationMecanico(Mecanico $mecanico, &$error = array() ){
        $event = new MecanicoRemovedVerificationEvent($mecanico);
        $this->ed->dispatch(MaquinariaEvents::MECANICO_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error =array_merge($error,$event->getMessages());
        	return false;
        }
        return true;
    }  



}
