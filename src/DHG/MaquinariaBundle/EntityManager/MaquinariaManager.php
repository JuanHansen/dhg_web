<?php

namespace DHG\MaquinariaBundle\EntityManager;

use DHG\MaquinariaBundle\Entity\Maquinaria;
use Doctrine\ORM\EntityManager;

use DHG\MaquinariaBundle\Events\MaquinariaEvents;
use DHG\MaquinariaBundle\Events\MaquinariaCreatedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaEditedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaRemovedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaRemovedVerificationEvent;

class MaquinariaManager{
	private $em;
	private $ed;
    private $cuenta;

	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed,$cuenta=null){
        $this->em = $em;
        $this->ed = $ed;
        $this->cuenta = $cuenta;
    }

    /**
     * Crea en base de datos la maquinaria
     *
	 * @param $maquinaria Maquinaria con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createMaquinaria(Maquinaria $maquinaria, &$error = null ){
    	try{
            if($maquinaria->getArchivos()!=null){
                $default = false;
                foreach ($maquinaria->getArchivos() as $arch  ){
                    $arch->preUpload();
                    $arch->upload();
                    if($default){
                        $arch->setImgDefault(false);
                    }
                    if($arch->getImgDefault()){
                        $default = true;
                    }
                }
            }

            $this->em->persist($maquinaria);
            $this->em->flush();
            $this->ed->dispatch(MaquinariaEvents::MAQUINARIA_CREATED, new MaquinariaCreatedEvent($maquinaria));

        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar crear la Maquinaria";
        	return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la maquinaria
     *
	 * @param $maquinaria Maquinaria con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editMaquinaria(Maquinaria $maquinaria, &$error = null ){
    	try{

            $default = false;
            foreach ( $maquinaria->getArchivos() as $arch  ){
                $arch->preUpload();
                $arch->upload();
                if($default){
                    $arch->setImgDefault(false);
                }
                if($arch->getImgDefault()){
                    $default = true;
                }
            }

            $this->em->persist($maquinaria);
            $this->em->flush();
            $this->ed->dispatch(MaquinariaEvents::MAQUINARIA_EDITED, new MaquinariaEditedEvent($maquinaria));
 
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar editar la Maquinaria";
        	return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la maquinaria
     *
	 * @param $maquinaria Maquinaria con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeMaquinaria(Maquinaria $maquinaria, &$error = array() ){
    	if ($this->removeVerificationMaquinaria($maquinaria, $error)){
            try{
	            $this->em->remove($maquinaria);
	            $this->em->flush();
	            $this->ed->dispatch(MaquinariaEvents::MAQUINARIA_REMOVED, new MaquinariaRemovedEvent($maquinaria));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar la Maquinaria"; 
	        	return false;
	        }

    	}else{
        	$error = $event->getMessages();
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $maquinaria Maquinaria con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationMaquinaria(Maquinaria $maquinaria, &$error = array() ){
        $event = new MaquinariaRemovedVerificationEvent($maquinaria);
        $this->ed->dispatch(MaquinariaEvents::MAQUINARIA_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    }  
}
