<?php

namespace DHG\MaquinariaBundle\EntityManager;

use DHG\InventarioBundle\EntityManager\MovimientoManager;
use DHG\MaquinariaBundle\Entity\Accion;
use DHG\MaquinariaBundle\Entity\Repostaje;
use Doctrine\ORM\EntityManager;
use DHG\MaquinariaBundle\Events\MaquinariaEvents;
use DHG\MaquinariaBundle\Events\AccionCreatedEvent;
use DHG\MaquinariaBundle\Events\AccionEditedEvent;
use DHG\MaquinariaBundle\Events\AccionRemovedEvent;
use DHG\MaquinariaBundle\Events\AccionRemovedVerificationEvent;
use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\Events\MovimientoCreatedEvent;
use DHG\InventarioBundle\Events\MovimientoEditedEvent;
use DHG\InventarioBundle\Events\MovimientoRemovedEvent;
use DHG\MaquinariaBundle\EntityManager\MaquinariaManager;

class AccionManager{
	private $em;
	private $ed;
    private $movManager;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em, $ed){
        $this->em = $em;
        $this->ed = $ed;
        $this->movManager = new MovimientoManager($this->em);
    }

    /**
     * Crea en base de datos la accion
     *
	 * @param $accion Accion con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 * @param $modificar Si es true, se modifica el odometro de la maquinaria
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createAccion($accion, &$error = null,$modificar=true ){
    	try{
            if ($accion instanceof Repostaje) {
                $mov = $accion->getMovimiento();
                if ($mov != null) {
                    $mov->setFecha($accion->getFecha());
                    $this->ed->dispatch(InventarioEvents::MOVIMIENTO_CREATED, new MovimientoCreatedEvent($mov));
                    if($accion->getInterno())
                        $accion->setCosto($mov->getCantidad()*$mov->getPrecioUnidad());
                }
            }else{
                $movimientos = $accion->getMovimientos();
                $costo = 0;
                if ($movimientos != null) {
                    foreach ( $movimientos as $mov ){
                        $mov->setFecha($accion->getFecha());
                        $this->ed->dispatch(InventarioEvents::MOVIMIENTO_CREATED, new MovimientoCreatedEvent($mov));
                    }
                }
            }
            //Actualizo el odometro de la maquinaria en cuestion
            if($modificar && $accion->getOdometro()!=0){
                $maquinaria = $accion->getMaquinaria();
                $maquinaria->setOdometro($accion->getOdometro());
                $um = new MaquinariaManager($this->em , $this->ed);
                $um->editMaquinaria($maquinaria, $error);
                $this->ed->dispatch(MaquinariaEvents::ACCION_CREATED, new AccionCreatedEvent($accion));
            }
            $this->em->persist($accion);
            $this->em->flush();
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar crear la Accion de Maquinaria";
            //$error[] = $e->getMessage();
        	return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la accion
     *
	 * @param $accion Accion con la informacion a almacenar (Repostaje o Mantenimiento)
	 * @param $error Mensaje de error en caso de suceder
     * @param $modificar Si es true, se modifica el odometro de la maquinaria
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editAccion($accion,&$error = null, $modificar = true){
        
    	try{
            if ($accion instanceof Repostaje) {
                $mov = $accion->getMovimiento();
                if ($mov != null) {
                    $mov->setFecha($accion->getFecha());
                    $this->em->persist($mov);
                    $this->ed->dispatch(InventarioEvents::MOVIMIENTO_CREATED, new MovimientoCreatedEvent($mov));
                }
            }else{
                foreach ($accion->getMovimientos() as $mov ){
                    $mov->setFecha($accion->getFecha());
                    $this->em->persist($mov);
                    $this->ed->dispatch(InventarioEvents::MOVIMIENTO_CREATED, new MovimientoCreatedEvent($mov));
                }
            }
            //Actualizo el odometro de la maquinaria en cuestion
            if($modificar && $accion->getOdometro()!=0){
                $maquinaria = $accion->getMaquinaria();
                $maquinaria->setOdometro($accion->getOdometro());
                $um = new MaquinariaManager($this->em , $this->ed);
                $um->editMaquinaria($maquinaria, $error);
                $this->ed->dispatch(MaquinariaEvents::ACCION_CREATED, new AccionCreatedEvent($accion));
            }
            $this->em->persist($accion);
            $this->em->flush();
            $this->ed->dispatch(MaquinariaEvents::ACCION_EDITED, new AccionEditedEvent($accion));
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar editar la Accion de Maquinaria";
        	return false;
        }
        return true;
    }      

    /**
     * Elimina en base de datos la accion
     *
	 * @param $accion Accion con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeAccion(Accion $accion, &$error = array() ){
    	if ($this->removeVerificationAccion($accion, $error)){
			try{
                 if ($accion instanceof Repostaje) {
                    $mov = $accion->getMovimiento();
                    if ($mov != null) {
                        $mov->setEliminado(true);
                        $this->em->persist($mov);
                        $this->ed->dispatch(InventarioEvents::MOVIMIENTO_REMOVED, new MovimientoRemovedEvent($mov));
                    }
                }else{
                    $movimientos = $accion->getMovimientos();
                    if ($movimientos != null) {
                        foreach ( $movimientos as $mov ){
                            $mov->setEliminado(true);
                            $this->em->persist($mov);
                            $this->ed->dispatch(InventarioEvents::MOVIMIENTO_REMOVED, new MovimientoRemovedEvent($mov));
                        }
                    }
                }
                $accion->setEliminado(true);
                $this->em->persist($accion);
	            $this->em->flush();
	            $this->ed->dispatch(MaquinariaEvents::ACCION_REMOVED, new AccionRemovedEvent($accion));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar la Accion de Maquinaria";
	        	return false;
	        }
    	}else{
        	$error = $event->getMessages();
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $accion Accion con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationAccion(Accion $accion, &$error = array() ){
        $event = new AccionRemovedVerificationEvent($accion);
        $this->ed->dispatch(MaquinariaEvents::ACCION_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    }  
}
