<?php

namespace DHG\MaquinariaBundle\EntityManager;

use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use Doctrine\ORM\EntityManager;

use DHG\MaquinariaBundle\Events\MaquinariaEvents;
use DHG\MaquinariaBundle\Events\MaquinariaClasifCreatedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaClasifEditedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaClasifRemovedEvent;
use DHG\MaquinariaBundle\Events\MaquinariaClasifRemovedVerificationEvent;

class MaquinariaClasifManager{
	private $em;
	private $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em, $ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la maquinariaClasif
     *
	 * @param $maquinariaClasif MaquinariaClasif con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createMaquinariaClasif(MaquinariaClasif $maquinariaClasif, &$error = null ){
    	try{
            $this->em->persist($maquinariaClasif);
            $this->em->flush();
            $this->ed->dispatch(MaquinariaEvents::MAQUINARIACLASIF_CREATED, new MaquinariaClasifCreatedEvent($maquinariaClasif));
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar crear la Clasificacion de Maquinaria";
        	return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la maquinariaClasif
     *
	 * @param $maquinariaClasif MaquinariaClasif con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editMaquinariaClasif(MaquinariaClasif $maquinariaClasif, &$error = null ){
    	try{
            $this->em->persist($maquinariaClasif);
            $this->em->flush();
            $this->ed->dispatch(MaquinariaEvents::MAQUINARIACLASIF_EDITED, new MaquinariaClasifEditedEvent($maquinariaClasif));
        } catch (\Exception $e){
        	$error[] = "Ocurrio un error al intentar editar la Clasificacion de Maquinaria";
        	return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la maquinariaClasif
     *
	 * @param $maquinariaClasif MaquinariaClasif con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeMaquinariaClasif(MaquinariaClasif $maquinariaClasif, &$error = array() ){
    	if ($this->removeVerificationMaquinariaClasif($maquinariaClasif, $error)){
			try{
	            $this->em->remove($maquinariaClasif);
	            $this->em->flush();
	            $this->ed->dispatch(MaquinariaEvents::MAQUINARIACLASIF_REMOVED, new MaquinariaClasifRemovedEvent($maquinariaClasif));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar la Clasificacion de Maquinaria";
	        	return false;
	        }
    	}else{
        	$error = $event->getMessages();
        	return false;
    	}
        return true;
    }  


    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $maquinariaClasif MaquinariaClasif con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationMaquinariaClasif(MaquinariaClasif $maquinariaClasif, &$error = array() ){
        $event = new MaquinariaClasifRemovedVerificationEvent($maquinariaClasif);
        $this->ed->dispatch(MaquinariaEvents::MAQUINARIACLASIF_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    }  
}
