<?php
namespace DHG\MaquinariaBundle\Security;

use DHG\UserBundle\Components\DHGPermissionVoter;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Components\DHGPermissionInterface;

class MaquinariaPermissions extends DHGPermissionVoter
{
    const PREFIX = 'maquinaria';
    const VERMAQUINAS = 'maquinaria_ver_maquinas';
    const GESTIONARMAQUINAS = 'maquinaria_gestionar_maquinas';
    const VEROPERACIONES = 'maquinaria_ver_operaciones';
    const GESTIONAROPERACIONES = 'maquinaria_gestionar_operaciones';

    /**
     * {@inheritdoc}
     */
    public function supportsAttribute($attribute)
    {
        return 0 === strpos($attribute, PREFIX);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedClasses(){
        $supportedClasses = array();
        $supportedClasses[] = 'DHG\MaquinariaBundle\Entity\Mantenimiento';
        $supportedClasses[] = 'DHG\MaquinariaBundle\Entity\Repostaje';
        $supportedClasses[] = 'DHG\MaquinariaBundle\Entity\MaquinariaClasif';
        $supportedClasses[] = 'DHG\MaquinariaBundle\Entity\Maquinaria';
        return $supportedClasses;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissions(ConfigurePermissions $event){
        $perm = $event->getPermissions();

        $permissionClasif = array();
        $permissionClasif[MaquinariaPermissions::PREFIX] = new PermissionClasification();
        $permissionClasif[MaquinariaPermissions::PREFIX]->setMachinename('maquinaria');
        $permissionClasif[MaquinariaPermissions::PREFIX]->setName('Gestion de maquinaria');
        $permissionClasif[MaquinariaPermissions::PREFIX]->setDetails('Gestion de la seccion de maquinaria');


        $permissions = array();
        $permissions[MaquinariaPermissions::VERMAQUINAS] = new Permission();
        $permissions[MaquinariaPermissions::VERMAQUINAS]->setMachinename(MaquinariaPermissions::VERMAQUINAS);
        $permissions[MaquinariaPermissions::VERMAQUINAS]->setName('Ver maquinas');
        $permissions[MaquinariaPermissions::VERMAQUINAS]->setDetails('Permite a los usuarios ver el listado de maquinas');
        $permissions[MaquinariaPermissions::VERMAQUINAS]->setModule(MaquinariaPermissions::PREFIX);
        $permissions[MaquinariaPermissions::VERMAQUINAS]->setClasification($permissionClasif[MaquinariaPermissions::PREFIX]);

        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS] = new Permission();
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setMachinename(MaquinariaPermissions::GESTIONARMAQUINAS);
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setName('Gestionar maquinas');
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setDetails('Permite a los usuarios crear, editar y eliminar maquinas');
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setDepends($permissions[MaquinariaPermissions::VERMAQUINAS]);
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setModule(MaquinariaPermissions::PREFIX);
        $permissions[MaquinariaPermissions::GESTIONARMAQUINAS]->setClasification($permissionClasif[MaquinariaPermissions::PREFIX]);

        $permissions[MaquinariaPermissions::VEROPERACIONES] = new Permission();
        $permissions[MaquinariaPermissions::VEROPERACIONES]->setMachinename(MaquinariaPermissions::VEROPERACIONES);
        $permissions[MaquinariaPermissions::VEROPERACIONES]->setName('Ver operaciones');
        $permissions[MaquinariaPermissions::VEROPERACIONES]->setDetails('Permite a los usuarios ver las operaciones');
        $permissions[MaquinariaPermissions::VEROPERACIONES]->setModule(MaquinariaPermissions::PREFIX);
        $permissions[MaquinariaPermissions::VEROPERACIONES]->setClasification($permissionClasif[MaquinariaPermissions::PREFIX]);

        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES] = new Permission();
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setMachinename(MaquinariaPermissions::GESTIONAROPERACIONES);
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setName('Gestionar operaciones');
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setDetails('Permite a los usuarios crear operaciones');
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setDepends($permissions[MaquinariaPermissions::VEROPERACIONES]);
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setModule(MaquinariaPermissions::PREFIX);
        $permissions[MaquinariaPermissions::GESTIONAROPERACIONES]->setClasification($permissionClasif[MaquinariaPermissions::PREFIX]);

        $perm['permissions'] = array_merge($perm['permissions'], $permissions);

        $perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO], array(MaquinariaPermissions::VERMAQUINAS,MaquinariaPermissions::VEROPERACIONES ));
        $perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN], 
            array(MaquinariaPermissions::VERMAQUINAS,MaquinariaPermissions::VEROPERACIONES, 
                    MaquinariaPermissions::GESTIONARMAQUINAS,MaquinariaPermissions::GESTIONAROPERACIONES));

        $event->setPermissions($perm);
    }


}