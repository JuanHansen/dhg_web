<?php

namespace DHG\MaquinariaBundle\Twig;
use DHG\MaquinariaBundle\Entity\Mantenimiento;
use DHG\MaquinariaBundle\Entity\Repostaje;
use DHG\MaquinariaBundle\Entity\OtroGasto;


class AccionMaquinariaExtension extends \Twig_Extension
{

    public function getTests ()
    {
        return array(
            'dhg_maquinaria_accion_matenimiento' => new \Twig_Test_Method($this, 'isMantenimiento'),
            'dhg_maquinaria_accion_repostaje' => new \Twig_Test_Method($this, 'isRepostaje'),
            'dhg_maquinaria_accion_otrogasto' => new \Twig_Test_Method($this, 'isOtroGasto'),
        );
    }

    public function isMantenimiento($accion)
    {
        return ($accion instanceof Mantenimiento);
    }

    public function isRepostaje($accion)
    {
        return ($accion instanceof Repostaje);
    }

    public function isOtroGasto($accion)
    {
        return ($accion instanceof OtroGasto);
    }

    public function getName()
    {
        return 'dhg_maquinaria_accion_extension';
    }

}

