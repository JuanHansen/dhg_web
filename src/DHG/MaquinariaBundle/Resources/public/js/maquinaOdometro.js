jQuery(document).ready(function() { 
    cambiarOdometro($('select[data_choice_change_source="maq_odometro"]'));
    $(document).on('change','select[data_choice_change_source="maq_odometro"]',function(item){
        cambiarOdometro($(this));
    });
});

function cambiarOdometro($item){
        if($item.length==0)
            return;
        $parent = $item.parent();
        while ( $parent != document && !$parent.find('input[data_choice_change_destination="maq_odometro"]').length ){
            $parent = $parent.parent();
        }
        ide = $parent.find('select[data_choice_change_source="maq_odometro"]').val();

        odo = 'Ultimo Registro: 0';
        km = "Hs";
        numero = 0;
        if(ide){
            t = getOdometro(ide);
            L = t[1];
            if(L){
                km = "Km";
            }
            odo = 'Ultimo Registro: '+t[0];
            numero = t[0];
        }
        
        
        $parent.find('input[data_choice_change_destination="maq_odometro"]').parent().parent().children('span.input-suffix:first').text(odo);
        $parent.find('input[data_choice_change_destination="maq_odometro"]').parent().children('span.input-group-addon:first').text(km);
        $("input[id$='Accion_odometro']").val(numero);
    }


function getOdometro($id){

    var url = urlOdometro+$id; 

    SR = ajax(url,'GET');
    obj = JSON.parse(SR);
    return [obj.message,obj.km];
}

function ajax(url, method, data, async)
{
    method = typeof method !== 'undefined' ? method : 'GET';
    async = typeof async !== 'undefined' ? async : false;

    if (window.XMLHttpRequest)
    {
        var xhReq = new XMLHttpRequest();
    }
    else
    {
        var xhReq = new ActiveXObject("Microsoft.XMLHTTP");
    }


    if (method == 'POST')
    {
        xhReq.open(method, url, async);
        xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhReq.send(data);
    }
    else
    {
        if(typeof data !== 'undefined' && data !== null)
        {
            url = url+'?'+data;
        }
        xhReq.open(method, url, async);
        xhReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhReq.send(null);
    }
    var serverResponse = xhReq.responseText;
    return serverResponse;
}