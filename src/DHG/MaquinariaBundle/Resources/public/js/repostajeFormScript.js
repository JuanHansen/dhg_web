
function internoChanged(item){
    var it = $(item).parent().parent().parent().parent().parent().parent();
    if(item.checked){
        it.find('.block_externo').fadeOut('slow', function(){
            it.find('.block_interno').fadeIn('slow');
        } );
    }else{
        it.find('.block_interno').fadeOut('slow', function(){
            it.find('.block_externo').fadeIn('slow');
        } );
    }
}
function inicializarComponentesFormulario(id){
    item=$(id+" .input_interno");
    if(item.length==0)return;
    if(item.get(0).checked){
        $(id+' .block_externo').hide();
        $(id+' .block_interno').show();
    }else{
        $(id+' .block_externo').show();
        $(id+' .block_interno').hide();
    }
}