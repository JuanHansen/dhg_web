<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Mecanico;
use Symfony\Component\EventDispatcher\Event;

class MecanicoRemovedVerificationEvent extends Event{
    private $mecanico;
    private $stoped; 
    private $messages;

    /**
     * @param Mecanico $mecanico 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Mecanico $mecanico){
        $this->mecanico = $mecanico;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return mecanico
     */
    public function getMecanico(){
        return $this->mecanico;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return mecanico
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
