<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Accion;
use Symfony\Component\EventDispatcher\Event;

class AccionRemovedVerificationEvent extends Event{
    private $accion;
    private $stoped; 
    private $messages;

    /**
     * @param Accion $accion 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Accion $accion){
        $this->accion = $accion;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return accion
     */
    public function getAccion(){
        return $this->accion;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return accion
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
