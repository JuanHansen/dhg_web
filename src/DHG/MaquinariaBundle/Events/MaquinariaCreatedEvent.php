<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Maquinaria;
use Symfony\Component\EventDispatcher\Event;

class MaquinariaCreatedEvent extends Event{
    private $maquinaria;

    /**
     * @param Maquinaria $maquinaria 
     *      Maquinaria created
     */
    public function __construct(Maquinaria $maquinaria){
        $this->maquinaria = $maquinaria;
    }

    /**
     * @return Maquinaria
     */
    public function getMaquinaria(){
        return $this->maquinaria;
    }
}