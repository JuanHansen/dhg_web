<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Maquinaria;
use Symfony\Component\EventDispatcher\Event;

class MaquinariaRemovedVerificationEvent extends Event{
    private $maquinaria;
    private $stoped; 
    private $messages;

    /**
     * @param Maquinaria $maquinaria 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Maquinaria $maquinaria){
        $this->maquinaria = $maquinaria;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return maquinaria
     */
    public function getMaquinaria(){
        return $this->maquinaria;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return maquinaria
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
