<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use Symfony\Component\EventDispatcher\Event;

class MaquinariaClasifCreatedEvent extends Event{
    private $maquinariaClasif;

    /**
     * @param MaquinariaClasif $maquinariaClasif 
     *      MaquinariaClasif created
     */
    public function __construct(MaquinariaClasif $maquinariaClasif){
        $this->maquinariaClasif = $maquinariaClasif;
    }

    /**
     * @return MaquinariaClasif
     */
    public function getMaquinariaClasif(){
        return $this->maquinariaClasif;
    }
}