<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Mecanico;
use Symfony\Component\EventDispatcher\Event;

class MecanicoCreatedEvent extends Event{
    private $mecanico;

    /**
     * @param Mecanico $mecanico 
     *      Mecanico created
     */
    public function __construct(Mecanico $mecanico){
        $this->mecanico = $mecanico;
    }

    /**
     * @return Mecanico
     */
    public function getMecanico(){
        return $this->mecanico;
    }
}