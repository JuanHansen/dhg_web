<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Accion;
use Symfony\Component\EventDispatcher\Event;

class AccionCreatedEvent extends Event{
    private $accion;

    /**
     * @param Accion $accion 
     *      Accion created
     */
    public function __construct(Accion $accion){
        $this->accion = $accion;
    }

    /**
     * @return Accion
     */
    public function getAccion(){
        return $this->accion;
    }
}