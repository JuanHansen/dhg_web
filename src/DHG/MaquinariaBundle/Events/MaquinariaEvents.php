<?php
namespace DHG\MaquinariaBundle\Events;

final class MaquinariaEvents {

    /**
     * @var string
     */
    const MAQUINARIACLASIF_CREATED = 'dhg_maquinaria.maquinariaClasifCreated';

    /**
     * @var string
     */
    const MAQUINARIACLASIF_EDITED = 'dhg_maquinaria.maquinariaClasifEdited';

    /**
     * @var string
     */
    const MAQUINARIACLASIF_REMOVED = 'dhg_maquinaria.maquinariaClasifRemoved';

    /**
     * @var string
     */
    const MAQUINARIACLASIF_REMOVED_VERIFICATION = 'dhg_maquinaria.maquinariaClasifRemovedVerification';



    /**
     * @var string
     */
    const MAQUINARIA_CREATED = 'dhg_maquinaria.maquinariaCreated';

    /**
     * @var string
     */
    const MAQUINARIA_EDITED = 'dhg_maquinaria.maquinariaEdited';

    /**
     * @var string
     */
    const MAQUINARIA_REMOVED = 'dhg_maquinaria.maquinariaRemoved';

    /**
     * @var string
     */
    const MAQUINARIA_REMOVED_VERIFICATION = 'dhg_maquinaria.maquinariafRemovedVerification';
    
    
    
    /**
     * @var string
     */
    const MECANICO_CREATED = 'dhg_maquinaria.mecanicoCreated';

    /**
     * @var string
     */
    const MECANICO_EDITED = 'dhg_maquinaria.mecanicoEdited';

    /**
     * @var string
     */
    const MECANICO_REMOVED = 'dhg_maquinaria.mecanicoRemoved';
    /**
     * @var string
     */
    const MECANICO_CREATED_VERIFICATION = 'dhg_maquinaria.mecanicoCreatedVerification';

    /**
     * @var string
     */
    const MECANICO_REMOVED_VERIFICATION = 'dhg_maquinaria.mecanicoRemovedVerification';

    
    /**
     * @var string
     */
    const ACCION_CREATED = 'dhg_maquinaria.accionCreated';

    /**
     * @var string
     */
    const ACCION_EDITED = 'dhg_maquinaria.accionEdited';

    /**
     * @var string
     */
    const ACCION_REMOVED = 'dhg_maquinaria.accionRemoved';

    /**
     * @var string
     */
    const ACCION_REMOVED_VERIFICATION = 'dhg_maquinaria.accionRemovedVerification';
    

    
}
