<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\MaquinariaClasif;
use Symfony\Component\EventDispatcher\Event;

class MaquinariaClasifRemovedEvent extends Event{
    private $maquinariaClasif;

    /**
     * @param MaquinariaClasif $maquinariaClasif 
     *      MaquinariaClasif
     */
    public function __construct(MaquinariaClasif $maquinariaClasif){
        $this->maquinariaClasif = $maquinariaClasif;
    }

    /**
     * @return MaquinariaClasif
     */
    public function getMaquinariaClasif(){
        return $this->maquinariaClasif;
    }
}
