<?php

namespace DHG\MaquinariaBundle\Events;

use DHG\MaquinariaBundle\Entity\Maquinaria;
use Symfony\Component\EventDispatcher\Event;

class MaquinariaEditedEvent extends Event{
    private $maquinaria;

    /**
     * @param Maquinaria $maquinaria 
     *      Maquinaria
     */
    public function __construct(Maquinaria $maquinaria){
        $this->maquinaria = $maquinaria;
    }

    /**
     * @return Maquinaria
     */
    public function getMaquinaria(){
        return $this->maquinaria;
    }
}
