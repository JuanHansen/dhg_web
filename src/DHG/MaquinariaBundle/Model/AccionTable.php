<?php
namespace DHG\MaquinariaBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class AccionTable
 *
 * @DataTable\Table(id="AccionTable"  )
 */
class AccionTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

    /**
     * @var string
     * @DataTable\Column(source="Accion.fecha", name="Fecha", stype="date-eu")
     * @DataTable\Format(dataFields={"value":"Accion.fecha"}, template="DHGcoreBundle:Datatable:cellDate.html.twig")
     */
    public $fecha;

   /**
     * @var string
     * @DataTable\Column(source="Accion.odometro", name="Odometro")
     * @DataTable\Format(dataFields={"valor":"Accion.odometro", "unidad_en_km":"Accion.maquinaria.odometro_km"}, template="DHGMaquinariaBundle:Table:CellOdometro.html.twig")
     * @DataTable\DefaultSort()
     */
    protected $odometro;

   /**
     * @var string
     * @DataTable\Column(source="Accion.maquinaria.nombre", name="Maquinaria")
     * @DataTable\DefaultSort()
     */
    protected $maquinaria;


   /**
     * @var string
     * @DataTable\Column(source="Accion.costo", name="Costo")
     * @DataTable\Format(dataFields={"value":"Accion.costo"},template="DHGcoreBundle:Datatable:cellPrice.html.twig")
     * @DataTable\DefaultSort()
     */
    protected $costo;

   /**
     * @var string
     * @DataTable\Column(source="null", name="Productos")
     * @DataTable\Format(dataFields={"accion":"Accion"}, template="DHGMaquinariaBundle:Table:CellProductos.html.twig")
     */
    protected $productos;


   /**
     * @var string
     * @DataTable\Column(source="Accion.type", name="Tipo")
     * @DataTable\Format(dataFields={"accion":"Accion"}, template="DHGMaquinariaBundle:Table:CellType.html.twig")
     * @DataTable\DefaultSort()
     */
    protected $type;

    /**
     * @var string
     * @DataTable\Column(source="Accion.detalle", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "Accion.maquinaria.nombre", "text":"Accion.detalle"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $detalle;

    /**
     * @var int
     * @DataTable\Column(source="Accion.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Accion"}, template="DHGMaquinariaBundle:Table:AccionCellAction.html.twig")
     */
    public $action;

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        $filtro->setParameter('eliminado',2);
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\MaquinariaBundle\Entity\Accion');
        $qb = $userRepository->createQueryBuilder('Accion')
                ->where('Accion.eliminado = false');;
        return $qb;
    }

}