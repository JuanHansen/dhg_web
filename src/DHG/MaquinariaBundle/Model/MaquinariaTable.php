<?php
namespace DHG\MaquinariaBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class MaquinariaTable
 *
 * @DataTable\Table(id="MaquinariaTable"  )
 */
class MaquinariaTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.imagen", name="Imagen")
     * @DataTable\Format(dataFields={"value":"Maquinaria.imagen"}, template="DHGcoreBundle:Datatable:cellImage.html.twig")
     */
    protected $imagen;
   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.nombre", name="Nombre")
     * @DataTable\DefaultSort()
     */
    protected $nombre;

   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.fabricante", name="Fabricante")
     * @DataTable\DefaultSort()
     */
    protected $fabricante;

   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.modelo", name="Modelo")
     * @DataTable\DefaultSort()
     */
    protected $modelo;
   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.anio", name="Año")
     * @DataTable\Format(dataFields={"value":"Maquinaria.anio"}, template="DHGcoreBundle:Datatable:cellDateOnlyYear.html.twig")
     * @DataTable\DefaultSort()
     */
    protected $anio;

    /**
     * @var string
     * @DataTable\Column(source="Maquinaria.precio_compra", name="Precio compra")
     * @DataTable\Format(dataFields={"value":"Maquinaria.precio_compra"}, template="DHGcoreBundle:Datatable:cellPrice.html.twig")
     */
   // public $precio_compra;




    /**
     * @var string
     * @DataTable\Column(source="Maquinaria.con_motor", name="Motor")
     * @DataTable\Format(dataFields={"value":"Producto.con_motor"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    public $con_motor;

     /**
     * @var string
     * @DataTable\Column(source="Maquinaria.clasificacion.nombre", name="Clasificacion")
     * @DataTable\DefaultSort()
     */
    protected $clasificacion;

   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.odometro", name="Odometro")
     * @DataTable\Format(dataFields={"valor":"Producto.odometro", "unidad_en_km":"Producto.odometro_km"}, template="DHGMaquinariaBundle:Table:CellOdometro.html.twig")
     * @DataTable\DefaultSort()
     */
    protected $odometro;

   /**
     * @var string
     * @DataTable\Column(source="Maquinaria.dominio", name="Dominio")
     * @DataTable\DefaultSort()
     */
   // protected $dominio;

    /**
     * @var string
     * @DataTable\Column(source="Maquinaria.detalle", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "nombre", "text":"Maquinaria.detalle"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $detalle;

    /**
     * @var int
     * @DataTable\Column(source="Maquinaria.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Maquinaria"}, template="DHGMaquinariaBundle:Table:MaquinariaCellAction.html.twig")
     */
    public $action;

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\MaquinariaBundle\Entity\Maquinaria');
        $qb = $userRepository->createQueryBuilder('Maquinaria');
        return $qb;
    }

}