<?php
namespace DHG\MaquinariaBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class MaquinariaClasifTable
 *
 * @DataTable\Table(id="MaquinariaClasifTable"  )
 */
class MaquinariaClasifTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="MaquinariaClasif.nombre", name="Nombre")
     * @DataTable\DefaultSort()
     */
    public $nombre;

    /**
     * @var string
     * @DataTable\Column(source="MaquinariaClasif.detalle", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "nombre", "text":"MaquinariaClasif.detalle"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $detalle;

    /**
     * @var int
     * @DataTable\Column(source="MaquinariaClasif.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"MaquinariaClasif", "sistema":"MaquinariaClasif.sistema"}, template="DHGMaquinariaBundle:Table:MaquinariaClasifCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\MaquinariaBundle\Entity\MaquinariaClasif');
        $qb = $userRepository->createQueryBuilder('MaquinariaClasif');
        return $qb;
    }

}