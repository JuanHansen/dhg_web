<?php
namespace DHG\PersonalBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="PersonalClasifications")
 */
class PersonalClasification extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $details;
    
     static public function getResourceEntityName(){
        return 'Clasificacion de Personal';
    }

    static public function getResourceIcon(){
        return 'users';
    }
 
    static public function getSeccionIcon(){
        return 'user';
    }

    static public function getResourceColor(){
        return '#085604';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'name' => 'Nombre',
                'details' => 'Detalles',
            );
    }


    public function __construct(){
        parent::__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        if(is_null($this->name))
            return "NULL";
        return (string) $this->name;
    }

    public function getName(){
	   return $this->name;
    }

    public function setName($name){
       $this->name = $name;
    }

    public function getDetails(){
	   return $this->details;
    }

    public function setDetails($details){
       $this->details = $details;
    }
}
