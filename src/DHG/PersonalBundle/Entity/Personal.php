<?php
namespace DHG\PersonalBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use DHG\ContactosBundle\Entity\Contacto;

/**
 * @ORM\Entity
 * @ORM\Table(name="Personals")
 */
class Personal extends MappedSuperclassBase
{

    
      /**
     * @ORM\ManyToOne(targetEntity="DHG\ContactosBundle\Entity\Contacto",cascade={"persist"})
     * @ORM\JoinColumn(name="fk_Contacto_id", referencedColumnName="id")
     */
    protected $contacto;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\PersonalBundle\Entity\PersonalClasification")
     * @ORM\JoinColumn(name="fk_clasification_id", referencedColumnName="id", nullable=false)
     */
    protected $clasification;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $price_hour;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $price_hour_extra;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $price_kg;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    protected $price_ha;


    public function __construct(){
        parent::__construct();
        $this->contacto = new Contacto();
        $this->contacto->__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString(){
        if($this->contacto!=null && $this->contacto->getNombre()!=null){
            return $this->contacto->getNombre();}
        else
          return "-";    
    }

    public function getName(){
      if($this->contacto!=null){
       return $this->contacto->getNombre();}
    }

    public function setName($nombre){
      if($this->contacto!=null){
       $this->contacto->setNombre($nombre);}
    }

    public function getClasification(){ 
       return $this->clasification;
    }

    public function setClasification($clasification){
       $this->clasification = $clasification;
    }
    
    public function getPriceHour(){
	   return $this->price_hour;
    }
    
    public function setPriceHour($price_hour){
       $this->price_hour = $price_hour;
    }

    public function getPriceHourExtra(){
	   return $this->price_hour_extra;
    }

    public function setPriceHourExtra($price_hour_extra){
       $this->price_hour_extra = $price_hour_extra;
    }

    public function getPriceKg(){
       return $this->price_kg;
    }
    
    public function setPriceKg($price_kg){
       $this->price_kg = $price_kg;
    }

    public function getPriceHa(){
       return $this->price_ha;
    }
    
    public function setPriceHa($price_ha){
       $this->price_ha = $price_ha;
    }

    public function setContacto($contacto){
      if($this->contacto!=null){
      $this->contacto = $contacto;}
    }
    public function getContacto(){
      if($this->contacto!=null){
      return $this->contacto;}
    }
}
