<?php

namespace DHG\PersonalBundle\Events;

use DHG\PersonalBundle\Entity\Personal;
use Symfony\Component\EventDispatcher\Event;

class PersonalCreatedEvent extends Event{
    private $personal;

    /**
     * @param Personal $personal 
     *      Personal created
     */
    public function __construct(Personal $personal){
        $this->personal = $personal;
    }

    /**
     * @return Personal
     */
    public function getPersonal(){
        return $this->personal;
    }
}
