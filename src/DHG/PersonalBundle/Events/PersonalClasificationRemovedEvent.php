<?php

namespace DHG\PersonalBundle\Events;

use DHG\PersonalBundle\Entity\PersonalClasification;
use Symfony\Component\EventDispatcher\Event;

class PersonalClasificationRemovedEvent extends Event{
    private $personal;

    /**
     * @param Personal $personal 
     *      Personal
     */
    public function __construct(PersonalClasification $personal){
        $this->personal = $personal;
    }

    /**
     * @return Personal
     */
    public function getPersonal(){
        return $this->personal;
    }
}
