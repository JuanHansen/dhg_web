<?php
namespace DHG\PersonalBundle\Events;

final class PersonalEvents {

    /**
     * @var string
     */
    const PERSONAL_CREATED = 'dhg_personal.personalCreated';

    /**
     * @var string
     */
    const PERSONAL_EDITED = 'dhg_personal.personalEdited';

    /**
     * @var string
     */
    const PERSONAL_REMOVED = 'dhg_personal.personalRemoved';

    /**
     * @var string
     */
    const PERSONAL_REMOVED_VERIFICATION = 'dhg_personal.personalRemovedVerification';

    /**
     * @var string
     */
    const PERSONALCLASIFICATION_CREATED = 'dhg_personal.personalClasificationCreated';

    /**
     * @var string
     */
    const PERSONALCLASIFICATION_REMOVED = 'dhg_personal.personalClasificationRemoved';

    /**
     * @var string
     */
    const PERSONALCLASIFICATION_EDITED = 'dhg_personal.personalClasificationEdited';

    /**
     * @var string
     */
    const PERSONALCLASIFICATION_REMOVED_VERIFICATION = 'dhg_personal.personalClasificationRemovedVerification';

}
