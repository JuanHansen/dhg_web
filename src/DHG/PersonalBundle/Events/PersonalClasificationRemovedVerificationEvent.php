<?php

namespace DHG\PersonalBundle\Events;

use DHG\PersonalBundle\Entity\PersonalClasification;
use Symfony\Component\EventDispatcher\Event;

class PersonalClasificationRemovedVerificationEvent extends Event{
    private $personalClasif;
    private $stoped; 
    private $messages;

    /**
     * @param PersonalClasification $personalClasif 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(PersonalClasification $personalClasif){
        $this->personalClasif = $personalClasif;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return personal
     */
    public function getPersonalClasif(){
        return $this->personalClasif;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return personal
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
