<?php
namespace DHG\PersonalBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class PersonalClasificationTable
 *
 * @DataTable\Table(id="PersonalClasificationTable"  )
 */
class PersonalClasificationTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="PersonalClasification.name", name="Nombre")
     * @DataTable\DefaultSort()
     */
    public $name;

    /**
     * @var string
     * @DataTable\Column(source="PersonalClasification.details", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "Detalle", "text":"PersonalClasification.details"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $details;

    /**
     * @var int
     * @DataTable\Column(source="PersonalClasification.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"PersonalClasification"}, template="DHGPersonalBundle:Table:ClasificationCellAction.html.twig")
     */
    public $action;

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\PersonalBundle\Entity\PersonalClasification');
        $qb = $userRepository->createQueryBuilder('PersonalClasification');
        return $qb;
    }

}