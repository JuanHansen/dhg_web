<?php

namespace DHG\PersonalBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use DHG\PersonalBundle\Entity\Personal;
use DHG\PersonalBundle\Entity\PersonalClasification;

class MenuEventsListener{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem('Tipo de Personal', array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
    }
}