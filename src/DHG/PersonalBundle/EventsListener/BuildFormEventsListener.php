<?php

namespace DHG\PersonalBundle\EventsListener;

use DHG\coreBundle\Event\BuildFormEvent;

class BuildFormEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param DHG\coreBundle\Events\BuildFormEvent $event
     */
    public function onBuildForm($event)
    {
        /*
    	if ($event->getFormName() === 'contactos'){

    		$builder = $event->getBuilder();
    		$builder->add('readonly', 'url', array(
                'label' => 'Personal',
				'required' => false,
                'read_only' => true,
                'mapped' => false,
            ));
            $event->setBuilder($builder);
    	}
        */
    }

}