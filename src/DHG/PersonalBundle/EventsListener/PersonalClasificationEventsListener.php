<?php

namespace DHG\PersonalBundle\EventsListener;

use DHG\PersonalBundle\Events\PersonalEvents;


class PersonalClasificationEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * Determina si existe almenos un personal dentro de la clasificacion. Si es asi, evita la eliminacion de esta
     *
     * @param DHG\PersonalBundle\Events\PersonalEventos $event
     */
    public function onPersonalClasificationRemovedVerification($event){
        $em = $this->entityManager;
        $id = $event->getPersonalClasif()->getId();
        $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array('clasification' => $id));
        if($personal != null){
            $event->stopRemove('Existe personal con esta clasificacion. Elimine primero el personal con esta clasificacion o reasignelo antes de proseguir.', 'Personal');
        }
    }


}
