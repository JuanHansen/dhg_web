<?php

namespace DHG\PersonalBundle\EventsListener;

use DHG\PersonalBundle\Events\PersonalEvents;
use DHG\PersonalBundle\EntityManager\PersonalManager;


class PersonalEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent $event
     */
    public function onContactoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array('contacto' => $idcontacto));
            if($personal!=null){
                $um = new PersonalManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeVerificationPersonal($personal,$error)){
                    return true;
                }else{
                    $event->stopRemove($error,'Personal');
                    return false;
                }
            }
        }
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedEvent $event
     */
    public function onContactoRemovedEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array('contacto' => $idcontacto));
            if($personal!=null){
                $um = new PersonalManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removePersonal($personal,$error)){
                    return true;
                }else{
                    //$error[]='No se pudo eliminar el personal';
                    $event->stopRemove($error,'Personal');
                    return false;
                }
            }
        }
    }



}
