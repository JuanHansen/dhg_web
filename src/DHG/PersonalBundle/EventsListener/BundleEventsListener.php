<?php
namespace DHG\PersonalBundle\EventsListener;

use DHG\PersonalBundle\Entity\PersonalClasification;

class BundleEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Metodo de instalacion
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    static public function onInstall($event)
    {
        $em = $event->getEntityManager();
        $logger = $event->getLogger();
        $logger->info('Install Personal');
       
        //Cargo clasificacion de personal
        $xml = simplexml_load_file(__DIR__.'/../Resources/static/PersonalClasif.xml');
        foreach($xml->children() as $child) {
            $movClasif = $em->getRepository('DHGPersonalBundle:PersonalClasification')->findOneByName($child->nombre);
            if (empty($movClasif)) {
                $obj = new PersonalClasification();
                $obj->setName((string)$child->nombre);
                $obj->setDetails((string)$child->detalle);
                try {
                    $em->persist($obj);
                } catch(Exception $e) {
                    $logger->error($e);
                }
            }
        }
        $em->flush();
    }

}
