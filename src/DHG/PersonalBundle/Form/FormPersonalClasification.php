<?php
namespace DHG\PersonalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormPersonalClasification extends AbstractType{
    private $name = 'Nueva_clasificacion_de_personal';
    /**
     * Builds the FormPersonal form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('name', 'text', array(
                'label' => 'Nombre',
                'attr' => array(
                    'placeholder' => 'Nombre clasificación',
                ),
                'constraints' =>  new NotBlank(array('message'=>'Es necesario ingresar un nombre')),
            ))

            ->add('details', 'textarea', array(
                'label' => 'Detalles',
                'required' => false
            ))

        ;
        $builder->setAttribute('show_legend', true); 

    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\PersonalBundle\Entity\PersonalClasification'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario 
     */
    public function __construct($name='Nueva_clasificacion_de_personal'){
        $this->name = $name;
    }
    
    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


