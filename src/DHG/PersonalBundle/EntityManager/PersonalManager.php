<?php

namespace DHG\PersonalBundle\EntityManager;

use DHG\PersonalBundle\Entity\Personal;
use Doctrine\ORM\EntityManager;

use DHG\PersonalBundle\Events\PersonalEvents;
use DHG\PersonalBundle\Events\PersonalCreatedEvent;
use DHG\PersonalBundle\Events\PersonalEditedEvent;
use DHG\PersonalBundle\Events\PersonalRemovedEvent;
use DHG\PersonalBundle\Events\PersonalRemovedVerificationEvent;

class PersonalManager{
	private $em;
	private $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la contacto
     *
	 * @param $contacto Personal con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createPersonal(Personal $contacto, &$error = null ){
        $event = new PersonalCreatedEvent($contacto);
        if($this->ed->dispatch(PersonalEvents::PERSONAL_CREATED, $event)){
        	try{
                $this->em->persist($contacto);
                $this->em->flush();
                
            } catch (\Exception $e){
            	$error[] = "Ocurrio un error al intentar crear el Personal";
            	return false;
            }
        }else{
            $error = $event->getMessages();
            return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos el cliente/proveedor
     *
	 * @param $contacto Personal con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
     public function editPersonal(Personal $contacto, &$error = null ){
       $event = new PersonalEditedEvent($contacto);
       if($this->ed->dispatch(PersonalEvents::PERSONAL_EDITED,$event) ){
            try{
                $this->em->persist($contacto);
                $this->em->flush();
            } catch (\Exception $e){
            	$error[] = "Ocurrio un error al intentar editar el Personal";
            	return false;
            }
        }else{
            $error = $event->getMessages();
            return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la contacto
     *
	 * @param $contacto Personal con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removePersonal(Personal $contacto, &$error = array() ){
    	if ($this->removeVerificationPersonal($contacto, $error)){
            $event = new PersonalRemovedEvent($contacto);
            if($this->ed->dispatch(PersonalEvents::PERSONAL_REMOVED, $event)){
    			try{
    	            $this->em->remove($contacto);
    	            $this->em->flush();
    	            
                } catch (\Exception $e){
    	        	$error[] = "Ocurrio un error al intentar eliminar el Personal";
    	        	return false;
    	        }
        	}else{
            	$error =array_merge($error,$event->getMessages());
            	return false;
        	}
        }else{
            return false;
        }
        return true;
    }  

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
     * @param $contacto Personal con la informacion a eliminar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al eliminado
     *
     */
    public function removeVerificationPersonal(Personal $contacto, &$error = array() ){
        $event = new PersonalRemovedVerificationEvent($contacto);
        $this->ed->dispatch(PersonalEvents::PERSONAL_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error =array_merge($error,$event->getMessages());
            return false;
        }
        return true;
    }  

}
