<?php

namespace DHG\PersonalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DHG\PersonalBundle\Entity\PersonalClasification;
use DHG\PersonalBundle\Form\FormPersonalClasification;
use DHG\PersonalBundle\Model\PersonalClasificationTable;

use DHG\PersonalBundle\Events\PersonalEvents;
use DHG\PersonalBundle\Events\PersonalClasificationCreatedEvent;
use DHG\PersonalBundle\Events\PersonalClasificationEditedEvent;
use DHG\PersonalBundle\Events\PersonalClasificationRemovedEvent;
use DHG\PersonalBundle\Events\PersonalClasificationRemovedVerificationEvent;
use DHG\PersonalBundle\EntityManager\PersonalManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\PersonalBundle\EventsListener\MenuEventsListener;
use DHG\ContactosBundle\Security\ContactosPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use DHG\coreBundle\Form\Type\FormEditableTableType;

class PersonalClasificationController extends Controller
{

    /**
     * Presenta el listado de clasificacion de personal
     * @Template("DHGPersonalBundle:Default:personalClasificationList.html.twig")
     */
    public function clasificationAction(Request $request){
        $clasifPersonal = new PersonalClasification();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::VERPERSONALCLASIFICACION, $clasifPersonal)) {
            throw new AccessDeniedException('No tiene acceso para acceder a la clasificacion de personal!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormPersonalClasification(), $clasifPersonal);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($clasifPersonal);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion de personal "%s" creada exitosamente!', $clasifPersonal->getName() ));
                $this->container->get('event_dispatcher')->dispatch(PersonalEvents::PERSONALCLASIFICATION_CREATED, new PersonalClasificationCreatedEvent($clasifPersonal));
                return $this->redirect($this->generateUrl('dhg_personal_claslistado'));
            }else{
                $showFormAtInit = true;
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('PersonalClasificationTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        $result = array('dataTable' => $dataTable);
        if ($this->get('security.context')->isGranted(ContactosPermissions::GESTIONPERSONALCLASIFICACION, $clasifPersonal)) {
        //Agrego menu
            $listener = new MenuEventsListener($this->container->get('event_dispatcher'));
            $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureMainMenu', array($listener, 'onMenuConfigureNavBarAddCreateItems'), -99);

    	      $result = array_merge($result, array(
                      'dataTable' => $dataTable,
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl('dhg_personal_claslistado'),
                      'form_submit_value' => 'Crear',
                      'form_class' => 'form-horizontal addPersonalClasification',
                      'form_show_at_init' => $showFormAtInit,
                      'dataTable_twigVars' => array(
                          'modalConfirmID' => $metaData['table']->id,
                          'modalConfirmTitle' => 'Atencion!',
                          'modalConfirmText' => 'Se procedera a eliminar esta clasificacion de personal. Esta operacion no puede deshacerse.',
                          'modalConfirmSubmitValue' => 'Eliminar',
                          'modalConfirmUrlAction' => $this->generateUrl('dhg_personal_clasremove',  array('id' => '#')),
                          'modalEditID' => 'edit'.$metaData['table']->id,
                       ),
                )
            );     
        }
        return $result;
	}

    /**
     * Elimina una clasificacion de personal
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeClasificationAction($id)  {
        $personalClas = new PersonalClasification();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONPERSONALCLASIFICACION, $personalClas)) {
              throw new AccessDeniedException('No tiene acceso para eliminar la clasificacion de personal!');
        }
        $em = $this->getDoctrine()->getManager();
        $personalClas = $em->getRepository('DHGPersonalBundle:PersonalClasification')->find($id);
        if (!$personalClas) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al borrar la clasificacion "%s" ', $id) );
        }else{
           $nombre = $personalClas->getName();
            $event = new PersonalClasificationRemovedVerificationEvent($personalClas);
            $this->container->get('event_dispatcher')->dispatch(PersonalEvents::PERSONALCLASIFICATION_REMOVED_VERIFICATION, $event);
            if ($event->isStoped()){
                foreach($event->getMessages() as $key => $val){
                   $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                }
            }else{
                $em->remove($personalClas);
                $em->flush();
                $this->container->get('event_dispatcher')->dispatch(PersonalEvents::PERSONALCLASIFICATION_REMOVED, new PersonalClasificationRemovedEvent($personalClas));
                $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion "%s" eliminada exitosamente!', $nombre ));
            }
        }
        return $this->redirect($this->generateUrl('dhg_personal_claslistado'));
    }


    /**
     * Edita un elemento del personal
     * 
     * @param Id ID del elemento
     */
    public function editClasificationAction($id) {
        $personalClas = new PersonalClasification();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONPERSONALCLASIFICACION, $personalClas)) {
              throw new AccessDeniedException('No tiene acceso para editar la clasificacion de personal!');
        }
 		    $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $personalClas = $em->getRepository('DHGPersonalBundle:PersonalClasification')->find($id);
        if ($personalClas){
          $form = $this->get('form.factory')->create( $formType = new FormPersonalClasification('Editar_clasificacion_de_personal'), $personalClas);

          if ($request->getMethod() == 'POST'){
              $form->bind($request);
              if ($form->isValid()){
                  $em = $this->getDoctrine()->getManager();
                  $em->persist($personalClas);
                  $em->flush();
                  $this->get('session')->getFlashBag()->add('success',sprintf('Clasificacion "%s" editada exitosamente!', $personalClas->getName() ));
                  $this->container->get('event_dispatcher')->dispatch(PersonalEvents::PERSONALCLASIFICATION_EDITED, new PersonalClasificationEditedEvent($personalClas));
                  return new Response("",Response::HTTP_OK);
              }
          }
          return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
              array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_personal_clasedit'),
                    'form_title' => 'Editar clasificacion '.$personalClas->getName(),
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editPersonalClasification',
                )
          ),Response::HTTP_CREATED);     
        }else{
           return new Response("No existe Clasificacion de Personal",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }


    /**
    * Muestra un elemento
    * 
    * @param Id ID del elemento
    * @return array
    */
    public function viewPersonalAction($id){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $personal = $em->getRepository('DHGPersonalBundle:Personal')->find($id);
        if ($personal){         
            return $this->render('DHGPersonalBundle:Modal:personal.html.twig',
                array(
                    'modalViewTitle' => 'Personal',
                    'content' => $personal,
                    )
                );  
        }else{
            return $this->render('DHGcoreBundle:Modal:modalError.html.twig',array('msg'=> "No existe "));
        };
    }

    /**
    * Crea una clasificacion de personal. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las personalClasifications
    * @return Formulario de creacion (200) si no hay parametros (POST) o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la personalClasification con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAjaxAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $personalClasification = new PersonalClasification();
        if (false === $this->get('security.context')->isGranted(ContactosPermissions::GESTIONPERSONALCLASIFICACION, $personalClasification)) {
            throw new AccessDeniedException('No tiene acceso para crear personalClasifications!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormPersonalClasification("Nueva_Clasificacion_de_Personal"), $personalClasification);
        $toR = null;
        if ($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()){
                $isok = true;
                try{
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($personalClasification);
                    $em->flush();
                }catch(\Exception $e){
                    $isok=false;
                }
                $error = null;
                if ($isok){
                    $toR = $this->getList();
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturn.html.twig',
                        array(
                            'listado' => $toR,
                            'selected'=>$personalClasification                            )
                        ),Response::HTTP_CREATED);
                    return $response;
                }else{
                    $toR=$error; 
                    $response = new Response($this->renderView('DHGcoreBundle:Form:formAjaxReturnError.html.twig',
                        array(
                            'error' => $toR,
                            )
                        ),Response::HTTP_ACCEPTED);
                    return $response;               
                }
            }else{
            }
        }
        return $this->render('DHGcoreBundle:Form:modularForms.html.twig',
            array(
                'modularForm' => $form->createView(),
                'formType' => $formType,
                'form_action' => $this->generateUrl('dhg_personal_personalClasificationCreate'),
                'form_title' => 'Crear Clasificacion de Personal',
                'form_submit_value' => 'Crear',
                'form_class' => 'form-horizontal',
                )
        ); 
    }
    /**
    * Retorna una lista de personalClasification    
    * @return array de personalClasification    
    */
    private function getList(){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('DHGPersonalBundle:PersonalClasification');
        $list = array();
        $query = $repo->createQueryBuilder('u')->getQuery();

        foreach($query->getResult() as $plu){
            $list[$plu->getId()]=$plu;
        }
        return $list;
    }

}
