<?php

namespace DHG\UserBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureHeaderRenderEvent;

class HeaderItemsEventsListener{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureHeaderRenderEvent $event
     */
    public function onHeaderItemConfigure($event)
    {
        $items = $event->getHeaderItems();
        $items->addElement('usermenu', 90, 'DHGUserBundle:Menu:user_menu.html.twig', true);


    }
}