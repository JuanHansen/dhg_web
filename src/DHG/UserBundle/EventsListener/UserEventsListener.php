<?php

namespace DHG\UserBundle\EventsListener;

class UserEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }
    /**
     *
     * @param DHG\DCCUserBundle\Events\UserEvents $event
     */
    public function onUserCreatedVerification($event){
        $em = $this->entityManager;
        $nombre = $event->getUser()->getUsername();
        $lab = $em->getRepository('DHGUserBundle:User')->findOneBy(array('username' => $nombre));
        if($lab != null){
            $event->stopRemove( sprintf('Existe un usuario que utiliza el nombre de "%s", por lo que no puede crearse uno con el mismo nombre.',$event->getUser()->getUsername() ), 'usernombre');
        }

        $email = $event->getUser()->getEmail();
        $lab = $em->getRepository('DHGUserBundle:User')->findOneBy(array('email' => $email));
        if($lab != null){
            $event->stopRemove( sprintf('Existe un usuario que utiliza el email "%s", por lo que no puede crearse uno con el mismo email.',$event->getUser()->getEmail() ), 'useremail');
        }
        
    }
    /**
     *
     * @param DHG\DCCUserBundle\Events\UserEvents $event
     */
    public function onUserEditedVerification($event){
        $em = $this->entityManager;
        $nombre = $event->getUser()->getUsername();
        $lab = $em->getRepository('DHGUserBundle:User')->findOneBy(array('username' => $nombre));
        if($lab != null&&$lab->getId()!=$event->getUser()->getId()){
            $event->stopRemove( sprintf('Existe un usuario que utiliza el nombre de "%s", por lo que no puede crearse uno con el mismo nombre.',$event->getUser()->getUsername() ), 'usernombre');
        }

        $email = $event->getUser()->getEmail();
        $lab = $em->getRepository('DHGUserBundle:User')->findOneBy(array('email' => $email));
        if($lab != null&&$lab->getId()!=$event->getUser()->getId()){
            $event->stopRemove( sprintf('Existe un usuario que utiliza el email "%s", por lo que no puede crearse uno con el mismo email.',$event->getUser()->getEmail() ), 'useremail');
        }
        
    }
}
