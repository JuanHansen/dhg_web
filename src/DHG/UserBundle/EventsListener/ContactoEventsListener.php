<?php

namespace DHG\UserBundle\EventsListener;

use DHG\UserBundle\Events\InventarioEvents;
use DHG\UserBundle\EntityManager\UserManager;

class ContactoEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent $event
     */
    public function onContactoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $user = $em->getRepository('DHGUserBundle:User')->findOneBy(array('contacto' => $idcontacto));
            if($user!=null){
                $um = new UserManager($em,$this->eventDispatcher);
                $error = array();
                $error[] = 'No se puede eliminar el contacto asociado a un usuario';
                $event->stopRemove($error,'User');
                return false;
                
            }
        }
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedEvent $event
     */
    public function onContactoRemovedEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $user = $em->getRepository('DHGUserBundle:User')->findOneBy(array('contacto' => $idcontacto));
            if($user!=null){
                $um = new UserManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeUser($user,$error)){
                    return true;
                }else{
                    $error[]='No se pudo eliminar el usuario';
                    $event->stopRemove($error,'User');
                    return false;
                }
            }
        }
    }

}
