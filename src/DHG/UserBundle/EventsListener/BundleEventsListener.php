<?php
namespace DHG\UserBundle\EventsListener;
use DHG\UserBundle\Event\PermissionEvents;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\Role;
use DHG\UserBundle\Components\DHGPermissionInterface;

class BundleEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Metodo de instalacion
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onInstall($event)
    {
        $em = $event->getEntityManager();
        $logger = $event->getLogger();
        $logger->info('Install User');
        $users = $em->getRepository('DHGUserBundle:User')->findAll();
        $res = $em->getRepository('DHGUserBundle:Role')->findOneByRole(DHGPermissionInterface::ROLE_USUARIO);
        if (empty($res)){
            $roleuser = new Role();
            $roleuser->setRole('Usuario');
            $roleuser->setSistema(true);
            $roleuser->setDetails('Rol del sistema, al que pertenecen todos los usuarios en el sistema');
            try {
                $em->persist($roleuser);
            } catch(Exception $e) {
                $logger->error($e);
            }
            foreach ($users as $key => $user) {
                $user->addRole($roleuser);
                try {
                    $em->persist($user);
                } catch(Exception $e) {
                    $logger->error($e);
                }
            }
            $em->flush();
        }

        $res = $em->getRepository('DHGUserBundle:Role')->findOneByRole(DHGPermissionInterface::ROLE_ADMIN);
        if (empty($res)){
            $roleadmin = new Role();
            $roleadmin->setRole('Administrador');
            $roleadmin->setSistema(true);
            $roleadmin->setDetails('Rol del sistema, al que se sugiere ser utilizado para los administradores.');

            try {
                $em->persist($roleadmin);
            } catch(Exception $e) {
                $logger->error($e);
            }
            foreach ($users as $key => $user) {
                $user->addRole($roleadmin);
                try {
                    $em->persist($user);
                } catch(Exception $e) {
                    $logger->error($e);
                }
            }
            $em->flush();
        }     

        $permissions = array();
        $permissions['permissions'] = array();
        $permissions['roleAssigment'] = array();
        $permissions['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array();
        $permissions['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array();

        $permEvent = new ConfigurePermissions($permissions);
        //Disparo el evento para buscar todos los permisos y persistirlos
        $this->eventDispatcher->dispatch(PermissionEvents::CONFIGURE_PERMISSIONS, $permEvent);
        $permissions = $permEvent->getPermissions();

        foreach ($permissions['permissions'] as $perm) {
            $res = $em->getRepository('DHGUserBundle:Permission')->findOneByMachinename($perm->getMachinename());
            if (empty($res)){
                $res = $em->getRepository('DHGUserBundle:Permission')->findOneByName($perm->getName());
            }
            if (empty($res)){
                $clas = $em->getRepository('DHGUserBundle:PermissionClasification')->findOneByMachinename($perm->getClasification()->getMachinename());
                if(!empty($clas)){
                    $perm->setClasification($clas);
                }
                $em->persist($perm);
                $em->flush();
            }
        }

        foreach ($permissions['roleAssigment'] as $rol => $permission) {
            $role = $em->getRepository('DHGUserBundle:Role')->findOneByRole($rol);
            if (!empty($role)){
                foreach ($permission as $perm) {
                    if (!$role->hasPermissions($perm)){
                        $logger->debug('No tiene el permiso' . $perm);
                        $permis = $em->getRepository('DHGUserBundle:Permission')->findOneByMachinename($perm);
                        if (!empty($permis)){
                            $role->addPermission($permis);
                            $em->persist($role);
                        }
                    }
                }
            }
        }
        try {
            $em->flush();
        } catch(Exception $e) {
            $logger->error($e);
        } catch(\Doctrine\DBAL\DBALException $e) {
            $logger->error($e->getPrevious()->getMessage());
        } catch(\PDOException $e) {
            $logger->error($e);
        }


    }

}
