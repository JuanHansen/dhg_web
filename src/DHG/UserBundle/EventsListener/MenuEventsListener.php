<?php

namespace DHG\UserBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use DHG\UserBundle\Security\UserPermissions;
use DHG\UserBundle\Entity\Permission;

class MenuEventsListener{

    protected $eventDispatcher;
    protected $contacto;
    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher,$contacto = false,$mensaje = "Agregar")
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->contacto = $contacto;
        $this->mensaje = $mensaje;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureAdminMenu($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        
        $MenuAnidado = $factory->createItem('Usuarios', array('route' => 'dhg_user_listado'))
        ->setExtra('orderNumber', 7)
        ->setAttribute('icon', 'user');
        
        $permission = new Permission();

        $MenuAnidado->addChild( $factory->createItem('Usuarios', array('route' => 'dhg_user_listado'))
                                        ->setAttribute('icon', 'users') 
                                        ->setExtra('orderNumber', 2)
                                        ->setExtra('permission', array('permission'=>UserPermissions::GESTIONARUSUARIOS, 'entity'=>$permission))

                    );

        $MenuAnidado->addChild( $factory->createItem('Roles', array('route' => 'dhg_user_roleslist'))
                                        ->setAttribute('icon', 'users')
                                        ->setExtra('orderNumber', 3)
                                        ->setExtra('permission', array('permission'=>UserPermissions::GESTIONARROLES, 'entity'=>$permission))
                     );

        $MenuAnidado->addChild( $factory->createItem('Permisos', array('route' => 'dhg_user_permissionslist'))
                                        ->setAttribute('icon', 'key') 
                                        ->setExtra('orderNumber', 4)
                                        ->setExtra('permission', array('permission'=>UserPermissions::GESTIONARPERMISOS, 'entity'=>$permission))
            );
        $MenuAnidado->addChild( $factory->createItem('Perfil', array('route' => 'dhg_user_perfilView'))
                                        ->setAttribute('icon', 'user') 
                                        ->setExtra('orderNumber', 5)
                                        ->setExtra('permission', array('permission'=>UserPermissions::VERUSUARIOS, 'entity'=>$permission))

                    );
        $parentMenu->addChild($MenuAnidado); 
        //$parentMenu->addChild($MenuPerfilado);    
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureAdminMenuAddCreateItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        $MenuAcciones->addChild($factory->createItem($this->mensaje, array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
    }
    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureAdminAddPerfilAccionsItems($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        $MenuAcciones = $parentMenu->getChild('action_menu');
        if($this->contacto){
            $MenuAcciones->addChild($factory->createItem('Editar', array('uri' => '#modal-create'))->setAttribute('data-toggle', 'modal'));
        }
        $MenuAcciones->addChild($factory->createItem('Cambiar Contraseña', array('uri' => '#modal-change-password'))->setAttribute('data-toggle', 'modal'));
        $MenuAcciones->addChild($factory->createItem('Cambiar Avatar', array('uri' => '#modal-change-avatar'))->setAttribute('data-toggle', 'modal'));
    }
}