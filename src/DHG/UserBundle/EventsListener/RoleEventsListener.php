<?php

namespace DHG\UserBundle\EventsListener;

class RoleEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\DCCUserBundle\Events\RoleEvents $event
     */
    public function onRoleRemovedVerification($event){
        $em = $this->entityManager;
        $id = $event->getRole()->getId();
        $lab = $em->getRepository('DHGUserBundle:User')->findOneBy(array('id' => $id));
        if($lab != null){
            $event->stopRemove( sprintf('Existe un usuario que utiliza el Rol de "%s", por lo que no puede eliminarse.',$event->getRole()->getName() ), 'role');
        }
        
    }
    /**
     *
     * @param DHG\DCCUserBundle\Events\RoleEvents $event
     */
    public function onRoleCreatedVerification($event){
        $em = $this->entityManager;
        $nombre = $event->getRole()->getRole();
        $lab = $em->getRepository('DHGUserBundle:Role')->findOneBy(array('role' => $nombre));
        if($lab != null&&$lab->getId()!=$event->getRole()->getId()){
            $event->stopRemove( sprintf('Existe un rol que utiliza el nombre de "%s", por lo que no puede crearse uno con el mismo nombre.',$event->getRole()->getRole() ), 'role');
        }
        
    }
}
