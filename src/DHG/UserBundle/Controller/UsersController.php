<?php

namespace DHG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DHG\UserBundle\DHGUserBundle;
use DHG\UserBundle\Form\FormCrearUser;
use DHG\UserBundle\Entity\User;
use DHG\UserBundle\Entity\Columna;
use DHG\UserBundle\EntityManager\UserManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\UserBundle\EventsListener\MenuEventsListener;
use DHG\UserBundle\Security\UserPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UsersController extends Controller
{
    /**
     * Presenta el listado de usuarios
     *
     */
    public function usersListAction(Request $request){
        $user = new User();
        if (false === $this->get('security.context')->isGranted(UserPermissions::VERUSUARIOS, $user)) {
            throw new AccessDeniedException('No tiene acceso a esta seccion!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormCrearUser($this->container->get('event_dispatcher')));
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $error = array();
                $um = new UserManager($em,$this->container->get('event_dispatcher'));
                if($um->createDataUser($form->getData(),$error,$this->get('security.encoder_factory'))){
                  $this->get('session')->getFlashBag()->add('success',sprintf('Usuario dado de alta exitosamente!' ));
                  return $this->redirect($this->generateUrl('dhg_user_listado'));
                }else{
                  foreach($error as $key => $val){
                      $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                  }  
                }
            }else{
                $this->get('session')->getFlashBag()->add('danger', 'Error al procesar el formulario.');
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('UserTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }

        $result = array('dataTable' => $dataTable);
         
        if ($this->get('security.context')->isGranted(UserPermissions::GESTIONARUSUARIOS, $user)) {
              //Agrego menu
              $listener = new MenuEventsListener($this->container->get('event_dispatcher'),null,"Usuario");
              $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureAdminMenu', array($listener, 'onMenuConfigureAdminMenuAddCreateItems'), -99);
        
              $result = array_merge($result, array(
                  'modularForm' => $form->createView(),
                  'formType' => $formType,
                  'form_action' => $this->generateUrl('dhg_user_listado'),
                  'form_submit_value' => 'Crear',
                  'form_class' => 'form-horizontal addUser',
                  'form_show_at_init' => $showFormAtInit,
                  'dataTable_twigVars' => array(
                      'modalConfirmID' => $metaData['table']->id,
                      'modalConfirmTitle' => 'Atencion!',
                      'modalConfirmText' => 'Se procedera a cambiar la activacion del user.',
                      'modalConfirmSubmitValue' => 'Aceptar',
                      'modalConfirmUrlAction' => $this->generateUrl('dhg_user_userremove',  array('id' => '#')),
                      'modalEditID' => 'edit'.$metaData['table']->id,
                      'modalViewID' => 'view'.$metaData['table']->id,
                )));   
        }else{
            $result = array_merge($result, array('dataTable_twigVars' => array('modalViewID' => 'view'.$metaData['table']->id)));
        } 

        return $this->render('DHGUserBundle:Default:usersList.html.twig',$result);     
    }

    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('DHGUserBundle:User')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARUSUARIOS, $user)) {
            throw new AccessDeniedException('No tiene permisos para editar usuarios!');
        }
        if ($user){
          $psw = $user->getPassword();
          $sal = $user->getSalt();
          $pswnull = false;
          $form = $this->get('form.factory')->create( $formType = new FormCrearUser($this->container->get('event_dispatcher'),'Editar_user',$user), $user);
          if ($request->getMethod() == 'POST'){
              $form->handleRequest($request);
              if ($form->isValid()){
                  if($user->getPassword()==null||$user->getPassword()==""){
                    $user->setPassword($psw);
                    $user->setSalt($sal);
                    $pswnull = true;
                  }
                  $um = new UserManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                  $error = [];
                  if ($um->editUser($user,$pswnull, $error,$this->get('security.encoder_factory'))){
                      $this->get('session')->getFlashBag()->add('success',sprintf('Usuario "%s" editado exitosamente!', $user->getName() ));
                  }else{
                      return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                  }
                  return new Response("",Response::HTTP_OK);
              }
          }
          
          return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
              array(
                    'modularForm' => $form->createView(),
                    'formType' => $formType,
                    'form_action' => $this->generateUrl('dhg_user_useredit'),
                    'form_title' => 'Editar user '.$user->getName(),
                    'form_submit_value' => 'Aplicar cambios',
                    'form_class' => 'form-horizontal editUser',
                )
          ),Response::HTTP_CREATED);     
        }else{
           return new Response("No existe el User",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }





    /**
    * Reactiva un usuario
    *@param Id ID del elemento a reactivar
    */
    public function reactivarAction($id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('DHGUserBundle:User')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARUSUARIOS, $user)) {
            throw new AccessDeniedException('No tiene permisos para editar usuarios!');
        }
        if (!$user) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al reactivar el user "%s". No existe. ', $id) );
        }else{
            $name = $user->getName() ;
            $um = new UserManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
              if ($um->reactivarUser($user, $error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('User "%s" reactivado exitosamente!', $name));
              }else{
                  foreach($error as $key => $val){
                    $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                  }
              }
        }
        return $this->redirect($this->generateUrl('dhg_user_listado'));
    }



    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('DHGUserBundle:User')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARUSUARIOS, $user)) {
            throw new AccessDeniedException('No tiene permisos para eliminar usuarios!');
        }
        if (!$user) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al desactivar el user "%s". No existe. ', $id) );
        }else{
          if(!$user->getActive()){
            return $this->reactivarAction($user->getID());
          }else{
            $name = $user->getName() ;
            $um = new UserManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
              if ($um->removeUser($user, $error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('User "%s" desactivado exitosamente!', $name));
              }else{
                  foreach($error as $key => $val){
                    $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                  }
              }
            }
        }
        return $this->redirect($this->generateUrl('dhg_user_listado'));
    }


    /**
    * Muestra un elemento
    * 
    * @param Id ID del elemento
    * @return array
    */
    public function viewAction($id){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('DHGUserBundle:User')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::VERUSUARIOS, $usuario)) {
            throw new AccessDeniedException('No tiene permisos ver informacion de otros usuarios!');
        }
        if ($usuario){         
          $request->attributes->add(array('id'=>$id)); 
          $personal = null;$mecanico=null;$clipro=null;
          if($usuario->getContacto()){
              $contacto = $usuario->getContacto();
              $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
              $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
              $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));
          }
          return $this->render('DHGUserBundle:Modal:user.html.twig',
              array(
                    'user'=>$usuario,
                    'modalViewTitle' => 'Usuario',
                    'content' => $usuario->getContacto(),
                    'personal' =>$personal,
                    'mecanico' =>$mecanico,
                    'clipro' =>$clipro,
                    'extra' => ['roles'=>$usuario->getRoles(),'email'=>$usuario->getEmail()],
                )
          );  
        }else{
           return $this->render('DHGcoreBundle:Modal:modalError.html.twig',array('msg'=> "No existe el usuario"));
        };
    }

    /**
    * Buscar y retorna la ultima posicion de la maquinaria con  el id $id
    * @param $table Tabla a la que corresponde la columna
    * @param $column Columna a la que se le asigna el valor
    * @param $value Valor de visibilidad de la columna (booleano)
    */
    public function columnValueAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $table = $request->request->get('tabla');
        $column = $request->request->get('numero');
        $value = $request->request->get('activo')=="true";

        $user = $this->getUser();
        $um = new UserManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
        $columnas = $user->getColumnas();
        $exist = false;
        foreach ($columnas as $columna) {
            if($columna->getTabla()==$table){
                if($columna->getNumero()==$column){
                    $columna->setActiva($value);
                    $exist = true;
                }
            }
        }
        if(!$exist){
            $newCol = new Columna();
            $newCol->setTabla($table);
            $newCol->setNumero($column);
            $newCol->setActiva($value);
            $columnas->add($newCol);
        }
        $error = array();
        $em->persist($user);
        $em->flush();
        return new Response();
    }
}
