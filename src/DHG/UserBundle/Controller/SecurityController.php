<?php

namespace DHG\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use DHG\UserBundle\Form\FormCreateAccount;
use Symfony\Component\HttpFoundation\Request;
use DHG\UserBundle\Entity\User;
use DHG\coreBundle\Entity\Account;
use Doctrine\ORM\Tools\SchemaTool;
use DHG\coreBundle\Event\BundleEvents;
use DHG\coreBundle\Event\ConfigureBundleEvent;
use Doctrine\DBAL\DBALException;
use DHG\UserBundle\Security\DHGUserToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends Controller
{
    public function loginAction()
    {
        
        $request = $this->container->get('request');
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session\Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            $error = $error->getMessage();
            $session->getFlashBag()->add('error',$error);
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');
        return $this->container->get('templating')->renderResponse('DHGUserBundle:Security:login.html.twig', array(
            //'last_account' => $lastAccount,
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token' => $csrfToken,
        ));
    }

    public function registerAction(Request $request)
    {
        $ed = $this->container->get('event_dispatcher');
        $form = $this->get('form.factory')->create( $formType = new FormCreateAccount($ed));
        $form->render_required_asterisk = true;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $formData = $form->getNormData();
                $em = $this->getDoctrine()->getManager();
                $this->get('doctrine.dbal.default_connection')->forceSwitchToDefault();
                $repo = $em->getRepository('DHGcoreBundle:Account');
                $q = $repo
                    ->createQueryBuilder('a')
                    ->select('a')
                    ->where('a.name = :name')
                    ->setParameter('name',  $formData['account'])
                    ->getQuery();
                $accountObj = $q->getOneOrNullResult();
                if ($accountObj!=NULL){
                    $this->get('session')->getFlashBag()->add('error', 'Nombre de cuenta existente. Por favor escoja otro nombre.');
                }else{
                    $account = new Account();
                    $account->setName($formData['account']);
                    $account->setVersion(1);
                    $prDBname = $this->container->getParameter('pre_db_name');
                    $account->setDb($prDBname.$formData['account']);
                    $account->setUsername($prDBname.$formData['account']);
                    $em->persist($account);
                    $em->flush();
                    echo "<br>Creada cuenta";
                    try{
                        //$em->getConnection()->exec( 'CREATE DATABASE '.$account->getDB() );
                        $this->get('doctrine.dbal.default_connection')->forceSwitchTOAccountDB($account->getDB(),$account->getUsername());
                        echo "<br>Cambiada a nueva base de datos";
                        $em2 = $this->getDoctrine()->getManager();
                        $schemaTool = new SchemaTool($em2);
                        $metadata = $em2->getMetadataFactory()->getAllMetadata();
                        $queryUpdate = $schemaTool->getUpdateSchemaSql($metadata,false);
                        echo "<br>Finalizado updateSchema";
                        foreach ($queryUpdate as $key => $value) {
                            echo '<br>Comando: '. $value;
                            try{
                                $this->container->get('doctrine.orm.entity_manager')->getConnection()->executeUpdate($value);
                            }catch(DBALException $e){
                                echo '<br> Error: '.$e->getMessage();
                            }
                        }
                        //Creo user
                        echo "<br>Finalizado execute Update";
                        $user = new User();
                        $user->setUsername($formData['username']);
                        $user->setPassword($formData['password']);
                        $user->setEmail($formData['email']);
                        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                        $user->setSalt(md5(time()));
                        $pass = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                        $user->setPassword($pass);
                        $user->setSuperadmin(FALSE);
                        $user->removeContacto();
                        $em2->persist($user);
                        $em2->flush();
                        echo "<br>Creado usuario principal";
                        //Creo superuser
                        $superUser = new User();
                        $superUser->setUsername("superadmin");
                        $superUser->setPassword("superadmin");
                        $superUser->setEmail("admin@distrubucionhg.com.ar");
                        $encoder = $this->get('security.encoder_factory')->getEncoder($superUser);
                        $superUser->setSalt(md5(time()));
                        $pass = $encoder->encodePassword($superUser->getPassword(), $superUser->getSalt());
                        $superUser->setPassword($pass);
                        $superUser->setSuperadmin(TRUE);
                        $superUser->removeContacto();
                        $em2->persist($superUser);
                        $em2->flush();
                        echo "<br>Creado superamdin";
                        //Logueo
                        $token = new DHGUserToken($user, $user->getPassword(), 'main', $account->getName(), $account->getDB(), $user->getSuperadmin(), $user->getRoles()); //TODO obtener el providerkey de la configuracion
                        $token->setUser($user);
                        $this->get("security.context")->setToken($token);
                        $event = new InteractiveLoginEvent($request, $token);
                        $ed->dispatch("security.interactive_login", $event);
                        //Dispatch evento de instalacion
                        echo "<br>Lanzado evento de instalacion";
                        $ed->dispatch(BundleEvents::CONFIGURE_BUNDLEINSTALL, new ConfigureBundleEvent($em2, $this->get('logger')));
                        $this->get('session')->getFlashBag()->add('success', 'Cuenta creada exitosamente!');
                        return $this->loginAction();
                    }catch( DBALException $e){
                        $this->get('session')->getFlashBag()->add('error', 'Error al crear la nueva cuenta. Contactese con el administrador');
                    }
                }
            }
        }
        return $this->render('DHGUserBundle:Security:register.html.twig',
            array(
                  'modularForm' => $form->createView(),
                  'formType' => $formType,
                  'form_action' => $this->generateUrl('dhg_user_register'),
                  'form_submit_value' => 'Crear nueva cuenta',
                  'form_class' => 'form-horizontal createAccount',            
           ));        
    }

}