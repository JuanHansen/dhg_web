<?php

namespace DHG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use DHG\UserBundle\DHGUserBundle;
use DHG\UserBundle\Form\FormPermissions;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\EntityManager\PermissionManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\UserBundle\EventsListener\MenuEventsListener;
use DHG\UserBundle\Security\UserPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PermissionsController extends Controller
{
    public function listAction(Request $request){
       $Permission = new Permission();
       if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARPERMISOS, $Permission)) {
            throw new AccessDeniedException('No tiene acceso a esta seccion!');
       }
       $form = $this->get('form.factory')->create( $formType = new FormPermissions($this->getDoctrine()->getManager()));
       $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $um = new PermissionManager($em,$this->container->get('event_dispatcher'));
                $error = array();
                if($um->setPermissions($form->getData(),$error)){
                  $this->get('session')->getFlashBag()->add('success','Permisos guardados exitosamente');
                  return $this->redirect($this->generateUrl('dhg_user_permissionslist'));
                }else{
                    foreach($error as $key => $val){
                       $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                    }  
                }
            }else{
                $this->get('session')->getFlashBag()->add('danger', 'Error al procesar el formulario.');
            }
        }
        return $this->render('DHGUserBundle:Default:permissionsTable.html.twig',
            array(
                  'roles' => $formType->getRolesShowed(),
                  'form' => $form->createView(),
                  'formType' => $formType,
                  'form_action' => $this->generateUrl('dhg_user_permissionslist'),
                  'form_submit_value' => 'Guardar',
                  'form_class' => 'form-horizontal setPermissions',                 
            )
        );      

    }

}
