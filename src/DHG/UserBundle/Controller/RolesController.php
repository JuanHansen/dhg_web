<?php

namespace DHG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DHG\UserBundle\Form\FormCrearRole;
use DHG\UserBundle\EntityManager\RoleManager;
use DHG\UserBundle\Entity\Role;
use DHG\UserBundle\Events\UserEvents;
/*
use DHG\UserBundle\Events\RoleCreatedEvent;
use DHG\UserBundle\Events\RoleRemovedEvent;
use DHG\UserBundle\Events\RoleEditedEvent;
use DHG\UserBundle\Events\RoleRemovedVerificationEvent;
use DHG\UserBundle\Events\RoleRemovedVerificationEvent;
*/
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\UserBundle\EventsListener\MenuEventsListener;
use DHG\UserBundle\Security\UserPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RolesController extends Controller
{
    /**
     * Presenta el listado de roles
     *
     */
    public function listAction(Request $request)
    {
        $role = new Role();
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARROLES, $role)) {
            throw new AccessDeniedException('No tiene acceso a esta seccion!');
        }
        $form = $this->get('form.factory')->create( $formType = new FormCrearRole(), $role);
        $form->render_required_asterisk = true;
        $showFormAtInit = false;
        if ($request->getMethod() == 'POST'){
            $form->bind($request);
            if ($form->isValid()){
              $em = $this->getDoctrine()->getManager();
              $um = new RoleManager($em,$this->container->get('event_dispatcher'));
              $error = array();
              if($um->createRole($role,$error)){
                  $this->get('session')->getFlashBag()->add('success',sprintf('Rol "%s" dado de alta exitosamente!', $role->getName() ));
                  return $this->redirect($this->generateUrl('dhg_user_roleslist'));
              }else{
                  foreach($error as $key => $val){
                     $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
                  }  
              }
            }else{
                $this->get('session')->getFlashBag()->add('danger', 'Error al procesar el formulario.');
            }
        }

        $dataTable = $this->get('data_tables.manager')->getTable('RoleTable');
        $metaData = $dataTable->getMetaData();
        if ($response = $dataTable->ProcessRequest($request)) {
            return $response;
        }
        
        $result = array('dataTable' => $dataTable);
         
        if ($this->get('security.context')->isGranted(UserPermissions::GESTIONARROLES, $role)) {
              //Agrego menu
              $listener = new MenuEventsListener($this->container->get('event_dispatcher'),null,"Rol");
              $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureAdminMenu', array($listener, 'onMenuConfigureAdminMenuAddCreateItems'), -99);
        
              $result = array_merge($result, array(
                  'modularForm' => $form->createView(),
                  'formType' => $formType,
                  'form_action' => $this->generateUrl('dhg_user_roleslist'),
                  'form_submit_value' => 'Crear',
                  'form_class' => 'form-horizontal addRoler',
                  'form_show_at_init' => $showFormAtInit,
                  'dataTable_twigVars' => array(
                      'modalConfirmID' => $metaData['table']->id,
                      'modalConfirmTitle' => 'Atencion!',
                      'modalConfirmText' => 'Se procedera a eliminar el rol. Esta operacion no puede deshacerse.',
                      'modalConfirmSubmitValue' => 'Eliminar',
                      'modalConfirmUrlAction' => $this->generateUrl('dhg_user_roleremove',  array('id' => '#')),
                      'modalEditID' => 'edit'.$metaData['table']->id,
                )));   
        }

        return $this->render('DHGUserBundle:Default:rolesList.html.twig',$result);       
    }


    /**
     * Edita un elemento
     * 
     * @param Id ID del elemento
     *
     */
    public function editAction($id){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('DHGUserBundle:Role')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARROLES, $role)) {
            throw new AccessDeniedException('No tiene permisos para editar un rol!');
        }
        if ($role){
          if($role->getSistema()==0){
            $form = $this->get('form.factory')->create( $formType = new FormCrearRole('Editar_rol'), $role);
            if ($request->getMethod() == 'POST'){
                $form->handleRequest($request);
                if ($form->isValid()){
                    $um = new RoleManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = [];
                    if ($um->editRole($role, $error)){
                        $this->get('session')->getFlashBag()->add('success',sprintf('Rol "%s" editado exitosamente!', $role->getName() ));
                    }else{
                        return new Response(implode($error,"<br>"),Response::HTTP_ACCEPTED);
                    }
                    return new Response("",Response::HTTP_OK);
                }
            }
            
            return new Response($this->renderView('DHGcoreBundle:Form:modularForms.html.twig',
                array(
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl('dhg_user_roleedit'),
                      'form_title' => 'Editar role '.$role->getRole(),
                      'form_submit_value' => 'Aplicar cambios',
                      'form_class' => 'form-horizontal editRole',
                  )
            ),Response::HTTP_CREATED); 
          }else{
              return new Response("No se puede editar un Rol del Sistema",Response::HTTP_INTERNAL_SERVER_ERROR);
          } 
        }else{
           return new Response("No existe el Rol",Response::HTTP_INTERNAL_SERVER_ERROR);
        };
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $em = $this->getDoctrine()->getManager();
        $role = $em->getRepository('DHGUserBundle:Role')->find($id);
        if (false === $this->get('security.context')->isGranted(UserPermissions::GESTIONARROLES, $role)) {
            throw new AccessDeniedException('No tiene permisos para eliminar un rol!');
        }
        if (!$role) {
            $this->get('session')->getFlashBag()->add('danger', sprintf('Error al eliminar el role "%s". No existe. ', $id) );
        }else{
            $name = $role->getName() ;
            $um = new RoleManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
            $error = array();
            if ($um->removeRole($role, $error)){
              $this->get('session')->getFlashBag()->add('success',sprintf('Role "%s" eliminado exitosamente!', $name));
            }else{
               foreach($error as $key => $val){
                  $this->get('session')->getFlashBag()->add('danger', sprintf('%s', $val) );
               }
            }
        }
        return $this->redirect($this->generateUrl('dhg_user_roleslist'));
    }

}
