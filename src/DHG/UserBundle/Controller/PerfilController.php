<?php

namespace DHG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use DHG\UserBundle\DHGUserBundle;
use DHG\UserBundle\Form\FormCrearUser;
use DHG\UserBundle\Form\FormAvatar;
use DHG\ContactosBundle\Form\FormContacto;
use DHG\UserBundle\Form\FormPerfilPassword;
use DHG\UserBundle\Entity\User;
use DHG\UserBundle\Entity\UserAvatar;
use DHG\ContactosBundle\Entity\Contacto;
use DHG\ContactosBundle\EntityManager\ContactoManager;
use DHG\UserBundle\EntityManager\PerfilManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use DHG\UserBundle\EventsListener\MenuEventsListener;
use DHG\UserBundle\Security\UserPermissions;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PerfilController extends Controller
{
    /**
     * muestra el perfil del usuario
     * @Template ("DHGUserBundle:Default:perfilView.html.twig")
     */
    public function viewAction(Request $request){
        $user = $this->getUser();
        $contacto = $user->getContacto();
        if (false === $this->get('security.context')->isGranted(UserPermissions::VERUSUARIOS, $user)) {
            throw new AccessDeniedException('No tiene acceso para ver el perfil!');
        }

        $rutas_ajax = array('grupo_create'=>$this->generateUrl('dhg_contactos_grupoCreate'),
                            'personalClasification_create'=>$this->generateUrl('dhg_personal_personalClasificationCreate'));
        $form = $this->get('form.factory')->create( $formType =  new FormContacto($this->container->get('event_dispatcher'),"Editar_contacto",$this->getDoctrine()->getManager(),$contacto,$rutas_ajax), $contacto);
        $form->render_required_asterisk = true;

        $formPassword = $this->get('form.factory')->create( $formPasswordType = new FormPerfilPassword("Cambio_Password"));
        $formPassword->render_required_asterisk = true;
        $avatar = new UserAvatar();
        $formAvatar = $this->get('form.factory')->create( $formAvatarType = new FormAvatar("Asignar_Avatar"),$avatar);
        $formAvatar->render_required_asterisk = true;

        $showFormAtInit = false;
        $showFormPasswordAtInit = false;
        $showFormAvatarAtInit = false;

        if ($request->getMethod() == 'POST'){
            $data = $request->request->all();
            if (isset($data['Editar_contacto'])) {
                $form->bind($request);
                if ($form->isValid()){
                    $um = new ContactoManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = [];
                    if ($um->editContacto($contacto,$form, $error,$this->get('security.context'))){
                        $this->get('session')->getFlashBag()->add('success',sprintf('Info de perfil "%s" editada exitosamente!', $contacto->getNombre() ));
                        foreach ($error as $key => $value) {
                            $this->get('session')->getFlashBag()->add('warning', $value);
                        }
                    }else{
                        foreach ($error as $key => $value) {
                            $this->get('session')->getFlashBag()->add('danger', $value);
                        }
                    }
                    return $this->redirect($this->generateUrl('dhg_user_perfilView'));
                }else{
                    $showFormAtInit = true;
                }
            }
            if (isset($data['Cambio_Password'])) {
                $formPassword->bind($request);
                if ($formPassword->isValid()){
                    $passVieja = $formPassword->get('passOriginal')->getData();
                    $passNueva = $formPassword->get('passNueva')->getData();
                    $um = new PerfilManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = array();
                      if ($um->changePassword($user,$passVieja,$passNueva, $error,$this->get('security.encoder_factory'))){
                           $this->get('session')->getFlashBag()->add('success',sprintf('Contraseña cambiada exitosamente!'));
                      }else{
                          foreach ($error as $key) {
                              $this->get('session')->getFlashBag()->add('danger',sprintf('%s',$key));
                          }
                      }
                        return $this->redirect($this->generateUrl('dhg_user_perfilView'));
                }else{
                    $showFormPasswordAtInit = true;
                }
            }
            if (isset($data['Asignar_Avatar'])) {
                $formAvatar->bind($request);
                if ($formAvatar->isValid()){
                    $um = new PerfilManager($this->getDoctrine()->getManager(), $this->container->get('event_dispatcher'));
                    $error = array();
                      if ($um->changeAvatar($user,$avatar, $error,$this->get('security.encoder_factory'))){
                           $this->get('session')->getFlashBag()->add('success',sprintf('Avatar cambiado exitosamente!'));
                      }else{
                          foreach ($error as $key) {
                              $this->get('session')->getFlashBag()->add('danger',sprintf('%s',$key));
                          }
                      }
                        return $this->redirect($this->generateUrl('dhg_user_perfilView'));
                }else{
                    $showFormAvatarAtInit = true;
                }
            }
        }
        $em = $this->getDoctrine()->getManager();
        if($contacto!=null){
            $personal = $em->getRepository('DHGPersonalBundle:Personal')->findOneBy(array("contacto"=>$contacto->getId()));
            $mecanico = $em->getRepository('DHGMaquinariaBundle:Mecanico')->findOneBy(array("contacto"=>$contacto->getId()));
            $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array("contacto"=>$contacto->getId()));
        }else{
            $personal = null;
            $mecanico = null;
            $clipro = null;
        }
        $extra = array();
        $extra['email']=$user->getEmail();
        $extra['roles']=$user->getRoles();
        $result = array('user' => $user,
                        'content' => $user->getContacto(),
                        'personal' =>$personal,
                        'mecanico' =>$mecanico,
                        'clipro' =>$clipro,
                        'extra' => $extra);

        if ($this->get('security.context')->isGranted(UserPermissions::VERUSUARIOS, $user)) {
             // if($contacto){
                //Agrego menu
                $listener = new MenuEventsListener($this->container->get('event_dispatcher'),$contacto);
                $this->container->get('event_dispatcher')->addListener('dhg_core.menuConfigureAdminMenu', array($listener, 'onMenuConfigureAdminAddPerfilAccionsItems'), -99);
              //}
            $formActivo = array('personal'=>false,'cliente'=>false,'proveedor'=>false,'mecanico'=>false);
            $result = array_merge($result, array(
                      'modularForm' => $form->createView(),
                      'formType' => $formType,
                      'form_action' => $this->generateUrl('dhg_user_perfilView'),
                      'form_submit_value' => 'Aplicar Cambios',
                      'form_class' => 'form-horizontal addContacto',
                      'formActivo' => $formActivo,
                      'form_edditing'=>"_create",
                      'form_show_at_init' => $showFormAtInit,
                      )                  
               ); 
               $result = array_merge($result, array(
                        'modularFormPassword'=> $formPassword->createView(),
                        'formPasswordType' => $formPasswordType,
                        'formPassword_action' => $this->generateUrl('dhg_user_perfilView'),
                        'formPassword_submit_value' => 'Cambio Password',
                        'formPassword_class' => 'form-horizontal changePassword',
                        'formPassword_show_at_init' => $showFormPasswordAtInit,
                )
              ); 
              $result = array_merge($result, array(
                        'modularFormAvatar'=> $formAvatar->createView(),
                        'formAvatarType' => $formAvatarType,
                        'formAvatar_action' => $this->generateUrl('dhg_user_perfilView'),
                        'formAvatar_submit_value' => 'Cambiar Avatar',
                        'formAvatar_class' => 'form-horizontal changeAvatar',
                        'formAvatar_show_at_init' => $showFormAvatarAtInit,
                )
              ); 
            }     
          return $result;
    }


}
