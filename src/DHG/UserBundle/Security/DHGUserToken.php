<?php
namespace DHG\UserBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class DHGUserToken extends AbstractToken 
{
    private $account;
    private $accountDB;
    private $superadmin;
    private $credentials;
    private $providerKey;

    public function __construct($user, $credentials, $providerKey, $account,  $accountDB = NULL, $superadmin = FALSE, $roles = array()){
        parent::__construct($roles);

        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }

        $this->setUser($user);
        $this->credentials = $credentials;
        $this->providerKey = $providerKey;
        $this->account = $account;
        $this->accountDB = $accountDB;
        $this->superadmin = $superadmin;

        parent::setAuthenticated(count($roles) > 0);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($isAuthenticated)
    {
        if ($isAuthenticated) {
            throw new \LogicException('Cannot set this token to trusted after instantiation.');
        }

        parent::setAuthenticated(false);
    }

    public function isSuperAdmin(){
        return $this->superadmin;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * Returns the provider key.
     *
     * @return string The provider key
     */
    public function getProviderKey()
    {
        return $this->providerKey;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        parent::eraseCredentials();

        $this->credentials = null;
    }

    public function getAccount(){
    	return $this->account;
    }

    public function setAccount($account){
    	$this->account = $account;
    }

    public function getAccountDB(){
    	return $this->accountDB;
    }

    public function setAccountDB($accountDB){
    	$this->accountDB = $accountDB;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(array($this->credentials, $this->providerKey, $this->account, $this->accountDB, $this->superadmin, parent::serialize()));
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list($this->credentials, $this->providerKey, $this->account, $this->accountDB, $this->superadmin, $parentStr) = unserialize($serialized);
        parent::unserialize($parentStr);
    }
}