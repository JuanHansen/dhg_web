<?php
namespace DHG\UserBundle\Security;

use Idb\SecurityBundle\Entity\SecurityLogin;
use Idb\ConfigBundle\Config\IdbConfig;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;
use Utwente\SecurityBundle\Security\DHGUserToken;
 
class DHGAuthListener extends AbstractAuthenticationListener
{
  public function __construct(SecurityContextInterface $securityContext, AuthenticationManagerInterface $authenticationManager,
                              SessionAuthenticationStrategyInterface $sessionStrategy, HttpUtils $httpUtils,
                              $providerKey, AuthenticationSuccessHandlerInterface $successHandler,
                              AuthenticationFailureHandlerInterface $failureHandler, array $options = array(),
                              LoggerInterface $logger, UserProviderInterface $userProvider)
  {
        // Needed to successfully use parent methods
        parent::__construct($securityContext, $authenticationManager, $sessionStrategy, $httpUtils, $providerKey, $successHandler, $failureHandler, $options, $logger);

        if($options["remember_me"]){
            // The 'main' option comes from the configured firewall
            $this->setRememberMeServices(new TokenBasedRememberMeServices(array($userProvider), 'main', $providerKey, array(), $logger));
        }
        $this->logger->debug("DHGLogin: atuhlistener");
  }


  public function attemptAuthentication(Request $request)
  {
        $account = trim($request->request->get('_account', NULL));
        $username = trim($request->request->get('_username', NULL));
        $password = $request->request->get('_password', NULL);
     
         
        if ($account == NULL || $username == NULL || $password == NULL) {
          $request->getSession()->set(SecurityContextInterface::AUTHENTICATION_ERROR,
            new AuthenticationException("Complete todos los campos.");
          $request->getSession()->set(SecurityContextInterface::LAST_USERNAME, $username);
          return null;
        }
        $this->logger->debug("DHGLogin: $account");
        $this->logger->debug("DHGLogin: $username");
     
        $token = new DHGUserToken();
        $token->setAccount($account);
        $token->setUser(new SecurityLogin($username, $password));
     
        // Try to authenticate by retrieving an authenticated token from the manager, catch any authenticationexception
        try {
          $authToken = $this->authenticationManager->authenticate($token);
          $this->logger->debug(sprintf("DHG Autenticacion exitosa Token: %s", $authToken));
     
          // Set the default event_id in the session
          $request->getSession()->set('event_id', $this->idbConfig->getVal('default_event_id'));
     
          return $authToken;
     
        } catch (AuthenticationException $failed) {
          $this->logger->debug("DHG Authentication failed.");
    
          $request->getSession()->set(SecurityContextInterface::LAST_USERNAME, $username);
          $request->getSession()->set(SecurityContext::AUTHENTICATION_ERROR, $failed);
     
          return null;
        }
    }

}