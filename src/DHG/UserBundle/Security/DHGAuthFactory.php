<?php
namespace DHG\UserBundle\Security;
 
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategy;
 
class DHGAuthFactory extends AbstractFactory{
 
  // Needed to add the option to the standard passed config
  public function __construct(){
    $this->options['remember_me'] = false;
    $this->addOption('account_parameter', '_account');
    $this->addOption('username_parameter', '_username');
    $this->addOption('password_parameter', '_password');
    $this->addOption('csrf_parameter', '_csrf_token');
    $this->addOption('intention', 'authenticate');
    $this->addOption('post_only', true);
  }
 
  public function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId){
    $providerId = $this->getProviderKey().'.'.$id;
    $container
        ->setDefinition($providerId, new DefinitionDecorator($this->getProviderKey()))
        ->replaceArgument(0, new Reference($userProviderId))
        ->replaceArgument(2, $id)
    ;
    return $providerId;
  }
 
  public function getListenerId(){
    print "\<pre\>";
      \Doctrine\Common\Util\Debug::dump('dhg_user.security.authentication.listener');
      print "\</pre\>";
      die;
    return 'dhg_user.security.authentication.listener';
  }
 
  /**
   * Return the position of the authentication
   * @return string
   */
  public function getPosition()
  {
    return 'form';
  }
 

  /**
   * Return the key which needs to be registered in the security context
   * @return string
   */
  public function getKey()
  {
    return 'dhg_user';
  }
 
  public function getProviderKey(){
    return 'dhg_user.security.authentication.provider';
  }

}