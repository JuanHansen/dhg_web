<?php
namespace DHG\UserBundle\Security;

use DHG\UserBundle\Components\DHGPermissionVoter;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Components\DHGPermissionInterface;

class UserPermissions extends DHGPermissionVoter
{
    const PREFIX = 'users';
    const VERUSUARIOS = 'users_ver_usuarios';
    const GESTIONARUSUARIOS = 'users_gestionar_usuarios';
    const GESTIONARROLES = 'users_gestionar_roles';
    const GESTIONARPERMISOS = 'users_gestionar_permisos';
    const ACCESOSMENUADMIN = 'users_acceso_menu_administracion';

    /**
     * {@inheritdoc}
     */
    public function supportsAttribute($attribute)
    {
        return 0 === strpos($attribute, PREFIX);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedClasses(){
        $supportedClasses = array();
        $supportedClasses[] = 'DHG\UserBundle\Entity\User';
        $supportedClasses[] = 'DHG\UserBundle\Entity\Role';
        $supportedClasses[] = 'DHG\UserBundle\Entity\Permission';
        $supportedClasses[] = 'DHG\UserBundle\Entity\PermissionClasification';
        return $supportedClasses;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissions(ConfigurePermissions $event){
        $perm = $event->getPermissions();

        $permissionClasif = array();
        $permissionClasif[UserPermissions::PREFIX] = new PermissionClasification();
        $permissionClasif[UserPermissions::PREFIX]->setMachinename('users');
        $permissionClasif[UserPermissions::PREFIX]->setName('Gestion de usuarios');
        $permissionClasif[UserPermissions::PREFIX]->setDetails('Gestion de usuarios, roles y permisos');
        $permissions = array();
        $permissions[UserPermissions::VERUSUARIOS] = new Permission();
        $permissions[UserPermissions::VERUSUARIOS]->setMachinename(UserPermissions::VERUSUARIOS);
        $permissions[UserPermissions::VERUSUARIOS]->setName('Ver informacion de usuarios');
        $permissions[UserPermissions::VERUSUARIOS]->setDetails('Permite a los usuarios ver informacion de otros usuarios.');
        $permissions[UserPermissions::VERUSUARIOS]->setModule(UserPermissions::PREFIX);
        $permissions[UserPermissions::VERUSUARIOS]->setClasification($permissionClasif['users']);

        $permissions[UserPermissions::GESTIONARUSUARIOS] = new Permission();
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setMachinename(UserPermissions::GESTIONARUSUARIOS);
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setName('Gestionar usuarios');
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setDetails('Permite a los usuarios dar de alta, modificar y bloquear otros usuarios');
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setDepends($permissions[UserPermissions::VERUSUARIOS]);
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setModule(UserPermissions::PREFIX);
        $permissions[UserPermissions::GESTIONARUSUARIOS]->setClasification($permissionClasif['users']);

        $permissions[UserPermissions::GESTIONARROLES] = new Permission();
        $permissions[UserPermissions::GESTIONARROLES]->setMachinename(UserPermissions::GESTIONARROLES);
        $permissions[UserPermissions::GESTIONARROLES]->setName('Gestionar roles');
        $permissions[UserPermissions::GESTIONARROLES]->setDetails('Permite a los usuarios crear, editar y eliminar roles');
        $permissions[UserPermissions::GESTIONARROLES]->setModule(UserPermissions::PREFIX);
        $permissions[UserPermissions::GESTIONARROLES]->setClasification($permissionClasif['users']);

        $permissions[UserPermissions::GESTIONARPERMISOS] = new Permission();
        $permissions[UserPermissions::GESTIONARPERMISOS]->setMachinename(UserPermissions::GESTIONARPERMISOS);
        $permissions[UserPermissions::GESTIONARPERMISOS]->setName('Gestionar permisos');
        $permissions[UserPermissions::GESTIONARPERMISOS]->setDetails('Permite a los usuarios modificar la asignacion de permisos a roles');
        $permissions[UserPermissions::GESTIONARPERMISOS]->setModule(UserPermissions::PREFIX);
        $permissions[UserPermissions::GESTIONARPERMISOS]->setClasification($permissionClasif['users']);

        $permissions[UserPermissions::ACCESOSMENUADMIN] = new Permission();
        $permissions[UserPermissions::ACCESOSMENUADMIN]->setMachinename(UserPermissions::ACCESOSMENUADMIN);
        $permissions[UserPermissions::ACCESOSMENUADMIN]->setName('Permite el acceso al menu de administracion');
        $permissions[UserPermissions::ACCESOSMENUADMIN]->setDetails('Permite a los usuarios ver el menu de administracion.');
        $permissions[UserPermissions::ACCESOSMENUADMIN]->setModule(UserPermissions::PREFIX);
        $permissions[UserPermissions::ACCESOSMENUADMIN]->setClasification($permissionClasif['users']);

        $perm['permissions'] = array_merge($perm['permissions'], $permissions);

        $perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO], array(UserPermissions::VERUSUARIOS ));
        $perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array_merge($perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN], array(UserPermissions::VERUSUARIOS,UserPermissions::GESTIONARUSUARIOS,UserPermissions::ACCESOSMENUADMIN));

        $event->setPermissions($perm);
    }


}