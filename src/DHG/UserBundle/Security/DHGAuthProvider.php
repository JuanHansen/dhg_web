<?php
namespace DHG\UserBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use DHG\UserBundle\Security\DHGUserToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
 
class DHGAuthProvider implements SimpleFormAuthenticatorInterface
{
    private $em;
    private $logger;
    private $db_connection;
    private $encoderFactory;
 
    /**
     * Constructor
     * @param \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Symfony\Component\HttpKernel\Log\LoggerInterface $logger
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, EntityManager $em, LoggerInterface $logger, $db_connection)
    {
        $this->em           = $em;
        $this->logger       = $logger;
        $this->encoderFactory = $encoderFactory;
        $this->db_connection = $db_connection;
    }
 
    /**
     * Tries to authenticate the given security token
     * @param \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token
     * @return \Utwente\SecurityBundle\Security\DHGUserToken
     * @throws AuthenticationException On any exception
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {

      $this->logger->debug("DHG Login: user load started");
      $accountObj = $this->checkAccountAndChangeDB($token->getAccount());
      if ( $accountObj === NULL){
            throw new AuthenticationException(sprintf('No existe una cuenta con el nombre "%s".', $token->getAccount()));
      }
      // Check s or m number
      $username = strtolower($token->getUsername());

      // Check is the user is in our db
      try {
        $loadedUser = $userProvider->loadUserByUsername($username);
      }catch (UsernameNotFoundException $e){
          throw new AuthenticationException('Nombre de usuario o contraseña incorrectos');
      }
 
      // If the token already has roles, than it can be authenticated
      if(count($token->getRoles()) > 0 || $token->isSuperAdmin()){
        $authenticatedToken = new DHGUserToken($token->getUsername(), $token->getCredentials(), $providerKey, $accountObj->getName(), $accountObj->getDB(), $token->isSuperAdmin(), $loadedUser->getRoles());
        $authenticatedToken->setUser($loadedUser);
        return $authenticatedToken;
      }
 
        $encoder = $this->encoderFactory->getEncoder($loadedUser);
        $passwordValid = $encoder->isPasswordValid(
            $loadedUser->getPassword(),
            $token->getCredentials(),
            $loadedUser->getSalt()
        );

        if ($passwordValid) {
            $this->logger->debug("DHG Login: account correct!");
            // Save the login time to the DB
            $loadedUser = $userProvider->refreshUser($loadedUser);
            $date = new \DateTime();
            $loadedUser->setLastConnection(clone $date);
            $this->em->persist($loadedUser);
            $this->em->flush();
     
            // Create an new token with the correct data
            $authenticatedToken = new DHGUserToken($token->getUsername(), $token->getCredentials(), $providerKey, $accountObj->getName(), $accountObj->getDB(), $loadedUser->getSuperadmin(), $loadedUser->getRoles());
            $authenticatedToken->setUser($loadedUser);
            return $authenticatedToken;
         }else{
            throw new AuthenticationException('Nombre de usuario o contraseña incorrectos');
        }
    }
 
    /**
     * Checks if token supported
     * @param \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token
     * @return bool
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof DHGUserToken;
    }
 
    /**
     * Check if haystack starts with the needle
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function startsWith($haystack, $needle)
    {
      return !strncmp($haystack, $needle, strlen($needle));
    }

    /**
     * Toma un nombre de cuenta y devuelve el nombre de la db de la correspondiente cuenta
     *
     * @param String account
     * @return String Null en caso de error
     *
     */
    private function checkAccountAndChangeDB($account){
        $this->db_connection->forceSwitchToDefault();
        $repo = $this->em->getRepository('DHGcoreBundle:Account');
        $q = $repo
            ->createQueryBuilder('a')
            ->select('a')
            ->where('a.name = :name')
            ->setParameter('name', $account)
            ->getQuery();

        $accountObj = $q->getOneOrNullResult();
        if($accountObj!=null && $accountObj->getFin()!=null && $accountObj->getFin() < new \DateTime()){
            throw new AuthenticationException('Su cuenta ha caducado');
        }

        if ($accountObj!=NULL){
            $this->db_connection->forceSwitchTOAccountDB($accountObj->getDB(),$accountObj->getUsername());
            return $accountObj;
        }
        return NULL;    
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof DHGUserToken
            && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
        $account = trim($request->request->get('_account', NULL));
        return new DHGUserToken($username, $password, $providerKey, $account );
    }
}