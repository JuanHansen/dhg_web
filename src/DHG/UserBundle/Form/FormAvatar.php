<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FormAvatar extends AbstractType {
    private $name = 'Asignar_Avatar';
    /**
     * Builds the FormCrearRole form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', array(
                'label' => 'Archivo',
                "file_path" => "webPath",
                "file_name" => "path"
            ));

        $builder->setAttribute('show_legend', true);

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\UserBundle\Entity\UserAvatar',
        ));
    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\UserBundle\Entity\UserAvatar'
        );
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($name='Asignar_Avatar'){
        $this->name = $name;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
        public function getName()
    {
        return $this->name;
    }
}


