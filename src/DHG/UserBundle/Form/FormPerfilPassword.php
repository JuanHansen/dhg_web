<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;

class FormPerfilPassword extends AbstractType {
    private $name = 'Cambio_Password';
    /**
     * Builds the FormCrearRole form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('passOriginal', 'password', array(
                'label' => 'Password Actual',
                'required' => true,
                'mapped' => false
            ))
            ->add('passNueva', 'repeated', array(
                'first_name'  => 'Password_Nueva',
                'second_name' => 'Reingresar',
                'type'        => 'password',
                'invalid_message' => 'Las contraseñas deben coincidir.',
                'required' => true,
                'mapped' => false
            ));


        $builder->setAttribute('show_legend', true);

    }


    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($name='Cambio_Password'){
        $this->name = $name;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


