<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use DHG\ContactosBundle\Form\FormContacto;
use DHG\ContactosBundle\Entity\Contacto;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use DHG\UserBundle\Components\DHGPermissionInterface;
class FormCrearUser extends AbstractType {
    private $name = 'Nuevo_Usuario';
    private $ed;
    private $usuario = null;
    /**
     * Builds the FormPersonal form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Nombre de usuario',
                'help_block' => 'Nombre de usuario. Por ej: Juan_88.',
                'attr' => array('placeholder' => 'Nombre de usuario'),
                'required' => true,
            ))

            ->add('email', 'email', array(
                'label' => 'Email',
                'help_block' => '',
                'attr' => array('placeholder' => 'nombre@dominio.com'),
                'required' => true,
            ))

            ->add('password', 'repeated', array(
                'first_name'  => 'Password',
                'second_name' => 'Confirmar',
                'type'        => 'password',
                'invalid_message' => 'Las contraseñas deben coincidir.',
            ));
            if($this->usuario!=null){
                $builder
                ->add('contacto', 'entity', array(
                    'label' => 'Contacto',
                    'class' => 'DHG\ContactosBundle\Entity\Contacto',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('c')
                                ->where('c.usuariorelacionado IS NULL OR c.usuariorelacionado = :ida')
                                ->setParameter('ida',$this->usuario->getId())
                                ->orderBy('c.nombre', 'ASC');
                    },
                    'empty_value' => 'Seleccione un contacto',
                    'empty_data'  => null,
                    'required' => false,
                ));
            }else{
                $builder
                ->add('contacto', 'entity', array(
                    'label' => 'Contacto',
                    'class' => 'DHG\ContactosBundle\Entity\Contacto',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('c')
                                ->where('c.usuariorelacionado IS NULL')
                                ->orderBy('c.nombre', 'ASC');
                    },
                    'empty_value' => 'Seleccione un contacto',
                    'empty_data'  => null,
                    'required' => false,
                ));
            }
        $builder
            ->add('roles','entity',array(
                'class' => 'DHGUserBundle:Role',
                'property' => 'name',
                'label' => 'Roles',
                'multiple' => true,
                'expanded' =>true,
                'empty_value' => 'Elige los roles',
                'required' => false,
            ));
        $builder->setAttribute('show_legend', true);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'presubmitEvent'));
    }

 public function presubmitEvent(FormEvent $event)   {
        $form = $event->getForm();


        if($form->getName()=='Editar_user'){
            $form->add('password', 'repeated', array(
                'first_name'  => 'Password',
                'second_name' => 'Confirmar',
                'type'        => 'password',
                'required'    =>false,
                'invalid_message' => 'Las contraseñas deben coincidir.',
            ));
        }
    }


    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        foreach ($view->children['roles']->children as $rol) {
            if ($rol->vars['label']==DHGPermissionInterface::ROLE_USUARIO){
                $rol->vars['checked'] = true;
                $rol->vars['attr']['disabled'] = true;
            }
        }
    }
    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\UserBundle\Entity\User'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($ed,$name='Nuevo_usuario',$usuario=null){
        $this->name = $name;
        $this->ed = $ed;
        $this->usuario = $usuario;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


