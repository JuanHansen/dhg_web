<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;

class FormCrearRole extends AbstractType {
    private $name = 'Nuevo_rol';
    /**
     * Builds the FormCrearRole form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', 'text', array(
                'label' => 'Nombre del rol',
                'attr' => array('placeholder' => 'Nombre del rol'),
                'required' => true,
            ))

            ->add('details', 'textarea', array(
                'label' => 'Detalles',
                'required' => false
            ));

        $builder->setAttribute('show_legend', true);

    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'DHG\UserBundle\Entity\Role'
        );
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($name='Nuevo_rol'){
        $this->name = $name;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


