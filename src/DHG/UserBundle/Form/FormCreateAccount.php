<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use DHG\UserBundle\Form\FormCrearUser;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;

class FormCreateAccount extends AbstractType {
    private $name = 'Nueva_cuenta';
    private $ed;
    /**
     * Builds the FormPersonal form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('account', 'text', array(
                'label' => 'Cuenta',
                'attr' => array('placeholder' => 'Nombre de la cuenta'),
                'required' => true,
            ))
            ->add('username', 'text', array(
                'label' => 'Usuario',
                'attr' => array('placeholder' => 'Nombre del usuario dueño de la Cuenta'),
                'required' => true,
            ))
            ->add('email', 'email', array(
                'label' => 'Email',
                'help_block' => '',
                'attr' => array('placeholder' => 'nombre@dominio.com'),
                'required' => true,
            ))
            ->add('password', 'repeated', array(
                'first_name'  => 'Password',
                'second_name' => 'Confirmar',
                'type'        => 'password',
                'invalid_message' => 'Las contraseñas deben coincidir.',
            ))
            ;
       
        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'postSubmitEvent'));
    }

    public function postSubmitEvent(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        if($data['username']=="superadmin"){
            $form->get('username')->addError(new FormError(sprintf('El nombre de usuario %s esta reservado',"superadmin")));
        }
        //TODO add chequeo de cuenta existente
    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {
    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct($ed,$name='Nueva_cuenta'){
        $this->name = $name;
        $this->ed = $ed;
    }

    public function getButtonValue()
    {
        return ""; # return here the name of the route the form should point to
    }
}


