<?php
namespace DHG\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\EntityRepository;
use DHG\UserBundle\Components\DHGPermissionInterface;

class FormPermissions extends AbstractType 
{
    private $name = 'Permisos';
    private $em;
    private $roles;

    /**
     * Funcion axuliar para el renderizado. Devuelve los roles en el orden que se utiliza en cad fila
     *
     */
    public function getRolesShowed()
    {
        return $this->roles;
    }

    /**
     * Genera el arreglo de roles ordenados para ser utilizados en los checks de cada fila
     *
     */
    private function getRoleChoices( )
    {
        $choices = array();
        $repository = $this->em->getRepository('DHGUserBundle:Role');
        $roles = $repository->findBy(array(), array('role' => 'ASC'));

        $rol = $repository->findOneByRole(DHGPermissionInterface::ROLE_USUARIO);
        if (!empty($rol)){
            $choices[$rol->getId()] = $rol->getRole();
        }

        foreach($roles as $rol){
            if ($rol->getRole() != DHGPermissionInterface::ROLE_USUARIO && $rol->getRole() != DHGPermissionInterface::ROLE_ADMIN){
                $choices[$rol->getId()] = $rol->getRole();
            }
        }
        $rol = $repository->findOneByRole(DHGPermissionInterface::ROLE_ADMIN);
        if (!empty($rol)){
            $choices[$rol->getId()] = $rol->getRole();
        }

        $this->roles = $choices;
        return $choices;

    }

    /**
     * Setea los roles como seleccionados
     *
     */
    private function getRoleSelectedData( $roles ){
        $choices = array();
        foreach($roles as $rol){
            $choices[] = $rol->getId();
        }
        return $choices;
    }


    /**
     * Builds the FormCrearRole form
     * @param  \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $repository = $this->em->getRepository('DHGUserBundle:PermissionClasification');
        $permisosClasif = $repository->findBy(array(), array('name' => 'ASC'));
        foreach ($permisosClasif as $permClas){
            $builder->add($permClas->getId(), 'form', array( 'label' => $permClas->getName(), 'virtual' => true, 'label_attr' => array('class'=>'permissionTableLabelClasification') ));
        }

        $roles = $this->getRoleChoices();
        $repository = $this->em->getRepository('DHGUserBundle:Permission');
        $permisos = $repository->findBy(array(), array('name' => 'ASC'));

        foreach ($permisos as $perm){
            $iddep = null;
            if($perm->getDepends())
                $iddep = $perm->getDepends()->getId();
            $builder
                ->get($perm->getClasification()->getId())
                ->add($perm->getId(), 'choice', array(
                    'label' => $perm->getName(),
                    'choices' => $roles,
                    'data' => $this->getRoleSelectedData($perm->getRoles()),
                    'multiple'  => true,
                    'expanded'  => true,
                    'label_attr' => array('class'=>'permissionTableLabel','depend'=>$iddep,'miid'=>$perm->getId()),
                ));
        }

        $builder->setAttribute('show_legend', true);

    }

    /**
     * Returns the default options/class for this form.
     * @param array $options
     * @return array The default options
     */
    public function getDefaultOptions(array $options)
    {

    }

    /**
     * Mandatory in Symfony2
     * Gets the unique name of this form.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name Nombre del formulario
     */
    public function __construct( $em, $name='Permisos'){
        $this->name = $name;
        $this->em = $em;
    }

    public function getButtonValue()
    {
        return ""; 
    }
}


