<?php 
namespace DHG\UserBundle\Menu;

use DHG\UserBundle\Entity\User;
use DHG\UserBundle\Events\MenuEvents;
use DHG\UserBundle\Events\ConfigureMenuEvent;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use DHG\UserBundle\Security\UserPermissions;

/**
*
*/
class UserMenuBuilder extends ContainerAware
{

    /**
     * Devuelve toda la estructura del menu de usuario
     *
     */
    public function UserMenu(FactoryInterface $factory, array $options)
    {
    	$menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'dropdown-menu animated fadeInLeft');
        $this->inicializar($menu, $factory);
    	$this->container->get('event_dispatcher')->dispatch(MenuEvents::CONFIGURE_USERMENU, new ConfigureMenuEvent($factory, $menu));
    	$this->reorderMenuItems($menu);

        return $menu;
    }


    public function inicializar($menu, $factory)
    {

        $user = new User();
        if ($this->container->get('security.context')->isGranted(UserPermissions::VERUSUARIOS, $user)) {
            $menu->addChild($factory->createItem('Usuarios', array('route' => 'dhg_user_listado'))
                ->setExtra('orderNumber', 1)
                ->setAttribute('icon', 'gear')); 
        }
        $menu->addChild($factory->createItem('Perfil', array('route' => 'dhg_user_perfilView'))
            ->setExtra('orderNumber', 2)
            ->setAttribute('icon', 'user')); 
        if ($this->container->get('security.context')->isGranted(UserPermissions::GESTIONARROLES, $user)) {
            $menu->addChild($factory->createItem('Roles', array('route' => 'dhg_user_roleslist'))
                ->setExtra('orderNumber', 3)
                ->setAttribute('icon', 'user')); 
        }
        if ($this->container->get('security.context')->isGranted(UserPermissions::GESTIONARPERMISOS, $user)) {
            $menu->addChild($factory->createItem('Permisos', array('route' => 'dhg_user_permissionslist'))
                ->setExtra('orderNumber', 4)
                ->setAttribute('icon', 'key')); 
        }
        
        $menu->addChild($factory->createItem('Salir', array('route' => 'dhg_user_logout'))
            ->setExtra('orderNumber', 15)
            ->setAttribute('icon', 'sign-out')); 

    }

    

    /**
     * Reordena los elementos del menu, en base al atributo orderNumber
     *
     * @param $menu Objeto menu a reordenar
     */
    public function reorderMenuItems($menu)
    {
        $menuOrderArray = array();
        $addLast = array();
        $alreadyTaken = array();
        foreach ($menu->getChildren() as $key => $menuItem) {

            if ($menuItem->hasChildren()) {
                $this->reorderMenuItems($menuItem);
            }

            $orderNumber = $menuItem->getExtra('orderNumber');

            if ($orderNumber != null) {
                if (!isset($menuOrderArray[$orderNumber])) {
                    $menuOrderArray[$orderNumber] = $menuItem->getName();
                } else {
                    $alreadyTaken[$orderNumber] = $menuItem->getName();
                    // $alreadyTaken[] = array('orderNumber' => $orderNumber, 'name' => $menuItem->getName());
                }
            } else {
                $addLast[] = $menuItem->getName();
            }
        }

        // sort them after first pass
        ksort($menuOrderArray);

        // handle position duplicates
        if (count($alreadyTaken)) {
            foreach ($alreadyTaken as $key => $value) {
                // the ever shifting target
                $keysArray = array_keys($menuOrderArray);

                $position = array_search($key, $keysArray);

                if ($position === false) {
                    continue;
                }

                $menuOrderArray = array_merge(array_slice($menuOrderArray, 0, $position), array($value), array_slice($menuOrderArray, $position));
            }
        }

        // sort them after second pass
        ksort($menuOrderArray);

        // add items without ordernumber to the end
        if (count($addLast)) {
            foreach ($addLast as $key => $value) {
                $menuOrderArray[] = $value;
            }
        }

        if (count($menuOrderArray)) {
            $menu->reorderChildren($menuOrderArray);
        }
    }






}