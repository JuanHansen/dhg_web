<?php

namespace DHG\UserBundle\Events;

use DHG\UserBundle\Entity\Role;
use Symfony\Component\EventDispatcher\Event;

class RoleEditedEvent extends Event{
    private $role;

    /**
     * @param Role $role 
     *      Role edited
     */
    public function __construct(Role $role){
        $this->role = $role;
    }

    /**
     * @return Role
     */
    public function getRole(){
        return $this->role;
    }
}
