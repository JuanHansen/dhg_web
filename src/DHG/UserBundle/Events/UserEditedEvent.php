<?php

namespace DHG\UserBundle\Events;

use DHG\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserEditedEvent extends Event{
    private $user;

    /**
     * @param User $user 
     *      User edited
     */
    public function __construct(User $user){
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(){
        return $this->user;
    }
}