<?php

namespace DHG\UserBundle\Events;

use DHG\UserBundle\Entity\Role;
use Symfony\Component\EventDispatcher\Event;

class RoleRemovedVerificationEvent extends Event{
    private $role;
    private $stoped; 
    private $messages;

    /**
     * @param Role $role 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de eliminacion
     *
     */
    public function __construct(Role $role){
        $this->role = $role;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return role
     */
    public function getRole(){
        return $this->role;
    }

    
    /**
     * Indicar que existe un problema que evita la eliminacion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuario
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return role
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de eliminado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
