<?php
namespace DHG\UserBundle\Events;

final class MenuEvents {

    /**
     * @var string
     */
    const CONFIGURE_USERMENU = 'dhg_core.userMenuConfigure';

}
