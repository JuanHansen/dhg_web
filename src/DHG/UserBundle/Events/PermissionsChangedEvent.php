<?php

namespace DHG\UserBundle\Events;

use Symfony\Component\EventDispatcher\Event;

class PermissionsChangedEvent extends Event{

    /**
     * @param Permission $permission 
     *      Permission edited
     */
    public function __construct(){
    }
}


