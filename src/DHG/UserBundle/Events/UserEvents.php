<?php
namespace DHG\UserBundle\Events;

final class UserEvents {

    /**
     * @var string
     */
    const USER_CREATED = 'dhg_user.userCreated';

    /**
     * @var string
     */
    const USER_EDITED = 'dhg_user.userEdited';

    /**
     * @var string
     */
    const USER_REMOVED = 'dhg_user.userRemoved';

    /**
     * @var string
     */
    const USER_ACTIVATED = 'dhg_user.userActivated';

    /**
     * @var string
     */
    const USER_REMOVED_VERIFICATION = 'dhg_user.userRemovedVerification';
    /**
     * @var string
     */
    const USER_CREATED_VERIFICATION = 'dhg_user.userCreatedVerification';
    /**
     * @var string
     */
    const USER_EDITED_VERIFICATION = 'dhg_user.userEditedVerification';

    /**
     * @var string
     */
    const ROLE_CREATED = 'dhg_user.roleCreated';

    /**
     * @var string
     */
    const ROLE_EDITED = 'dhg_user.roleEdited';

    /**
     * @var string
     */
    const ROLE_REMOVED = 'dhg_user.roleRemoved';

    /**
     * @var string
     */
    const ROLE_REMOVED_VERIFICATION = 'dhg_user.roleRemovedVerification';
    /**
     * @var string
     */
    const ROLE_CREATED_VERIFICATION = 'dhg_user.roleCreatedVerification';

    /**
     * @var string
     */
    const PERMISSIONS_CHANGED = 'dhg_user.permissionsChanged';

    /**
     * @var string
     */
    const PERFIL_PSW_CHANGED = 'dhg_perfil.pswChanged';

    /**
     * @var string
     */
    const PERFIL_AVATAR_CHANGED = 'dhg_perfil.avatarChanged';

}
