<?php

namespace DHG\UserBundle\Events;

use DHG\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserEditedVerificationEvent extends Event{
    private $user;
    private $stoped; 
    private $messages;

    /**
     * @param User $user 
     * @param $message Mensaje que se podra utilizar para mostrar al usuaio
     * @param $stoped Si esta detenido el proceso de edicion
     *
     */
    public function __construct(User $user){
        $this->user = $user;
        $this->stoped = false; 
        $this->messages = array();
    }

    /**
     * @return user
     */
    public function getUser(){
        return $this->user;
    }

    
    /**
     * Indicar que existe un problema que evita la edicion del elemento en cuestion.
     * @param $message Mensaje que se podra utilizar para mostrar al usuario
     * @param $nameSender Quien determina la detencion del proceso
     *
     * @return user
     */
    public function stopRemove($message, $nameSender){
        $this->messages[$nameSender] = $message;
        $this->stoped = true;
    }

    /**
     * @return true si el procesao de creado debe detenerse
     */
    public function isStoped(){
        return $this->stoped;
    }

    /**
     * @return Arreglo con los mensajes de los motivos de la detencion, si es que existe alguno
     */
    public function getMessages(){
        return $this->messages;
    }

}
