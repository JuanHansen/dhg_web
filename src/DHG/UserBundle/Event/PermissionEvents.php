<?php
namespace DHG\UserBundle\Event;

final class PermissionEvents {

    /**
     * @var string
     */
    const CONFIGURE_PERMISSIONS = 'dhg_user.configurePermissions';

}
