<?php

namespace DHG\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ConfigurePermissions extends Event
{
    private $permissions;

    /**
     */
    public function __construct($permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
    }
}