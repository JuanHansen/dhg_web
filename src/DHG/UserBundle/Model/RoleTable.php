<?php
namespace DHG\UserBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class RoleTable
 *
 * @DataTable\Table(id="RoleTable"  )
 */
class RoleTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface 
{

   /**
     * @var string
     * @DataTable\Column(source="Role.role", name="Nombre")
     * @DataTable\DefaultSort()
     */
    public $role;

    /**
     * @var string
     * @DataTable\Column(source="Role.details", name="Detalles")
     * @DataTable\Format(dataFields={"titulo": "Detalle", "text":"Role.details"}, template="DHGcoreBundle:Datatable:cellTextoLargo.html.twig")
     */
    public $details;

    /**
     * @var int
     * @DataTable\Column(source="Role.id", name="Accion" )
     * @DataTable\Format(dataFields={"id":"Role.id","sistema":"Role.sistema"}, template="DHGUserBundle:Table:RoleCellAction.html.twig")
     */
    public $action;

    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\UserBundle\Entity\Role');
        $qb = $userRepository->createQueryBuilder('Role')
            ;
        return $qb;
    }
}