<?php
namespace DHG\UserBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class UserTable
 *
 * @DataTable\Table(id="UserTable"  )
 */
class UserTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {


   /**
     * @var string
     * @DataTable\Column(source="User.username", name="Nombre usuario")
     * @DataTable\DefaultSort()
     */
    public $username;

       /**
     * @var string
     * @DataTable\Column(source="User.email", name="Email")
     * @DataTable\DefaultSort()
     */
    public $email;

    /**
     * @var string
     * @DataTable\Column(source="User.contacto.nombre", name="Nombre")
     * @DataTable\Format(dataFields={"test":"User.contacto","valor":"User.contacto.nombre"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     * @DataTable\DefaultSort()
     */
    public $nombre;
    /**
     * @var boolean
     * @DataTable\Column(source="User.roles", name="Roles" )
     * @DataTable\Format(dataFields={"roles":"User.roles"}, template="DHGUserBundle:Table:UserRolesCell.html.twig")
     */
    public $roles;

    /**
     * @var boolean
     * @DataTable\Column(source="User.active", name="Activo" )
     * @DataTable\Format(dataFields={"active":"User.active"}, template="DHGUserBundle:Table:UserValidCell.html.twig")
     */
    public $active;

    /**
     * @var int
     * @DataTable\Column(source="User.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"User"}, template="DHGUserBundle:Table:UserCellAction.html.twig")
     */
    public $action;
    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\UserBundle\Entity\User');
        $qb = $userRepository->createQueryBuilder('User')
            ->where('User.superadmin = false')
            ;
        return $qb;
    }
}