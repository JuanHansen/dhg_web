<?php
namespace DHG\UserBundle\Entity;

use DHG\coreBundle\UUID;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\ContactosBundle\Entity\Contacto;
use DHG\UserBundle\Entity\UserAvatar;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="DHG\UserBundle\Entity\UserRepository")
 */
class User implements AdvancedUserInterface
{

    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(name="lastConnection", type="datetime", nullable = true)
     */
    private $lastConnection;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $salt;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(name="superadmin", type="boolean")
     */
    private $superadmin;

    /**
     * @ORM\OneToOne(targetEntity="DHG\ContactosBundle\Entity\Contacto", inversedBy="usuariorelacionado",cascade={"persist"})
     * @ORM\JoinColumn(name="contacto_id", referencedColumnName="id", nullable = true)
     */
    protected $contacto;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     */
    protected $roles;
    /**
     * @ORM\OneToOne(targetEntity="DHG\UserBundle\Entity\UserAvatar",cascade={"persist"})
     * @ORM\JoinColumn(name="avatar_id", referencedColumnName="id", nullable = true)
     */
    protected $avatar;
    /**
     * En realidad es un OneToMany unidireccional
     *
     * @ORM\ManyToMany(targetEntity="DHG\UserBundle\Entity\Columna",cascade={"persist","remove"})
     * @ORM\JoinTable(name="User_Columna",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="columna_id", referencedColumnName="id")}
     * ) 
     */
    protected $columnas;

    public function __construct()
    {
        $this->id = UUID::v4();
        $this->created = new \DateTime('now');
        $this->roles = new ArrayCollection();
        $this->active = true;
        $this->superadmin = false;
        $this->generateContacto();
        $this->salt = "0";
        $this->columns = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * {inheritdoc}
     */
    public function prepareToJson()
    {
        return array(
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'active' => $this->active,
        );
    }

    public function __sleep()
    {
        return array('id', 'username', 'password', 'salt');
    }

    public function __toString ()
    {
        return $this->username;
    }


    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PersonalClasification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return \DateTime
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set contacto
     *
     * @param \DHG\ContactosBundle\Entity\Contacto $contacto
     * @return User
     */
    public function setContacto(\DHG\ContactosBundle\Entity\Contacto $contacto =null)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return string 
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * remove Contacto
     *
     */
    public function removeContacto()
    {
        unset($this->contacto);
    }

    /**
     * generate Contacto
     *
     */
    public function generateContacto()
    {
        $this->contacto = new Contacto();
        $this->contacto->__construct();
        $this->contacto->setNombre($this->username);
        $contemails = new \Doctrine\Common\Collections\ArrayCollection([$this->email]);
        $this->contacto->setEmails($contemails);
    }

    /**
     * Add roles
     *
     * @param \DHG\UserBundle\Entity\Role $roles
     * @return User
     */
    public function addRole( $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \DHG\UserBundle\Entity\Role $roles
     */
    public function removeRole( $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

     /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }   

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt="0")
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function getName(){
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set superadmin
     *
     * @param boolean $superadmin
     * @return User
     */
    public function setSuperadmin($superadmin)
    {
        $this->superadmin = $superadmin;

        return $this;
    }

    /**
     * Get superadmin
     *
     * @return boolean 
     */
    public function getSuperadmin()
    {
        return $this->superadmin;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */
        return \serialize(array(
            $this->id,
            $this->created,
            $this->lastConnection,
            $this->username,
            $this->email,
            $this->salt,
            $this->password,
            $this->active,
            $this->superadmin,
        ));
    }


    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->created,
            $this->lastConnection,
            $this->username,
            $this->email,
            $this->salt,
            $this->password,
            $this->active,
            $this->superadmin,
        ) = \unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->active;
    }


    /**
     * Set lastConnection
     *
     * @param \DateTime $lastConnection
     * @return User
     */
    public function setLastConnection($lastConnection)
    {
        $this->lastConnection = $lastConnection;

        return $this;
    }

    /**
     * Get lastConnection
     *
     * @return \DateTime 
     */
    public function getLastConnection()
    {
        return $this->lastConnection;
    }

    /**
     * Set validUntil
     *
     * @param \DateTime $validUntil
     * @return User
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    /**
     * Get validUntil
     *
     * @return \DateTime 
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return UserAvatar 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    /**
     * Set column
     *
     * @param \Doctrine\Common\Collections\Collection $direcciones
     * @return Contacto
     */
    public function setColumnas(\Doctrine\Common\Collections\Collection $columnas)
    {
        $this->columnas = $columnas;

    }
    /**
     * Get columnas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColumnas()
    {
        return $this->columnas;
    }
    /**
     * Get columnas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTablaColumnas($table)
    {
        $toR = array();
        $columnas = $this->columnas;
        foreach ($columnas as $columna) {
            if($columna->getTabla()==$table&&!$columna->getActiva()){
                $toR[] = $columna->getNumero();
            }
        }
        return $toR;
    }

    /**
     * Remove column
     *
     * @param \DHG\ContactosBundle\Entity\Direccion $direccion
     */
    public function removeColumna(\DHG\UserBundle\Entity\Columna $column)
    {
        $this->columnas->removeElement($column);
    }
}
