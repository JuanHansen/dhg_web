<?php
namespace DHG\UserBundle\Entity;

use DHG\coreBundle\UUID;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Roles")
 * @ORM\Entity()
 */
class Role implements RoleInterface
{
    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    private $users;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $details;

    /**
     * @ORM\Column(name="sistema", type="boolean")
     */
    private $sistema;

    /**
     * @ORM\ManyToMany(targetEntity="Permission", inversedBy="roles")
     *
     */
    protected $permissions;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->id = UUID::v4();
        $this->created = new \DateTime('now');
        $this->sistema = false;
    }

    public function __toString(){
        return $this->role;
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    public function getName(){
        return $this->role;

    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Role
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \DHG\UserBundle\Entity\User $users
     * @return Role
     */
    public function addUser(\DHG\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \DHG\UserBundle\Entity\User $users
     */
    public function removeUser(\DHG\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Role
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set sistema
     *
     * @param boolean $sistema
     * @return Role
     */
    public function setSistema($sistema)
    {
        $this->sistema = $sistema;

        return $this;
    }

    /**
     * Get sistema
     *
     * @return boolean 
     */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Role
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Add permissions
     *
     * @param \DHG\UserBundle\Entity\Permission $permissions
     * @return Role
     */
    public function addPermission(\DHG\UserBundle\Entity\Permission $permissions)
    {
        $this->permissions[] = $permissions;

        return $this;
    }

    /**
     * Remove permissions
     *
     * @param \DHG\UserBundle\Entity\Permission $permissions
     */
    public function removePermission(\DHG\UserBundle\Entity\Permission $permissions)
    {
        $this->permissions->removeElement($permissions);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Get permissions
     *
     * @return boolean 
     */
    public function hasPermissions( $permissionMachineName)
    {
        $exists = false;
        if (!empty($this->permissions)){
            foreach ($this->permissions as $val){
                if ($val->getMachinename() === $permissionMachineName){
                    $exists = true;
                    break;
                }
            }
        }
        return $exists;
    }

    /**
     * Get permissions
     *
     * @return boolean 
     */
    public function hasPermissionsInArray(array $permissionMachineNames)
    {
        $exists = false;
        foreach ($this->permissions as $val){
            if (in_array($val->getMachinename(), $permissionMachineNames)){
                $exists = true;
                break;
            }
        }
        return $exists;
    }

    /**
     * Elimina todos los permisos asignados al rol
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function cleanPermissions()
    {
        unset($this->permissions);
        $this->permissions = array();
    }

      /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $users field !
         */
        return \serialize(array(
            $this->id,
            $this->role,
            $this->created,
            $this->details
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->role,
            $this->created,
            $this->details
        ) = \unserialize($serialized);
    }


}
