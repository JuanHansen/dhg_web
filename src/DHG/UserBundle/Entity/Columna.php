<?php
namespace DHG\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DHG\coreBundle\Entity\MappedSuperclassBase;

/**
 * @ORM\Entity
 * @ORM\Table(name="Columna")
 */
class Columna extends MappedSuperclassBase
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $tabla;
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $numero;  
    /**
     * @ORM\Column(type="boolean")
     */
    protected $activa;

    public function __construct(){
        parent::__construct();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->tabla." ".$this->number;
    }

    public function setTabla($tabla){
        $this->tabla = $tabla;
    }

    public function getTabla(){
        return $this->tabla;
    }

    public function setNumero($numero){
        $this->numero = $numero;
    }

    public function getNumero(){
        return $this->numero;
    }

    public function setActiva($activa){
        $this->activa = $activa;
    }

    public function getActiva(){
        return $this->activa;
    }
}
