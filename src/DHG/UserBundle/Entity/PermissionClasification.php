<?php
namespace DHG\UserBundle\Entity;

use DHG\coreBundle\UUID;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="PermissionClasification")
 */
class PermissionClasification
{
    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $machinename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $details;
    

    public function __construct()
    {
        $this->id = UUID::v4();
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Permission
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Set machinename
     *
     * @param string $machinename
     * @return PermissionClasification
     */
    public function setMachinename($machinename)
    {
        $this->machinename = $machinename;

        return $this;
    }

    /**
     * Get machinename
     *
     * @return string 
     */
    public function getMachinename()
    {
        return $this->machinename;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PermissionClasification
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return PermissionClasification
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }
}
