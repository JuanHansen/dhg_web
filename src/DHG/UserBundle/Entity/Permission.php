<?php
namespace DHG\UserBundle\Entity;

use DHG\coreBundle\UUID;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Permissions")
 */
class Permission
{

    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $machinename;
    
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $module;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\UserBundle\Entity\PermissionClasification", cascade={"persist"})
     * @ORM\JoinColumn(name="fk_clasification_id", referencedColumnName="id")
     */
    protected $clasification;

    /**
     * @ORM\OneToOne(targetEntity="DHG\UserBundle\Entity\Permission")
     * @ORM\JoinColumn(name="depends_id", referencedColumnName="id")
     */
    protected $depends;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $details;

    /**
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="permissions")
     */
    private $roles;


    public function __construct()
    {
        $this->id = UUID::v4();   
    }

    public function __toString(){
        return $this->machinename;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set machinename
     *
     * @param string $machinename
     * @return Permission
     */
    public function setMachinename($machinename)
    {
        $this->machinename = $machinename;

        return $this;
    }

    /**
     * Get machinename
     *
     * @return string 
     */
    public function getMachinename()
    {
        return $this->machinename;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Permission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Permission
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set clasification
     *
     * @param \DHG\UserBundle\Entity\PermissionClasification $clasification
     * @return Permission
     */
    public function setClasification(\DHG\UserBundle\Entity\PermissionClasification $clasification)
    {
        $this->clasification = $clasification;

        return $this;
    }

    /**
     * Get clasification
     *
     * @return \DHG\UserBundle\Entity\PermissionClasification 
     */
    public function getClasification()
    {
        return $this->clasification;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Permission
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set depends
     *
     * @param \DHG\UserBundle\Entity\Permission $depends
     * @return Permission
     */
    public function setDepends(\DHG\UserBundle\Entity\Permission $depends = null)
    {
        $this->depends = $depends;

        return $this;
    }

    /**
     * Get depends
     *
     * @return \DHG\UserBundle\Entity\Permission 
     */
    public function getDepends()
    {
        return $this->depends;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Permission
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Add roles
     *
     * @param \DHG\UserBundle\Entity\Role $roles
     * @return Permission
     */
    public function addRole(\DHG\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \DHG\UserBundle\Entity\Role $roles
     */
    public function removeRole(\DHG\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * remove all roles asignados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function cleanRoles()
    {
        unset($this->roles);
        $this->roles = array();
    }

}
