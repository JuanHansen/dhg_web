<?php
namespace DHG\UserBundle\Components;

use Doctrine\ORM\EntityManager;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Entity\PermissionClasification;

/**
 * DHGPermissionInterface.
 *
 */
interface DHGPermissionInterface
{
	const ROLE_USUARIO = 'Usuario';
	const ROLE_ADMIN = 'Administrador';

    /**
     * Devuelve un arreglo con un listado objetos de permisos y clasificacion
     *
     */
    public function getPermissions(ConfigurePermissions $event);

}
