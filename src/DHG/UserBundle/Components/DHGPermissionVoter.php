<?php
namespace DHG\UserBundle\Components;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use DHG\UserBundle\Components\DHGPermissionInterface;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\Entity\Permission;

abstract class DHGPermissionVoter implements VoterInterface, DHGPermissionInterface
{

    public $perm;
    public $actualModule;
    public $permissionClasif;

    /**
     * Devuelve un arreglo con las clases soportadas para ser evaluadas luego
     */
    abstract public function getSupportedClasses();

    /**
     * Implementaciond de la verificacion de clases
     *
     */
    public function supportsClass($class)
    {
        $supportedClasses = $this->getSupportedClasses();
        if (!in_array($class, $supportedClasses)){
            foreach ($supportedClasses as $val){
                if (is_subclass_of($class, $val)){
                    return true;
                }
            }
        }else{
            return true;
        }
          
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function vote(TokenInterface $token, $class, array $attributes)
    {
        if (!$this->supportsClass(get_class($class))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        if ($user->getSuperadmin()){
            return VoterInterface::ACCESS_GRANTED;
        }

        $roles = $user->getRoles();

        $exists = false;
        foreach ($roles as $val){
            if ($val->hasPermissionsInArray($attributes)){
                $exists = true;
                break;
            }
        }

        if ($exists){
            return VoterInterface::ACCESS_GRANTED;
        }
      
        return VoterInterface::ACCESS_DENIED;
    }

    /**
     * Seteo los permisos que seran utilizados.
     *
     * @param DHG\UserBundle\Event\ConfigurePermissions event
     * @return array 
     *
     */
    abstract public function getPermissions(ConfigurePermissions $event);


    public function crearPermiso($identificador,$nombre,$admin,$depends=null){
        $newPer = new Permission();
        $newPer->setMachinename($identificador);
        if($admin){
            $newPer->setName("Gestionar ".$nombre);
            $newPer->setDetails("Permite a los usuarios crear,editar y eliminar ".$nombre);
        }else{
            $newPer->setName("Ver ".$nombre);
            $newPer->setDetails("Permite a los usuarios ver ".$nombre);
        }
        if($depends != null){
            $newPer->setDepends($this->perm['permissions'][$depends]);
        }
        $newPer->setModule($this->actualModule);
        $newPer->setClasification($this->permissionClasif[$this->actualModule]);
        $this->perm['permissions'][$identificador] = $newPer;
        $this->perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO] = array_merge($this->perm['roleAssigment'][DHGPermissionInterface::ROLE_USUARIO], array($identificador));
        if($admin){
            $this->perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN] = array_merge($this->perm['roleAssigment'][DHGPermissionInterface::ROLE_ADMIN],array($identificador));
        }
    }

}