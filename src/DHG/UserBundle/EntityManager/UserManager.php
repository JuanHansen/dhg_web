<?php

namespace DHG\UserBundle\EntityManager;

use DHG\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use DHG\UserBundle\Events\UserEvents;
use DHG\UserBundle\Events\UserCreatedEvent;
use DHG\UserBundle\Events\UserEditedEvent;
use DHG\UserBundle\Events\UserRemovedEvent;
use DHG\UserBundle\Events\UserActivatedEvent;
use DHG\UserBundle\Events\UserRemovedVerificationEvent;
use DHG\UserBundle\Events\UserCreatedVerificationEvent;
use DHG\UserBundle\Events\UserEditedVerificationEvent;
use DHG\UserBundle\Components\DHGPermissionInterface;

class UserManager{
	private $em;
	private $ed;
    private $factory;

	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    
    /**
     * Crea en base de datos la user
     *
     * @param $data Datos del usuario provenientes del form con la informacion a almacenar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si todo fue executado correctamente
     *
     */
    public function createDataUser($userdata, &$error = null, $factory){
        $usuario = new User();
        $usuario->setUsername($userdata['username']);
        $usuario->setPassword($userdata['password']);
        $usuario->setContacto($userdata['contacto']);
        $usuario->setEmail($userdata['email']);

        foreach ($userdata['roles'] as $key => $value) {
            $usuario->addRole($value);
        }

        //$usuario->setRoles($userdata['roles']);

        return $this->createUser($usuario,$error,$factory);
    }










    /**
     * Crea en base de datos la user
     *
	 * @param $user User con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createUser(User $user, &$error = null, $factory){
        $this->factory = $factory;
        if ($this->createVerificationUser($user, $error)){
            if($this->validarUsuario($user,$error)){
                try{
                    if($user->getContacto()!=null)
                        $user->getContacto()->setUsuariorelacionado($user);
                    
                    $this->generarSal($user);
                    $this->em->persist($user);
                    $this->em->flush();
                    $this->ed->dispatch(UserEvents::USER_CREATED, new UserCreatedEvent($user));
                } catch (\Exception $e){
                    $error[] = "Ocurrio un error al intentar crear un Usuario";
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }      


    protected function validarUsuario($user,&$error){
        return( ($this->validarNombre($user->getUsername(),$error))
                &($this->validarMail($user->getEmail(),$error))
                &($this->validarRolUsuario($user,$user->getRoles(),$error))
                &($this->validarUsuarioDuplicado($user,$error)));
    }
    protected function validarUsuarioDuplicado($user,&$error){
        if($user->getContacto()==null)
            return true;
        $r=$this->em->getRepository('DHGUserBundle:User')->findOneBy(array("contacto"=>$user->getContacto()->getId()));
        if($r==null)
            return true;
        else{
            if($r->getId()==$user->getId())
                return true;
            else{
                $error[]="El contacto elegido ya esta asociado a un usuario";
                return false;
            }
        }
    }
    protected function validarNombre($nombre,&$error){
        if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9_-])*$/",$nombre)) { 
            return true; 
        } 
            $error[] = "El nombre de usuario solo puede contener letras, numeros, '_' y '-'";
            return false; 
        return true;
    }

    protected function validarMail($email,&$error) {
         if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email)) { 
            return true; 
        } 
            $error[] = "La direccion de email es invalida";
            return false; 
    }

    protected function validarRolUsuario($user,$roles,&$error){
        $rolUsuario= DHGPermissionInterface::ROLE_USUARIO;
        foreach ($roles as $rol) {
            //$error[]=$rol->getRole();
            if($rol->getRole()==$rolUsuario){
                return true;
            }
        }
            $r=$this->em->getRepository('DHGUserBundle:Role')->findOneByRole($rolUsuario);
            $user->addRole($r);
            return true;
    }


    protected function generarSal(&$user){
                $encoder = $this->factory->getEncoder($user);
                $user->setSalt(md5(time()));
                $pass = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($pass);
    }


    /**
     * Edita en base de datos el user
     *
	 * @param $user User con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue ejecutado correctamente
	 *
     */
    public function editUserOld(User $user,$pswnull,$telefonosOriginales,$direccionesOriginales,$emailsOriginales, &$error = null, $factory ){
        $this->factory = $factory;
        if ($this->editVerificationUser($user, $error)){
            if($this->validarUsuario($user,$error)){
                try{
                    foreach ($user->getContacto()->getTelefonos() as $telefono) {
                        foreach ($telefonosOriginales as $key => $toDel) {
                            if ($toDel->getId() === $telefono->getId()) {
                                unset($telefonosOriginales[$key]);
                            }
                        }
                    }
                    foreach ($telefonosOriginales as $telefono) {
                        $user->getContacto()->getTelefonos()->removeElement($telefono);
                        $this->em->remove($telefono);
                    }
                foreach ($user->getContacto()->getDirecciones() as $direccion) {
                        foreach ($direccionesOriginales as $key => $toDel) {
                            if ($toDel->getId() === $direccion->getId()) {
                                unset($direccionesOriginales[$key]);
                            }
                        }
                    }
                    foreach ($direccionesOriginales as $direccion) {
                        $user->getContacto()->getDirecciones()->removeElement($direccion);
                        $this->em->remove($direccion);
                    }
                foreach ($user->getContacto()->getEmails() as $email) {
                        foreach ($emailsOriginales as $key => $toDel) {
                            if ($toDel->getId() === $email->getId()) {
                                unset($emailsOriginales[$key]);
                            }
                        }
                    }
                    foreach ($emailsOriginales as $email) {
                        $user->getContacto()->getEmails()->removeElement($email);
                        $this->em->remove($email);
                    }

                    if(!$pswnull)
                        $this->generarSal($user);
                    $this->em->persist($user);
                    $this->em->flush();
                    $this->ed->dispatch(UserEvents::USER_EDITED, new UserEditedEvent($user));
                } catch (\Exception $e){
                    $error[] = "Ocurrio un error al intentar editar un Usuario"; 
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos el user
     *
     * @param $user User con la informacion a almacenar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si todo fue ejecutado correctamente
     *
     */
    public function editUser(User $user,$pswnull, &$error = null, $factory ){
        $this->factory = $factory;
        if ($this->editVerificationUser($user, $error)){
            if($this->validarUsuario($user,$error)){
                try{
                    if(!$pswnull)
                        $this->generarSal($user);
                    $this->reasignarContacto($user);
                    $this->em->persist($user);
                    $this->em->flush();
                    $this->ed->dispatch(UserEvents::USER_EDITED, new UserEditedEvent($user));
                } catch (\Exception $e){
                    $error[] = "Ocurrio un error al intentar editar un Usuario";
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }   



    public function reasignarContacto($user){

        $r=$this->em->getRepository('DHGContactosBundle:Contacto')->findOneBy(array("usuariorelacionado"=>$user->getId()));
        if($r==null){}
            //return true;
        else{
            if($user->getContacto()!=null&&$r->getId()==$user->getContacto()->getId()){}
                //return true;
            else{
                $r->setUsuariorelacionado(null);
                $this->em->persist($r);
            }
        }
        if($user->getContacto()!=null){
            $user->getContacto()->setUsuariorelacionado($user);
            $this->em->persist($user->getContacto());
        }
    }



    /**
     * Bloquea al usuario (simula una eliminacion, pero persiste en la base de datos como bloqueado)
     *
	 * @param $user User con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 * 
     */
    public function removeUser(User $user, &$error = array() ){
    	if ($this->removeVerificationUser($user, $error)){
			try{
                $user->setActive(false);
	            $this->em->persist($user);
	            $this->em->flush();
	            $this->ed->dispatch(UserEvents::USER_REMOVED, new UserRemovedEvent($user));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar un Usuario";
	        	return false;
	        }
    	}else{
        	return false;
    	}
        return true;
    }  

    /**
     * Desbloquea al usuario 
     *
     * @param $user User con la informacion a desbloquear
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si todo fue executado correctamente
     * 
     */
    public function reactivarUser(User $user, &$error = array() ){
            try{
                $user->setActive(true);
                $this->em->persist($user);
                $this->em->flush();
                $this->ed->dispatch(UserEvents::USER_ACTIVATED, new UserActivatedEvent($user));
            } catch (\Exception $e){
                $error[] = "Ocurrio un error al intentar reactivar un Usuario";
                return false;
            }
        return true;
    }  

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
	 * @param $user User con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationUser(User $user, &$error = array() ){
        $event = new UserRemovedVerificationEvent($user);
        $this->ed->dispatch(UserEvents::USER_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    } 

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la creacion
     *
     * @param $user User con la informacion a crear
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al creado
     *
     */
    public function createVerificationUser(User $user, &$error = array() ){
        $event = new UserCreatedVerificationEvent($user);
        $this->ed->dispatch(UserEvents::USER_CREATED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error = $event->getMessages();
            return false;
        }
        return true;
    } 
        /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la creacion
     *
     * @param $user User con la informacion a crear
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al creado
     *
     */
    public function editVerificationUser(User $user, &$error = array() ){
        $event = new UserEditedVerificationEvent($user);
        $this->ed->dispatch(UserEvents::USER_EDITED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error = $event->getMessages();
            return false;
        }
        return true;
    } 
}
