<?php

namespace DHG\UserBundle\EntityManager;

use DHG\UserBundle\Entity\Role;
use Doctrine\ORM\EntityManager;

use DHG\UserBundle\Events\UserEvents;
use DHG\UserBundle\Events\RoleCreatedEvent;
use DHG\UserBundle\Events\RoleEditedEvent;
use DHG\UserBundle\Events\RoleRemovedEvent;
use DHG\UserBundle\Events\RoleRemovedVerificationEvent;
use DHG\UserBundle\Events\RoleCreatedVerificationEvent;

class RoleManager{
	private $em;
	private $ed;

	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la role
     *
	 * @param $role Role con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createRole(Role $role, &$error = null ){
        if ($this->createVerificationRole($role, $error)){
            try{
                $this->em->persist($role);
                $this->em->flush();
                $this->ed->dispatch(UserEvents::ROLE_CREATED, new RoleCreatedEvent($role));
            } catch (\Exception $e){
                $error[] = "Ocurrio un error al intentar crear un Rol";
                return false;
            }
        }else{
            return false;
        }
        return true;
    }      


    /**
     * Edita en base de datos la role
     *
	 * @param $role Role con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function editRole(Role $role, &$error = null ){
        if ($this->createVerificationRole($role, $error)){
        	try{
                $this->em->persist($role);
                $this->em->flush();
                $this->ed->dispatch(UserEvents::ROLE_EDITED, new RoleEditedEvent($role));
            } catch (\Exception $e){
            	$error[] = "Ocurrio un error al intentar editar un Rol";
            	return false;
            }
        }else{
            return false;
        }
        return true;
    }      


    /**
     * Elimina en base de datos la role
     *
	 * @param $role Role con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeRole(Role $role, &$error = array() ){
    	if ($this->removeVerificationRole($role, $error)){
			try{
	            $this->em->remove($role);
	            $this->em->flush();
	            $this->ed->dispatch(UserEvents::ROLE_REMOVED, new RoleRemovedEvent($role));
            } catch (\Exception $e){
	        	$error[] = "Ocurrio un error al intentar eliminar un Rol";
	        	return false;
	        }
    	}else{
        	return false;
    	}
        return true;
    }  

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
	 * @param $role Role con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si se puede proceder al eliminado
	 *
     */
    public function removeVerificationRole(Role $role, &$error = array() ){
        $event = new RoleRemovedVerificationEvent($role);
        $this->ed->dispatch(UserEvents::ROLE_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
        	$error = $event->getMessages();
        	return false;
        }
        return true;
    } 

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la creacion
     * @param $role Role con la informacion a eliminar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al creado
     *
     */
    public function createVerificationRole(Role $role, &$error = array() ){
        $event = new RoleCreatedVerificationEvent($role);
        $this->ed->dispatch(UserEvents::ROLE_CREATED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error = $event->getMessages();
            return false;
        }
        return true;
    }
}
