<?php

namespace DHG\UserBundle\EntityManager;

use Doctrine\ORM\EntityManager;

use DHG\UserBundle\Events\UserEvents;
use DHG\UserBundle\Events\PermissionsChangedEvent;

class PermissionManager{
	private $em;
	private $ed;

	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Setea los permisos para determinados roles
     *
	 * @param $data arreglo associativo con lo permisos y los roles
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function setPermissions($data, &$error = null ){
        try{
            //Vacio todos los permisos de todos los roles
            $repoRol = $this->em->getRepository('DHGUserBundle:Role');
            $roles = $repoRol->findAll();
            foreach ($roles as $rol) {
                $rol->cleanPermissions();
            }
            foreach ( $data as $permission => $roles){
                $repository = $this->em->getRepository('DHGUserBundle:Permission');
                $perm = $repository->findOneById($permission);
                foreach ($roles as $rol) {
                   $rolObj = $repoRol->findOneById($rol);
                   $rolObj->addPermission($perm);
                   if ($perm->getDepends()&&!$rolObj->hasPermissions($perm->getDepends())){
                      $rolObj->addPermission($perm->getDepends());  
                   }
                   $this->em->persist($rolObj);
                }
            }
            $this->em->flush();
            $this->ed->dispatch(UserEvents::PERMISSIONS_CHANGED, new PermissionsChangedEvent());
        } catch (\Exception $e){
            $error[] = "Ocurrio un error al intentar establecer los Permisos";
            return false;
        }
        return true;
    }      

}