<?php
namespace DHG\UserBundle\EntityManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DHG\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use DHG\UserBundle\Events\UserEvents;
use DHG\UserBundle\Events\PerfilPswChangedEvent;
use DHG\UserBundle\Events\PerfilAvatarChangedEvent;
use DHG\UserBundle\Components\DHGPermissionInterface;

class PerfilManager extends Controller{
	private $em;
	private $ed;
    private $factory;

	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em,$ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Modifica la contrasela
     *
	 * @param $user User al que se le cambiará la contraseña
     * @param $oldPassword contraseña vieja
     * @param $newPassword contraseña nueva
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function changePassword(User $user,$oldPassword, $newPassword, &$error = null,$factory){
        if($this->checkPass($user,$oldPassword,$factory)){
            $user->setPassword($newPassword);
            $this->generarSal($user,$factory);
            $this->em->persist($user);
            $this->em->flush();
            $this->ed->dispatch(UserEvents::PERFIL_PSW_CHANGED, new PerfilPswChangedEvent($user));
            return true;
        }else{
            $error[]="El password ingresado no coincide";
            return false;
        }
        
    }      
    /**
    * Cambia el avatar del usuario. Se elimina el archivo correspondiente al avatar antiguo (si existe)
    * mediante "unlink" y se le asigna el archivo correspondiente al avatar nuevo
    * @param $user User al que se le cambiará el avatar
    * @param $avatar Nuevo avatar
    * @param $error Mensaje de error en caso de suceder
    *
    * @return true si todo fue ejecutado correctamente
    */
    public function changeAvatar(User $user,$avatar, &$error = array() ){
        try{
            $avatar->preUpload();
            $avatar->upload();
            if($user->getAvatar()){
                try{
                    unlink($user->getAvatar()->getAbsolutePath());
                }catch(\Exception $e){}
            }
            $user->setAvatar($avatar);
            $this->em->persist($user);
            $this->em->flush();
            $this->ed->dispatch(UserEvents::PERFIL_AVATAR_CHANGED, new PerfilAvatarChangedEvent($user));
            return true;
        }catch(Exception $e){
            $error[] = "Ocurrio un error al intentar cambiar el Avatar";
            return false;
        }
    }  
    //Verifica si el password $pass cohincide con el password que tiene asignado el usuario $user
    protected function checkPass($user,$pass,$factory){
        $encoder = $factory->getEncoder($user);
        $passwordValid = $encoder->isPasswordValid(
            $user->getPassword(),
            $pass,
            $user->getSalt()
        );

        return $passwordValid;
    }

    protected function generarSal(&$user,$factory){
                $encoder = $factory->getEncoder($user);
                $user->setSalt(md5(time()));
                $pass = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($pass);
    }
}
