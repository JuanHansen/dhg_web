<?php
namespace DHG\EntityHistoryBundle\EntityManager;

use DHG\EntityHistoryBundle\Entity\EntityHistory;


class EntityHistoryManager
{
	private $em;

	public function __construct( $em ){
		$this->em = $em;
	}

	/**
	 * Obtiene todo el historial de cambios de un recurso	
	 * @param $resourseName nombre del recurso que se desea obtener el historial
	 * @return Todos los elementos EntityHistori relacionados al recurso pasado por parametro
	 */
	public function getAllHistory( $resourseName){
		$repository = $this->em->getRepository('DHGEntityHistoryBundle:EntityHistory');
		return  $repository->findBy( array('resourceName' => $resourseName), array('fecha' => 'DESC') );
	}	

	/**
	 * Obtiene un grupo de historias. Las historias retornadas dependen de los parametros.
	 * Se ordenan las historias por fecha (las mas nuevas primero) y se extraen desde la $offset*$limit hasta la ($offset*$limit)+$limit
	 * @param $offset Pagina que se desea obtener
	 * @param $limit Cantidad de items que se obtendrán
	 * @return Colleccion de historias. 
	 */
	public function getAllHistoryGrouped($offset = 0,$limit = 10){
		$repository = $this->em->getRepository('DHGEntityHistoryBundle:EntityHistory');
		$query = $repository->createQueryBuilder('o')
							->where('o.changedData NOT LIKE :formato')
							->orderBy('o.fecha','DESC')
						    ->setFirstResult($offset*$limit)
						    ->setMaxResults($limit)  
   							->setParameter('formato', 'a:1:{s:10:"lastupdate"%')
				            ->getQuery();

	   return $query->getResult();
	}	
}