<?php

namespace DHG\EntityHistoryBundle\Twig;

use Symfony\Component\HttpFoundation\Response;


class FiltersEntityHistoryExtension extends \Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dhg_entity_history.twig.extension.filters';
    }


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('human_attribute', array($this, 'get_human_attribute')),
            new \Twig_SimpleFilter('human_accion', array($this, 'get_human_accion')),
        );
    }

    public function get_human_attribute($attribute, $class)
    {
        if (class_exists($class) && method_exists($class, 'getAttributeHumanReadableMap')){
            $humanArray = call_user_func_array(array($class, 'getAttributeHumanReadableMap'), array());
            if(array_key_exists($attribute,$humanArray))
                return $humanArray[$attribute];
            else
                return null;
        }
        return null;
    }

    public function get_human_accion($accion)
    {
        if ($accion === 'CREATE'){
            return 'creo';
        }elseif ($accion === 'EDIT') {
            return 'edito';
        }elseif ($accion === 'REMOVE') {
            return 'elimino';
        }else{
            return '';
        }
    }

}
