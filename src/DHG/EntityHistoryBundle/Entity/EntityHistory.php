<?php
namespace DHG\EntityHistoryBundle\Entity;

use DHG\coreBundle\UUID;
use DHG\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="EntityHistory")
 */
class EntityHistory
{
    /**
     * @ORM\Column(type="string", length=36, unique=true)
     * @ORM\Id
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    protected $usuario;

    /** 
     * @ORM\Column(type="string")
     */
    private $resourceClass;

    /** 
     * @ORM\Column(type="string")
     */
    private $resourceName;

    /**
     * @ORM\Column(type="string") 
     */
    private $resourceId;

    /** 
     * @ORM\Column(type="array") 
     */
    private $changedData;

    /**
     * @ORM\Column(type="string",columnDefinition="ENUM('CREATE', 'EDIT', 'REMOVE')")
     */
    protected $accion;


    public function __construct(){
        $this->id = UUID::v4();
        $this->fecha = new \DateTime('now');
        $this->localized = true;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PersonalClasification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Reset id
     *
     * @return this
     */
    public function resetId()
    {
        $this->id = UUID::v4();

        return $this;
    }
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return \DateTime
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    public function setUsuario(User $usuario = null)
    {
        $this->usuario = $usuario;
    }

    public function getUsuario()
    {
       return $this->usuario;
    }


    /**
     * Get accion
     *
     * @return string 
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Set accion
     *
     * @param string $accion
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;
    }

    /**
     * Set resourceClass
     *
     * @param string $resourceClass
     */
    public function setResourceClass($resourceClass)
    {
        $this->resourceClass = $resourceClass;
    }

    /**
     * Get resourceClass
     *
     * @return string 
     */
    public function getResourceClass()
    {
        return $this->resourceClass;
    }

    /**
     * Set resourceName
     *
     * @param string $resourceName
     */
    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;
    }

    /**
     * Get resourceName
     *
     * @return string 
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * Set resourceId
     *
     * @param string $resourceId
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;
    }

    /**
     * Get resourceId
     *
     * @return string 
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Set changedData
     *
     * @param string $changedData
     */
    public function setChangedData($changedData)
    {
        $this->changedData = $changedData;
    }

    /**
     * Get changedData
     *
     * @return string 
     */
    public function getChangedData()
    {
        return $this->changedData;
    }


}
