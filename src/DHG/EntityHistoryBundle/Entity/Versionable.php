<?php 
namespace DHG\EntityHistoryBundle\Entity;

interface Versionable
{
	/**
	 * @return string
	 */
	static public function getResourceEntityName();

	/**
	 * @return string
	 */
	static public function getResourceIcon();

	/**
	 * @return string
	 */
	static public function getSeccionIcon();

	/**
	 * @return string
	 */
	static public function getResourceColor();

	/**
	 * @return array
	 */
	static public function getAttributeHumanReadableMap();

}