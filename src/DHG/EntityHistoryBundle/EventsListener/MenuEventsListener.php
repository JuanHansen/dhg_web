<?php

namespace DHG\EntityHistoryBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use DHG\EntityHistoryBundle\Entity\EntityHistory;

class MenuEventsListener{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBar($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
 
        $MenuAnidado = $factory->createItem('Timeline', array())
                        	    ->setExtra('orderNumber', 9)
                        	    ->setAttribute('icon', 'history');
        
        $MenuAnidado->addChild(
                    $factory->createItem('Timeline', array('route' => 'dhg_entity_history_timeline'))
                            ->setAttribute('icon', 'history')
                            ->setExtra('orderNumber',2)
                    );
        $parentMenu->addChild($MenuAnidado);
    }

    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBarAddCreateItems($event)
    {

    }
}