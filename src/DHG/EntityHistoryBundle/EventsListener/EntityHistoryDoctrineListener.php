<?php

namespace DHG\EntityHistoryBundle\EventsListener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\PerssistentCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DHG\EntityHistoryBundle\Entity\Versionable;
use DHG\EntityHistoryBundle\Entity\EntityHistory;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class EntityHistoryDoctrineListener
{
    protected $container;
    protected $uow;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $this->uow = $uow;
        $class = $em->getClassMetadata('DHG\EntityHistoryBundle\Entity\EntityHistory');

        foreach ($uow->getScheduledEntityInsertions() AS $entity) {
            if ($entity instanceof Versionable) {
                $history = $this->createHistory(get_class($entity), strval($entity), $entity->getId(), $this->limpiarChangedData($uow->getEntityChangeSet($entity)), 'CREATE');
                $em->persist($history);
                $em->getUnitOfWork()->computeChangeSet($class, $history);
            }
        }
        foreach ($uow->getScheduledEntityDeletions() AS $entity) {
            if ($entity instanceof Versionable) {
                $history = $this->createHistory(get_class($entity), strval($entity), $entity->getId(), $this->limpiarChangedData($uow->getEntityChangeSet($entity)), 'REMOVE');
                $em->persist($history);
                $em->getUnitOfWork()->computeChangeSet($class, $history);
            }
        }
        foreach ($uow->getScheduledEntityUpdates() AS $entity) {
            if ($entity instanceof Versionable) {
                $history = $this->createHistory(get_class($entity), strval($entity), $entity->getId(), $this->limpiarChangedData($uow->getEntityChangeSet($entity)), 'EDIT');
                $em->persist($history);
                $em->getUnitOfWork()->computeChangeSet($class, $history);
            }
        }
    }



    private function createHistory($resourceClass, $resourceName, $resourseID, $changeData, $accion){
        $history = new EntityHistory();
        $history->setResourceName($resourceName);
        $history->setResourceId($resourseID);
        $history->setChangedData($changeData);
        $history->setAccion($accion);
        $history->setResourceClass($resourceClass);
        $usuario = $this->container->get('security.context')->getToken()->getUser();
        $history->setUsuario($usuario);
        return $history;
    }

    private function limpiarChangedData($changedData){
        foreach ($changedData as $key => $value) {
            $persistent = "Doctrine\ORM\PersistentCollection";
            //Si es un objeto, no es un proxy y tiene ID, es porque no es un valor viejo, sino una coleccion de valores nuevos
            if($value instanceof $persistent){
                $valores = array();
                foreach ($value as $k => $val) {
                    if(method_exists($val, 'getId')){
                        $valores[] = array('id'=>$val->getId(),'value'=>strval($val),'class'=>get_class($val));
                    }else{
                        $valores[] =strval($val);
                    }
                }
                $changedData[$key] = array();
                $changedData[$key][0] = $valores;
                $changedData[$key][1] = null;
            }else{
                if (is_object($value[0])){
                    if(method_exists($value[0], 'getId')){
                            $changedData[$key][0] = array('id' => $value[0]->getId(), 'value' => strval($value[0]), 'class' => get_class($value[0]));
                    }
                }
                if (is_object($value[1])){
                    if(method_exists($value[1], 'getId')){
                            $changedData[$key][1] = array('id' => $value[1]->getId(), 'value' => strval($value[1]), 'class' => get_class($value[1]));
                    }
                }
            }

        }
        return $changedData;
    }

}
