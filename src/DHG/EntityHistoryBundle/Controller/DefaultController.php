<?php
namespace DHG\EntityHistoryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DHG\EntityHistoryBundle\EntityManager\EntityHistoryManager;

class DefaultController extends Controller
{
     /**
     * Presenta el listado de historial de cambios
     *
     * @Template("DHGEntityHistoryBundle:Default:timeline.html.twig")
     */
    public function timelineAction(Request $request){
        return array();
    } 
     /**
     * Presenta el listado de historial de cambios
     *
     * @Template("DHGEntityHistoryBundle:Default:timelineItems.html.twig")
     */
    public function timelineAjaxAction(Request $request,$of = 0){
        $EHM = new EntityHistoryManager($this->getDoctrine()->getManager());
        return array('histories' => $EHM->getAllHistoryGrouped($of));
    } 
}
