<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;


class MovClasifEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onMovClasifVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getMovClasif()->getId();
        $movClasif = $em->getRepository('DHGInventarioBundle:Movimiento')->findOneBy(array('clasificacion' => $id));
        if($movClasif != null){
            $event->stopRemove(sprintf('No es posible eliminar la clasificacion de movimiento "%s", existen movimientos que la utilizan.', $event->getMovClasif()->getNombre() ), 'Inventario');
        }
    }


}
