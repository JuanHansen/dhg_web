<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\Entity\Stock;


class MovimientoEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    public function onMovimientoCreatedEvent($event){
        

        $producto   = $event->getEntity()->getProducto();
        $idAlmacenO = $event->getEntity()->getAlmacenOrigen();
        $idAlmacenD = $event->getEntity()->getAlmacenDestino();
        $cantidad   = $event->getEntity()->getCantidad();
        $peso       = $event->getEntity()->getPeso();

        $this->cargarStock($idAlmacenO,$producto,$cantidad*-1,$peso*-1);
        $this->cargarStock($idAlmacenD,$producto,$cantidad,$peso);

        return;
    }
    public function onMovimientoEditedEvent($event){
        $em         = $this->entityManager;
        $changeset  = $em->getUnitOfWork()->getOriginalEntityData($event->getEntity());
        $cantidadOld= $changeset['cantidad'];
        $pesoOld    = $changeset['peso'];
        $almacenOOld= $changeset['almacenOrigen'];
        $almacenDOld= $changeset['almacenDestino'];
        $productoOld= $changeset['producto'];

        $this->cargarStock($almacenDOld,$productoOld,$cantidadOld*-1,$pesoOld*-1);
        $this->cargarStock($almacenOOld,$productoOld,$cantidadOld,$pesoOld);

        $producto   = $event->getEntity()->getProducto();
        $idAlmacenO = $event->getEntity()->getAlmacenOrigen();
        $idAlmacenD = $event->getEntity()->getAlmacenDestino();
        $cantidad   = $event->getEntity()->getCantidad();
        $peso       = $event->getEntity()->getPeso();

        $this->cargarStock($idAlmacenO,$producto,$cantidad*-1,$peso*-1);
        $this->cargarStock($idAlmacenD,$producto,$cantidad,$peso);

        return;
    }
    public function onMovimientoRemovedEvent($event){

        $producto   = $event->getEntity()->getProducto();
        $idAlmacenO = $event->getEntity()->getAlmacenOrigen();
        $idAlmacenD = $event->getEntity()->getAlmacenDestino();
        $cantidad   = $event->getEntity()->getCantidad();
        $peso       = $event->getEntity()->getPeso();

        $this->cargarStock($idAlmacenD,$producto,$cantidad*-1,$peso*-1);
        $this->cargarStock($idAlmacenO,$producto,$cantidad,$peso);

        return;
    }

    public function cargarStock($almacen,$producto,$cantidad = 0,$peso = 0){
        if($almacen == null) return;
        $em      = $this->entityManager;
        $stock   = $em->getRepository('DHGInventarioBundle:Stock')->findOneBy(array('producto' => $producto,'almacen'=>$almacen));
        if($stock == null){
            $stock = new Stock();
            $stock->setAlmacen($almacen);
            $stock->setProducto($producto);
            $stock->setCantidad(0);
            $stock->setPeso(0);
        }
        $stock->setCantidad($stock->getCantidad()+$cantidad);
        $stock->setPeso($stock->getPeso()+$peso);

        $em->persist($stock);
        $em->flush();
    }
}
