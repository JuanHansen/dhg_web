<?php
namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Entity\ProductoClasif;
use DHG\InventarioBundle\Entity\MovClasif;

class BundleEventsListener{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Metodo de instalacion
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    static public function onInstall($event)
    {
        $em = $event->getEntityManager();
        $logger = $event->getLogger();
        $logger->info('Install Inventario');

        //Cargo clasificacion de movimientos
        $xml = simplexml_load_file(__DIR__.'/../Resources/static/MovimientoClasif.xml');
        foreach($xml->children() as $child) {
            $movClasif = $em->getRepository('DHGInventarioBundle:MovClasif')->findOneByNombre($child->nombre);
            if (empty($movClasif)) {
                $obj = new MovClasif();
                $obj->setNombre((string)$child->nombre);
                $obj->setDetalle((string)$child->detalle);
                $obj->setSistema(self::get_bool($child->sistem));
                $em->persist($obj);
            }
        }
        try {
            $em->flush();
        } catch(Exception $e) {
            $logger->error($e);
        }

        //Cargo clasificacion de productos
        $xml = simplexml_load_file(__DIR__.'/../Resources/static/ProductoClasif.xml');
        foreach($xml->children() as $child) {
            $productoClasif = $em->getRepository('DHGInventarioBundle:ProductoClasif')->findOneByNombre($child->nombre);
            if (empty($productoClasif)) {
                $obj = new ProductoClasif();
                $obj->setNombre((string)$child->nombre);
                $obj->setDetalle((string)$child->detalle);
                $obj->setSistema((integer)$child->sistem);
                $em->persist($obj);
            }
        }
        try {
            $em->flush();
        } catch(Exception $e) {
            $logger->error($e);
        }

    }
    static public function get_bool($value){
        switch( strtolower($value) ){
            case 'true': return true;
            case 'false': return false;
            default: return false;
        }
    }
}
