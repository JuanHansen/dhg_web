<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\EntityManager\ProductoManager;

class ProductoEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onProductoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getProducto()->getId();
        $movimiento = $em->getRepository('DHGInventarioBundle:Movimiento')->findOneBy(array('producto' => $id));
        if($movimiento != null){
            $event->stopRemove(sprintf('No es posible eliminar el Producto "%s", ya que está asociado a un Movimiento', $event->getProducto()->getNombre() ), 'Inventario');
            return false;
        }
    }
}
