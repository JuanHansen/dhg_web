<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;


class ProductoClasifEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * Determina si existe almenos un producto dentro de la clasificacion. Si es asi, evita la eliminacion de esta
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onProductoClasifRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getProductoClasif()->getId();
        $productoClasif = $em->getRepository('DHGInventarioBundle:Producto')->findOneBy(array('clasificacion' => $id));
        if($productoClasif != null){
            $event->stopRemove('Existe almenos un producto con esta clasificacion. Elimine primero el producto con esta clasificacion o reasignelo antes de proseguir.', 'Inventario');
        }
    }


}
