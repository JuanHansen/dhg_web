<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\EntityManager\CliProManager;

class CliProEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onCliProRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getCliPro()->getId();
        $movimiento = $em->getRepository('DHGInventarioBundle:Movimiento')->findOneBy(array('clienteproveedor' => $id));
        if($movimiento != null){
            $event->stopRemove(sprintf('No es posible eliminar el Cliente/Proveedor "%s", ya que está asociado a un Movimiento', $event->getCliPro()->getNombre() ), 'Inventario');
            return false;
        }
    }
}
