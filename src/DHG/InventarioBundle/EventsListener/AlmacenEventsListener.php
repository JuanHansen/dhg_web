<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;


class AlmacenEventsListener{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\InventarioBundle\Events\InventarioEvents $event
     */
    public function onAlmacenRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $id = $event->getAlmacen()->getId();
        $productoClasif = $em->getRepository('DHGInventarioBundle:Movimiento')->findOneBy(array('Almacen' => $id));
        if($productoClasif != null){
            $event->stopRemove(sprintf('No es posible eliminar la Almacen %s, existen movimientos que la utilizan.', $event->getAlmacen()->getName() ), 'Inventario');
        }
    }


}
