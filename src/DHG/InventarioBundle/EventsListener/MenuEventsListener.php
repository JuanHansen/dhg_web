<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\coreBundle\Event\ConfigureMenuEvent;
use DHG\coreBundle\Menu\DefMenuEventsListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use DHG\InventarioBundle\Entity\Movimiento;
use DHG\InventarioBundle\Entity\Producto;
use DHG\InventarioBundle\Entity\CliPro;
use DHG\InventarioBundle\Entity\MovClasif;
use DHG\InventarioBundle\Entity\ProductoClasif;
use DHG\InventarioBundle\Entity\Stock;
use DHG\InventarioBundle\Security\InventarioPermissions;
use DHG\InventarioBundle\Security\AlmacenesPermissions;
use DHG\InventarioBundle\Entity\Almacen;

class MenuEventsListener extends DefMenuEventsListener{

    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct( $eventDispatcher,$mensaje = "Agregar")
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->mensaje = $mensaje;
    }


    /**
     * @param DHG\coreBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureNavBar($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        
        $MenuAnidado = $factory ->createItem('Inventario', array())
                        	    ->setExtra('orderNumber', 5)
                                ->setAttribute('dropdown', true)
                        	    ->setAttribute('icon', 'eye');
        $MenuAnidado->addChild(
                    $factory->createItem('Movimiento', array('route' => 'dhg_inventario_movimientoList'))
                            ->setExtra('orderNumber',4)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERMOVIMIENTO,'entity'=>new \DHG\InventarioBundle\Entity\Movimiento()))
                    );
        $MenuAnidado->addChild(
                    $factory->createItem('Productos', array('route' => 'dhg_inventario_productoList'))
                            ->setExtra('orderNumber',6)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERPRODUCTO,'entity'=>new Producto()))
                    );
        $MenuAnidado->addChild(
                    $factory->createItem('Stock', array('route' => 'dhg_inventario_stockList'))
                            ->setExtra('orderNumber',1)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERSTOCK,'entity'=>new \DHG\InventarioBundle\Entity\Stock()))
                    );
        $MainMenuAjustes = $parentMenu->getChild('Ajustes');
        $MenuAjustes = $factory ->createItem('Inventario', array())
                                ->setAttribute('dropright', true);
        $MenuAjustes->addChild(
                    $factory->createItem('Marca', array('route' => 'dhg_inventario_marcaList'))
                            ->setExtra('orderNumber',4)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERMARCA,'entity'=>new \DHG\InventarioBundle\Entity\Marca()))
                    );
        $MenuAjustes->addChild(
                    $factory->createItem('Familia', array('route' => 'dhg_inventario_familiaList'))
                            ->setExtra('orderNumber',5)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERFAMILIA,'entity'=>new \DHG\InventarioBundle\Entity\Familia()))
                    );
        $MenuAjustes->addChild(
                    $factory->createItem('Envase', array('route' => 'dhg_inventario_envaseList'))
                            ->setExtra('orderNumber',6)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERENVASE,'entity'=>new \DHG\InventarioBundle\Entity\Envase()))
                    );
        $MenuAjustes->addChild(
                    $factory->createItem('Clasificacion de movimientos', array('route' => 'dhg_inventario_movclasifList'))
                            ->setExtra('orderNumber',1)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERMOVCLASIF,'entity'=>new \DHG\InventarioBundle\Entity\MovClasif()))
                    );
        $MenuAjustes->addChild(
                    $factory->createItem('Clasificacion de productos', array('route' => 'dhg_inventario_productoclasifList'))
                            ->setExtra('orderNumber',2)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERPRODUCTOCLASIF,'entity'=>new \DHG\InventarioBundle\Entity\ProductoClasif()))

                    );
        $MenuAjustes->addChild(
                    $factory->createItem('Almacen', array('route' => 'dhg_inventario_almacenList'))
                            ->setExtra('orderNumber',3)
                            ->setAttribute('icon', 'cube')
                            ->setExtra('permission',array('permission'=>InventarioPermissions::VERALMACEN,'entity'=>new \DHG\InventarioBundle\Entity\Almacen()))
            );

        $MainMenuAjustes->addChild($MenuAjustes); 
        $parentMenu->addChild($MenuAnidado); 
    }
}
        
        