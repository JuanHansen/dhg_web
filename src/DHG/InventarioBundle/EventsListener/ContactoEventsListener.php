<?php

namespace DHG\InventarioBundle\EventsListener;

use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\EntityManager\CliProManager;

class ContactoEventsListener
{
    
    protected $eventDispatcher;
    protected $entityManager;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $entityManager){
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedVerificationEvent $event
     */
    public function onContactoRemovedVerificationEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array('contacto' => $idcontacto));
            if($clipro!=null){
                $um = new CliProManager($em,$this->eventDispatcher);
                $error = array();
                if($um->removeVerificationCliPro($clipro,$error)){
                    return true;
                }else{
                    $event->stopRemove($error,'Clienteproveedor');
                    return false;
                }
            }
        }
    }

    /**
     *
     * @param DHG\ContactosBundle\Events\ContactoRemovedEvent $event
     */
    public function onContactoRemovedEvent($event){
        $em = $this->entityManager;
        $idcontacto = $event->getContacto()->getId();
        if($idcontacto){
            $clipro = $em->getRepository('DHGInventarioBundle:CliPro')->findOneBy(array('contacto' => $idcontacto));
            if($clipro!=null){
                $um = new CliProManager($em, $this->eventDispatcher);
                $error = array();
                if($um->removeCliPro($clipro,$error)){
                    return true;
                }else{
                    $error[]='No se pudo eliminar el clipro';
                    $event->stopRemove($error,'CliPro');
                    return false;
                }
            }
        }
    }

}
