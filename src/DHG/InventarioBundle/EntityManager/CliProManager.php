<?php

namespace DHG\InventarioBundle\EntityManager;

use DHG\InventarioBundle\Entity\CliPro;
use Doctrine\ORM\EntityManager;

use DHG\InventarioBundle\Events\InventarioEvents;
use DHG\InventarioBundle\Events\CliProCreatedEvent;
use DHG\InventarioBundle\Events\CliProEditedEvent;
use DHG\InventarioBundle\Events\CliProRemovedEvent;
use DHG\InventarioBundle\Events\CliProRemovedVerificationEvent;
use DHG\InventarioBundle\Events\CliProCreatedVerificationEvent;

class CliProManager{
	private $em;
	private $ed;


	/**
	 * Constructor
	 *
	 * @param $em EntityManager que dara el acceso al mandejo de db
	 * @param $ed EventDispatcher que llama a los eventos necesarios
	 *
	 */
    public function __construct(EntityManager $em, $ed){
        $this->em = $em;
        $this->ed = $ed;
    }

    /**
     * Crea en base de datos la contacto
     *
	 * @param $contacto CliPro con la informacion a almacenar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function createCliPro(CliPro $contacto, &$error = null ){
        $event = new CliProCreatedEvent($contacto);
        if($this->ed->dispatch(InventarioEvents::CLIPRO_CREATED, $event)){
    	   try{
                $this->em->persist($contacto);
                $this->em->flush();
            } catch (\Exception $e){
        	   $error[] = "Ocurrio un error al intentar crear un Cliente/Proveedor";
        	   return false;
            }
        }else{
            $error =array_merge($error,$event->getMessages());
            return false;
        }
        return true;
    }      

    /**
     * Edita en base de datos la clipro
     *
     * @param $clipro CliPro con la informacion a almacenar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si todo fue executado correctamente
     *
     */
    public function editCliPro(CliPro $clipro, &$error = null ){
        $event = new CliProEditedEvent($clipro);
        if($this->ed->dispatch(InventarioEvents::CLIPRO_EDITED, $event)){
        try{
            $this->em->persist($clipro);
            $this->em->flush();
        } catch (\Exception $e){
            $error[] = "Ocurrio un error al intentar editar un Cliente/Proveedor";
            return false;
        }
    }else{
        $error =array_merge($error,$event->getMessages());
        return false;
    }
        return true;
    }   

    /**
     * Elimina en base de datos la contacto
     *
	 * @param $contacto CliPro con la informacion a eliminar
	 * @param $error Mensaje de error en caso de suceder
	 *
	 * @return true si todo fue executado correctamente
	 *
     */
    public function removeCliPro(CliPro $contacto, &$error = array() ){
    	if ($this->removeVerificationCliPro($contacto, $error)){
            $event = new CliProRemovedEvent($contacto);
            if($this->ed->dispatch(InventarioEvents::CLIPRO_REMOVED, $event)){
                try{
                    $this->em->remove($contacto);
                    $this->em->flush();
                } catch (\Exception $e){
                    $error[] = "Ocurrio un error al intentar eliminar el Cliente/Proveedor";
                    return false;
                }
            }else{
                $error =array_merge($error,$event->getMessages());
                return false;
            }
        }else{
            //$error = $event->getMessages();
            return false;
        }
        return true;
    }  

    /**
     * Realiza el llamado al evento de verificacion de cada modulo externo previo a la eliminacion
     *
     * @param $contacto CliPro con la informacion a eliminar
     * @param $error Mensaje de error en caso de suceder
     *
     * @return true si se puede proceder al eliminado
     *
     */
    public function removeVerificationCliPro(CliPro $contacto, &$error = array() ){
        $event = new CliProRemovedVerificationEvent($contacto);
        $this->ed->dispatch(InventarioEvents::CLIPRO_REMOVED_VERIFICATION, $event);
        if ($event->isStoped()){
            $error =array_merge($error,$event->getMessages());
            return false;
        }
        return true;
    }  

}
