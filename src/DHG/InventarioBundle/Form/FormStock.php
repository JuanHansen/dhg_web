<?php
namespace DHG\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class FormStock extends AbstractType
{


        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder       
        ->add('cantidad', 'number', array(
            'label' => "cantidad",
            'required' => false,
            'attr' => array(
                'placeholder' => "0",
            ),
        ))
        ->add('peso', 'number', array(
            'label' => "peso",
            'required' => false,
            'attr' => array(
                'placeholder' => "0",
            ),
        ))
        ->add('producto', 'entity', array(
            'label' => 'producto',
            'class' => 'DHG\InventarioBundle\Entity\Producto',
            'disabled' => true,
        ))

        ->add('almacen', 'entity', array(
            'label' => 'almacen',
            'class' => 'DHG\InventarioBundle\Entity\Almacen',
            'disabled' => true,
        ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\InventarioBundle\Entity\Stock'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dhg_inventariobundle_stock';
    }
}
