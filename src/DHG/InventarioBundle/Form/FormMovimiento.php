<?php
namespace DHG\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class FormMovimiento extends AbstractType
{


        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder        
        ->add('fecha','date',array(
            'label' => 'Fecha',
            'widget' => 'single_text',
            'constraints' =>  new NotBlank(array('message'=>'Es necesario ingresar una fecha')),
        ))
        ->add('cantidad', 'number', array(
            'label' => "cantidad",
            'required' => true,
            'attr' => array(
                'placeholder' => "0",
            ),
            'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
        ))
        ->add('peso', 'number', array(
            'label' => "peso",
            'required' => true,
            'attr' => array(
                'placeholder' => "0",
            ),
            'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
        ))
        ->add('detalle', 'textarea', array(
            'label' => "detalle",
            'required' => false,
        ))
        ->add('producto', 'entity', array(
            'label' => 'producto',
            'class' => 'DHG\InventarioBundle\Entity\Producto',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un valor')),
            'required' => true
        ,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_productoCreate',
            ),
        ))

        ->add('clasificacion', 'entity', array(
            'label' => 'clasificacion',
            'class' => 'DHG\InventarioBundle\Entity\MovClasif',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un valor')),
            'required' => true
        ,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_movclasifCreate',
            ),
        ))

        ->add('almacenOrigen', 'entity', array(
            'label' => 'almacenOrigen',
            'class' => 'DHG\InventarioBundle\Entity\Almacen',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_almacenCreate',
            ),
        ))

        ->add('almacenDestino', 'entity', array(
            'label' => 'almacenDestino',
            'class' => 'DHG\InventarioBundle\Entity\Almacen',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_almacenCreate',
            ),
        ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\InventarioBundle\Entity\Movimiento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dhg_inventariobundle_movimiento';
    }
}
