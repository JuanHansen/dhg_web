<?php
namespace DHG\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class FormAlmacen extends AbstractType
{


        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder        
        ->add('nombre', 'text', array(
            'label' => "nombre",
            'required' => true,
            'attr' => array(
                'placeholder' => "Nombre",
            ),
            'constraints' =>  new NotBlank(array('message'=>'El campo es obligatorio')),
        ))
        ->add('codigoLocal', 'text', array(
            'label' => "codigoLocal",
            'required' => true,
            'attr' => array(
                'placeholder' => "Codigolocal",
            ),
            'constraints' =>  new NotBlank(array('message'=>'El campo es obligatorio')),
        ))
        ->add('padre', 'entity', array(
            'label' => 'padre',
            'class' => 'DHG\InventarioBundle\Entity\Almacen',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_almacenCreate',
            ),
        ))
        ->add('description', 'textarea', array(
            'label' => "description",
            'required' => false,
        ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\InventarioBundle\Entity\Almacen'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dhg_inventariobundle_almacen';
    }
}
