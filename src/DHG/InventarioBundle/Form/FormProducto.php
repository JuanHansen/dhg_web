<?php
namespace DHG\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class FormProducto extends AbstractType
{


        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre', 'text', array(
            'label' => "nombre",
            'required' => true,
            'attr' => array(
                'placeholder' => "Nombre",
            ),
            'constraints' =>  new NotBlank(array('message'=>'El campo es obligatorio')),
        ))
        ->add('codigo_local', 'text', array(
            'label' => "codigo_local",
            'required' => false,
            'attr' => array(
                'placeholder' => "Codigo_Local",
            ),
            'constraints' =>  new NotBlank(array('message'=>'El campo es obligatorio')),
        ))
        ->add('codigo_producto', 'text', array(
            'label' => "codigo_producto",
            'required' => false,
            'attr' => array(
                'placeholder' => "Codigo_Producto",
            ),
            'constraints' =>  new NotBlank(array('message'=>'El campo es obligatorio')),
        ))
        ->add('unidadesEnvase', 'number', array(
            'label' => "unidadesEnvase",
            'required' => true,
            'attr' => array(
                'placeholder' => "0",
            ),
            'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
        ))
        ->add('unidadesPalet', 'number', array(
            'label' => "unidadesPalet",
            'required' => true,
            'attr' => array(
                'placeholder' => "0",
            ),
            'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
        ))
        ->add('controlUnidad', 'checkbox', array(
            'label'  => "controlUnidad",
            'widget_checkbox_label' => "label",
            'required' => false,
            'help_label_popover'=>array('title'=>"Controlunidad",'content'=>"-"),
        ))
        ->add('controlPeso', 'checkbox', array(
            'label'  => "controlPeso",
            'widget_checkbox_label' => "label",
            'required' => false,
            'help_label_popover'=>array('title'=>"Controlpeso",'content'=>"-"),
        ))
        ->add('pesoProm', 'number', array(
            'label' => "pesoProm",
            'required' => true,
            'attr' => array(
                'placeholder' => "0",
            ),
            'constraints' =>  new  GreaterThanOrEqual(array('value'=>0,'message'=>"La cantidad debe ser un numero positivo")),
        ))
        ->add('detalle', 'textarea', array(
            'label' => "detalle",
            'required' => false,
        ))
        ->add('marca', 'entity', array(
            'label' => 'marca',
            'class' => 'DHG\InventarioBundle\Entity\Marca',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_marcaCreate',
            ),
        ))

        ->add('familia', 'entity', array(
            'label' => 'familia',
            'class' => 'DHG\InventarioBundle\Entity\Familia',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_familiaCreate',
            ),
        ))

        ->add('envase', 'entity', array(
            'label' => 'envase',
            'class' => 'DHG\InventarioBundle\Entity\Envase',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'empty_value' => 'Seleccione un valor',
            'required' => false,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_envaseCreate',
            ),
        ))

        ->add('clasificacion', 'entity', array(
            'label' => 'clasificacion',
            'class' => 'DHG\InventarioBundle\Entity\ProductoClasif',
            'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('m')
                                ->orderBy('m.nombre', 'ASC');
            },
            'constraints' =>  new NotBlank(array('message'=>'Es necesario seleccionar un valor')),
            'required' => true
        ,
            'attr' => array(),
            'widget_addon_append' => array(
                'form_ajax'=>'dhg_inventario_productoclasifCreate',
            ),
        ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\InventarioBundle\Entity\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dhg_inventariobundle_producto';
    }
}
