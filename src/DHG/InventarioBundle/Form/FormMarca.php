<?php

namespace DHG\InventarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormMarca extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre')

            ->add('codigoLocal')

            ->add('codigoEmpresa')

            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DHG\InventarioBundle\Entity\Marca'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dhg_inventariobundle_marca';
    }
}
