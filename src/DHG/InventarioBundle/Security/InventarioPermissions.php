<?php
namespace DHG\InventarioBundle\Security;

use DHG\UserBundle\Components\DHGPermissionVoter;
use DHG\UserBundle\Event\ConfigurePermissions;
use DHG\UserBundle\Entity\PermissionClasification;
use DHG\UserBundle\Entity\Permission;
use DHG\UserBundle\Components\DHGPermissionInterface;

class InventarioPermissions extends DHGPermissionVoter
{
    const PREFIX = 'inventario';
    const VERPRODUCTO = 'inventario_ver_productos';
    const GESTIONARPRODUCTO = 'inventario_gestionar_productos';
    const VERMARCA = 'inventario_ver_marca';
    const GESTIONARMARCA = 'inventario_gestionar_marca';

    const VERFAMILIA = 'inventario_ver_familia';
    const GESTIONARFAMILIA = 'inventario_gestionar_familia';
    const VERENVASE = 'inventario_ver_envase';
    const GESTIONARENVASE = 'inventario_gestionar_envase';
    const VERALMACEN = 'inventario_ver_almacen';
    const GESTIONARALMACEN = 'inventario_gestionar_almacen';
    const VERPRODUCTOCLASIF = 'inventario_ver_productoclasif';
    const GESTIONARPRODUCTOCLASIF = 'inventario_gestionar_productoclasif';      
    const VERMOVCLASIF = 'inventario_ver_movclasif';
    const GESTIONARMOVCLASIF = 'inventario_gestionar_movclasif';
    const VERMOVIMIENTO = 'inventario_ver_movimiento';
    const GESTIONARMOVIMIENTO = 'inventario_gestionar_movimiento';
    const VERSTOCK = 'inventario_ver_stock';
    const GESTIONARSTOCK = 'inventario_gestionar_stock';
    /**
     * {@inheritdoc}
     */
    public function supportsAttribute($attribute)
    {
        return 0 === strpos($attribute, PREFIX);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedClasses(){
        $supportedClasses = array();
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Producto';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\ProductoClasif';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\MovClasif';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Movimiento';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Stock';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Marca';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Familia';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Envase';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Almacen';
        $supportedClasses[] = 'DHG\InventarioBundle\Entity\Stock';
        return $supportedClasses;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissions(ConfigurePermissions $event){
        $this->actualModule = InventarioPermissions::PREFIX;
        $this->perm = $event->getPermissions();

        $this->permissionClasif = array();
        $this->permissionClasif[InventarioPermissions::PREFIX] = new PermissionClasification();
        $this->permissionClasif[InventarioPermissions::PREFIX]->setMachinename('inventario');
        $this->permissionClasif[InventarioPermissions::PREFIX]->setName('Gestion de inventario');
        $this->permissionClasif[InventarioPermissions::PREFIX]->setDetails('Gestion de la seccion de inventario');

        $this->crearPermiso(InventarioPermissions::VERPRODUCTO,"Producto",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARPRODUCTO,"Producto",true,InventarioPermissions::VERPRODUCTO);

        //$this->crearPermiso(InventarioPermissions::VERSTOCK,"Stock",false);

        $this->crearPermiso(InventarioPermissions::VERMARCA,"Marca",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARMARCA,"Marca",true,InventarioPermissions::VERMARCA);

        $this->crearPermiso(InventarioPermissions::VERFAMILIA,"Familia",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARFAMILIA,"Familia",true,InventarioPermissions::VERFAMILIA);

        $this->crearPermiso(InventarioPermissions::VERENVASE,"Envase",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARENVASE,"Envase",true,InventarioPermissions::VERENVASE);

        $this->crearPermiso(InventarioPermissions::VERALMACEN,"Almacen",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARALMACEN,"Almacen",true,InventarioPermissions::VERALMACEN);

        $this->crearPermiso(InventarioPermissions::VERPRODUCTOCLASIF,"Clasificacion de Productos",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARPRODUCTOCLASIF,"Almacen",true,InventarioPermissions::VERPRODUCTOCLASIF);

        $this->crearPermiso(InventarioPermissions::VERMOVCLASIF,"Clasificacion de Movimientos",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARMOVCLASIF,"Clasificacion de Movimientos",true,InventarioPermissions::VERMOVCLASIF);
       
        $this->crearPermiso(InventarioPermissions::VERMOVIMIENTO,"Movimiento",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARMOVIMIENTO,"Movimiento",true,InventarioPermissions::VERMOVIMIENTO);
        
        $this->crearPermiso(InventarioPermissions::VERSTOCK,"Stock",false);
        $this->crearPermiso(InventarioPermissions::GESTIONARSTOCK,"Stock",true,InventarioPermissions::VERSTOCK);
        
        $event->setPermissions($this->perm);
    }    
}		
		






        
