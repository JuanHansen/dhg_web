<?php

namespace DHG\InventarioBundle\Controller;

use DHG\coreBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use DHG\InventarioBundle\Entity\ProductoClasif;
use DHG\InventarioBundle\Form\FormProductoClasif;
use DHG\InventarioBundle\Model\ProductoClasifTable;
use DHG\InventarioBundle\EntityManager\ProductoClasifManager;
use DHG\InventarioBundle\EventsListener\MenuEventsListener;
use DHG\InventarioBundle\Security\InventarioPermissions;


class ProductoClasifController extends DefaultController {
    /**
     * @Template("DHGcoreBundle:Default:tablelist.html.twig")
     */
    public function listAction(Request $request){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        $this->genero = "f";
        return $this->defListAction($request);
 
    }

    /**
    * Edita un elemento
    * 
    * @param Id ID del elemento
    *
    */
    public function editAction($id){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        $this->genero = "f";
        return $this->defEditAction($id);
    }

    /**
     * Elimina un elemento
     * 
     * @param Id ID del elemento a eliminar
     */
    public function removeAction($id){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        $this->genero = "f";
        return $this->defRemoveAction($id);
    }

    /**
    * Crea una entidad. Si es un GET retorna el formulario, sino, si la creacion es exitosa, retorna un listado de las entidades
    * @return Formulario de creacion (200) si no hay parametros (POST) o los parametros no validan
    *    Forbidden (403) si no se tiene acceso
    *    Options (201) Si se creo la entidad con exito (retorna el listado de entidades listo para poner en el select)
    *    Mensajes de Error (202) Si no se pudo crear por alguna cuestión
    *    Error de Servidor (500) Si sucede algun error del servidor
    */
    public function createAjaxAction(){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        $this->genero = "f";
        return $this->defCreateAjaxAction();
    }

    /**
    * Retorna una lista de entidades
    * @return array de entidades
    */
    private function getList(){
        $this->iniciarVariables();
        $this->autoGenerateVars();
        $this->genero = "f";
        return $this->defGetList();
    }

    public function iniciarVariables(){
        $this->namespace    = "DHG\InventarioBundle";
        $this->name         = "ProductoClasif";
        $this->route         = "dhg_inventario_";
    }
}
