<?php

namespace DHG\InventarioBundle\Events;

use DHG\InventarioBundle\Entity\CliPro;
use Symfony\Component\EventDispatcher\Event;

class CliProCreatedEvent extends Event{
    private $contacto;

    /**
     * @param CliPro $contacto 
     *      CliPro created
     */
    public function __construct(CliPro $contacto){
        $this->contacto = $contacto;
    }

    /**
     * @return CliPro
     */
    public function getCliPro(){
        return $this->contacto;
    }
}