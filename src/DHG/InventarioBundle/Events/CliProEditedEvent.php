<?php

namespace DHG\InventarioBundle\Events;

use DHG\InventarioBundle\Entity\CliPro;
use Symfony\Component\EventDispatcher\Event;

class CliProEditedEvent extends Event{
    private $contacto;

    /**
     * @param CliPro $contacto 
     *      CliPro
     */
    public function __construct(CliPro $contacto){
        $this->contacto = $contacto;
    }

    /**
     * @return CliPro
     */
    public function getCliPro(){
        return $this->contacto;
    }
}
