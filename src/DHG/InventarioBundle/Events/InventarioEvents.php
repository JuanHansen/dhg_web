<?php
namespace DHG\InventarioBundle\Events;

final class InventarioEvents {

    /**
     * @var string
     */
    const CLIPRO_CREATED = 'dhg_inventario.cliproCreated';

    /**
     * @var string
     */
    const CLIPRO_EDITED = 'dhg_inventario.cliproEdited';

    /**
     * @var string
     */
    const CLIPRO_REMOVED = 'dhg_inventario.cliproRemoved';

    /**
     * @var string
     */
    const CLIPRO_REMOVED_VERIFICATION = 'dhg_inventario.cliproRemovedVerification';
    /**
     * @var string
     */
    const CLIPRO_CREATED_VERIFICATION = 'dhg_inventario.cliproCreatedVerification';



    
    /**
     * @var string
     */
    const MARCA_CREATED = 'dhg_inventario.marcaCreated';
    /**
     * @var string
     */
    const MARCA_EDITED = 'dhg_inventario.marcaEdited';
    /**
     * @var string
     */
    const MARCA_REMOVED = 'dhg_inventario.marcaRemoved';
    /**
     * @var string
     */
    const MARCA_REMOVED_VERIFICATION = 'dhg_inventario.marcaRemovedVerification';

    /**
     * @var string
     */
    const FAMILIA_CREATED = 'dhg_inventario.familiaCreated';
    /**
     * @var string
     */
    const FAMILIA_EDITED = 'dhg_inventario.familiaEdited';
    /**
     * @var string
     */
    const FAMILIA_REMOVED = 'dhg_inventario.familiaRemoved';
    /**
     * @var string
     */
    const FAMILIA_REMOVED_VERIFICATION = 'dhg_inventario.familiaRemovedVerification';
    /**
     * @var string
     */
    const ENVASE_CREATED = 'dhg_inventario.envaseCreated';
    /**
     * @var string
     */
    const ENVASE_EDITED = 'dhg_inventario.envaseEdited';
    /**
     * @var string
     */
    const ENVASE_REMOVED = 'dhg_inventario.envaseRemoved';
    /**
     * @var string
     */
    const ENVASE_REMOVED_VERIFICATION = 'dhg_inventario.envaseRemovedVerification';
    /**
     * @var string
     */
    const PRODUCTO_CREATED = 'dhg_inventario.productoCreated';
    /**
     * @var string
     */
    const PRODUCTO_EDITED = 'dhg_inventario.productoEdited';
    /**
     * @var string
     */
    const PRODUCTO_REMOVED = 'dhg_inventario.productoRemoved';
    /**
     * @var string
     */
    const PRODUCTO_REMOVED_VERIFICATION = 'dhg_inventario.productoRemovedVerification';
    /**
     * @var string
     */
    const ALMACEN_CREATED = 'dhg_inventario.almacenCreated';
    /**
     * @var string
     */
    const ALMACEN_EDITED = 'dhg_inventario.almacenEdited';
    /**
     * @var string
     */
    const ALMACEN_REMOVED = 'dhg_inventario.almacenRemoved';
    /**
     * @var string
     */
    const ALMACEN_REMOVED_VERIFICATION = 'dhg_inventario.almacenRemovedVerification';
    /**
     * @var string
     */
    const PRODUCTOCLASIF_CREATED = 'dhg_inventario.productoclasifCreated';
    /**
     * @var string
     */
    const PRODUCTOCLASIF_EDITED = 'dhg_inventario.productoclasifEdited';
    /**
     * @var string
     */
    const PRODUCTOCLASIF_REMOVED = 'dhg_inventario.productoclasifRemoved';
    /**
     * @var string
     */
    const PRODUCTOCLASIF_REMOVED_VERIFICATION = 'dhg_inventario.productoclasifRemovedVerification';
    /**
     * @var string
     */
    const MOVCLASIF_CREATED = 'dhg_inventario.movclasifCreated';
    /**
     * @var string
     */
    const MOVCLASIF_EDITED = 'dhg_inventario.movclasifEdited';
    /**
     * @var string
     */
    const MOVCLASIF_REMOVED = 'dhg_inventario.movclasifRemoved';
    /**
     * @var string
     */
    const MOVCLASIF_REMOVED_VERIFICATION = 'dhg_inventario.movclasifRemovedVerification';
    /**
     * @var string
     */
    const MOVIMIENTO_CREATED = 'dhg_inventario.movimientoCreated';
    /**
     * @var string
     */
    const MOVIMIENTO_EDITED = 'dhg_inventario.movimientoEdited';
    /**
     * @var string
     */
    const MOVIMIENTO_REMOVED = 'dhg_inventario.movimientoRemoved';
    /**
     * @var string
     */
    const MOVIMIENTO_REMOVED_VERIFICATION = 'dhg_inventario.movimientoRemovedVerification';
    /**
     * @var string
     */
    const STOCK_CREATED = 'dhg_inventario.stockCreated';
    /**
     * @var string
     */
    const STOCK_EDITED = 'dhg_inventario.stockEdited';
    /**
     * @var string
     */
    const STOCK_REMOVED = 'dhg_inventario.stockRemoved';
    /**
     * @var string
     */
    const STOCK_REMOVED_VERIFICATION = 'dhg_inventario.stockRemovedVerification';
}