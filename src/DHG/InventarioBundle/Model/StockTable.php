<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class StockTable
 *
 * @DataTable\Table(id="StockTable"  )
 */
class StockTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {/**
     * @var decimal
     * @DataTable\Column(source="Stock.cantidad", name="cantidad")
     */
    protected $cantidad;
    /**
     * @var decimal
     * @DataTable\Column(source="Stock.peso", name="peso")
     */
    protected $peso;
    /**
     * @var string
     * @DataTable\Column(source="Stock.producto", name="producto")
     * @DataTable\Format(dataFields={"test":"producto","valor":"producto"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $producto;

    /**
     * @var string
     * @DataTable\Column(source="Stock.almacen", name="almacen")
     * @DataTable\Format(dataFields={"test":"almacen","valor":"almacen"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $almacen;    
    /**
     * @var int
     * @DataTable\Column(source="Stock.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Stock","entityName":"'Stock'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Stock');
        $qb = $userRepository->createQueryBuilder('Stock');
        return $qb;
    }

}