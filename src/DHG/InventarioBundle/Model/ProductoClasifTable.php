<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class ProductoClasifTable
 *
 * @DataTable\Table(id="ProductoClasifTable"  )
 */
class ProductoClasifTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {/**
     * @var string
     * @DataTable\Column(source="ProductoClasif.nombre", name="nombre")
     */
    protected $nombre;
    /**
     * @var text
     * @DataTable\Column(source="ProductoClasif.detalle", name="detalle")
     */
    protected $detalle;    
    /**
     * @var int
     * @DataTable\Column(source="ProductoClasif.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"ProductoClasif","entityName":"'ProductoClasif'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\ProductoClasif');
        $qb = $userRepository->createQueryBuilder('ProductoClasif');
        return $qb;
    }

}