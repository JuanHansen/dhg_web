<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class MovimientoTable
 *
 * @DataTable\Table(id="MovimientoTable"  )
 */
class MovimientoTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {/**
     * @var string
     * @DataTable\Column(source="Movimiento.fecha", name="fecha", stype="date-eu")
     * @DataTable\Format(dataFields={"value":"fecha"}, template="DHGcoreBundle:Datatable:cellDate.html.twig")
     */
    protected $fecha;
    /**
     * @var decimal
     * @DataTable\Column(source="Movimiento.cantidad", name="cantidad")
     */
    protected $cantidad;
    /**
     * @var decimal
     * @DataTable\Column(source="Movimiento.peso", name="peso")
     */
    protected $peso;
    /**
     * @var text
     * @DataTable\Column(source="Movimiento.detalle", name="detalle")
     */
    protected $detalle;
    /**
     * @var string
     * @DataTable\Column(source="Movimiento.producto", name="producto")
     * @DataTable\Format(dataFields={"test":"producto","valor":"producto"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $producto;

    /**
     * @var string
     * @DataTable\Column(source="Movimiento.clasificacion", name="clasificacion")
     * @DataTable\Format(dataFields={"test":"clasificacion","valor":"clasificacion"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $clasificacion;

    /**
     * @var string
     * @DataTable\Column(source="Movimiento.almacenOrigen", name="almacenOrigen")
     * @DataTable\Format(dataFields={"test":"almacenOrigen","valor":"almacenOrigen"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $almacenOrigen;

    /**
     * @var string
     * @DataTable\Column(source="Movimiento.almacenDestino", name="almacenDestino")
     * @DataTable\Format(dataFields={"test":"almacenDestino","valor":"almacenDestino"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $almacenDestino;

    
    /**
     * @var int
     * @DataTable\Column(source="Movimiento.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Movimiento","entityName":"'Movimiento'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Movimiento');
        $qb = $userRepository->createQueryBuilder('Movimiento');
        return $qb;
    }

}