<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class AlmacenTable
 *
 * @DataTable\Table(id="AlmacenTable"  )
 */
class AlmacenTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {/**
     * @var string
     * @DataTable\Column(source="Almacen.nombre", name="nombre")
     */
    protected $nombre;
    /**
     * @var string
     * @DataTable\Column(source="Almacen.codigoLocal", name="codigoLocal")
     */
    protected $codigoLocal;
    /**
     * @var string
     * @DataTable\Column(source="Almacen.padre", name="padre")
     * @DataTable\Format(dataFields={"test":"padre","valor":"padre"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $padre;
    /**
     * @var text
     * @DataTable\Column(source="Almacen.description", name="description")
     */
    protected $description;
    /**
     * @var int
     * @DataTable\Column(source="Almacen.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Almacen","entityName":"'Almacen'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Almacen');
        $qb = $userRepository->createQueryBuilder('Almacen');
        return $qb;
    }

}