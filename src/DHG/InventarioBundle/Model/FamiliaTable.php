<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class FamiliaTable
 *
 * @DataTable\Table(id="FamiliaTable"  )
 */
class FamiliaTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {    
    /**
     * @var string
     * @DataTable\Column(source="Familia.nombre", name="nombre")
     */
    protected $nombre;
        
    /**
     * @var string
     * @DataTable\Column(source="Familia.codigo_local", name="codigoLocal")
     */
    protected $codigo_local;
    
    /**
     * @var int
     * @DataTable\Column(source="Familia.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Familia","entityName":"'Familia'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Familia');
        $qb = $userRepository->createQueryBuilder('Familia');
        return $qb;
    }

}