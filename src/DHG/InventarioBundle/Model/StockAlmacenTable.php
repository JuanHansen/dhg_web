<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class StockAlmacenTable
 *
 * @DataTable\Table(id="StockAlmacenTable")
 */
class StockAlmacenTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {

   /**
     * @var string
     * @DataTable\Column(source="Movimiento.producto.nombre", name="Producto")
     * @DataTable\DefaultSort()
     */
    public $producto;

   /**
     * @var string
     * @DataTable\Column(source="Movimiento.producto.clasificacion.nombre", name="Tipo")
     * @DataTable\DefaultSort()
     */
    public $tipo_producto;

    /**
     * @var string
     * @DataTable\Column(source="Movimiento.almacenDestino.name", name="Almacen")
     * @DataTable\DefaultSort()
     */
    protected $Almacen;
    
    /**
     * @var string
     * @DataTable\Column(source="Movimiento.producto", name="Disponibilidad")
     * @DataTable\Format(dataFields={"movimiento":"Movimiento"}, template="DHGInventarioBundle:Table:cantStockCell.html.twig")
     */
    public $cantidad;
    
    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null) {
       // $filtro = $this->container->get('doctrine.orm.entity_manager')->getFilters()->enable('removed');
        //$filtro->setParameter('eliminado',2);
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Movimiento');
        $qb = $userRepository->createQueryBuilder('Movimiento')
            ->where('Movimiento.almacenDestino IS NOT NULL')
            ->groupBy('Movimiento.producto')
            ->addGroupBy('Movimiento.almacenDestino');
        return $qb;

    
    }

}