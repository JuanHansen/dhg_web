<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class ProductoTable
 *
 * @DataTable\Table(id="ProductoTable"  )
 */
class ProductoTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {    /**
     * @var string
     * @DataTable\Column(source="Producto.nombre", name="nombre")
     */
    protected $nombre;

            /**
     * @var string
     * @DataTable\Column(source="Producto.codigo_local", name="codigo_local")
     */
    protected $codigo_local;

            /**
     * @var string
     * @DataTable\Column(source="Producto.codigo_producto", name="codigo_producto")
     */
    protected $codigo_producto;

            /**
     * @var integer
     * @DataTable\Column(source="Producto.unidadesEnvase", name="unidadesEnvase")
     */
    protected $unidadesEnvase;

            /**
     * @var integer
     * @DataTable\Column(source="Producto.unidadesPalet", name="unidadesPalet")
     */
    protected $unidadesPalet;

            /**
     * @var boolean
     * @DataTable\Column(source="Producto.controlUnidad", name="controlUnidad")
     * @DataTable\Format(dataFields={"value":"controlUnidad"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    protected $controlUnidad;
            /**
     * @var boolean
     * @DataTable\Column(source="Producto.controlPeso", name="controlPeso")
     * @DataTable\Format(dataFields={"value":"controlPeso"}, template="DHGcoreBundle:Datatable:cellBool.html.twig")
     */
    protected $controlPeso;
            /**
     * @var decimal
     * @DataTable\Column(source="Producto.pesoProm", name="pesoProm")
     */
    protected $pesoProm;

            /**
     * @var text
     * @DataTable\Column(source="Producto.detalle", name="detalle")
     */
    protected $detalle;

        /**
     * @var string
     * @DataTable\Column(source="Producto.marca", name="marca")
     * @DataTable\Format(dataFields={"test":"marca","valor":"marca"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $marca;/**
     * @var string
     * @DataTable\Column(source="Producto.familia", name="familia")
     * @DataTable\Format(dataFields={"test":"familia","valor":"familia"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $familia;/**
     * @var string
     * @DataTable\Column(source="Producto.envase", name="envase")
     * @DataTable\Format(dataFields={"test":"envase","valor":"envase"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $envase;/**
     * @var string
     * @DataTable\Column(source="Producto.clasificacion", name="clasificacion")
     * @DataTable\Format(dataFields={"test":"clasificacion","valor":"clasificacion"}, template="DHGcoreBundle:Datatable:cellNullable.html.twig")
     */
    protected $clasificacion;
    /**
     * @var int
     * @DataTable\Column(source="Producto.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Producto","entityName":"'Producto'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Producto');
        $qb = $userRepository->createQueryBuilder('Producto');
        return $qb;
    }

}