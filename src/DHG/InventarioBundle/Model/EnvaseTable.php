<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class EnvaseTable
 *
 * @DataTable\Table(id="EnvaseTable"  )
 */
class EnvaseTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {    
    /**
     * @var string
     * @DataTable\Column(source="Envase.nombre", name="nombre")
     */
    protected $nombre;
        
    /**
     * @var string
     * @DataTable\Column(source="Envase.codigo_local", name="codigo_local")
     */
    protected $codigo_local;
    
    /**
     * @var int
     * @DataTable\Column(source="Envase.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Envase","entityName":"'Envase'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Envase');
        $qb = $userRepository->createQueryBuilder('Envase');
        return $qb;
    }

}