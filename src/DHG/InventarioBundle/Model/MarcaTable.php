<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class MarcaTable
 *
 * @DataTable\Table(id="MarcaTable"  )
 */
class MarcaTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {    
    /**
     * @var string
     * @DataTable\Column(source="Marca.nombre", name="nombre")
     */
    protected $nombre;
        
    /**
     * @var string
     * @DataTable\Column(source="Marca.codigo_local", name="codigoLocal")
     */
    protected $codigo_local;
        
    /**
     * @var string
     * @DataTable\Column(source="Marca.codigo_empresa", name="codigoEmpresa")
     */
    protected $codigo_empresa;
    
    /**
     * @var int
     * @DataTable\Column(source="Marca.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"Marca","entityName":"'Marca'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\Marca');
        $qb = $userRepository->createQueryBuilder('Marca');
        return $qb;
    }

}