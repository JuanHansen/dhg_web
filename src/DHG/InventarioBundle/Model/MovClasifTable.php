<?php
namespace DHG\InventarioBundle\Model;

use Brown298\DataTablesBundle\MetaData as DataTable;
use Brown298\DataTablesBundle\Model\DataTable\QueryBuilderDataTableInterface;
use Brown298\DataTablesBundle\Model\DataTable\AbstractQueryBuilderDataTable;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class MovClasifTable
 *
 * @DataTable\Table(id="MovClasifTable"  )
 */
class MovClasifTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface {    /**
     * @var string
     * @DataTable\Column(source="MovClasif.nombre", name="nombre")
     */
    protected $nombre;

    /**
     * @var text
     * @DataTable\Column(source="MovClasif.detalle", name="detalle")
     */
    protected $detalle;

        
    /**
     * @var int
     * @DataTable\Column(source="MovClasif.id", name="Accion" )
     * @DataTable\Format(dataFields={"entity":"MovClasif","entityName":"'MovClasif'","bundle":"'Inventario'"},  template="DHGcoreBundle:Datatable:defCellAction.html.twig")
     */
    public $action;


    /**
     * @var bool hydrate results to doctrine objects
     */
    public $hydrateObjects = true;

    /**
     * getQueryBuilder
     *
     * @param Request $request
     *
     * @return null
     */
    public function getQueryBuilder(Request $request = null)
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('DHG\InventarioBundle\Entity\MovClasif');
        $qb = $userRepository->createQueryBuilder('MovClasif');
        return $qb;
    }

}