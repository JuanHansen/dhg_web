<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Events;
use DHG\InventarioBundle\Entity\ProductoFichero;
use DHG\InventarioBundle\Entity\ProductoClasif;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\EntityHistoryBundle\Entity\Versionable;
use Doctrine\ORM\Event\LifecycleEventArgs;
/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Table(name="Productos")
 */
class Producto extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;
    
    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $codigo_local;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    protected $codigo_producto;

    /**
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumn(name="fk_marca_id", referencedColumnName="id", nullable=true)
     */
    protected $marca;
    /**
     * @ORM\ManyToOne(targetEntity="Familia")
     * @ORM\JoinColumn(name="fk_familia_id", referencedColumnName="id", nullable=true)
     */
    protected $familia;
    /**
     * @ORM\ManyToOne(targetEntity="Envase")
     * @ORM\JoinColumn(name="fk_envase_id", referencedColumnName="id", nullable=true)
     */
    protected $envase;
    /**
     * @ORM\ManyToOne(targetEntity="ProductoClasif")
     * @ORM\JoinColumn(name="fk_clasification_id", referencedColumnName="id", nullable=false)
     */
    protected $clasificacion;    

    /**
     * @ORM\Column(type="integer")
     */
    protected $unidadesEnvase;
    /**
     * @ORM\Column(type="integer")
     */
    protected $unidadesPalet;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $controlUnidad;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $controlPeso;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $pesoProm;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $detalle;

    private $em;
     static public function getResourceEntityName(){
        return 'Producto';
    }

    static public function getResourceIcon(){
        return 'cube';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#5E4896';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'clasificacion' => 'Clasificacion',
            );
    }
    public function __construct(){
        parent::__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        if($this->nombre!=null)
            return $this->nombre;
        else
            return " - ";
    }

    public function caltulateTotal(){
        if($this->em!=null){
            $qb1 = $this->em->createQueryBuilder('Movimiento');
            $qb1->select('SUM(m.cantidad)')
                ->from('DHG\InventarioBundle\Entity\Movimiento', 'm')
                ->where('m.producto = ?1')
                ->andwhere('m.almacenOrigen IS NULL')
                ->setParameter(1,$this->getId());

            $qb2 = $this->em->createQueryBuilder('Movimiento');
            $qb2->select('SUM(m.cantidad)')
                ->from('DHG\InventarioBundle\Entity\Movimiento', 'm')
                ->where('m.producto = ?1')
                ->andwhere('m.almacenDestino IS NULL')
                ->setParameter(1,$this->getId());
            $a = $qb1->getQuery()->getResult();
            $b = $qb2->getQuery()->getResult();
            $total =$a[0][1]-$b[0][1];
            return $total;
        }else{
            return 0;
        }
    }

    public function caltulateTotalByAlmacen($almacen){
        if($this->em!=null && $almacen != null){
            $qb1 = $this->em->createQueryBuilder('Movimiento');
            $qb1->select('SUM(m.cantidad)')
                ->from('DHG\InventarioBundle\Entity\Movimiento', 'm')
                ->where('m.producto = ?1')
                ->andwhere('m.almacenOrigen =?2')
                ->setParameter(1,$this->getId())
                ->setParameter(2,$almacen->getId());

            $qb2 = $this->em->createQueryBuilder('Movimiento');
            $qb2->select('SUM(m.cantidad)')
                ->from('DHG\InventarioBundle\Entity\Movimiento', 'm')
                ->where('m.producto = ?1')
                ->andwhere('m.almacenDestino =?2')
                ->setParameter(1,$this->getId())
                ->setParameter(2,$almacen->getId());
            $a = $qb1->getQuery()->getResult();
            $b = $qb2->getQuery()->getResult();
            $total = $b[0][1]-$a[0][1];
            return $total;
        }else{
            return 0;
        }
    }

    /**
     * @ORM\PostLoad()
     */
    public function postLoad(LifecycleEventArgs $args){
        
        $this->em = $args->getEntityManager();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigo_local
     *
     * @param string $codigoLocal
     * @return Producto
     */
    public function setCodigoLocal($codigoLocal)
    {
        $this->codigo_local = $codigoLocal;
    
        return $this;
    }

    /**
     * Get codigo_local
     *
     * @return string 
     */
    public function getCodigoLocal()
    {
        return $this->codigo_local;
    }

    /**
     * Set codigo_producto
     *
     * @param string $codigoProducto
     * @return Producto
     */
    public function setCodigoProducto($codigoProducto)
    {
        $this->codigo_producto = $codigoProducto;
    
        return $this;
    }

    /**
     * Get codigo_producto
     *
     * @return string 
     */
    public function getCodigoProducto()
    {
        return $this->codigo_producto;
    }

    /**
     * Set unidadesEnvase
     *
     * @param integer $unidadesEnvase
     * @return Producto
     */
    public function setUnidadesEnvase($unidadesEnvase)
    {
        $this->unidadesEnvase = $unidadesEnvase;
    
        return $this;
    }

    /**
     * Get unidadesEnvase
     *
     * @return integer 
     */
    public function getUnidadesEnvase()
    {
        return $this->unidadesEnvase;
    }

    /**
     * Set unidadesPalet
     *
     * @param integer $unidadesPalet
     * @return Producto
     */
    public function setUnidadesPalet($unidadesPalet)
    {
        $this->unidadesPalet = $unidadesPalet;
    
        return $this;
    }

    /**
     * Get unidadesPalet
     *
     * @return integer 
     */
    public function getUnidadesPalet()
    {
        return $this->unidadesPalet;
    }

    /**
     * Set controlUnidad
     *
     * @param boolean $controlUnidad
     * @return Producto
     */
    public function setControlUnidad($controlUnidad)
    {
        $this->controlUnidad = $controlUnidad;
    
        return $this;
    }

    /**
     * Get controlUnidad
     *
     * @return boolean 
     */
    public function getControlUnidad()
    {
        return $this->controlUnidad;
    }

    /**
     * Set controlPeso
     *
     * @param boolean $controlPeso
     * @return Producto
     */
    public function setControlPeso($controlPeso)
    {
        $this->controlPeso = $controlPeso;
    
        return $this;
    }

    /**
     * Get controlPeso
     *
     * @return boolean 
     */
    public function getControlPeso()
    {
        return $this->controlPeso;
    }

    /**
     * Set pesoProm
     *
     * @param string $pesoProm
     * @return Producto
     */
    public function setPesoProm($pesoProm)
    {
        $this->pesoProm = $pesoProm;
    
        return $this;
    }

    /**
     * Get pesoProm
     *
     * @return string 
     */
    public function getPesoProm()
    {
        return $this->pesoProm;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Producto
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    
        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }
    /**
     * Set marca
     *
     * @param \DHG\InventarioBundle\Entity\Marca $marca
     * @return Producto
     */
    public function setMarca( $marca)
    {
        $this->marca = $marca;
    
        return $this;
    }

    /**
     * Get marca
     *
     * @return \DHG\InventarioBundle\Entity\Marca 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set familia
     *
     * @param \DHG\InventarioBundle\Entity\Familia $familia
     * @return Producto
     */
    public function setFamilia(\DHG\InventarioBundle\Entity\Familia $familia)
    {
        $this->familia = $familia;
    
        return $this;
    }

    /**
     * Get familia
     *
     * @return \DHG\InventarioBundle\Entity\Familia 
     */
    public function getFamilia()
    {
        return $this->familia;
    }

    /**
     * Set envase
     *
     * @param \DHG\InventarioBundle\Entity\Envase $envase
     * @return Producto
     */
    public function setEnvase(\DHG\InventarioBundle\Entity\Envase $envase)
    {
        $this->envase = $envase;
    
        return $this;
    }

    /**
     * Get envase
     *
     * @return \DHG\InventarioBundle\Entity\Envase 
     */
    public function getEnvase()
    {
        return $this->envase;
    }

    /**
     * Set clasificacion
     *
     * @param \DHG\InventarioBundle\Entity\ProductoClasif $clasificacion
     * @return Producto
     */
    public function setClasificacion(\DHG\InventarioBundle\Entity\ProductoClasif $clasificacion)
    {
        $this->clasificacion = $clasificacion;
    
        return $this;
    }

    /**
     * Get clasificacion
     *
     * @return \DHG\InventarioBundle\Entity\ProductoClasif 
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

}
