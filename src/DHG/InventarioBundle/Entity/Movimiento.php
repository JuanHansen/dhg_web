<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\UUID;
use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use DHG\InventarioBundle\Entity\Producto;
use DHG\InventarioBundle\Entity\ProductoClasif;
use DHG\InventarioBundle\Entity\MovClasif;
use Symfony\Component\Validator\Constraints as Assert;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="Movimientos")
 */
class Movimiento extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\ManyToOne(targetEntity="Producto")
     * @ORM\JoinColumn(name="fk_producto_id", referencedColumnName="id", nullable=false)
     */
    protected $producto;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $cantidad;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $peso;

    /**
     * @ORM\ManyToOne(targetEntity="MovClasif")
     * @ORM\JoinColumn(name="fk_clasificacion_id", referencedColumnName="id", nullable=false)
     */
    protected $clasificacion;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\Almacen")
     * @ORM\JoinColumn(name="fk_almacen_o_id", referencedColumnName="id", nullable=true)
     */
    protected $almacenOrigen;
    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\Almacen")
     * @ORM\JoinColumn(name="fk_almacen_d_id", referencedColumnName="id", nullable=true)
     */
    protected $almacenDestino;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $detalle;


    /**
     * Determina si solo el sistema puede modificarlo.
     * @ORM\Column(type="boolean")
     */
    protected $sistema;
     static public function getResourceEntityName(){
        return 'Movimiento';
    }

    static public function getResourceIcon(){
        return 'exchange';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#5E4896';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'producto' => 'Producto',
                'fecha' => 'Fecha',
                'cantidad' => 'Cantidad',
                'peso' => 'Peso',
                'precio_unidad' => 'Precio por Unidad',
                'clasificacion' => 'Clasificacion',
                'almacenOrigen' => 'Almacen Origen',
                'almacenDestino' => "Almacen Destino",
                'clienteproveedor' => 'Cliente/Proveedor',
                'comprobante' => 'Comprobante',
                'detalle' => 'Detalle',
            );
    }

    public function __construct(){
        parent::__construct();
        $this->sistema = false;
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        if($this->producto!=null)
            return $this->clasificacion." de ".$this->getCantidad()." de ".$this->producto->__toString();
        else
            return "Movimiento";
    }

    public function regenerateId() {
        return $this->id = UUID::v4();
    }

    public function getDetalle(){
       return $this->detalle;
    }

    public function setDetalle($detalle){
       $this->detalle = $detalle;
    }

    public function getSistema(){
       return $this->sistema;
    }

    public function setSistema($sistema){
       $this->sistema = $sistema;
    }
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Movimiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     * @return Movimiento
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set peso
     *
     * @param string $peso
     * @return Movimiento
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string 
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set producto
     *
     * @param \DHG\InventarioBundle\Entity\Producto $producto
     * @return Movimiento
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \DHG\InventarioBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set clasificacion
     *
     * @param \DHG\InventarioBundle\Entity\MovClasif $clasificacion
     * @return Movimiento
     */
    public function setClasificacion($clasificacion)
    {
        $this->clasificacion = $clasificacion;

        return $this;
    }

    /**
     * Get clasificacion
     *
     * @return \DHG\InventarioBundle\Entity\MovClasif 
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

    /**
     * Set Almacen
     *
     * @param \DHG\InventarioBundle\Entity\Almacen $Almacen
     * @return Movimiento
     */
    public function setAlmacenOrigen($Almacen)
    {
        $this->almacenOrigen = $Almacen;

        return $this;
    }

    /**
     * Get Almacen
     *
     * @return \DHG\InventarioBundle\Entity\Almacen 
     */
    public function getAlmacenOrigen()
    {
        return $this->almacenOrigen;
    }
    /**
     * Set Almacen
     *
     * @param \DHG\InventarioBundle\Entity\Almacen $Almacen
     * @return Movimiento
     */
    public function setAlmacenDestino($Almacen)
    {
        $this->almacenDestino = $Almacen;

        return $this;
    }

    /**
     * Get Almacen
     *
     * @return \DHG\InventarioBundle\Entity\Almacen 
     */
    public function getAlmacenDestino()
    {
        return $this->almacenDestino;
    }
    public function getMontoTotal(){
        return $this->getCantidad()*$this->getPrecioUnidad();
    }
}
