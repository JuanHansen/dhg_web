<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="Almacenes")
 */
class Almacen extends MappedSuperclassBase  implements Versionable
{

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\Almacen")
     * @ORM\JoinColumn(name="fk_almacen_id", referencedColumnName="id",nullable=true)
     */
    protected $padre;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $codigoLocal;


    
     static public function getResourceEntityName(){
        return 'Almacen';
    }

    static public function getResourceIcon(){
        return 'thumb-tack';
    }
 
    static public function getSeccionIcon(){
        return 'thumb-tack';
    }
    static public function getResourceColor(){
        return '#ADA479';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'description' => 'Descripcion',
        );
    }

    public function __construct(){
        parent::__construct();
    }

    /**
     * Override toString() method to return the nombre of the unit
     * @return string nombre
     */
    public function __toString()
    {
        if($this->nombre)
            return $this->nombre;
        else
            return " - ";
    }

    public function getNombre(){
	   return $this->nombre;
    }

    public function setNombre($nombre){
       $this->nombre = $nombre;
    }
    public function getDescription(){
	   return $this->description;
    }

    public function setDescription($description){
       $this->description = $description;
    }

    public function getCodigolocal(){
       return $this->codigoLocal;
    }

    public function setCodigoLocal($codigoLocal){
       $this->codigoLocal = $codigoLocal;
   }

   public function getPadre(){
        return $this->padre;
   }
   public function setPadre($padre){
        $this->padre = $padre;
   }
}