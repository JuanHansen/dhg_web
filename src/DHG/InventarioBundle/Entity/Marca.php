<?php

namespace DHG\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DHG\coreBundle\Entity\MappedSuperclassBase;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * Marca
 *
 * @ORM\Table(name="Marca")
 * @ORM\Entity
 */
class Marca extends MappedSuperclassBase implements Versionable
{

    /**
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_local", type="string", length=25)
     */
    private $codigoLocal;

    /**
     *
     * @ORM\Column(name="codigo_empresa", type="string", length=255)
     */
    private $codigoEmpresa;


     static public function getResourceEntityName(){
        return 'Marca';
    }

    static public function getResourceIcon(){
        return 'phone';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#669B6B';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'codigoLocal' => 'Codigo Local',
                'codigoEmpresa' => 'Codigo Empresa',
            );
    }

    public function toString(){
        return $this->getNombre();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Marca
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigoLocal
     *
     * @param string $codigoLocal
     * @return Marca
     */
    public function setCodigoLocal($codigoLocal)
    {
        $this->codigoLocal = $codigoLocal;
    
        return $this;
    }

    /**
     * Get codigoLocal
     *
     * @return string 
     */
    public function getCodigoLocal()
    {
        return $this->codigoLocal;
    }

    /**
     * Set codigoEmpresa
     *
     * @param string $codigoEmpresa
     * @return Marca
     */
    public function setCodigoEmpresa($codigoEmpresa)
    {
        $this->codigoEmpresa = $codigoEmpresa;
    
        return $this;
    }

    /**
     * Get codigoEmpresa
     *
     * @return string 
     */
    public function getCodigoEmpresa()
    {
        return $this->codigoEmpresa;
    }
}
