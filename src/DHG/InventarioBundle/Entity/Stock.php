<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Events;
use DHG\InventarioBundle\Entity\Producto;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Entity
 * @ORM\Table(name="Stock")
 */
class Stock extends MappedSuperclassBase
{
    /**
     * @ORM\ManyToOne(targetEntity="Producto")
     * @ORM\JoinColumn(name="fk_producto_id", referencedColumnName="id", nullable=false)
     */
    protected $producto;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $cantidad;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $peso;
    /**
     * @ORM\ManyToOne(targetEntity="DHG\InventarioBundle\Entity\Almacen")
     * @ORM\JoinColumn(name="fk_Almacen_id", referencedColumnName="id", nullable=false)
     */
    protected $almacen;
    

    public function __construct(){
        parent::__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->producto->getNombre();
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     * @return Movimiento
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set producto
     *
     * @param \DHG\InventarioBundle\Entity\Producto $producto
     * @return Movimiento
     */
    public function setProducto(\DHG\InventarioBundle\Entity\Producto $producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \DHG\InventarioBundle\Entity\Producto 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set Almacen
     *
     * @param \DHG\InventarioBundle\Entity\Almacen $Almacen
     * @return Movimiento
     */
    public function setAlmacen(\DHG\InventarioBundle\Entity\Almacen $Almacen)
    {
        $this->almacen = $Almacen;

        return $this;
    }

    /**
     * Get Almacen
     *
     * @return \DHG\InventarioBundle\Entity\Almacen 
     */
    public function getAlmacen()
    {
        return $this->almacen;
    }
    /**
     * Set peso
     *
     * @param string $peso
     * @return Movimiento
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string 
     */
    public function getPeso()
    {
        return $this->peso;
    }
}
