<?php

namespace DHG\InventarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DHG\coreBundle\Entity\MappedSuperclassBase;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * Familia
 *
 * @ORM\Table(name="Familia")
 * @ORM\Entity
 */
class Familia extends MappedSuperclassBase implements Versionable
{

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_local", type="string", length=25)
     */
    private $codigoLocal;



     static public function getResourceEntityName(){
        return 'Familia';
    }

    static public function getResourceIcon(){
        return 'phone';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#669B6B';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'codigoLocal' => 'Codigo Local',
            );
    }

    public function toString(){
        return $this->getNombre();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Familia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigoLocal
     *
     * @param string $codigoLocal
     * @return Familia
     */
    public function setCodigoLocal($codigoLocal)
    {
        $this->codigoLocal = $codigoLocal;
    
        return $this;
    }

    /**
     * Get codigoLocal
     *
     * @return string 
     */
    public function getCodigoLocal()
    {
        return $this->codigoLocal;
    }
}
