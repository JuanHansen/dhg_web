<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use DHG\ContactosBundle\Entity\Contacto;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="CliPro")
 */
class CliPro extends MappedSuperclassBase implements Versionable
{
     /**
     * @ORM\ManyToOne(targetEntity="DHG\ContactosBundle\Entity\Contacto",cascade={"persist"})
     * @ORM\JoinColumn(name="fk_Contacto_id", referencedColumnName="id")
     */
    protected $contacto;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $cliente;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $proveedor;

     static public function getResourceEntityName(){
        return 'Cliente/Proovedor';
    }

    static public function getResourceIcon(){
        return 'phone';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#669B6B';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'contacto' => 'Contacto',
                'cliente' => 'Es Cliente',
                'proveedor' => 'Es Proveedor',
            );
    }

    

    public function __construct(){
        parent::__construct();
        $this->contacto = new Contacto();
        $this->contacto->__construct();
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString(){
      if($this->contacto!=null){
        return $this->contacto->getNombre();}
      else
        return " - ";
    }


    public function getNombre(){
      if($this->contacto!=null){
       return $this->contacto->getNombre();}
    }

    public function setNombre($nombre){
      if($this->contacto!=null){
       $this->contacto->setNombre($nombre);}
    }


    public function getCliente(){
       return $this->cliente;
    }

    public function setCliente($cliente){
       $this->cliente = $cliente;
    }

    public function getProveedor(){
       return $this->proveedor;
    }

    public function setProveedor($proveedor){
       $this->proveedor = $proveedor;
    }
    public function setContacto($contacto){
      if($this->contacto!=null){
      $this->contacto = $contacto;}
    }
    public function getContacto(){
      if($this->contacto!=null){
      return $this->contacto;}
    }





}
