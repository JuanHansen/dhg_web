<?php
namespace DHG\InventarioBundle\Entity;

use DHG\coreBundle\Entity\MappedSuperclassBase;
use Doctrine\ORM\Mapping as ORM;
use DHG\EntityHistoryBundle\Entity\Versionable;

/**
 * @ORM\Entity
 * @ORM\Table(name="ProductoClasificaciones")
 */
class ProductoClasif extends MappedSuperclassBase implements Versionable
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $detalle;
    
    /**
     * Determina si solo el sistema puede modificarlo.
     * @ORM\Column(type="boolean")
     */
    protected $sistema;
     static public function getResourceEntityName(){
        return 'Clasificacion de Productos';
    }

    static public function getResourceIcon(){
        return 'cubes';
    } 
    static public function getSeccionIcon(){
        return 'eye';
    }

    static public function getResourceColor(){
        return '#5E4896';
    }

    static public function getAttributeHumanReadableMap(){
        return array(
                'nombre' => 'Nombre',
                'detalle' => 'Detalle',
            );
    }

    public function __construct(){
        parent::__construct();
        $this->sistema = false;
    }

    /**
     * Override toString() method to return the name of the unit
     * @return string name
     */
    public function __toString()
    {
        return $this->nombre;
    }

    public function getNombre(){
	   return $this->nombre;
    }
    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ProductoClasif
     */
    public function setNombre($nombre){
       $this->nombre = $nombre;
    }

    public function getDetalle(){
	   return $this->detalle;
    }

    public function setDetalle($detalle){
       $this->detalle = $detalle;
    }

    public function getSistema(){
       return $this->sistema;
    }

    public function setSistema($sistema){
       $this->sistema = $sistema;
    }
    
}
